﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.IO;
using System.ComponentModel;
using CommsPacket;

namespace DataFortress_Client
{
    public class Communications
    {
        private TcpClient _tcpClient = new TcpClient();
        NetworkStream _networkStream = null;
        BackgroundWorker _filesendworker = new BackgroundWorker();

        public delegate void SendProgressUpdate(SendWorkerProgress progressinfo);
        public event SendProgressUpdate OnSendProgressUpdate;

        public delegate void SendCompleted();
        public event SendCompleted OnSendCompleted;

        // Receive large data variables
        const long _maxbuffersize = 2048000;
        MemoryStream memStream;
        BinaryWriter binBuffer;
        private byte[] bytes; 		// Data buffer for incoming data.
        CommsPacket.CommsPacket _largereceivedobject = null;

        //private const int _portNum = 10116;

        public Communications()
        {
            _filesendworker.WorkerSupportsCancellation = true;
            _filesendworker.WorkerReportsProgress = true;

            _filesendworker.ProgressChanged += new ProgressChangedEventHandler(_filesendworker_ProgressChanged);
            _filesendworker.DoWork += new DoWorkEventHandler(_filesendworker_DoWork);
            _filesendworker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(_filesendworker_RunWorkerCompleted);

        }

        void _filesendworker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (OnSendCompleted != null)
            {
                OnSendCompleted();
            }
        }

        void _filesendworker_DoWork(object sender, DoWorkEventArgs e)
        {
            SendWorkerParams workerparams = (SendWorkerParams)e.Argument;

            SendFile(workerparams.sourcepath, workerparams.destpath, _filesendworker, workerparams.useraccount, workerparams.authtoken, workerparams.jobname);
        }

        void _filesendworker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            SendWorkerProgress workerprogress = (SendWorkerProgress)e.UserState;

            if (OnSendProgressUpdate != null)
            {
                OnSendProgressUpdate(workerprogress);
            }
            //throw new NotImplementedException();
        }

        public void SendFileAsync(string filepath, string destfilepath, string useraccount, string authtoken, string jobname)
        {
            SendWorkerParams sendparams = new SendWorkerParams();
            sendparams.sourcepath = filepath;
            sendparams.destpath = destfilepath;
            sendparams.useraccount = useraccount;
            sendparams.authtoken = authtoken;
            sendparams.jobname = jobname;

            if (_filesendworker.IsBusy == false)
            {
                _filesendworker.RunWorkerAsync(sendparams);
            }
            else
            {
                throw new Exception("There is already a file send operation in progress. Please wait for that to finish before starting another file send.");
            }
        }

        public bool Connect(string serveraddress, int portnum, out string reason)
        {
            try
            {
                _tcpClient = new TcpClient();
                _tcpClient.Connect(serveraddress, portnum);

                _networkStream = _tcpClient.GetStream();

                if (_networkStream.CanWrite && _networkStream.CanRead)
                {
                    reason = "Connection Successful";
                    return true;
                }
                else
                {
                    reason = "Cannot write or read from network stream.";
                    _tcpClient.Close();
                    return false;
                }

            }
            catch (SocketException sockex)
            {
                reason = sockex.Message;
                return false;
            }
            catch (System.IO.IOException ioex)
            {
                reason = ioex.Message;
                return false;
            }
            catch (Exception e)
            {
                reason = e.Message;
                return false;
            }
        }

        public void Disconnect()
        {
            try
            {
                _networkStream.Close();
                _tcpClient.Close();
                //_tcpClient.Close();
            }
            catch (Exception ex)
            {

            }
        }

        private void SendFile(string filepath, string destfilepath, BackgroundWorker bw, string useraccount, string authtoken, string jobname)
        {
            byte[] chunkbuffer;

            FileStream fileStream = new FileStream(filepath, FileMode.Open, FileAccess.Read);

            //FileStream writeStream = new FileStream(filepath + "write", FileMode.Append, FileAccess.Write);

            int chunksize = 262144;

            long numchunks = fileStream.Length / chunksize;

            long calcsize = numchunks * chunksize;

            if (fileStream.Length > calcsize)
            {
                numchunks++;
            }

            long filepointer = 0;
            long workingchunksize = 0;

            if (PrepareServerForFileTransfer_Linux(useraccount, authtoken, Path.GetFileName (filepath), fileStream.Length, destfilepath, jobname) == false)
            {
                throw new Exception("Unable to prepare server for file transfer");
            }

            //throw new Exception("Ready to send file chunks.");

            for (int a = 0; a < numchunks; a++)
            {

                if ((fileStream.Length - filepointer) >= chunksize)
                {
                    workingchunksize = chunksize;
                    chunkbuffer = new byte[workingchunksize];

                    fileStream.Read(chunkbuffer, 0, (int)workingchunksize);
                }
                else
                {
                    long lastchunksize = fileStream.Length - filepointer;
                    workingchunksize = lastchunksize;
                    chunkbuffer = new byte[lastchunksize];

                    fileStream.Read(chunkbuffer, 0, (int)workingchunksize);
                }


                //writeStream.Write(chunkbuffer, 0, chunkbuffer.Length);
                SendFileChunk_Linux(chunkbuffer, filepath, fileStream.Length, useraccount, authtoken);

                filepointer += workingchunksize;

                //System.Threading.Thread.Sleep(1000);
                SendWorkerProgress workerprogress = new SendWorkerProgress();
                workerprogress.sourcepath = filepath;
                workerprogress.bytessent = filepointer;
                workerprogress.totalbytes = fileStream.Length;
                workerprogress.bytesremaining = (fileStream.Length - filepointer);
                int percentprogress = (a * 100) / (int)numchunks;
                workerprogress.percentcompleted = percentprogress;

                bw.ReportProgress(percentprogress, workerprogress);

                System.Diagnostics.Debug.WriteLine("File Chunk has been sent. Filepointer: " + filepointer.ToString() + " File Length: " + fileStream.Length.ToString ());

            }

            fileStream.Close();

            //writeStream.Close();
        }



        public DateTime GetServerFileLastWriteTime(string accountname, string usertoken, string strpath, string jobname)
        {
            // NOTE: strpath needs to looklike $$$ACCOUNTROOT$$$\SomeFolder\SubFolder\File.txt

            CommsPacket.CommsPacket myObject = new CommsPacket.CommsPacket();
            myObject.iMessagetype = (int)CommsPacket.MessageTypes.LastWriteTime;
            myObject.filename = strpath;
            myObject.useraccount = accountname;
            myObject.usertoken = usertoken;
            myObject.jobname = jobname;

            if (_networkStream != null)
            {
                Byte[] sendBytes = myObject.Serialize();
                Byte[] encbytes = myObject.Encapsulate(sendBytes);

                _networkStream.Write(encbytes, 0, encbytes.Length);

                //Read the reply
                byte[] bytes = new byte[_tcpClient.ReceiveBufferSize];
                int BytesRead = _networkStream.Read(bytes, 0, (int)_tcpClient.ReceiveBufferSize);

                CommsPacket.CommsResponse response = new CommsPacket.CommsResponse();
                response = response.DeSerialize(bytes);

                if (response.Message.Contains("I received the packet") == false)
                {
                    throw new Exception("Did not receive packet acknowledgement");
                }

                return response.LastWriteTime;

            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
        }

        public long GetServerFileLength(string accountname, string usertoken, string strpath, string jobname)
        {
            // NOTE: strpath needs to looklike $$$ACCOUNTROOT$$$\SomeFolder\SubFolder\File.txt

            CommsPacket.CommsPacket myObject = new CommsPacket.CommsPacket();
            myObject.iMessagetype = (int)CommsPacket.MessageTypes.FileLength;
            myObject.filename = strpath;
            myObject.useraccount = accountname;
            myObject.usertoken = usertoken;
            myObject.jobname = jobname;

            if (_networkStream != null)
            {
                Byte[] sendBytes = myObject.Serialize();
                Byte[] encbytes = myObject.Encapsulate(sendBytes);

                _networkStream.Write(encbytes, 0, encbytes.Length);

                //Read the reply
                byte[] bytes = new byte[_tcpClient.ReceiveBufferSize];
                int BytesRead = _networkStream.Read(bytes, 0, (int)_tcpClient.ReceiveBufferSize);

                CommsPacket.CommsResponse response = new CommsPacket.CommsResponse();
                response = response.DeSerialize(bytes);

                if (response.Message.Contains("I received the packet") == false)
                {
                    throw new Exception("Did not receive packet acknowledgement");
                }

                return response.FileLength;
            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
        }

        public long GetServerFileLength_Linux(string accountname, string usertoken, string strpath, string jobname)
        {
            int signatureone = (int)CommsPacket.MessageTypes.LinuxSignatureOne;
            int signaturetwo = (int)CommsPacket.MessageTypes.LinuxSignatureTwo;
            int messageidentifier = (int)CommsPacket.MessageTypes.LinuxFileLength;
            //int bulkmessagelength = sizeof(int) + sizeof(int) + accountname.Length + sizeof(int) + passwordhash.Length;

            int bulkmessagelength = sizeof(int); // Message ident
            bulkmessagelength += sizeof(int); // User account len
            bulkmessagelength += accountname.Length;
            bulkmessagelength += sizeof(int); // File path len
            bulkmessagelength += strpath.Length;
            bulkmessagelength += sizeof(int); // Job name len
            bulkmessagelength += jobname.Length;

            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms);
            UTF8Encoding enc = new UTF8Encoding();

            bw.Write(signatureone);
            bw.Write(signaturetwo);
            bw.Write(bulkmessagelength);
            bw.Write(messageidentifier);
            bw.Write(accountname.Length);
            bw.Write(enc.GetBytes(accountname));
            bw.Write(strpath.Length);
            bw.Write(enc.GetBytes(strpath));
            bw.Write(jobname.Length);
            bw.Write(enc.GetBytes(jobname));

            SendBytes(ms.ToArray());

            //Read the reply
            byte[] bytes = new byte[_tcpClient.ReceiveBufferSize];
            int BytesRead = _networkStream.Read(bytes, 0, (int)_tcpClient.ReceiveBufferSize);

            int ipointer = 0;

            int sig1 = BitConverter.ToInt32(bytes, ipointer);
            ipointer += sizeof(int);

            int sig2 = BitConverter.ToInt32(bytes, ipointer);
            ipointer += sizeof(int);

            int msgident = BitConverter.ToInt32(bytes, ipointer);
            ipointer += sizeof(int);

            if (SignatureValid(sig1, sig2))
            {
                if (msgident == (int)CommsPacket.MessageTypes.LinuxResponseFileLength)
                {
                    long filelength = BitConverter.ToInt32(bytes, ipointer);
                    ipointer += sizeof(long);

                    return filelength;
                }

                if (msgident == (int)CommsPacket.MessageTypes.LinuxResponseFileNotFound)
                {
                    throw new Exception("Get File Length on server failed because the server reported the file does not exist.");
                }
            }

            // If we got here then the message identifier returned from the server did not match or the signatures did not match
            throw new Exception("Get File Length on server failed because the communication signature in the reply was not valid.");
        }

        public bool DoesServerFileExist(string accountname, string usertoken, string strpath, string jobname)
        {
            // NOTE: strpath needs to looklike $$$ACCOUNTROOT$$$\SomeFolder\SubFolder\File.txt

            CommsPacket.CommsPacket myObject = new CommsPacket.CommsPacket();
            myObject.iMessagetype = (int)CommsPacket.MessageTypes.FileExists;
            myObject.filename = strpath;
            myObject.useraccount = accountname;
            myObject.usertoken = usertoken;
            myObject.jobname = jobname;

            if (_networkStream != null)
            {
                Byte[] sendBytes = myObject.Serialize();
                Byte[] encbytes = myObject.Encapsulate(sendBytes);

                _networkStream.Write(encbytes, 0, encbytes.Length);

                //Read the reply
                byte[] bytes = new byte[_tcpClient.ReceiveBufferSize];
                int BytesRead = _networkStream.Read(bytes, 0, (int)_tcpClient.ReceiveBufferSize);

                CommsPacket.CommsResponse response = new CommsPacket.CommsResponse();
                response = response.DeSerialize(bytes);

                if (response.Message.Contains("I received the packet") == false)
                {
                    throw new Exception("Did not receive packet acknowledgement");
                }

                if (response.FileExists == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
        }


        public bool DoesServerFileExist_Linux (string accountname, string usertoken, string strpath, string jobname)
        {
            int signatureone = (int)CommsPacket.MessageTypes.LinuxSignatureOne;
            int signaturetwo = (int)CommsPacket.MessageTypes.LinuxSignatureTwo;
            int messageidentifier = (int)CommsPacket.MessageTypes.LinuxFileExists;
            //int bulkmessagelength = sizeof(int) + sizeof(int) + accountname.Length + sizeof(int) + passwordhash.Length;

            int bulkmessagelength = sizeof(int); // Message ident
            bulkmessagelength += sizeof(int); // User account len
            bulkmessagelength += accountname.Length;
            bulkmessagelength += sizeof(int); // File path len
            bulkmessagelength += strpath.Length;
            bulkmessagelength += sizeof(int); // Job name len
            bulkmessagelength += jobname.Length;

            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms);
            UTF8Encoding enc = new UTF8Encoding();

            bw.Write(signatureone);
            bw.Write(signaturetwo);
            bw.Write(bulkmessagelength);
            bw.Write(messageidentifier);
            bw.Write(accountname.Length);
            bw.Write(enc.GetBytes(accountname));
            bw.Write(strpath.Length);
            bw.Write(enc.GetBytes(strpath));
            bw.Write(jobname.Length);
            bw.Write(enc.GetBytes(jobname));

            SendBytes(ms.ToArray());

            //Read the reply
            byte[] bytes = new byte[_tcpClient.ReceiveBufferSize];
            int BytesRead = _networkStream.Read(bytes, 0, (int)_tcpClient.ReceiveBufferSize);

            int ipointer = 0;

            int sig1 = BitConverter.ToInt32(bytes, ipointer);
            ipointer += sizeof(int);

            int sig2 = BitConverter.ToInt32(bytes, ipointer);
            ipointer += sizeof(int);

            int msgident = BitConverter.ToInt32(bytes, ipointer);
            ipointer += sizeof(int);

            if (SignatureValid(sig1, sig2))
            {
                if (msgident == (int)CommsPacket.MessageTypes.LinuxResponseFileExists)
                {                    
                    return true;
                }

                if (msgident == (int)CommsPacket.MessageTypes.LinuxResponseFileNotFound)
                {                    
                    return false;
                }
            }
            
            // If we got here then the message identifier returned from the server did not match or the signatures did not match
            return false;
        }


        public DateTime GetServerFileLastWriteTime_Linux (string accountname, string usertoken, string strpath, string jobname)
        {
            int signatureone = (int)CommsPacket.MessageTypes.LinuxSignatureOne;
            int signaturetwo = (int)CommsPacket.MessageTypes.LinuxSignatureTwo;
            int messageidentifier = (int)CommsPacket.MessageTypes.LinuxLastWriteTime;
            //int bulkmessagelength = sizeof(int) + sizeof(int) + accountname.Length + sizeof(int) + passwordhash.Length;

            int bulkmessagelength = sizeof(int); // Message ident
            bulkmessagelength += sizeof(int); // User account len
            bulkmessagelength += accountname.Length;
            bulkmessagelength += sizeof(int); // File path len
            bulkmessagelength += strpath.Length;
            bulkmessagelength += sizeof(int); // Job name len
            bulkmessagelength += jobname.Length;

            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms);
            UTF8Encoding enc = new UTF8Encoding();

            bw.Write(signatureone);
            bw.Write(signaturetwo);
            bw.Write(bulkmessagelength);
            bw.Write(messageidentifier);
            bw.Write(accountname.Length);
            bw.Write(enc.GetBytes(accountname));
            bw.Write(strpath.Length);
            bw.Write(enc.GetBytes(strpath));
            bw.Write(jobname.Length);
            bw.Write(enc.GetBytes(jobname));

            SendBytes(ms.ToArray());

            //Read the reply
            byte[] bytes = new byte[_tcpClient.ReceiveBufferSize];
            int BytesRead = _networkStream.Read(bytes, 0, (int)_tcpClient.ReceiveBufferSize);

            int ipointer = 0;

            int sig1 = BitConverter.ToInt32(bytes, ipointer);
            ipointer += sizeof(int);

            int sig2 = BitConverter.ToInt32(bytes, ipointer);
            ipointer += sizeof(int);

            int msgident = BitConverter.ToInt32(bytes, ipointer);
            ipointer += sizeof(int);

            if (SignatureValid(sig1, sig2))
            {
                if (msgident == (int)CommsPacket.MessageTypes.LinuxResponseLastWriteTime)
                {
                    int seconds = BitConverter.ToInt32(bytes, ipointer);
                    ipointer += sizeof(int);

                    int minutes = BitConverter.ToInt32(bytes, ipointer);
                    ipointer += sizeof(int);

                    int hour = BitConverter.ToInt32(bytes, ipointer);
                    ipointer += sizeof(int);

                    int dayofmonth = BitConverter.ToInt32(bytes, ipointer);
                    ipointer += sizeof(int);

                    int month = BitConverter.ToInt32(bytes, ipointer);
                    ipointer += sizeof(int);

                    int year = BitConverter.ToInt32(bytes, ipointer);
                    ipointer += sizeof(int);

                    int dayofweek = BitConverter.ToInt32(bytes, ipointer);
                    ipointer += sizeof(int);

                    int dayinyear = BitConverter.ToInt32(bytes, ipointer);
                    ipointer += sizeof(int);

                    int isdaylightsaving = BitConverter.ToInt32(bytes, ipointer);
                    ipointer += sizeof(int);

                    DateTime filelastwritetime = new DateTime(year, month, dayofmonth, hour, minutes, seconds);
                    return filelastwritetime;
                }

                if (msgident == (int)CommsPacket.MessageTypes.LinuxResponseFileNotFound)
                {
                    throw new Exception("Request was made for Last Write Time on a File that the server reported does not exist.");
                }
            }

            // If we got here then the message identifier returned from the server did not match or the signatures did not match
            throw new Exception("Last Write Time could not be obtained - the communication signature of the reply was not valid.");
        }


        public bool PrepareServerForFileTransfer(string useraccountname, string usertoken, string localfilename, long filelength, string destfilepath, string jobname)
        {
            CommsPacket.CommsPacket myObject = new CommsPacket.CommsPacket();
            myObject.iMessagetype = (int)CommsPacket.MessageTypes.PrepareServerForFileTransfer;
            myObject.filename = Path.GetFileName(localfilename);
            myObject.filelength = filelength;
            myObject.sourcepath = localfilename;
            myObject.destpath = destfilepath;
            myObject.useraccount = useraccountname;
            myObject.usertoken = usertoken;
            myObject.jobname = jobname;
            

            myObject.Somemessage = "PrepareFileTransfer";
            myObject.somethingelse = "PrepareFileTransfer";
            myObject.somebuffer = new byte[10];

            if (_networkStream != null)
            {
                Byte[] sendBytes = myObject.Serialize();
                Byte[] encbytes = myObject.Encapsulate(sendBytes);

                _networkStream.Write(encbytes, 0, encbytes.Length);

                // Reads the NetworkStream into a byte buffer.
                byte[] bytes = new byte[_tcpClient.ReceiveBufferSize];
                int BytesRead = _networkStream.Read(bytes, 0, (int)_tcpClient.ReceiveBufferSize);

                CommsPacket.CommsResponse response = new CommsPacket.CommsResponse();
                response = response.DeSerialize(bytes);


                if (response.Message.Contains("I received the packet") == false)
                {
                    throw new Exception("Did not receive packet acknowledgement");
                }

                return true;
            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
        }


        public bool PrepareServerForFileTransfer_Linux(string useraccountname, string usertoken, string localfilename, long filelength, string destfilepath, string jobname)
        {
            int signatureone = (int)CommsPacket.MessageTypes.LinuxSignatureOne;
            int signaturetwo = (int)CommsPacket.MessageTypes.LinuxSignatureTwo;
            int messageidentifier = (int)CommsPacket.MessageTypes.LinuxPrepareFileTransfer;
            //int bulkmessagelength = sizeof(int) + sizeof(int) + accountname.Length + sizeof(int) + passwordhash.Length;

            int bulkmessagelength = sizeof(int); // Message ident
            bulkmessagelength += sizeof(int); // User account len
            bulkmessagelength += useraccountname.Length;

            bulkmessagelength += sizeof(int); // User token len
            bulkmessagelength += usertoken.Length;

            bulkmessagelength += sizeof(int); // local filename len
            bulkmessagelength += localfilename.Length;

            bulkmessagelength += sizeof(long); // filelength
            bulkmessagelength += sizeof(int); // Dest file path len
            bulkmessagelength += destfilepath.Length;

            bulkmessagelength += sizeof(int); // job name len
            bulkmessagelength += jobname.Length;

            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms);
            UTF8Encoding enc = new UTF8Encoding();

            bw.Write(signatureone);
            bw.Write(signaturetwo);
            bw.Write(bulkmessagelength);
            bw.Write(messageidentifier);

            bw.Write(useraccountname.Length);
            bw.Write(enc.GetBytes(useraccountname));
            
            bw.Write(usertoken.Length);
            bw.Write(enc.GetBytes(usertoken));
            
            bw.Write(localfilename.Length);
            bw.Write(enc.GetBytes(localfilename));

            bw.Write(filelength);

            bw.Write(destfilepath.Length);
            bw.Write(enc.GetBytes(destfilepath));

            bw.Write(jobname.Length);
            bw.Write(enc.GetBytes(jobname));


            SendBytes(ms.ToArray());

            //Read the reply
            byte[] bytes = new byte[_tcpClient.ReceiveBufferSize];
            int BytesRead = _networkStream.Read(bytes, 0, (int)_tcpClient.ReceiveBufferSize);

            int ipointer = 0;

            int sig1 = BitConverter.ToInt32(bytes, ipointer);
            ipointer += sizeof(int);

            int sig2 = BitConverter.ToInt32(bytes, ipointer);
            ipointer += sizeof(int);

            int msgident = BitConverter.ToInt32(bytes, ipointer);
            ipointer += sizeof(int);

            if (SignatureValid(sig1, sig2))
            {
                if (msgident == (int)CommsPacket.MessageTypes.LinuxFileTransferReady)
                {                    
                    return true;
                }

                return false;
            }

            return false;
        }

        public bool SendCompletedFileTable(List<FileChunkEntry> completedfiletable, string filepath, string destfilepath, string jobname, string useraccount, string usertoken)
        {
            if (_networkStream != null)
            {
                CommsPacket.CommsPacket myObject = new CommsPacket.CommsPacket();

                myObject.iMessagetype = (int)CommsPacket.MessageTypes.CompletedFileTable;
                myObject.filename = filepath;
                myObject.destpath = destfilepath;
                myObject.useraccount = useraccount;
                myObject.usertoken = usertoken;
                myObject.jobname = jobname;

                myObject.Somemessage = "CompletedFileTable";
                myObject.somethingelse = "CompletedFileTable";
                
                // Time to serialize the list of chunk entries into a byte array
                byte[] serializedfiletable = CommsPacket.CommsPacket.SerializeObject(completedfiletable);
                
                myObject.somebuffer = serializedfiletable;

                byte[] sendbytes = myObject.Serialize();
                byte[] encbytes = myObject.Encapsulate(sendbytes);

                _networkStream.Write(encbytes, 0, encbytes.Length);

                // Reads the NetworkStream into a byte buffer.
                byte[] bytes = new byte[_tcpClient.ReceiveBufferSize];
                int BytesRead = _networkStream.Read(bytes, 0, (int)_tcpClient.ReceiveBufferSize);

                CommsPacket.CommsResponse response = new CommsPacket.CommsResponse();
                response = response.DeSerialize(bytes);

                if (response.Message.Contains("I received the packet") == false)
                {
                    throw new Exception("Did not receive packet acknowledgement");
                }

                return true;
            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
        }

        public bool SendFileChunk(byte[] chunk, string filepath, long filelength, string useraccount, string usertoken)
        {
            if (_networkStream != null)
            {
                CommsPacket.CommsPacket myObject = new CommsPacket.CommsPacket();

                myObject.iMessagetype = (int)CommsPacket.MessageTypes.FileChunk;
                myObject.filename = Path.GetFileName(filepath);
                myObject.sourcepath = filepath;
                myObject.filelength = filelength;
                myObject.useraccount = useraccount;
                myObject.usertoken = usertoken;

                myObject.Somemessage = "FileChunk";
                myObject.somethingelse = "FileChunk";
                myObject.somebuffer = chunk;

                byte[] sendbytes = myObject.Serialize();
                byte[] encbytes = myObject.Encapsulate(sendbytes);

                _networkStream.Write(encbytes, 0, encbytes.Length);
                
                // Reads the NetworkStream into a byte buffer.
                byte[] bytes = new byte[_tcpClient.ReceiveBufferSize];
                int BytesRead = _networkStream.Read(bytes, 0, (int)_tcpClient.ReceiveBufferSize);

                CommsPacket.CommsResponse response = new CommsPacket.CommsResponse();
                response = response.DeSerialize(bytes);

                if (response.Message.Contains("I received the packet") == false)
                {
                    throw new Exception("Did not receive packet acknowledgement");
                }

                return true;
            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
        }

        public long SendFileChunk_Linux(byte[] chunk, string filepath, long filelength, string useraccount, string usertoken)
        {         
            SendBytes(chunk);

            return chunk.Length;
        }

        public bool SendBytes(byte[] bytes)
        {
            if (_networkStream != null)
            {
                _networkStream.Write(bytes, 0, bytes.Length);
                return true;
            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
        }

        public bool AuthenticateClientAccount(string accountname, string passwordhash, out string authtoken)
        {
            CommsPacket.CommsPacket myObject = new CommsPacket.CommsPacket();
            myObject.iMessagetype = (int)CommsPacket.MessageTypes.AuthenticateClient;
            myObject.useraccount = accountname;
            myObject.passwordhash = passwordhash;

            if (_networkStream != null)
            {
                Byte[] sendBytes = myObject.Serialize();
                Byte[] encbytes = myObject.Encapsulate(sendBytes);

                _networkStream.Write(encbytes, 0, encbytes.Length);

                //Read the reply
                byte[] bytes = new byte[_tcpClient.ReceiveBufferSize];
                int BytesRead = _networkStream.Read(bytes, 0, (int)_tcpClient.ReceiveBufferSize);

                CommsPacket.CommsResponse response = new CommsPacket.CommsResponse();
                response = response.DeSerialize(bytes);

                if (response.Message.Contains("I received the packet") == false)
                {
                    throw new Exception("Did not receive packet acknowledgement");
                }

                if (response.authenticated == true)
                {
                    authtoken = response.token;
                    return true;
                }
                else
                {
                    authtoken = "";
                    return false;
                }
            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
        }

        public bool SignatureValid(int sig1, int sig2)
        {
            if (sig1 == (int)CommsPacket.MessageTypes.LinuxSignatureOne && sig2 == (int)CommsPacket.MessageTypes.LinuxSignatureTwo)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool AuthenticateClientAccount_Linux(string accountname, string passwordhash, out string authtoken)
        {
            int signatureone = (int)CommsPacket.MessageTypes.LinuxSignatureOne;
            int signaturetwo = (int)CommsPacket.MessageTypes.LinuxSignatureTwo;
            int messageidentifier = (int)CommsPacket.MessageTypes.AuthenticateClient_Linux;
            int bulkmessagelength = sizeof(int) + sizeof(int) + accountname.Length + sizeof(int) + passwordhash.Length;

            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms);
            UTF8Encoding enc = new UTF8Encoding();

            bw.Write(signatureone);
            bw.Write(signaturetwo);
            bw.Write(bulkmessagelength);
            bw.Write(messageidentifier);
            bw.Write(accountname.Length);
            bw.Write(enc.GetBytes(accountname));
            bw.Write(passwordhash.Length);
            bw.Write(enc.GetBytes(passwordhash));

            SendBytes(ms.ToArray());

            //Read the reply
            byte[] bytes = new byte[_tcpClient.ReceiveBufferSize];
            int BytesRead = _networkStream.Read(bytes, 0, (int)_tcpClient.ReceiveBufferSize);

            int ipointer = 0;

            int sig1 = BitConverter.ToInt32(bytes, ipointer);
            ipointer += sizeof(int);

            int sig2 = BitConverter.ToInt32(bytes, ipointer);
            ipointer += sizeof(int);

            int msgident = BitConverter.ToInt32(bytes, ipointer);
            ipointer += sizeof(int);

            if (SignatureValid(sig1, sig2))
            {
                if (msgident == (int)CommsPacket.MessageTypes.LinuxAuthClientOk)
                {
                    authtoken = "";
                    return true;
                }

                if (msgident == (int)CommsPacket.MessageTypes.LinuxAuthClientDenied)
                {
                    authtoken = "";
                    return false;
                }
            }

            authtoken = "";

            //FileStream fs = new FileStream("C:\\Temp\\AuthAdminLinux.dat", FileMode.CreateNew);
            //fs.Write(ms.ToArray (), 0, (int)ms.Length);
            //fs.Close();


            // If we got here then the message identifier returned from the server did not match or the signatures did not match
            return false;
        }

        public List<FileChunkEntry> RequestFileChunkTable(string accountname, string usertoken, string sourcefilepath, string jobname)
        {
            // NOTE: sourcefilepath needs to looklike $$$ACCOUNTROOT$$$\SomeFolder\SubFolder\File.txt
            if (_networkStream != null)
            {
                CommsPacket.CommsPacket myObject = new CommsPacket.CommsPacket();

                myObject.iMessagetype = (int)CommsPacket.MessageTypes.BuildFileChunkTable;

                myObject.filename = sourcefilepath;
                myObject.useraccount = accountname;
                myObject.usertoken = usertoken;
                myObject.jobname = jobname;
                myObject.Somemessage = "BuildFileChunkTable";
                myObject.somethingelse = "BuildFileChunkTable";

                byte[] sendbytes = myObject.Serialize();
                byte[] encbytes = myObject.Encapsulate(sendbytes);

                _networkStream.Write(encbytes, 0, encbytes.Length);

                // Now process the large bit of data we're gonna receive
                BeginReceivingLargeData();

                // By the time we get to here our _largereceivedobject should be populated with stuff

                if (_largereceivedobject != null)
                {
                    if (_largereceivedobject.iMessagetype == (int)CommsPacket.MessageTypes.BuildFileChunkTable)
                    {

                        byte[] serializedchunklist = _largereceivedobject.somebuffer;

                        List<FileChunkEntry> chunklist = (List<FileChunkEntry>)CommsPacket.CommsPacket.DeSerializeObject(serializedchunklist);

                        return chunklist;

                    }
                    else
                    {
                        throw new Exception("Did not receive chunk table. Server responded with: " + _largereceivedobject.iMessagetype.ToString ());
                    }                
                }
                else
                {
                    throw new Exception("Large Received Object was NULL, when requesting the File Chunk Table");
                }
            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
           
        }


        public bool SendLargeDataRequest()
        {
            if (_networkStream != null)
            {
                CommsPacket.CommsPacket myObject = new CommsPacket.CommsPacket();

                myObject.iMessagetype = (int)CommsPacket.MessageTypes.LargeDataTest;

                myObject.Somemessage = "LargeDataRequest";
                myObject.somethingelse = "LargeDataRequest";

                byte[] sendbytes = myObject.Serialize();
                byte[] encbytes = myObject.Encapsulate(sendbytes);

                _networkStream.Write(encbytes, 0, encbytes.Length);

                // Now process the large bit of data we're gonna receive
                BeginReceivingLargeData();

                // By the time we get to here our _largereceivedobject should be populated with stuff

                if (_largereceivedobject != null)
                {
                    System.Windows.Forms.MessageBox.Show("Large Buffer Length: " + _largereceivedobject.somebuffer.Length.ToString());
                }
                else
                {
                    throw new Exception("Large Received Object was NULL.");
                }

                return true;
            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
        }


        private void BeginReceivingLargeData()
        {
            // This resets the global variables and kicks of the method that actually appends to the buffer of large data
            memStream = new MemoryStream();
            binBuffer = new BinaryWriter(memStream);

            ReceiveLargeData();
        }

        private void ReceiveLargeData()
        {
            CommsPacket.CommsPacket myPacket = new CommsPacket.CommsPacket();
            bool continuereceiving = true;

            while (continuereceiving)
            {
                try
                {
                    if (_networkStream.DataAvailable == true)
                    {
                        bytes = new byte[_maxbuffersize];

                        int dataavailable = _tcpClient.Available;

                        int BytesRead = _networkStream.Read(bytes, 0, dataavailable);

                        if (BytesRead > 0)
                        {
                            byte[] actualbytes = new byte[dataavailable];
                            Array.Copy(bytes, actualbytes, dataavailable);

                            // There might be more data, so store the data received so far.
                            binBuffer.Write(actualbytes);
                        }

                        // Copy the received buffer to a trimmed buffer. Why? Because memStream.GetBuffer() is a bit deceiving. Basically
                        // it gives you the buffer thats been received yes, but the size of the buffer it gives you is the maximum capacity
                        // of the buffer, not the actual length of data received, and this is what we need.
                        byte[] membuffer = memStream.GetBuffer();
                        byte[] trimmedbuffer = new byte[memStream.Length];
                        Array.Copy(membuffer, trimmedbuffer, memStream.Length);

                        if (myPacket.ContainsSignature(trimmedbuffer) == true)
                        {
                            // We've found an end of packet signature, so process it!!
                            //System.Diagnostics.Debug.WriteLine("We've found a signature. Received data length: " + memStream.GetBuffer().Length.ToString());
                            continuereceiving = false;
                            ProcessLargeData(trimmedbuffer);
                        }
                        else
                        {
                            //System.Diagnostics.Debug.WriteLine("No signature found. Waiting for more." + memStream.GetBuffer().Length.ToString());
                        }


                        if (_networkStream.DataAvailable == true)
                        {
                            continuereceiving = true;
                        }
                        else
                        {
                            
                        }
                    }
                }
                catch (IOException)
                {

                }
                catch (SocketException)
                {

                }

                System.Threading.Thread.Sleep(5);
            }
        }


        private void ProcessLargeData(byte[] trimmedbuffer)
        {
            if (memStream.Length > 0)
            {
                _largereceivedobject = new CommsPacket.CommsPacket();

                byte[] decapsulatedbytes = _largereceivedobject.Decapsulate(trimmedbuffer);

                try
                {
                    _largereceivedobject = _largereceivedobject.DeSerialize(decapsulatedbytes);
                }
                catch (Exception ex)
                {
                    _largereceivedobject = null;
                }
            }
            else
            {
                _largereceivedobject = null;
            }
        }


        public bool SendSampleData(string messagetosend)
        {
            CommsPacket.CommsPacket myObject = new CommsPacket.CommsPacket();
            myObject.iMessagetype = 100;
            myObject.Somemessage = messagetosend;
            myObject.somethingelse = "Another message";
            myObject.somebuffer = new byte[20000];

           
            //Byte[] sendBytes = Encoding.ASCII.GetBytes(DataToSend);
            byte[] sendBytes = myObject.Serialize();

            if (myObject.ContainsSignature(sendBytes) == true)
            {
                System.Windows.Forms.MessageBox.Show("send bytes contains the signature!");
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("send bytes DOES NOT contain the signature!");
            }

           

            byte[] encbytes = myObject.Encapsulate(sendBytes);


            if (myObject.ContainsSignature(encbytes) == true)
            {
                System.Windows.Forms.MessageBox.Show("encbytes contains the signature!");
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("encbytes DOES NOT contain the signature!");
            }

            
            // Try and decapsulate
            byte[] decapsulatedbytes = myObject.Decapsulate(encbytes);



            return true;
            
        }
    }

    public class SendWorkerParams
    {
        public string sourcepath;
        public string destpath;
        public string useraccount;
        public string authtoken;
        public string jobname;
    }

    public class SendWorkerProgress
    {
        public string sourcepath;
        public long bytessent;
        public long totalbytes;
        public long bytesremaining;
        public int percentcompleted;
    }
}
