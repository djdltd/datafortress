﻿namespace DataFortress_Client
{
    partial class BackupJob
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BackupJob));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnBrowseSource = new System.Windows.Forms.Button();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSource = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lnkExplainNormalBak = new System.Windows.Forms.LinkLabel();
            this.lnkExplainCumBak = new System.Windows.Forms.LinkLabel();
            this.optCumBackup = new System.Windows.Forms.RadioButton();
            this.optNormalBackup = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.chkLowpriority = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtComputerName = new System.Windows.Forms.TextBox();
            this.chkRunonnetwork = new System.Windows.Forms.CheckBox();
            this.ddlTime = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ddlDay = new System.Windows.Forms.ComboBox();
            this.optRunweek = new System.Windows.Forms.RadioButton();
            this.ddlMinutes = new System.Windows.Forms.ComboBox();
            this.optRunminutes = new System.Windows.Forms.RadioButton();
            this.chkRunatintervals = new System.Windows.Forms.CheckBox();
            this.chkRunatstartup = new System.Windows.Forms.CheckBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.folderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnBrowseSource);
            this.groupBox1.Controls.Add(this.txtName);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtSource);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(13, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(667, 110);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Source Directory and Name";
            // 
            // btnBrowseSource
            // 
            this.btnBrowseSource.Location = new System.Drawing.Point(621, 61);
            this.btnBrowseSource.Name = "btnBrowseSource";
            this.btnBrowseSource.Size = new System.Drawing.Size(30, 20);
            this.btnBrowseSource.TabIndex = 11;
            this.btnBrowseSource.Text = "...";
            this.btnBrowseSource.UseVisualStyleBackColor = true;
            this.btnBrowseSource.Click += new System.EventHandler(this.btnBrowseSource_Click);
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(55, 35);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(565, 20);
            this.txtName.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Name";
            // 
            // txtSource
            // 
            this.txtSource.Location = new System.Drawing.Point(55, 61);
            this.txtSource.Name = "txtSource";
            this.txtSource.Size = new System.Drawing.Size(565, 20);
            this.txtSource.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Source";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lnkExplainNormalBak);
            this.groupBox2.Controls.Add(this.lnkExplainCumBak);
            this.groupBox2.Controls.Add(this.optCumBackup);
            this.groupBox2.Controls.Add(this.optNormalBackup);
            this.groupBox2.Location = new System.Drawing.Point(15, 140);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(665, 66);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Job Type";
            // 
            // lnkExplainNormalBak
            // 
            this.lnkExplainNormalBak.AutoSize = true;
            this.lnkExplainNormalBak.Location = new System.Drawing.Point(205, 39);
            this.lnkExplainNormalBak.Name = "lnkExplainNormalBak";
            this.lnkExplainNormalBak.Size = new System.Drawing.Size(41, 13);
            this.lnkExplainNormalBak.TabIndex = 7;
            this.lnkExplainNormalBak.TabStop = true;
            this.lnkExplainNormalBak.Text = "Explain";
            // 
            // lnkExplainCumBak
            // 
            this.lnkExplainCumBak.AutoSize = true;
            this.lnkExplainCumBak.Location = new System.Drawing.Point(370, 39);
            this.lnkExplainCumBak.Name = "lnkExplainCumBak";
            this.lnkExplainCumBak.Size = new System.Drawing.Size(41, 13);
            this.lnkExplainCumBak.TabIndex = 6;
            this.lnkExplainCumBak.TabStop = true;
            this.lnkExplainCumBak.Text = "Explain";
            // 
            // optCumBackup
            // 
            this.optCumBackup.AutoSize = true;
            this.optCumBackup.Location = new System.Drawing.Point(373, 19);
            this.optCumBackup.Name = "optCumBackup";
            this.optCumBackup.Size = new System.Drawing.Size(117, 17);
            this.optCumBackup.TabIndex = 3;
            this.optCumBackup.TabStop = true;
            this.optCumBackup.Text = "Cumulative Backup";
            this.optCumBackup.UseVisualStyleBackColor = true;
            // 
            // optNormalBackup
            // 
            this.optNormalBackup.AutoSize = true;
            this.optNormalBackup.Location = new System.Drawing.Point(208, 19);
            this.optNormalBackup.Name = "optNormalBackup";
            this.optNormalBackup.Size = new System.Drawing.Size(98, 17);
            this.optNormalBackup.TabIndex = 2;
            this.optNormalBackup.TabStop = true;
            this.optNormalBackup.Text = "Normal Backup";
            this.optNormalBackup.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.chkLowpriority);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.txtComputerName);
            this.groupBox3.Controls.Add(this.chkRunonnetwork);
            this.groupBox3.Controls.Add(this.ddlTime);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.ddlDay);
            this.groupBox3.Controls.Add(this.optRunweek);
            this.groupBox3.Controls.Add(this.ddlMinutes);
            this.groupBox3.Controls.Add(this.optRunminutes);
            this.groupBox3.Controls.Add(this.chkRunatintervals);
            this.groupBox3.Controls.Add(this.chkRunatstartup);
            this.groupBox3.Location = new System.Drawing.Point(15, 228);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(665, 202);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "How to run";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label6.Location = new System.Drawing.Point(165, 151);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(348, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "(job takes more time to scan for changes, but takes much less CPU time)";
            // 
            // chkLowpriority
            // 
            this.chkLowpriority.AutoSize = true;
            this.chkLowpriority.Location = new System.Drawing.Point(28, 150);
            this.chkLowpriority.Name = "chkLowpriority";
            this.chkLowpriority.Size = new System.Drawing.Size(138, 17);
            this.chkLowpriority.TabIndex = 11;
            this.chkLowpriority.Text = "Run in low priority mode";
            this.chkLowpriority.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label5.Location = new System.Drawing.Point(491, 118);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(111, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "(ComputerName or IP)";
            // 
            // txtComputerName
            // 
            this.txtComputerName.Enabled = false;
            this.txtComputerName.Location = new System.Drawing.Point(348, 114);
            this.txtComputerName.Name = "txtComputerName";
            this.txtComputerName.Size = new System.Drawing.Size(137, 20);
            this.txtComputerName.TabIndex = 9;
            // 
            // chkRunonnetwork
            // 
            this.chkRunonnetwork.AutoSize = true;
            this.chkRunonnetwork.Location = new System.Drawing.Point(28, 117);
            this.chkRunonnetwork.Name = "chkRunonnetwork";
            this.chkRunonnetwork.Size = new System.Drawing.Size(314, 17);
            this.chkRunonnetwork.TabIndex = 8;
            this.chkRunonnetwork.Text = "Only run if the following computer is available on the network:";
            this.chkRunonnetwork.UseVisualStyleBackColor = true;
            this.chkRunonnetwork.CheckedChanged += new System.EventHandler(this.chkRunonnetwork_CheckedChanged);
            // 
            // ddlTime
            // 
            this.ddlTime.Enabled = false;
            this.ddlTime.FormattingEnabled = true;
            this.ddlTime.Items.AddRange(new object[] {
            "12 am",
            "1 am",
            "2 am",
            "3 am",
            "4 am",
            "5 am",
            "6 am",
            "7 am",
            "8 am",
            "9 am",
            "10 am",
            "11 am",
            "12 pm",
            "1 pm",
            "2 pm",
            "3 pm",
            "4 pm",
            "5 pm",
            "6 pm",
            "7 pm",
            "8 pm",
            "9 pm",
            "10 pm",
            "11 pm"});
            this.ddlTime.Location = new System.Drawing.Point(522, 70);
            this.ddlTime.Name = "ddlTime";
            this.ddlTime.Size = new System.Drawing.Size(84, 21);
            this.ddlTime.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Enabled = false;
            this.label4.Location = new System.Drawing.Point(500, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(16, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "at";
            // 
            // ddlDay
            // 
            this.ddlDay.Enabled = false;
            this.ddlDay.FormattingEnabled = true;
            this.ddlDay.Items.AddRange(new object[] {
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
            "Sunday"});
            this.ddlDay.Location = new System.Drawing.Point(384, 70);
            this.ddlDay.Name = "ddlDay";
            this.ddlDay.Size = new System.Drawing.Size(110, 21);
            this.ddlDay.TabIndex = 5;
            // 
            // optRunweek
            // 
            this.optRunweek.AutoSize = true;
            this.optRunweek.Enabled = false;
            this.optRunweek.Location = new System.Drawing.Point(239, 71);
            this.optRunweek.Name = "optRunweek";
            this.optRunweek.Size = new System.Drawing.Size(139, 17);
            this.optRunweek.TabIndex = 4;
            this.optRunweek.TabStop = true;
            this.optRunweek.Text = "Run once a week every";
            this.optRunweek.UseVisualStyleBackColor = true;
            // 
            // ddlMinutes
            // 
            this.ddlMinutes.Enabled = false;
            this.ddlMinutes.FormattingEnabled = true;
            this.ddlMinutes.Items.AddRange(new object[] {
            "10 mins",
            "20 mins",
            "30 mins",
            "40 mins",
            "50 mins",
            "1 hour",
            "2 hours",
            "3 hours",
            "4 hours",
            "5 hours",
            "6 hours",
            "7 hours",
            "8 hours",
            "9 hours",
            "10 hours",
            "11 hours",
            "12 hours"});
            this.ddlMinutes.Location = new System.Drawing.Point(108, 71);
            this.ddlMinutes.Name = "ddlMinutes";
            this.ddlMinutes.Size = new System.Drawing.Size(98, 21);
            this.ddlMinutes.TabIndex = 3;
            // 
            // optRunminutes
            // 
            this.optRunminutes.AutoSize = true;
            this.optRunminutes.Enabled = false;
            this.optRunminutes.Location = new System.Drawing.Point(28, 71);
            this.optRunminutes.Name = "optRunminutes";
            this.optRunminutes.Size = new System.Drawing.Size(74, 17);
            this.optRunminutes.TabIndex = 2;
            this.optRunminutes.TabStop = true;
            this.optRunminutes.Text = "Run every";
            this.optRunminutes.UseVisualStyleBackColor = true;
            // 
            // chkRunatintervals
            // 
            this.chkRunatintervals.AutoSize = true;
            this.chkRunatintervals.Location = new System.Drawing.Point(168, 28);
            this.chkRunatintervals.Name = "chkRunatintervals";
            this.chkRunatintervals.Size = new System.Drawing.Size(145, 17);
            this.chkRunatintervals.TabIndex = 1;
            this.chkRunatintervals.Text = "Run at specified intervals";
            this.chkRunatintervals.UseVisualStyleBackColor = true;
            this.chkRunatintervals.CheckedChanged += new System.EventHandler(this.chkRunatintervals_CheckedChanged);
            // 
            // chkRunatstartup
            // 
            this.chkRunatstartup.AutoSize = true;
            this.chkRunatstartup.Location = new System.Drawing.Point(28, 28);
            this.chkRunatstartup.Name = "chkRunatstartup";
            this.chkRunatstartup.Size = new System.Drawing.Size(93, 17);
            this.chkRunatstartup.TabIndex = 0;
            this.chkRunatstartup.Text = "Run at startup";
            this.chkRunatstartup.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(492, 436);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(91, 29);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(589, 436);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(91, 29);
            this.btnOk.TabIndex = 6;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // BackupJob
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(702, 477);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "BackupJob";
            this.Text = "BackupJob";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnBrowseSource;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtSource;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.LinkLabel lnkExplainNormalBak;
        private System.Windows.Forms.LinkLabel lnkExplainCumBak;
        private System.Windows.Forms.RadioButton optCumBackup;
        private System.Windows.Forms.RadioButton optNormalBackup;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox chkLowpriority;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtComputerName;
        private System.Windows.Forms.CheckBox chkRunonnetwork;
        private System.Windows.Forms.ComboBox ddlTime;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox ddlDay;
        private System.Windows.Forms.RadioButton optRunweek;
        private System.Windows.Forms.ComboBox ddlMinutes;
        private System.Windows.Forms.RadioButton optRunminutes;
        private System.Windows.Forms.CheckBox chkRunatintervals;
        private System.Windows.Forms.CheckBox chkRunatstartup;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.FolderBrowserDialog folderBrowser;
    }
}