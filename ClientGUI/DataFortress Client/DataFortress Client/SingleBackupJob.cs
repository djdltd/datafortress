﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataFortress_Client
{
    public enum BackupType
    {
        NormalBackup = 3,
        CumulativeBackup = 4
    }

    public enum IntervalType
    {
        Regular = 1,
        Weekly = 2
    }

    public class SingleBackupJob
    {
        public String JobName;
        public String SourceDir;
        public BackupType JobType = BackupType.NormalBackup;
        public bool RunAtStartup;
        public bool RunAtIntervals;
        public IntervalType JobInterval = IntervalType.Regular;
        public String DayofWeek;
        public DateTime TimeToRun;
        public String Text_TimeToRun;
        public int IntervalMinutes;
        public String Text_IntervalMinutes;
        public bool CheckNetworkComputer;
        public String ComputerName;
        public bool LowPriority;
        public DateTime LastRunDate;
    }
}
