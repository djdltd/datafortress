﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.IO;

namespace DataFortress_Client
{
    public class ApplicationSettings
    {
        public ServerLogonDetails serverlogondetails = new ServerLogonDetails();
        public DFASettings dfasettings = new DFASettings();

        public void SaveXMLSettings()
        {
            XmlSerializer mySerializer = new XmlSerializer(typeof(ApplicationSettings));
            StreamWriter myWriter = new StreamWriter(CommonFunctions.GetAppSettingsDir() + "FortressClientSettings.xml");

            mySerializer.Serialize(myWriter, this);
            myWriter.Close();
        }


        public ApplicationSettings LoadXMLSettings()
        {
            ApplicationSettings loadedsettings = new ApplicationSettings();

            if (File.Exists(CommonFunctions.GetAppSettingsDir() + "FortressClientSettings.xml") == true)
            {

                XmlSerializer mySerializer = new XmlSerializer(typeof(ApplicationSettings));

                // To read the xml file, create a FileStream.
                FileStream myFileStream = new FileStream(CommonFunctions.GetAppSettingsDir() + "FortressClientSettings.xml", FileMode.Open);

                // Call the Deserialize method and cast to the object type.
                loadedsettings = (ApplicationSettings)mySerializer.Deserialize(myFileStream);

                myFileStream.Close();
            }

            return loadedsettings;
        }
    }

    public class ServerLogonDetails
    {
        public string ServerName;
        public string ServerPort;
        public string Username;
        public string PasswordHash;
    }

    public class DFASettings
    {
        public bool DFAEnabled = true;
        public int MinimumFileSizeMB = 2;
        public bool UsingInclusions = false;
        public bool UsingExclusions = true;
        public List<string> FileExtInclusionList = new List<string>();
        public List<string> FileExtExclusionList = new List<string>();

    }
}
