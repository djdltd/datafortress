﻿namespace DataFortress_Client
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabSummary = new System.Windows.Forms.TabPage();
            this.progressOverall = new System.Windows.Forms.ProgressBar();
            this.progressIndividual = new System.Windows.Forms.ProgressBar();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lblNumWarnings = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lblNumErrors = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblTimetaken = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblNumCopyActioned = new System.Windows.Forms.Label();
            this.lblSizeCopyActioned = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblSizeDeleteActioned = new System.Windows.Forms.Label();
            this.lblSizecompared = new System.Windows.Forms.Label();
            this.lblSizetobeupdated = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblSizetobecopied = new System.Windows.Forms.Label();
            this.lblSizetobedeleted = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblNumCompared = new System.Windows.Forms.Label();
            this.lblToBeCopied = new System.Windows.Forms.Label();
            this.lblToBeUpdated = new System.Windows.Forms.Label();
            this.lblDeleteActioned = new System.Windows.Forms.Label();
            this.lblNumDeleted = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblGroupName = new System.Windows.Forms.Label();
            this.lblHeading = new System.Windows.Forms.Label();
            this.lblSubheading = new System.Windows.Forms.Label();
            this.tabBackupJobs = new System.Windows.Forms.TabPage();
            this.btnRunselected = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.btnDeleteEntry = new System.Windows.Forms.Button();
            this.btnNewbackupjob = new System.Windows.Forms.Button();
            this.lvBackupJobs = new System.Windows.Forms.ListView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.tabBackupLog = new System.Windows.Forms.TabPage();
            this.tabSettings = new System.Windows.Forms.TabPage();
            this.btnApplySettings = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtServer = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.txtPort = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btnTestEncaps = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnReadFile = new System.Windows.Forms.Button();
            this.btnSendSample = new System.Windows.Forms.Button();
            this.btnConnect = new System.Windows.Forms.Button();
            this.tabVerboseLog = new System.Windows.Forms.TabPage();
            this.label13 = new System.Windows.Forms.Label();
            this.lstVerboseLog = new System.Windows.Forms.ListBox();
            this.tabDFA = new System.Windows.Forms.TabPage();
            this.btnExcludeRemove = new System.Windows.Forms.Button();
            this.btnExcludeAdd = new System.Windows.Forms.Button();
            this.txtExcludeExt = new System.Windows.Forms.TextBox();
            this.btnIncludeRemove = new System.Windows.Forms.Button();
            this.btnIncludeAdd = new System.Windows.Forms.Button();
            this.txtIncludeExt = new System.Windows.Forms.TextBox();
            this.lstExclude = new System.Windows.Forms.ListBox();
            this.lstInclude = new System.Windows.Forms.ListBox();
            this.optExclude = new System.Windows.Forms.RadioButton();
            this.optInclude = new System.Windows.Forms.RadioButton();
            this.lblMB = new System.Windows.Forms.Label();
            this.cmbMinimumFileSizes = new System.Windows.Forms.ComboBox();
            this.lblMinimumFileSize = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.chkDFAEnabled = new System.Windows.Forms.CheckBox();
            this.btnBackupNow = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tmrJobCheck = new System.Windows.Forms.Timer(this.components);
            this.btnTest = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabSummary.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabBackupJobs.SuspendLayout();
            this.tabSettings.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabVerboseLog.SuspendLayout();
            this.tabDFA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabSummary);
            this.tabControl1.Controls.Add(this.tabBackupJobs);
            this.tabControl1.Controls.Add(this.tabBackupLog);
            this.tabControl1.Controls.Add(this.tabSettings);
            this.tabControl1.Controls.Add(this.tabVerboseLog);
            this.tabControl1.Controls.Add(this.tabDFA);
            this.tabControl1.Location = new System.Drawing.Point(169, 6);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(678, 359);
            this.tabControl1.TabIndex = 0;
            // 
            // tabSummary
            // 
            this.tabSummary.Controls.Add(this.progressOverall);
            this.tabSummary.Controls.Add(this.progressIndividual);
            this.tabSummary.Controls.Add(this.groupBox4);
            this.tabSummary.Controls.Add(this.groupBox2);
            this.tabSummary.Controls.Add(this.groupBox1);
            this.tabSummary.Location = new System.Drawing.Point(4, 22);
            this.tabSummary.Name = "tabSummary";
            this.tabSummary.Padding = new System.Windows.Forms.Padding(3);
            this.tabSummary.Size = new System.Drawing.Size(670, 333);
            this.tabSummary.TabIndex = 0;
            this.tabSummary.Text = "Summary";
            this.tabSummary.UseVisualStyleBackColor = true;
            // 
            // progressOverall
            // 
            this.progressOverall.Location = new System.Drawing.Point(6, 302);
            this.progressOverall.Name = "progressOverall";
            this.progressOverall.Size = new System.Drawing.Size(300, 16);
            this.progressOverall.TabIndex = 43;
            // 
            // progressIndividual
            // 
            this.progressIndividual.Location = new System.Drawing.Point(336, 302);
            this.progressIndividual.Name = "progressIndividual";
            this.progressIndividual.Size = new System.Drawing.Size(328, 16);
            this.progressIndividual.TabIndex = 42;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lblNumWarnings);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.lblNumErrors);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.lblTimetaken);
            this.groupBox4.Location = new System.Drawing.Point(3, 247);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(661, 49);
            this.groupBox4.TabIndex = 41;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "General Statistics";
            // 
            // lblNumWarnings
            // 
            this.lblNumWarnings.AutoSize = true;
            this.lblNumWarnings.Location = new System.Drawing.Point(330, 22);
            this.lblNumWarnings.Name = "lblNumWarnings";
            this.lblNumWarnings.Size = new System.Drawing.Size(13, 13);
            this.lblNumWarnings.TabIndex = 35;
            this.lblNumWarnings.Text = "0";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(264, 22);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(60, 13);
            this.label16.TabIndex = 34;
            this.label16.Text = "Warnings";
            // 
            // lblNumErrors
            // 
            this.lblNumErrors.AutoSize = true;
            this.lblNumErrors.Location = new System.Drawing.Point(510, 22);
            this.lblNumErrors.Name = "lblNumErrors";
            this.lblNumErrors.Size = new System.Drawing.Size(13, 13);
            this.lblNumErrors.TabIndex = 33;
            this.lblNumErrors.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(15, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 13);
            this.label7.TabIndex = 30;
            this.label7.Text = "Time Taken";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(448, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 13);
            this.label8.TabIndex = 32;
            this.label8.Text = "Errors";
            // 
            // lblTimetaken
            // 
            this.lblTimetaken.AutoSize = true;
            this.lblTimetaken.Location = new System.Drawing.Point(123, 22);
            this.lblTimetaken.Name = "lblTimetaken";
            this.lblTimetaken.Size = new System.Drawing.Size(13, 13);
            this.lblTimetaken.TabIndex = 31;
            this.lblTimetaken.Text = "0";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblNumCopyActioned);
            this.groupBox2.Controls.Add(this.lblSizeCopyActioned);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.lblSizeDeleteActioned);
            this.groupBox2.Controls.Add(this.lblSizecompared);
            this.groupBox2.Controls.Add(this.lblSizetobeupdated);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.lblSizetobecopied);
            this.groupBox2.Controls.Add(this.lblSizetobedeleted);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.lblNumCompared);
            this.groupBox2.Controls.Add(this.lblToBeCopied);
            this.groupBox2.Controls.Add(this.lblToBeUpdated);
            this.groupBox2.Controls.Add(this.lblDeleteActioned);
            this.groupBox2.Controls.Add(this.lblNumDeleted);
            this.groupBox2.Location = new System.Drawing.Point(6, 132);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(658, 109);
            this.groupBox2.TabIndex = 28;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Numbers of files";
            // 
            // lblNumCopyActioned
            // 
            this.lblNumCopyActioned.AutoSize = true;
            this.lblNumCopyActioned.Location = new System.Drawing.Point(549, 51);
            this.lblNumCopyActioned.Name = "lblNumCopyActioned";
            this.lblNumCopyActioned.Size = new System.Drawing.Size(13, 13);
            this.lblNumCopyActioned.TabIndex = 29;
            this.lblNumCopyActioned.Text = "0";
            // 
            // lblSizeCopyActioned
            // 
            this.lblSizeCopyActioned.AutoSize = true;
            this.lblSizeCopyActioned.Location = new System.Drawing.Point(611, 51);
            this.lblSizeCopyActioned.Name = "lblSizeCopyActioned";
            this.lblSizeCopyActioned.Size = new System.Drawing.Size(13, 13);
            this.lblSizeCopyActioned.TabIndex = 36;
            this.lblSizeCopyActioned.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(445, 51);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(94, 13);
            this.label6.TabIndex = 28;
            this.label6.Text = "Actually copied";
            // 
            // lblSizeDeleteActioned
            // 
            this.lblSizeDeleteActioned.AutoSize = true;
            this.lblSizeDeleteActioned.Location = new System.Drawing.Point(611, 76);
            this.lblSizeDeleteActioned.Name = "lblSizeDeleteActioned";
            this.lblSizeDeleteActioned.Size = new System.Drawing.Size(13, 13);
            this.lblSizeDeleteActioned.TabIndex = 33;
            this.lblSizeDeleteActioned.Text = "0";
            // 
            // lblSizecompared
            // 
            this.lblSizecompared.AutoSize = true;
            this.lblSizecompared.Location = new System.Drawing.Point(611, 25);
            this.lblSizecompared.Name = "lblSizecompared";
            this.lblSizecompared.Size = new System.Drawing.Size(13, 13);
            this.lblSizecompared.TabIndex = 38;
            this.lblSizecompared.Text = "0";
            // 
            // lblSizetobeupdated
            // 
            this.lblSizetobeupdated.AutoSize = true;
            this.lblSizetobeupdated.Location = new System.Drawing.Point(173, 50);
            this.lblSizetobeupdated.Name = "lblSizetobeupdated";
            this.lblSizetobeupdated.Size = new System.Drawing.Size(13, 13);
            this.lblSizetobeupdated.TabIndex = 28;
            this.lblSizetobeupdated.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(445, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 13);
            this.label5.TabIndex = 27;
            this.label5.Text = "Actually deleted";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(15, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "To be deleted";
            // 
            // lblSizetobecopied
            // 
            this.lblSizetobecopied.AutoSize = true;
            this.lblSizetobecopied.Location = new System.Drawing.Point(173, 25);
            this.lblSizetobecopied.Name = "lblSizetobecopied";
            this.lblSizetobecopied.Size = new System.Drawing.Size(13, 13);
            this.lblSizetobecopied.TabIndex = 27;
            this.lblSizetobecopied.Text = "0";
            // 
            // lblSizetobedeleted
            // 
            this.lblSizetobedeleted.AutoSize = true;
            this.lblSizetobedeleted.Location = new System.Drawing.Point(173, 75);
            this.lblSizetobedeleted.Name = "lblSizetobedeleted";
            this.lblSizetobedeleted.Size = new System.Drawing.Size(13, 13);
            this.lblSizetobedeleted.TabIndex = 29;
            this.lblSizetobedeleted.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(14, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "To be updated";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(14, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 24;
            this.label2.Text = "To be copied";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(445, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 13);
            this.label1.TabIndex = 23;
            this.label1.Text = "Files Compared";
            // 
            // lblNumCompared
            // 
            this.lblNumCompared.AutoSize = true;
            this.lblNumCompared.Location = new System.Drawing.Point(549, 25);
            this.lblNumCompared.Name = "lblNumCompared";
            this.lblNumCompared.Size = new System.Drawing.Size(13, 13);
            this.lblNumCompared.TabIndex = 18;
            this.lblNumCompared.Text = "0";
            // 
            // lblToBeCopied
            // 
            this.lblToBeCopied.AutoSize = true;
            this.lblToBeCopied.Location = new System.Drawing.Point(123, 25);
            this.lblToBeCopied.Name = "lblToBeCopied";
            this.lblToBeCopied.Size = new System.Drawing.Size(13, 13);
            this.lblToBeCopied.TabIndex = 19;
            this.lblToBeCopied.Text = "0";
            // 
            // lblToBeUpdated
            // 
            this.lblToBeUpdated.AutoSize = true;
            this.lblToBeUpdated.Location = new System.Drawing.Point(123, 50);
            this.lblToBeUpdated.Name = "lblToBeUpdated";
            this.lblToBeUpdated.Size = new System.Drawing.Size(13, 13);
            this.lblToBeUpdated.TabIndex = 20;
            this.lblToBeUpdated.Text = "0";
            // 
            // lblDeleteActioned
            // 
            this.lblDeleteActioned.AutoSize = true;
            this.lblDeleteActioned.Location = new System.Drawing.Point(549, 76);
            this.lblDeleteActioned.Name = "lblDeleteActioned";
            this.lblDeleteActioned.Size = new System.Drawing.Size(13, 13);
            this.lblDeleteActioned.TabIndex = 22;
            this.lblDeleteActioned.Text = "0";
            // 
            // lblNumDeleted
            // 
            this.lblNumDeleted.AutoSize = true;
            this.lblNumDeleted.Location = new System.Drawing.Point(123, 75);
            this.lblNumDeleted.Name = "lblNumDeleted";
            this.lblNumDeleted.Size = new System.Drawing.Size(13, 13);
            this.lblNumDeleted.TabIndex = 21;
            this.lblNumDeleted.Text = "0";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblGroupName);
            this.groupBox1.Controls.Add(this.lblHeading);
            this.groupBox1.Controls.Add(this.lblSubheading);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(658, 120);
            this.groupBox1.TabIndex = 27;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Current Operation";
            // 
            // lblGroupName
            // 
            this.lblGroupName.AutoSize = true;
            this.lblGroupName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGroupName.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lblGroupName.Location = new System.Drawing.Point(10, 28);
            this.lblGroupName.Name = "lblGroupName";
            this.lblGroupName.Size = new System.Drawing.Size(121, 13);
            this.lblGroupName.TabIndex = 24;
            this.lblGroupName.Text = "Welcome to Backup";
            // 
            // lblHeading
            // 
            this.lblHeading.AutoSize = true;
            this.lblHeading.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeading.Location = new System.Drawing.Point(10, 55);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(113, 13);
            this.lblHeading.TabIndex = 17;
            this.lblHeading.Text = "No Backup started";
            // 
            // lblSubheading
            // 
            this.lblSubheading.Location = new System.Drawing.Point(10, 79);
            this.lblSubheading.Name = "lblSubheading";
            this.lblSubheading.Size = new System.Drawing.Size(437, 38);
            this.lblSubheading.TabIndex = 23;
            this.lblSubheading.Text = "No data has been backed up";
            // 
            // tabBackupJobs
            // 
            this.tabBackupJobs.Controls.Add(this.btnRunselected);
            this.tabBackupJobs.Controls.Add(this.label12);
            this.tabBackupJobs.Controls.Add(this.btnDeleteEntry);
            this.tabBackupJobs.Controls.Add(this.btnNewbackupjob);
            this.tabBackupJobs.Controls.Add(this.lvBackupJobs);
            this.tabBackupJobs.Location = new System.Drawing.Point(4, 22);
            this.tabBackupJobs.Name = "tabBackupJobs";
            this.tabBackupJobs.Padding = new System.Windows.Forms.Padding(3);
            this.tabBackupJobs.Size = new System.Drawing.Size(670, 333);
            this.tabBackupJobs.TabIndex = 1;
            this.tabBackupJobs.Text = "Backup Jobs";
            this.tabBackupJobs.UseVisualStyleBackColor = true;
            // 
            // btnRunselected
            // 
            this.btnRunselected.Location = new System.Drawing.Point(173, 11);
            this.btnRunselected.Name = "btnRunselected";
            this.btnRunselected.Size = new System.Drawing.Size(102, 23);
            this.btnRunselected.TabIndex = 13;
            this.btnRunselected.Text = "Run selected";
            this.btnRunselected.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label12.Location = new System.Drawing.Point(6, 40);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(427, 14);
            this.label12.TabIndex = 12;
            this.label12.Text = "These are all of your Backup jobs. To add a new Backup job, click New.";
            // 
            // btnDeleteEntry
            // 
            this.btnDeleteEntry.Location = new System.Drawing.Point(90, 11);
            this.btnDeleteEntry.Name = "btnDeleteEntry";
            this.btnDeleteEntry.Size = new System.Drawing.Size(77, 23);
            this.btnDeleteEntry.TabIndex = 11;
            this.btnDeleteEntry.Text = "Delete";
            this.btnDeleteEntry.UseVisualStyleBackColor = true;
            this.btnDeleteEntry.Click += new System.EventHandler(this.btnDeleteEntry_Click);
            // 
            // btnNewbackupjob
            // 
            this.btnNewbackupjob.Location = new System.Drawing.Point(7, 11);
            this.btnNewbackupjob.Name = "btnNewbackupjob";
            this.btnNewbackupjob.Size = new System.Drawing.Size(77, 23);
            this.btnNewbackupjob.TabIndex = 10;
            this.btnNewbackupjob.Text = "New";
            this.btnNewbackupjob.UseVisualStyleBackColor = true;
            this.btnNewbackupjob.Click += new System.EventHandler(this.btnNewbackupjob_Click);
            // 
            // lvBackupJobs
            // 
            this.lvBackupJobs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvBackupJobs.LargeImageList = this.imageList1;
            this.lvBackupJobs.Location = new System.Drawing.Point(6, 62);
            this.lvBackupJobs.Name = "lvBackupJobs";
            this.lvBackupJobs.Size = new System.Drawing.Size(658, 260);
            this.lvBackupJobs.TabIndex = 0;
            this.lvBackupJobs.UseCompatibleStateImageBehavior = false;
            this.lvBackupJobs.DoubleClick += new System.EventHandler(this.lvBackupJobs_DoubleClick);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "FolderIcon48-2.png");
            // 
            // tabBackupLog
            // 
            this.tabBackupLog.Location = new System.Drawing.Point(4, 22);
            this.tabBackupLog.Name = "tabBackupLog";
            this.tabBackupLog.Size = new System.Drawing.Size(670, 333);
            this.tabBackupLog.TabIndex = 2;
            this.tabBackupLog.Text = "Backup Log";
            this.tabBackupLog.UseVisualStyleBackColor = true;
            // 
            // tabSettings
            // 
            this.tabSettings.Controls.Add(this.btnApplySettings);
            this.tabSettings.Controls.Add(this.groupBox3);
            this.tabSettings.Controls.Add(this.btnTestEncaps);
            this.tabSettings.Controls.Add(this.button1);
            this.tabSettings.Controls.Add(this.btnReadFile);
            this.tabSettings.Controls.Add(this.btnSendSample);
            this.tabSettings.Controls.Add(this.btnConnect);
            this.tabSettings.Location = new System.Drawing.Point(4, 22);
            this.tabSettings.Name = "tabSettings";
            this.tabSettings.Size = new System.Drawing.Size(670, 333);
            this.tabSettings.TabIndex = 3;
            this.tabSettings.Text = "Settings";
            this.tabSettings.UseVisualStyleBackColor = true;
            // 
            // btnApplySettings
            // 
            this.btnApplySettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnApplySettings.Location = new System.Drawing.Point(558, 263);
            this.btnApplySettings.Name = "btnApplySettings";
            this.btnApplySettings.Size = new System.Drawing.Size(101, 34);
            this.btnApplySettings.TabIndex = 17;
            this.btnApplySettings.Text = "Apply";
            this.btnApplySettings.UseVisualStyleBackColor = true;
            this.btnApplySettings.Click += new System.EventHandler(this.btnApplySettings_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtServer);
            this.groupBox3.Controls.Add(this.txtPassword);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.txtUsername);
            this.groupBox3.Controls.Add(this.txtPort);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Location = new System.Drawing.Point(77, 47);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(494, 185);
            this.groupBox3.TabIndex = 16;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Server Connection Details";
            // 
            // txtServer
            // 
            this.txtServer.Location = new System.Drawing.Point(199, 34);
            this.txtServer.Name = "txtServer";
            this.txtServer.Size = new System.Drawing.Size(226, 20);
            this.txtServer.TabIndex = 9;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(199, 136);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '●';
            this.txtPassword.Size = new System.Drawing.Size(226, 20);
            this.txtPassword.TabIndex = 15;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(49, 37);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(70, 13);
            this.label14.TabIndex = 8;
            this.label14.Text = "Server name:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(49, 139);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 13);
            this.label9.TabIndex = 14;
            this.label9.Text = "Password:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(49, 67);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(62, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Server port:";
            // 
            // txtUsername
            // 
            this.txtUsername.Location = new System.Drawing.Point(199, 99);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(226, 20);
            this.txtUsername.TabIndex = 13;
            // 
            // txtPort
            // 
            this.txtPort.Location = new System.Drawing.Point(199, 64);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(226, 20);
            this.txtPort.TabIndex = 11;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(49, 102);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(61, 13);
            this.label10.TabIndex = 12;
            this.label10.Text = "User name:";
            // 
            // btnTestEncaps
            // 
            this.btnTestEncaps.Location = new System.Drawing.Point(86, 375);
            this.btnTestEncaps.Name = "btnTestEncaps";
            this.btnTestEncaps.Size = new System.Drawing.Size(119, 28);
            this.btnTestEncaps.TabIndex = 4;
            this.btnTestEncaps.Text = "TestEncapsulation";
            this.btnTestEncaps.UseVisualStyleBackColor = true;
            this.btnTestEncaps.Visible = false;
            this.btnTestEncaps.Click += new System.EventHandler(this.btnTestEncaps_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(13, 409);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(117, 28);
            this.button1.TabIndex = 3;
            this.button1.Text = "Add Thread";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // btnReadFile
            // 
            this.btnReadFile.Location = new System.Drawing.Point(211, 375);
            this.btnReadFile.Name = "btnReadFile";
            this.btnReadFile.Size = new System.Drawing.Size(78, 28);
            this.btnReadFile.TabIndex = 2;
            this.btnReadFile.Text = "Read File";
            this.btnReadFile.UseVisualStyleBackColor = true;
            this.btnReadFile.Visible = false;
            this.btnReadFile.Click += new System.EventHandler(this.btnReadFile_Click);
            // 
            // btnSendSample
            // 
            this.btnSendSample.Location = new System.Drawing.Point(136, 410);
            this.btnSendSample.Name = "btnSendSample";
            this.btnSendSample.Size = new System.Drawing.Size(119, 27);
            this.btnSendSample.TabIndex = 1;
            this.btnSendSample.Text = "Send Sample Data";
            this.btnSendSample.UseVisualStyleBackColor = true;
            this.btnSendSample.Visible = false;
            this.btnSendSample.Click += new System.EventHandler(this.btnSendSample_Click);
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(13, 374);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(67, 29);
            this.btnConnect.TabIndex = 0;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Visible = false;
            this.btnConnect.Click += new System.EventHandler(this.button1_Click);
            // 
            // tabVerboseLog
            // 
            this.tabVerboseLog.Controls.Add(this.label13);
            this.tabVerboseLog.Controls.Add(this.lstVerboseLog);
            this.tabVerboseLog.Location = new System.Drawing.Point(4, 22);
            this.tabVerboseLog.Name = "tabVerboseLog";
            this.tabVerboseLog.Size = new System.Drawing.Size(670, 333);
            this.tabVerboseLog.TabIndex = 4;
            this.tabVerboseLog.Text = "Verbose Log";
            this.tabVerboseLog.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label13.Location = new System.Drawing.Point(3, 8);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(617, 28);
            this.label13.TabIndex = 11;
            this.label13.Text = "When files are due to be copied or deleted they will be logged here. If an error " +
                "occurs it will also be listed here including all information about the file.";
            // 
            // lstVerboseLog
            // 
            this.lstVerboseLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstVerboseLog.FormattingEnabled = true;
            this.lstVerboseLog.Location = new System.Drawing.Point(3, 42);
            this.lstVerboseLog.Name = "lstVerboseLog";
            this.lstVerboseLog.Size = new System.Drawing.Size(656, 277);
            this.lstVerboseLog.TabIndex = 0;
            // 
            // tabDFA
            // 
            this.tabDFA.Controls.Add(this.btnExcludeRemove);
            this.tabDFA.Controls.Add(this.btnExcludeAdd);
            this.tabDFA.Controls.Add(this.txtExcludeExt);
            this.tabDFA.Controls.Add(this.btnIncludeRemove);
            this.tabDFA.Controls.Add(this.btnIncludeAdd);
            this.tabDFA.Controls.Add(this.txtIncludeExt);
            this.tabDFA.Controls.Add(this.lstExclude);
            this.tabDFA.Controls.Add(this.lstInclude);
            this.tabDFA.Controls.Add(this.optExclude);
            this.tabDFA.Controls.Add(this.optInclude);
            this.tabDFA.Controls.Add(this.lblMB);
            this.tabDFA.Controls.Add(this.cmbMinimumFileSizes);
            this.tabDFA.Controls.Add(this.lblMinimumFileSize);
            this.tabDFA.Controls.Add(this.label15);
            this.tabDFA.Controls.Add(this.chkDFAEnabled);
            this.tabDFA.Location = new System.Drawing.Point(4, 22);
            this.tabDFA.Name = "tabDFA";
            this.tabDFA.Size = new System.Drawing.Size(670, 333);
            this.tabDFA.TabIndex = 5;
            this.tabDFA.Text = "DFA";
            this.tabDFA.UseVisualStyleBackColor = true;
            // 
            // btnExcludeRemove
            // 
            this.btnExcludeRemove.Location = new System.Drawing.Point(567, 280);
            this.btnExcludeRemove.Name = "btnExcludeRemove";
            this.btnExcludeRemove.Size = new System.Drawing.Size(62, 23);
            this.btnExcludeRemove.TabIndex = 14;
            this.btnExcludeRemove.Text = "Remove";
            this.btnExcludeRemove.UseVisualStyleBackColor = true;
            this.btnExcludeRemove.Click += new System.EventHandler(this.btnExcludeRemove_Click);
            // 
            // btnExcludeAdd
            // 
            this.btnExcludeAdd.Location = new System.Drawing.Point(514, 280);
            this.btnExcludeAdd.Name = "btnExcludeAdd";
            this.btnExcludeAdd.Size = new System.Drawing.Size(50, 23);
            this.btnExcludeAdd.TabIndex = 13;
            this.btnExcludeAdd.Text = "Add";
            this.btnExcludeAdd.UseVisualStyleBackColor = true;
            this.btnExcludeAdd.Click += new System.EventHandler(this.btnExcludeAdd_Click);
            // 
            // txtExcludeExt
            // 
            this.txtExcludeExt.Location = new System.Drawing.Point(408, 281);
            this.txtExcludeExt.Name = "txtExcludeExt";
            this.txtExcludeExt.Size = new System.Drawing.Size(101, 20);
            this.txtExcludeExt.TabIndex = 12;
            // 
            // btnIncludeRemove
            // 
            this.btnIncludeRemove.Location = new System.Drawing.Point(243, 280);
            this.btnIncludeRemove.Name = "btnIncludeRemove";
            this.btnIncludeRemove.Size = new System.Drawing.Size(62, 23);
            this.btnIncludeRemove.TabIndex = 11;
            this.btnIncludeRemove.Text = "Remove";
            this.btnIncludeRemove.UseVisualStyleBackColor = true;
            this.btnIncludeRemove.Click += new System.EventHandler(this.btnIncludeRemove_Click);
            // 
            // btnIncludeAdd
            // 
            this.btnIncludeAdd.Location = new System.Drawing.Point(190, 280);
            this.btnIncludeAdd.Name = "btnIncludeAdd";
            this.btnIncludeAdd.Size = new System.Drawing.Size(50, 23);
            this.btnIncludeAdd.TabIndex = 10;
            this.btnIncludeAdd.Text = "Add";
            this.btnIncludeAdd.UseVisualStyleBackColor = true;
            this.btnIncludeAdd.Click += new System.EventHandler(this.btnIncludeAdd_Click);
            // 
            // txtIncludeExt
            // 
            this.txtIncludeExt.Location = new System.Drawing.Point(84, 281);
            this.txtIncludeExt.Name = "txtIncludeExt";
            this.txtIncludeExt.Size = new System.Drawing.Size(101, 20);
            this.txtIncludeExt.TabIndex = 9;
            // 
            // lstExclude
            // 
            this.lstExclude.FormattingEnabled = true;
            this.lstExclude.Location = new System.Drawing.Point(408, 169);
            this.lstExclude.Name = "lstExclude";
            this.lstExclude.Size = new System.Drawing.Size(221, 108);
            this.lstExclude.TabIndex = 8;
            // 
            // lstInclude
            // 
            this.lstInclude.FormattingEnabled = true;
            this.lstInclude.Location = new System.Drawing.Point(84, 169);
            this.lstInclude.Name = "lstInclude";
            this.lstInclude.Size = new System.Drawing.Size(221, 108);
            this.lstInclude.TabIndex = 7;
            // 
            // optExclude
            // 
            this.optExclude.AutoSize = true;
            this.optExclude.Location = new System.Drawing.Point(398, 135);
            this.optExclude.Name = "optExclude";
            this.optExclude.Size = new System.Drawing.Size(231, 17);
            this.optExclude.TabIndex = 6;
            this.optExclude.TabStop = true;
            this.optExclude.Text = "Perform DFA on all files except the following";
            this.optExclude.UseVisualStyleBackColor = true;
            this.optExclude.CheckedChanged += new System.EventHandler(this.optExclude_CheckedChanged);
            // 
            // optInclude
            // 
            this.optInclude.AutoSize = true;
            this.optInclude.Location = new System.Drawing.Point(51, 135);
            this.optInclude.Name = "optInclude";
            this.optInclude.Size = new System.Drawing.Size(254, 17);
            this.optInclude.TabIndex = 5;
            this.optInclude.TabStop = true;
            this.optInclude.Text = "Only perform DFA on the following file extensions";
            this.optInclude.UseVisualStyleBackColor = true;
            this.optInclude.CheckedChanged += new System.EventHandler(this.optInclude_CheckedChanged);
            // 
            // lblMB
            // 
            this.lblMB.AutoSize = true;
            this.lblMB.Location = new System.Drawing.Point(365, 94);
            this.lblMB.Name = "lblMB";
            this.lblMB.Size = new System.Drawing.Size(23, 13);
            this.lblMB.TabIndex = 4;
            this.lblMB.Text = "MB";
            // 
            // cmbMinimumFileSizes
            // 
            this.cmbMinimumFileSizes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMinimumFileSizes.FormattingEnabled = true;
            this.cmbMinimumFileSizes.Location = new System.Drawing.Point(294, 91);
            this.cmbMinimumFileSizes.Name = "cmbMinimumFileSizes";
            this.cmbMinimumFileSizes.Size = new System.Drawing.Size(65, 21);
            this.cmbMinimumFileSizes.TabIndex = 3;
            this.cmbMinimumFileSizes.SelectedIndexChanged += new System.EventHandler(this.cmbMinimumFileSizes_SelectedIndexChanged);
            // 
            // lblMinimumFileSize
            // 
            this.lblMinimumFileSize.AutoSize = true;
            this.lblMinimumFileSize.Location = new System.Drawing.Point(48, 94);
            this.lblMinimumFileSize.Name = "lblMinimumFileSize";
            this.lblMinimumFileSize.Size = new System.Drawing.Size(240, 13);
            this.lblMinimumFileSize.TabIndex = 2;
            this.lblMinimumFileSize.Text = "Only perform a byte level comparison on files over";
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(12, 10);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(640, 31);
            this.label15.TabIndex = 1;
            this.label15.Text = resources.GetString("label15.Text");
            // 
            // chkDFAEnabled
            // 
            this.chkDFAEnabled.AutoSize = true;
            this.chkDFAEnabled.Location = new System.Drawing.Point(13, 48);
            this.chkDFAEnabled.Name = "chkDFAEnabled";
            this.chkDFAEnabled.Size = new System.Drawing.Size(172, 17);
            this.chkDFAEnabled.TabIndex = 0;
            this.chkDFAEnabled.Text = "Enable Differential File Analysis";
            this.chkDFAEnabled.UseVisualStyleBackColor = true;
            this.chkDFAEnabled.CheckedChanged += new System.EventHandler(this.chkDFAEnabled_CheckedChanged);
            // 
            // btnBackupNow
            // 
            this.btnBackupNow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnBackupNow.Location = new System.Drawing.Point(731, 371);
            this.btnBackupNow.Name = "btnBackupNow";
            this.btnBackupNow.Size = new System.Drawing.Size(116, 37);
            this.btnBackupNow.TabIndex = 1;
            this.btnBackupNow.Text = "Backup Now";
            this.btnBackupNow.UseVisualStyleBackColor = true;
            this.btnBackupNow.Click += new System.EventHandler(this.btnBackupNow_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::DataFortress_Client.Properties.Resources.ClientBanner;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(169, 424);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // tmrJobCheck
            // 
            this.tmrJobCheck.Enabled = true;
            this.tmrJobCheck.Interval = 2000;
            this.tmrJobCheck.Tick += new System.EventHandler(this.tmrJobCheck_Tick);
            // 
            // btnTest
            // 
            this.btnTest.Location = new System.Drawing.Point(590, 371);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(98, 37);
            this.btnTest.TabIndex = 3;
            this.btnTest.Text = "Test";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(855, 420);
            this.Controls.Add(this.btnTest);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnBackupNow);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(871, 458);
            this.MinimumSize = new System.Drawing.Size(871, 458);
            this.Name = "Form1";
            this.Text = "Data Fortress Client v1.1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabSummary.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabBackupJobs.ResumeLayout(false);
            this.tabSettings.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabVerboseLog.ResumeLayout(false);
            this.tabDFA.ResumeLayout(false);
            this.tabDFA.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabSummary;
        private System.Windows.Forms.TabPage tabBackupJobs;
        private System.Windows.Forms.TabPage tabBackupLog;
        private System.Windows.Forms.TabPage tabSettings;
        private System.Windows.Forms.TabPage tabVerboseLog;
        private System.Windows.Forms.ListBox lstVerboseLog;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Button btnSendSample;
        private System.Windows.Forms.Button btnReadFile;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnTestEncaps;
        private System.Windows.Forms.Button btnRunselected;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnDeleteEntry;
        private System.Windows.Forms.Button btnNewbackupjob;
        private System.Windows.Forms.ListView lvBackupJobs;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblNumCopyActioned;
        private System.Windows.Forms.Label lblSizeCopyActioned;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblSizeDeleteActioned;
        private System.Windows.Forms.Label lblSizecompared;
        private System.Windows.Forms.Label lblSizetobeupdated;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblSizetobecopied;
        private System.Windows.Forms.Label lblSizetobedeleted;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblNumCompared;
        private System.Windows.Forms.Label lblToBeCopied;
        private System.Windows.Forms.Label lblToBeUpdated;
        private System.Windows.Forms.Label lblDeleteActioned;
        private System.Windows.Forms.Label lblNumDeleted;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblGroupName;
        private System.Windows.Forms.Label lblHeading;
        private System.Windows.Forms.Label lblSubheading;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label lblNumWarnings;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lblNumErrors;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblTimetaken;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnBackupNow;
        private System.Windows.Forms.ProgressBar progressOverall;
        private System.Windows.Forms.ProgressBar progressIndividual;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtServer;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnApplySettings;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TabPage tabDFA;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.CheckBox chkDFAEnabled;
        private System.Windows.Forms.Label lblMB;
        private System.Windows.Forms.ComboBox cmbMinimumFileSizes;
        private System.Windows.Forms.Label lblMinimumFileSize;
        private System.Windows.Forms.ListBox lstExclude;
        private System.Windows.Forms.ListBox lstInclude;
        private System.Windows.Forms.RadioButton optExclude;
        private System.Windows.Forms.RadioButton optInclude;
        private System.Windows.Forms.Button btnExcludeRemove;
        private System.Windows.Forms.Button btnExcludeAdd;
        private System.Windows.Forms.TextBox txtExcludeExt;
        private System.Windows.Forms.Button btnIncludeRemove;
        private System.Windows.Forms.Button btnIncludeAdd;
        private System.Windows.Forms.TextBox txtIncludeExt;
        private System.Windows.Forms.Timer tmrJobCheck;
        private System.Windows.Forms.Button btnTest;
    }
}

