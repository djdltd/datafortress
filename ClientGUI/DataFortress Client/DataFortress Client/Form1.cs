﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.IO;

namespace DataFortress_Client
{
    public partial class Form1 : Form
    {
        Communications _comms = new Communications();
        int _mynumber = 0;
        BackupJobManager _jobmanager = new BackupJobManager();
        FileSystemAnalyser _fsanalyser = new FileSystemAnalyser();
        ApplicationSettings _appsettings = new ApplicationSettings();
        string _authenticatedclienttoken;

        List<SingleBackupJob> _processingjobs = new List<SingleBackupJob>();

        SingleBackupJob _currentbackupjob = new SingleBackupJob();
        int _currentbackupjobindex;

        public Form1()
        {
            InitializeComponent();

            _fsanalyser.OnListProgressUpdate += new FileSystemAnalyser.ListProgressUpdate(_fsanalyser_OnListProgressUpdate);
            _fsanalyser.OnListCompleted += new FileSystemAnalyser.ListCompleted(_fsanalyser_OnListCompleted);
            _fsanalyser.OnSendFilesProgressUpdate += new FileSystemAnalyser.SendFilesProgressUpdate(_fsanalyser_OnSendFilesProgressUpdate);
            _fsanalyser.OnIndividualSendFileProgress += new FileSystemAnalyser.IndividualSendFileProgress(_fsanalyser_OnIndividualSendFileProgress);
            _fsanalyser.OnSendFilesCompleted += new FileSystemAnalyser.SendFilesCompleted(_fsanalyser_OnSendFilesCompleted);

            _fsanalyser.OnDifferentialProgressUpdate += new FileSystemAnalyser.DifferentialProgressUpdate(_fsanalyser_OnDifferentialProgressUpdate);

            _fsanalyser.ServerCommunications = _comms;
        
            
        }

        void _fsanalyser_OnDifferentialProgressUpdate(DifferentialProgressInformation progressinfo)
        {
            //throw new NotImplementedException();

            lblHeading.Text = progressinfo.currentoperation;
            lblSubheading.Text = progressinfo.currentfilepath;
            lstVerboseLog.Items.Add("Performing Differential Analysis on file: " + progressinfo.currentfilepath);
        }

        void _fsanalyser_OnSendFilesCompleted()
        {
            //throw new NotImplementedException();
            btnBackupNow.Enabled = true;

            lblHeading.Text = "Backup Completed.";
            lblSubheading.Text = "All relevant files have been backed up to the Fortress Server.";
            _comms.Disconnect();
        }

        void _fsanalyser_OnIndividualSendFileProgress(SendWorkerProgress fileprogress)
        {
            progressIndividual.Value = fileprogress.percentcompleted;

            //throw new NotImplementedException();
        }

        void _fsanalyser_OnSendFilesProgressUpdate(SendFilesProgressInformation progressinfo)
        {
            lblHeading.Text = "Sending File to Server...";
            lblSubheading.Text = progressinfo.currentfilepath;
            lstVerboseLog.Items.Add("Sending File " + progressinfo.currentfilepath);

            lblNumCopyActioned.Text = progressinfo.numfilessent.ToString();
            progressOverall.Value = progressinfo.percentprogress;

            //throw new NotImplementedException();
        }

        void _fsanalyser_OnListCompleted()
        {
            List<FileCopyEntry> copyentries = _fsanalyser.CopyEntries;

            /*

            if (MessageBox.Show("List has completed, do you want to start sending files?", "List Completed.", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {
                _fsanalyser.SendFilesAsync(_appsettings.serverlogondetails.Username, _authenticatedclienttoken);
            }
            else
            {
                _comms.Disconnect();
            }
             */

            if (_currentbackupjobindex < (_processingjobs.Count - 1))
            {
                _currentbackupjobindex++;
                _currentbackupjob = _processingjobs[_currentbackupjobindex];

                _fsanalyser.ListFilesNeedingCopyUsingServer(_currentbackupjob.SourceDir, false, _appsettings.serverlogondetails.Username, _authenticatedclienttoken, false, _currentbackupjob.JobName);
            }
            else
            {
                if (_fsanalyser.CopyEntries.Count > 0)
                {
                    _fsanalyser.SendFilesAsync(_appsettings.serverlogondetails.Username, _authenticatedclienttoken);
                }
                else
                {
                    lblHeading.Text = "All Files are up to date. No Backup necessary.";
                    lblSubheading.Text = "Backup scan completed.";
                    btnBackupNow.Enabled = true;
                    _comms.Disconnect();
                }

                /*
                if (MessageBox.Show("List has completed, do you want to start sending files?", "List Completed.", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                {
                    
                }
                else
                {
                    
                }
                 */
            }
        }

        void _fsanalyser_OnListProgressUpdate(ListProgressInformation progressinfo)
        {
            lblNumCompared.Text = progressinfo.NumFilesCompared;
            lblToBeCopied.Text = progressinfo.NumFilesToBeCopied;
            lblToBeUpdated.Text = progressinfo.NumFilesToBeUpdated;
            //lblNumDeleted.Text = 
            lblNumErrors.Text = progressinfo.NumErrorsOccured;
            lblNumWarnings.Text = progressinfo.NumWarningsOccured;

            lblSizecompared.Text = progressinfo.SizeCompared;
            lblSizetobeupdated.Text = progressinfo.SizeToBeUpdated;
            lblSizetobecopied.Text = progressinfo.SizeToBeCopied;

            if (progressinfo.UsingHeading)
            {
                lblHeading.Text = progressinfo.ProgressHeading;
            }

            if (progressinfo.UsingSubHeading)
            {
                lblSubheading.Text = progressinfo.ProgressSubHeading;
            }

            if (progressinfo.UsingListOutput)
            {
                lstVerboseLog.Items.Add(progressinfo.ProgressListOutput);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _jobmanager.LoadXMLBackupEntries();
            _appsettings = _appsettings.LoadXMLSettings();
            RefreshBackupJobList();
            RefreshSettings();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string connecterror;
            if (_comms.Connect("localhost", 10116, out connecterror) == true)
            {
                MessageBox.Show("Connection successful.");
            }
            else
            {
                MessageBox.Show("Connection failed.");
            }
        }

        private void btnSendSample_Click(object sender, EventArgs e)
        {
            _comms.SendLargeDataRequest();
        }

        private void btnReadFile_Click(object sender, EventArgs e)
        {
            //_comms.SendFile("C:\\Temp\\IFC\\Original.avi", "OriginalCopied.avi");
            MessageBox.Show("File has been read.");
        }


        public void ClientThread(object threadargs)
        {
            string connecterror;

            ThreadArgs args = (ThreadArgs)threadargs;
            Communications comms = new Communications();

            if (comms.Connect("localhost", 10116, out connecterror) == true)
            {
                //comms.SendFile(args.sourcefile, args.destfilename);
                System.Diagnostics.Debug.WriteLine("Thread connection ok.");
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Thread connection failed.");
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            //_comms.SendFile("C:\\Temp\\IFC\\Modified.avi", "ModifiedCopied.avi");
            Thread newthread = new Thread(new ParameterizedThreadStart(ClientThread));

            _mynumber++;

            ThreadArgs myargs = new ThreadArgs();
            myargs.sourcefile = "C:\\Temp\\IFC\\Original.avi";
            myargs.destfilename = "OriginalTransfer" + _mynumber.ToString() + ".avi";

            newthread.Start((ThreadArgs)myargs);
            
            
            MessageBox.Show("Thread has been added");
        }

        private void btnTestEncaps_Click(object sender, EventArgs e)
        {
            _comms.SendSampleData("This is a really big message that I want to send.");
        }

        private void btnNewsyncjob_Click(object sender, EventArgs e)
        {

        }

        private void lvSyncjobs_DoubleClick(object sender, EventArgs e)
        {

        }

        private void btnNewbackupjob_Click(object sender, EventArgs e)
        {
            BackupJob jobform = new BackupJob();

            jobform.OnFormCompleted += new BackupJob.FormCompleted(jobform_OnFormCompleted);
            jobform.Show();
        }

        void jobform_OnFormCompleted(SingleBackupJob backupjob)
        {
            //throw new NotImplementedException();
            if (_jobmanager.DoesBackupJobExist(backupjob.JobName))
            {
                _jobmanager.UpdateBackupJob(backupjob);
            }
            else
            {
                _jobmanager.BackupJobs.Add(backupjob);
            }

            _jobmanager.SaveXMLBackupEntries();

            RefreshBackupJobList();

        }



        private void RefreshBackupJobList()
        {
            lvBackupJobs.Clear();

            foreach (SingleBackupJob job in _jobmanager.BackupJobs)
            {
                lvBackupJobs.Items.Add(new ListViewItem(job.JobName, 0));
            }
        }

        private void lvBackupJobs_DoubleClick(object sender, EventArgs e)
        {
            String strItemName = lvBackupJobs.Items[lvBackupJobs.SelectedIndices[0]].Text;

            SingleBackupJob job = _jobmanager.GetBackupEntry(strItemName);

            if (job != null)
            {
                BackupJob jobform = new BackupJob();

                jobform.OnFormCompleted += new BackupJob.FormCompleted(jobform_OnFormCompleted);
                jobform.BackupJobToForm(job);

                jobform.Show();
            }
        }

        private void btnDeleteEntry_Click(object sender, EventArgs e)
        {
            if (lvBackupJobs.SelectedItems.Count > 0)
            {
                String strItemName = lvBackupJobs.Items[lvBackupJobs.SelectedIndices[0]].Text;

                if (MessageBox.Show("Delete " + strItemName + " Backup Entry?", "Backup", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    _jobmanager.DeleteBackupJob(strItemName);
                    _jobmanager.SaveXMLBackupEntries();
                    RefreshBackupJobList();
                }
            }
        }

        private void BeginListingFiles()
        {
            _fsanalyser.ListFilesNeedingCopyUsingServer(_currentbackupjob.SourceDir, false, _appsettings.serverlogondetails.Username, _authenticatedclienttoken, true, _currentbackupjob.JobName);
            // Now need to decide what happens when the listing is complete.
        }

        private void btnBackupNow_Click(object sender, EventArgs e)
        {
            //_fsanalyser.ListFilesNeedingCopy("C:\\Users\\Danny\\Downloads", "C:\\Temp\\IFC", false);
            string connecterror;
            if (_appsettings.serverlogondetails.ServerName == null || _appsettings.serverlogondetails.ServerPort == null || _appsettings.serverlogondetails.Username == null || _appsettings.serverlogondetails.PasswordHash == null)
            {
                MessageBox.Show("Unable to initiate backup because the server connection details have not been set. Please set the Fortress Server Details under Settings.", "Server Connection not set.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (_appsettings.serverlogondetails.ServerName.Trim() == "" || _appsettings.serverlogondetails.ServerPort.Trim() == "" || _appsettings.serverlogondetails.Username.Trim() == "" || _appsettings.serverlogondetails.PasswordHash.Trim() == "")
            {
                MessageBox.Show("Unable to initiate backup because the server connection details have not been set. Please set the Fortress Server Details under Settings.", "Server Connection not set.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (_comms.Connect(_appsettings.serverlogondetails.ServerName, Int32.Parse (_appsettings.serverlogondetails.ServerPort), out connecterror) == true)
            {
                //MessageBox.Show("Connection successful.");
                // Connection successful, now we need to authenticate ourselves with the server
                string authtoken;
                if (_comms.AuthenticateClientAccount_Linux(_appsettings.serverlogondetails.Username, _appsettings.serverlogondetails.PasswordHash, out authtoken) == true)
                {
                    _authenticatedclienttoken = authtoken;
                    // We are now authenticated, so start backing up

                    // Now we need to take all of the backup jobs and add them to a list of backup jobs to be processed.
                    _processingjobs.Clear();
                    foreach (SingleBackupJob backupjob in _jobmanager.BackupJobs)
                    {
                        // You were here.
                        _processingjobs.Add(backupjob);
                    }

                    if (_processingjobs.Count > 0)
                    {
                        _currentbackupjob = _processingjobs[0];
                        _currentbackupjobindex = 0;
                        btnBackupNow.Enabled = false;
                        BeginListingFiles();
                    }
                    else
                    {
                        MessageBox.Show("The Fortress Server has accepted your Logon details, however you have not created any Backup Jobs to be processed. Please create one or more backup jobs under Backup Jobs", "No Backup Jobs", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    _comms.Disconnect();
                    MessageBox.Show("Unable to initiate backup, because your Username or Password was invalid. Access to server Denied.", "Access Denied.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            else
            {
                MessageBox.Show("Unable to connect to the server, due to the follow reason: " + connecterror, "Unable to connect", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            
        }

        private void btnApplySettings_Click(object sender, EventArgs e)
        {
            // Save any application settings

            _appsettings.serverlogondetails.ServerName = txtServer.Text;
            _appsettings.serverlogondetails.ServerPort = txtPort.Text;
            _appsettings.serverlogondetails.Username = txtUsername.Text;

            if (txtPassword.Text != _appsettings.serverlogondetails.PasswordHash)
            {
                _appsettings.serverlogondetails.PasswordHash = CommonFunctions.CreateMD5HashFromString(txtPassword.Text);
            }

            _appsettings.SaveXMLSettings();
        }


        public void RefreshSettings()
        {
            // takes the app settings class and ensures all the relevant controls are set to their corresponding values

            txtServer.Text = _appsettings.serverlogondetails.ServerName;
            txtPort.Text = _appsettings.serverlogondetails.ServerPort;
            txtUsername.Text = _appsettings.serverlogondetails.Username;
            txtPassword.Text = _appsettings.serverlogondetails.PasswordHash;

            RefreshDFASettings();
        }

        public void RefreshDFASettings()
        {
            chkDFAEnabled.Checked = _appsettings.dfasettings.DFAEnabled;

            PopulateMinimumFileSizes();

            cmbMinimumFileSizes.Text = _appsettings.dfasettings.MinimumFileSizeMB.ToString();

            optInclude.Checked = _appsettings.dfasettings.UsingInclusions;
            optExclude.Checked = _appsettings.dfasettings.UsingExclusions;

            // If there are no exclusions in the list then add the defaults
            if (_appsettings.dfasettings.FileExtExclusionList.Count == 0)
            {
                _appsettings.dfasettings.FileExtExclusionList.Add(".JPG");
            }

            lstInclude.Items.Clear();
            foreach (string fileext in _appsettings.dfasettings.FileExtInclusionList)
            {
                lstInclude.Items.Add(fileext);
            }

            lstExclude.Items.Clear();
            foreach (string fileext in _appsettings.dfasettings.FileExtExclusionList)
            {
                lstExclude.Items.Add(fileext);
            }

            UpdateFSAWithDFASettings();
        }

        public void UpdateFSAWithDFASettings()
        {
            _fsanalyser.DFAEnabled = _appsettings.dfasettings.DFAEnabled;
            _fsanalyser.DFAMinimumFileSizeMB = _appsettings.dfasettings.MinimumFileSizeMB;
            _fsanalyser.DFAUseFileExtensionInclusionList = _appsettings.dfasettings.UsingInclusions;
            _fsanalyser.DFAUseFileExtensionExclusionList = _appsettings.dfasettings.UsingExclusions;
            _fsanalyser.DFAFileExtensionExclusionList = _appsettings.dfasettings.FileExtExclusionList;
            _fsanalyser.DFAFileExtensionInclusionList = _appsettings.dfasettings.FileExtInclusionList;
        }

        public void PopulateMinimumFileSizes()
        {
            cmbMinimumFileSizes.Items.Clear();

            cmbMinimumFileSizes.Items.Add("2");
            cmbMinimumFileSizes.Items.Add("10");
            cmbMinimumFileSizes.Items.Add("50");
            cmbMinimumFileSizes.Items.Add("100");
            cmbMinimumFileSizes.Items.Add("250");
            cmbMinimumFileSizes.Items.Add("500");
            cmbMinimumFileSizes.Items.Add("750");
            cmbMinimumFileSizes.Items.Add("1000");
            cmbMinimumFileSizes.Items.Add("2000");
            cmbMinimumFileSizes.Items.Add("3000");
            cmbMinimumFileSizes.Items.Add("4000");
            cmbMinimumFileSizes.Items.Add("5000");
        }

        public void SetDFAControlsEnabled(bool bvisible)
        {
            lblMinimumFileSize.Enabled = bvisible;
            lblMB.Enabled = bvisible;
            cmbMinimumFileSizes.Enabled = bvisible;
            optInclude.Enabled = bvisible;
            optExclude.Enabled = bvisible;
            lstInclude.Enabled = bvisible;
            txtIncludeExt.Enabled = bvisible;
            btnIncludeAdd.Enabled = bvisible;
            btnIncludeRemove.Enabled = bvisible;
            lstExclude.Enabled = bvisible;
            txtExcludeExt.Enabled = bvisible;
            btnExcludeAdd.Enabled = bvisible;
            btnExcludeRemove.Enabled = bvisible;

            if (optInclude.Checked == true)
            {
                SetInclusionsEnabled(true);
                SetExclusionsEnabled(false);
            }
        
            if (optExclude.Checked == true)
            {
                SetInclusionsEnabled(false);
                SetExclusionsEnabled(true);
            }
        }

        private void SetInclusionsEnabled(bool bEnabled)
        {
            lstInclude.Enabled = bEnabled;
            txtIncludeExt.Enabled = bEnabled;
            btnIncludeAdd.Enabled = bEnabled;
            btnIncludeRemove.Enabled = bEnabled;
        }

        private void SetExclusionsEnabled(bool bEnabled)
        {
            lstExclude.Enabled = bEnabled;
            txtExcludeExt.Enabled = bEnabled;
            btnExcludeAdd.Enabled = bEnabled;
            btnExcludeRemove.Enabled = bEnabled;
        }

        private void chkDFAEnabled_CheckedChanged(object sender, EventArgs e)
        {
            SetDFAControlsEnabled(chkDFAEnabled.Checked);

            _appsettings.dfasettings.DFAEnabled = chkDFAEnabled.Checked;
            UpdateFSAWithDFASettings();
            _appsettings.SaveXMLSettings();
        }

        private void optInclude_CheckedChanged(object sender, EventArgs e)
        {
            if (optInclude.Checked == true)
            {
                SetInclusionsEnabled(true);
                SetExclusionsEnabled(false);

                _appsettings.dfasettings.UsingInclusions = true;
                _appsettings.dfasettings.UsingExclusions = false;
                UpdateFSAWithDFASettings();
                _appsettings.SaveXMLSettings();
            }
        }

        private void optExclude_CheckedChanged(object sender, EventArgs e)
        {
            if (optExclude.Checked == true)
            {
                SetInclusionsEnabled(false);
                SetExclusionsEnabled(true);

                _appsettings.dfasettings.UsingInclusions = false;
                _appsettings.dfasettings.UsingExclusions = true;
                UpdateFSAWithDFASettings();
                _appsettings.SaveXMLSettings();
            }
        }

        private void RefreshDFAInclusionList()
        {
            lstInclude.Items.Clear();

            foreach (string ext in _appsettings.dfasettings.FileExtInclusionList)
            {
                lstInclude.Items.Add(ext);
            }
        }

        private void RefreshDFAExclusionList ()
        {
            lstExclude.Items.Clear ();

            foreach (string ext in _appsettings.dfasettings.FileExtExclusionList)
            {
                lstExclude.Items.Add (ext);
            }
        }

        private void btnIncludeAdd_Click(object sender, EventArgs e)
        {
            string fileext = txtIncludeExt.Text;

            _appsettings.dfasettings.FileExtInclusionList.Add(fileext);
            _appsettings.SaveXMLSettings();
            RefreshDFAInclusionList();
            UpdateFSAWithDFASettings();
        }

        private void btnIncludeRemove_Click(object sender, EventArgs e)
        {
            if (lstInclude.SelectedIndex >= 0)
            {
                _appsettings.dfasettings.FileExtInclusionList.RemoveAt(lstInclude.SelectedIndex);
                RefreshDFAInclusionList();
                _appsettings.SaveXMLSettings();
                UpdateFSAWithDFASettings();
            }
        }

        private void btnExcludeAdd_Click(object sender, EventArgs e)
        {
            string fileext = txtExcludeExt.Text;

            _appsettings.dfasettings.FileExtExclusionList.Add(fileext);
            _appsettings.SaveXMLSettings();
            RefreshDFAExclusionList();
            UpdateFSAWithDFASettings();
        }

        private void btnExcludeRemove_Click(object sender, EventArgs e)
        {
            if (lstExclude.SelectedIndex >= 0)
            {
                _appsettings.dfasettings.FileExtExclusionList.RemoveAt(lstExclude.SelectedIndex);
                RefreshDFAExclusionList();
                _appsettings.SaveXMLSettings();
                UpdateFSAWithDFASettings();
            }
        }

        private void cmbMinimumFileSizes_SelectedIndexChanged(object sender, EventArgs e)
        {
            _appsettings.dfasettings.MinimumFileSizeMB = Int32.Parse(cmbMinimumFileSizes.Text);
            _appsettings.SaveXMLSettings();
            UpdateFSAWithDFASettings();
        }

        private void tmrJobCheck_Tick(object sender, EventArgs e)
        {
            foreach (SingleBackupJob backupjob in _jobmanager.BackupJobs)
            {
                
            }
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            int signatureone = 47577;
            int signaturetwo = 77777;
            int testmessageidentifier = 10001;

            string connecterror;

            if (_comms.Connect(_appsettings.serverlogondetails.ServerName, Int32.Parse(_appsettings.serverlogondetails.ServerPort), out connecterror) == true)
            {
                MessageBox.Show("Connect ok.");

                /*
                MemoryStream ms = new MemoryStream();
                BinaryWriter bw = new BinaryWriter(ms);

                bw.Write(signatureone);
                bw.Write(signaturetwo);
                bw.Write(sizeof(int));
                bw.Write(testmessageidentifier);

                _comms.SendBytes(ms.ToArray());
                */

                if (_comms.DoesServerFileExist_Linux ("ddraper", "mytoken", "$$$ACCOUNTROOT$$$\\data\\dir\\myfile.txt", "graphics") == true) {
                    MessageBox.Show ("Server said file exists!");
                } else {
                    MessageBox.Show ("Server said file not found.");
                }

                DateTime lastwritetime = _comms.GetServerFileLastWriteTime_Linux("ddraper", "mytoken", "$$$ACCOUNTROOT$$$\\clientaccounts.dat", "DataFortressServer");

                MessageBox.Show("LastWritetime was: " + lastwritetime.ToString());

                long filelength = _comms.GetServerFileLength_Linux("ddraper", "mytoken", "$$$ACCOUNTROOT$$$\\clientaccounts.dat", "DataFortressServer");

                MessageBox.Show("Filelength was: " + filelength.ToString());

            }
            else
            {
                MessageBox.Show("Unable to connect to the server, due to the follow reason: " + connecterror, "Unable to connect", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }

    public class ThreadArgs
    {
        public string sourcefile;
        public string destfilename;
    }
}
