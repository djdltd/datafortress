﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.IO;

namespace DataFortress_Client
{
    public class BackupJobManager
    {
        List<SingleBackupJob> _backupjoblist = new List<SingleBackupJob>();

        public List<SingleBackupJob> BackupJobs
        {
            get { return _backupjoblist; }
            set { _backupjoblist = value; }
        }

        public bool DoesBackupJobExist(string jobname)
        {
            foreach (SingleBackupJob job in _backupjoblist)
            {
                if (job.JobName.ToUpper() == jobname.ToUpper())
                {
                    return true;
                }
            }

            return false;
        }

        public bool UpdateBackupJob(SingleBackupJob backupjob)
        {
            for (int i = 0; i < _backupjoblist.Count; i++)
            {
                SingleBackupJob currentjob = _backupjoblist[i];

                if (currentjob.JobName.ToUpper() == backupjob.JobName.ToUpper())
                {
                    _backupjoblist[i] = backupjob;
                    return true;
                }
            }

            return false;
        }

        public bool DeleteBackupJob(string jobname)
        {
            for (int i = 0; i < _backupjoblist.Count; i++)
            {
                SingleBackupJob currentjob = _backupjoblist[i];

                if (currentjob.JobName.ToUpper() == jobname.ToUpper())
                {
                    _backupjoblist.RemoveAt(i);
                    return true;
                }
            }

            return false;
        }

        public SingleBackupJob GetBackupEntry(String entryName)
        {
            foreach (SingleBackupJob job in _backupjoblist)
            {
                if (job.JobName.ToUpper() == entryName.ToUpper())
                {
                    return job;
                }
            }

            return null;
        }



        public void SaveXMLBackupEntries()
        {
            if (_backupjoblist.Count > 0)
            {

                XmlSerializer mySerializer = new XmlSerializer(typeof(List<SingleBackupJob>));
                StreamWriter myWriter = new StreamWriter(CommonFunctions.GetAppSettingsDir() + "BackupEntries.xml");

                mySerializer.Serialize(myWriter, _backupjoblist);
                myWriter.Close();
            }
            else
            {
                if (File.Exists(CommonFunctions.GetAppSettingsDir() + "BackupEntries.xml") == true)
                {
                    File.Delete(CommonFunctions.GetAppSettingsDir() + "BackupEntries.xml");
                }
            }
        }


        public void LoadXMLBackupEntries()
        {
            _backupjoblist.Clear();

            if (File.Exists(CommonFunctions.GetAppSettingsDir() + "BackupEntries.xml") == true)
            {

                XmlSerializer mySerializer = new XmlSerializer(typeof(List<SingleBackupJob>));

                // To read the xml file, create a FileStream.
                FileStream myFileStream = new FileStream(CommonFunctions.GetAppSettingsDir() + "BackupEntries.xml", FileMode.Open);

                // Call the Deserialize method and cast to the object type.
                _backupjoblist = (List<SingleBackupJob>)mySerializer.Deserialize(myFileStream);

                myFileStream.Close();
            }
        }


    }
}
