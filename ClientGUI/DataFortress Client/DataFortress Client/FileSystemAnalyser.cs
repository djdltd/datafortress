﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.IO;
using System.Threading;
using System.Collections;
using CommsPacket;

namespace DataFortress_Client
{
    public class FileSystemAnalyser
    {
        BackgroundWorker _listworker = new BackgroundWorker();
        BackgroundWorker _differentialworker = new BackgroundWorker();

        const int _chunksize = 16384;

        bool _listerror = false;
        bool _usercancelled = false;
        long _numtobeupdated = 0;
        long _numtobecopied = 0;
        long _sizetobecopied = 0;
        long _sizetobeupdated = 0;
        long _sizecompared = 0;
        long _numwarnings = 0;
        long _numerrors = 0;
        long _numcompared = 0;

        int _currentcopyentryindex = 0;
        FileCopyEntry _currentfilecopyentry;

        string _currentfilecopyuseraccount;
        string _currentfilecopyauthtoken;
        string _currentfilecopyjobname;

        private bool _dfaenabled = false;
        private bool _usingdfaexclusions = true;
        private bool _usingdfainclusions = false;
        private int _dfaminimumfilesizemb = 2; // DFA will only be performed on file sizes above this amount. Default is 2MB
        private List<string> _dfafileextinclusions = new List<string>(); // Format of the string in this list will be e.g. .TXT including the dot
        private List<string> _dfafileextexclusions = new List<string>();

        Communications _servercommunications = null;
        bool _commseventset = false;

        public Communications ServerCommunications
        {
            get { return _servercommunications; }
            set { 
                
                _servercommunications = value;

                if (_commseventset == false)
                {
                    _commseventset = true;

                    _servercommunications.OnSendProgressUpdate += new Communications.SendProgressUpdate(_servercommunications_OnSendProgressUpdate);
                    _servercommunications.OnSendCompleted += new Communications.SendCompleted(_servercommunications_OnSendCompleted);
                }
            }
        }

        // Settings for differential file analysis
        public bool DFAEnabled
        {
            get
            {
                return _dfaenabled;
            }
            set
            {
                _dfaenabled = value;
            }
        }

        public bool DFAUseFileExtensionInclusionList
        {
            get
            {
                return _usingdfainclusions;
            }
            set
            {
                _usingdfainclusions = value;
                _usingdfaexclusions = !value;
            }
        }

        public bool DFAUseFileExtensionExclusionList
        {
            get
            {
                return _usingdfaexclusions;
            }
            set
            {
                _usingdfaexclusions = value;
                _usingdfainclusions = !value;
            }
        }

        public int DFAMinimumFileSizeMB
        {
            get
            {
                return _dfaminimumfilesizemb;
            }
            set
            {
                _dfaminimumfilesizemb = value;
            }
        }

        public List<string> DFAFileExtensionInclusionList
        {
            get
            {
                return _dfafileextinclusions;
            }
            set
            {
                _dfafileextinclusions = value;
            }
        }

        public List<string> DFAFileExtensionExclusionList
        {
            get
            {
                return _dfafileextexclusions;
            }
            set
            {
                _dfafileextexclusions = value;
            }
        }

        // The list of files and directories that are CURRENT - built by the list dirs function
        // This list will then be saved over the previous XML backup but only AFTER the backup file has
        // been processed for possible deletions.
        List<FileListDBEntry> _currentfileentries = new List<FileListDBEntry>();

        // The list of Folder exclusions that exist
        ArrayList _Folderexclusions = new ArrayList();

        // The list of extensions to be excluded from syncs or backups
        ArrayList _Exclusions = new ArrayList();

        // The list of files to be copied - used only by the copy file function which processes this list.
        List<FileCopyEntry> _copyentries = new List<FileCopyEntry>();

        public List<FileCopyEntry> CopyEntries
        {
            get { return _copyentries; }
            set { _copyentries = value; }
        }

        // Events that this class will fire, if hooked into.
        public delegate void ListProgressUpdate(ListProgressInformation progressinfo);
        public event ListProgressUpdate OnListProgressUpdate;

        public delegate void ListCompleted();
        public event ListCompleted OnListCompleted;

        public delegate void SendFilesProgressUpdate(SendFilesProgressInformation progressinfo);
        public event SendFilesProgressUpdate OnSendFilesProgressUpdate;

        public delegate void IndividualSendFileProgress(SendWorkerProgress fileprogress);
        public event IndividualSendFileProgress OnIndividualSendFileProgress;

        public delegate void SendFilesCompleted();
        public event SendFilesCompleted OnSendFilesCompleted;

        public delegate void DifferentialProgressUpdate(DifferentialProgressInformation progressinfo);
        public event DifferentialProgressUpdate OnDifferentialProgressUpdate;

        public FileSystemAnalyser()
        {
            _listworker.WorkerSupportsCancellation = true;
            _listworker.WorkerReportsProgress = true;

            _listworker.ProgressChanged += new ProgressChangedEventHandler(_listworker_ProgressChanged);
            _listworker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(_listworker_RunWorkerCompleted);
            _listworker.DoWork += new DoWorkEventHandler(_listworker_DoWork);

            _differentialworker.WorkerSupportsCancellation = true;
            _differentialworker.WorkerReportsProgress = true;

            _differentialworker.ProgressChanged += new ProgressChangedEventHandler(_differentialworker_ProgressChanged);
            _differentialworker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(_differentialworker_RunWorkerCompleted);
            _differentialworker.DoWork += new DoWorkEventHandler(_differentialworker_DoWork);
        }

        private void SendDifferentialProgressNotification(string currentoperation)
        {
            DifferentialProgressInformation progressinfo = new DifferentialProgressInformation();
            progressinfo.currentoperation = currentoperation;
            progressinfo.currentfilepath = _currentfilecopyentry.SourceFile;

            _differentialworker.ReportProgress(0, progressinfo);
        }

        private void NotifyDifferentialProgress(string currentoperation)
        {
            if (OnDifferentialProgressUpdate != null)
            {
                DifferentialProgressInformation progress = new DifferentialProgressInformation();
                progress.currentoperation = currentoperation;
                progress.currentfilepath = _currentfilecopyentry.SourceFile;

                OnDifferentialProgressUpdate(progress);
            }
        }

        void _differentialworker_DoWork(object sender, DoWorkEventArgs e)
        {
            SendDifferentialProgressNotification("DFA: Retrieving server file hash table...");

            List<FileChunkEntry> chunkentries = _servercommunications.RequestFileChunkTable(_currentfilecopyuseraccount, _currentfilecopyauthtoken, _currentfilecopyentry.DestFile, _currentfilecopyentry.JobName);

            SendDifferentialProgressNotification("DFA: Working out byte level changes...");

            int nummismatches = 0;
            List<FileChunkEntry> completedfiletable = MapChunkTableToFileEx(_currentfilecopyentry.SourceFile, chunkentries, out nummismatches);

            if (nummismatches == 0)
            {
                // Files are identical - there were no changes/mismatches
                SendDifferentialProgressNotification("DFA: Files are identical. No byte level changes.");
            }
            else
            {
                // There were changes in the file.
                SendDifferentialProgressNotification("DFA: There were " + nummismatches + " block differences, sending changed bytes...");

                // Time to send the completed file table
                _servercommunications.SendCompletedFileTable(completedfiletable, _currentfilecopyentry.SourceFile, _currentfilecopyentry.DestFile, _currentfilecopyjobname, _currentfilecopyuseraccount, _currentfilecopyauthtoken);

                // File has been sent
                SendDifferentialProgressNotification("DFA: Differential File Sending Completed.");
            }

            if (completedfiletable.Count == 0)
            {
                // No discernable similarities. Files are completely different, need to send the whole thing.
                SendDifferentialProgressNotification("DFA: No similarities. Sending entire file...");
            }
        }

        void _differentialworker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //throw new NotImplementedException();

            NotifyDifferentialProgress("Differential Analysis complete.");

            NextFileSendAsync();
        }

        void _differentialworker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (OnDifferentialProgressUpdate != null)
            {
                DifferentialProgressInformation progressinfo = (DifferentialProgressInformation)e.UserState;
                OnDifferentialProgressUpdate(progressinfo);
            }
        }

        public static long ReadFileChunk(FileStream fileStream, long filepointer, int chunksize, byte[] buffer)
        {
            long workingchunksize = 0;

            if ((fileStream.Length - filepointer) >= chunksize)
            {
                workingchunksize = chunksize;

                fileStream.Seek(filepointer, SeekOrigin.Begin);
                fileStream.Read(buffer, 0, chunksize);

                //ByteArrayToFile("C:\\Temp\\IFC\\BufferFileOffset" + a.ToString() + ".dat", buffer);
            }
            else
            {
                long lastchunksize = fileStream.Length - filepointer;
                workingchunksize = lastchunksize;

                buffer = new byte[lastchunksize];

                fileStream.Seek(filepointer, SeekOrigin.Begin);
                fileStream.Read(buffer, 0, (int)lastchunksize);

                //ByteArrayToFile("C:\\Temp\\IFC\\LastBufferFileOffset" + a.ToString() + ".dat", buffer);
            }

            return workingchunksize;
        }

        private long GetSkipSize(long filelengthremaining)
        {
            // Calculate 30% of the file length remaining.

            long mult = filelengthremaining * 30;
            long skipresult = mult / 100;

            if (skipresult > 2048000)
            {
                skipresult = 2048000; // We cap the skip result at 2MB. We can't be skipping over chunks of greater sizes.
            }

            return skipresult;
        }

        /// <summary>
        /// Calculates a MD5 hash from the given string and uses the given
        /// encoding.
        /// </summary>
        /// <param name="Input">Input string</param>
        /// <param name="UseEncoding">Encoding method</param>
        /// <returns>MD5 computed string</returns>
        public string CalculateMD5(byte[] Input, Encoding UseEncoding)
        {
            //System.Security.Cryptography.MD5CryptoServiceProvider CryptoService;
            //CryptoService = new System.Security.Cryptography.MD5CryptoServiceProvider();

            System.Security.Cryptography.SHA1CryptoServiceProvider CryptoService;
            CryptoService = new System.Security.Cryptography.SHA1CryptoServiceProvider();

            byte[] OutputBytes;
            OutputBytes = CryptoService.ComputeHash(Input);
            return BitConverter.ToString(OutputBytes).Replace("-", "");
        }

        /// <summary>
        /// Calculates a MD5 hash from the given string. 
        /// (By using the default encoding)
        /// </summary>
        /// <param name="Input">Input string</param>
        /// <returns>MD5 computed string</returns>
        public string CalculateMD5(byte[] Input)
        {
            // That's just a shortcut to the base method
            return CalculateMD5(Input, System.Text.Encoding.Default);
        }

        public FileChunkEntry GetChunkEntry(List<FileChunkEntry> chunktable, int possibleindex, string hashtosearch)
        {
            // This function looks up the given hash in the supplied chunktable. But first it checks if the hastosearch matches
            // the possible index given, to save having to run through the entire list.

            // First look up the possible index.
            if (possibleindex >= 0)
            {
                if (possibleindex < chunktable.Count)
                {
                    FileChunkEntry possibleentry = chunktable[possibleindex];

                    if (hashtosearch == possibleentry.primaryhash)
                    {
                        // Great! The possible index given and hash matched immedietely, so return this - no need to do a full search
                        return possibleentry;
                    }
                }
            }

            // If we got here then the possible index was not matched or the possible index was outside the count of chunktable items
            // so we have to do a full search

            foreach (FileChunkEntry entry in chunktable)
            {
                if (entry.primaryhash == hashtosearch)
                {
                    return entry;
                }
            }

            // Now if we got here then we couldn't find the hash, so return null
            return null;
        }

        public List<FileChunkEntry> MapChunkTableToFileEx(string filetomap, List<FileChunkEntry> sourcechunktable, out int numfilemismatches)
        {
            // This method always operates on the modified file. The source chunk table must come from the
            // unmodified file.

            byte[] buffer;
            byte[] smallchunkbuffer;

            FileStream fileStream = new FileStream(filetomap, FileMode.Open, FileAccess.Read);

            int chunksize = _chunksize;
            int smallchunksize = 8192;
            long filepointer = 0;
            long possibleindex = 0;
            bool incrementalmode = false;
            long mismatchfilepointer = 0;
            long diffsize = 0;
            int skipcount = 0;
            int nummismatches = 0;
            long percentagecomplete = 0;
            long currentpercentage = 0;

            List<FileChunkEntry> completedfiletable = new List<FileChunkEntry>();

            long workingchunksize = 0;
            buffer = new byte[chunksize];
            smallchunkbuffer = new byte[smallchunksize];

            while (filepointer < fileStream.Length)
            {
                if (incrementalmode == false) // Normal mode operating on 16K chunks
                {
                    // Grab a chunk of data from the modified file.
                    if ((fileStream.Length - filepointer) >= chunksize)
                    {
                        workingchunksize = chunksize;

                        fileStream.Seek(filepointer, SeekOrigin.Begin);
                        fileStream.Read(buffer, 0, chunksize);
                    }
                    else
                    {
                        long lastchunksize = fileStream.Length - filepointer;
                        workingchunksize = lastchunksize;

                        buffer = new byte[lastchunksize];

                        fileStream.Seek(filepointer, SeekOrigin.Begin);
                        fileStream.Read(buffer, 0, (int)lastchunksize);
                    }


                    //Console.WriteLine("WORKINGCHUNKSIZE: " + workingchunksize);

                    string currenthash = CalculateMD5(buffer);

                    // The possible index just aids in finding chunk entries in the source table, basically
                    // it means we don't have to search the whole table from start to finish, as we kinda know where to look
                    if ((filepointer % chunksize) == 0) // This means the file pointer is easily divisible by the chunk size and we can obtain a possible index
                    {
                        possibleindex = filepointer / chunksize;
                        //Console.WriteLine("Possible IDX: " + possibleindex.ToString());
                    }
                    else
                    {
                        possibleindex = -1;
                    }

                    // See if the buffer (hash) we've just grabbed from our modified file exists in the source table
                    // Basically do we have this chunk of file in the source file.
                    FileChunkEntry chunkentry = GetChunkEntry(sourcechunktable, (int)possibleindex, currenthash);

                    if (chunkentry != null)
                    {
                        // Found it
                        //Console.WriteLine("CHUNK ENTRY MATCH! OFF: " + filepointer.ToString() + " SOURCE IDX: " + chunkentry.index.ToString() + " SOURCE OFF: " + chunkentry.offset.ToString());

                        // Add this meta chunk to the completed file table as it's the same as the source
                        FileChunkEntry completedentry = new FileChunkEntry();
                        completedentry.offset = filepointer;
                        completedentry.length = chunkentry.length;
                        completedentry.primaryhash = chunkentry.primaryhash;
                        completedentry.index = chunkentry.index;
                        completedentry.dataincluded = false;

                        completedfiletable.Add(completedentry);

                        filepointer += chunksize;
                    }
                    else
                    {
                        // Did not find it, this means the file we are scanning has come across a mismatch on the source file, this means
                        // our file has changed and at this point no longer matches the source file at the same place, so we need to do incremental
                        // file mapping by advancing the pointer one byte at a time instead of a whole chunk at a time.
                        //Console.WriteLine("CHUNK ENTRY MISMATCH!");
                        mismatchfilepointer = filepointer;
                        diffsize = 0;
                        skipcount = 0;
                        nummismatches++;

                        //filepointer++;
                        incrementalmode = true;
                    }

                    //Console.WriteLine("NON INCREMENTAL HASH: " + currenthash + " OFF: " + filepointer.ToString() + " LEN: " + fileStream.Length.ToString());
                }


                if (incrementalmode == true) // Incremental mode searching for the next match in the source file to see where the file lines up again
                {

                    if ((fileStream.Length - filepointer) >= chunksize)
                    {
                        workingchunksize = chunksize;

                        fileStream.Seek(filepointer, SeekOrigin.Begin);
                        fileStream.Read(buffer, 0, chunksize);
                    }
                    else
                    {
                        long lastchunksize = fileStream.Length - filepointer;
                        workingchunksize = lastchunksize;

                        buffer = new byte[lastchunksize];

                        fileStream.Seek(filepointer, SeekOrigin.Begin);
                        fileStream.Read(buffer, 0, (int)lastchunksize);
                    }

                    string currenthash = CalculateMD5(buffer);

                    FileChunkEntry chunkentry = GetChunkEntry(sourcechunktable, -1, currenthash);

                    if (chunkentry != null)
                    {
                        // Found it
                        long changeddatalength = filepointer - mismatchfilepointer;

                        byte[] changeddata = new byte[changeddatalength];

                        ReadFileChunk(fileStream, mismatchfilepointer, (int)changeddatalength, changeddata);

                        FileChunkEntry completedentry = new FileChunkEntry();
                        completedentry.offset = mismatchfilepointer;
                        completedentry.length = (int)changeddatalength;
                        completedentry.primaryhash = null;
                        completedentry.index = -1;
                        completedentry.dataincluded = true;
                        completedentry.data = new byte[changeddatalength];
                        completedentry.data = changeddata;

                        completedfiletable.Add(completedentry);

                        //Console.WriteLine("SEARCH MATCH FOUND! OFF: " + filepointer.ToString() + " SOURCE IDX: " + chunkentry.index.ToString() + " SOURCE OFF: " + chunkentry.offset.ToString());
                        incrementalmode = false;
                        skipcount = 0;
                    }
                    else
                    {
                        //Console.WriteLine("SEARCH MODE. INCREMENTAL HASH: " + currenthash + " FILE OFFSET: " + filepointer.ToString() + " DIFF SIZE: " + (filepointer - mismatchfilepointer).ToString() + " ACTUAL DIFF: " + diffsize.ToString());
                        diffsize++;

                        if (diffsize > _chunksize)
                        {
                            diffsize = 0;
                            skipcount++;

                            long lengthremaining = fileStream.Length - filepointer;

                            long skipsize = GetSkipSize(lengthremaining);

                            if ((filepointer * skipcount) < lengthremaining)
                            {
                                filepointer += (skipsize * skipcount); // When we skip over chunks of file we start small then if there are still
                                // differences then we skip over larger chunks. There are pros and cons to doing this. The advantage is that we'll
                                // be done checking the file quite quickly. But the disadvantage is that the differencing precision will not be
                                // as good as it could be if we didn't skip. This basically means we'll be sending more data over to complete
                                // the destination file than we really have to which results in more data being sent. But hey, it's my call
                                // and I've called it - sacrifice differencing precision for speed, we can't have the differencing taking longer
                                // than it would actually take to send the file now can we!
                            }
                            else
                            {
                                filepointer += skipsize; // Skip over a chunk of the file otherwise we will be incrementally searching for ages.
                            }
                        }

                        filepointer++;
                    }
                }

                currentpercentage = (filepointer * 100) / fileStream.Length;

                if (currentpercentage != percentagecomplete)
                {
                    // Trigger the percentage complete.
                    SendDifferentialProgressNotification("Working out file byte changes..." + percentagecomplete.ToString () + "%");
                }

                percentagecomplete = currentpercentage;
            }

            fileStream.Close();
            numfilemismatches = nummismatches;

            return completedfiletable;
        }

        void _listworker_DoWork(object sender, DoWorkEventArgs e)
        {
            ListWorkerParams listparams = (ListWorkerParams)e.Argument;

            if (listparams.UseServerCommunications == true)
            {
                ListDirsUsingServer(listparams.Source, listparams.Source, listparams.ServerAccount, listparams.UserToken, _listworker, listparams.LowPriority, listparams.JobName);
            }
            else
            {
                ListDirs(listparams.Source, listparams.Source, listparams.Destination, _listworker, listparams.LowPriority);
            }          
        }

        void _listworker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (OnListCompleted != null)
            {
                OnListCompleted();
            }
        }

        void ResetListDirCounters()
        {
            _copyentries = new List<FileCopyEntry>();
            _currentfileentries = new List<FileListDBEntry>();
            _listerror = false;
            _usercancelled = false;
            _numtobeupdated = 0;
            _numtobecopied = 0;
            _sizetobecopied = 0;
            _sizetobeupdated = 0;
            _sizecompared = 0;
            _numwarnings = 0;
            _numerrors = 0;
            _numcompared = 0;
        }

        void _listworker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //throw new NotImplementedException();

            ListProgressInformation progressinfo = new ListProgressInformation();

            progressinfo.NumFilesCompared = _numcompared.ToString();
            progressinfo.NumFilesToBeCopied = _numtobecopied.ToString();
            progressinfo.NumFilesToBeUpdated = _numtobeupdated.ToString();
            //progressinfo.NumFilesToBeDeleted = _numtobedeleted.ToString();
            //progressinfo.NumFilesActuallyDeleted = _numdeleteactioned.ToString();
            progressinfo.NumErrorsOccured = _numerrors.ToString();
            progressinfo.NumWarningsOccured = _numwarnings.ToString();

            progressinfo.SizeCompared = FormatFileSize(_sizecompared);
            progressinfo.SizeToBeUpdated = FormatFileSize(_sizetobeupdated);
            progressinfo.SizeToBeCopied = FormatFileSize(_sizetobecopied);

            if (e.UserState.GetType() == typeof(StandardProgress))
            {
                StandardProgress stdprogress = (StandardProgress)e.UserState;

                if (stdprogress.useHeading == true)
                {
                    progressinfo.UsingHeading = true;
                    progressinfo.ProgressHeading = stdprogress.Heading;
                }

                if (stdprogress.useSubheading == true)
                {
                    progressinfo.UsingSubHeading = true;
                    progressinfo.ProgressSubHeading = stdprogress.Subheading;
                }

                if (stdprogress.useListoutput == true)
                {
                    progressinfo.UsingListOutput = true;
                    progressinfo.ProgressListOutput = stdprogress.ListOutput;
                }
            }

            // Fire the event if it has been hooked up to.
            if (OnListProgressUpdate != null)
            {
                OnListProgressUpdate(progressinfo);
            }
        }


        void ListDirsUsingServer(String strSource, String strSourceRoot, String strServerAccount, String strUsertoken, BackgroundWorker bw, bool lowpriority, string jobname)
        {
            if (_servercommunications == null)
            {
                throw new Exception("Cannot List Directories using a server connection without first setting the ServerCommunications property to an communications class.");
            }

            String strHeader = "Checking for added and updated files...";

            if (bw.CancellationPending == true)
            {
                return;
            }

            if (_listerror == true)
            {
                if (Directory.Exists(strSourceRoot) == false)
                {
                    _usercancelled = true;
                    bw.ReportProgress(0, new StandardProgress(false, "", false, "", true, " *** BEGIN LIST ERROR - SOURCE NO LONGER AVAILABLE ***  " + strSourceRoot));
                    return;
                }
            }

            try
            {
                string[] dirs;
                string[] files;

                dirs = Directory.GetDirectories(strSource);
                files = Directory.GetFiles(strSource);
                FileInfo finfo;

                _currentfileentries.Add(new FileListDBEntry(strSource, true));

                for (int f = 0; f < files.Length; f++)
                {
                    //Thread.Sleep(1);
                    //Thread.Sleep(5);
                    if (lowpriority == true)
                    {
                        Thread.Sleep(100);
                    }

                    if (bw.CancellationPending == true)
                    {
                        _usercancelled = true;
                        return;
                    }

                    if (_listerror == false)
                    {
                        String strSourcepath = (String)files[f];

                        if (IsFileExcluded(strSourcepath) == false)
                        {
                            _currentfileentries.Add(new FileListDBEntry(strSourcepath, false, File.GetLastWriteTime(strSourcepath)));

                            String strDestpath = strSourcepath.Replace(strSourceRoot, "$$$ACCOUNTROOT$$$");

                            FileCompareResults CompareResults = CompareServerFile(strServerAccount, strUsertoken, strSourcepath, strDestpath, jobname);

                            bw.ReportProgress(0, new StandardProgress(true, strHeader, true, FormatPath(strSourcepath, 100)));

                            if (CompareResults.bToBeUpdated == true) // This is a newer/updated file
                            {
                                _numtobeupdated++;
                                finfo = new FileInfo(strSourcepath);
                                _sizetobeupdated += finfo.Length;

                                bw.ReportProgress(0, new StandardProgress(false, "", false, "", true, " *** UPDATED FILE ***  " + strSourcepath));
                            }

                            if (CompareResults.bToBeCopied == true) // This is an added file
                            {
                                _numtobecopied++;
                                finfo = new FileInfo(strSourcepath);
                                _sizetobecopied += finfo.Length;
                                bw.ReportProgress(0, new StandardProgress(false, "", false, "", true, " *** NEW FILE ***  " + strSourcepath));
                            }

                            if (CompareResults.bTimeSpanFiltered == true) // This file was updated but filtered because it was not new enough.
                            {
                                //bw.ReportProgress(0, new StandardProgress(false, "", false, "", true, " *** UPDATED BUT NOT NEW ENOUGH ***  Diff: " + CompareResults.bTimeSpanAmount.ToString () + " - " + strSourcepath));
                            }

                            if (CompareResults.FileSizeInconsistent == true) // The files have the same modified date, but the sizes don't match - flag it
                            {
                                _numwarnings++;
                                bw.ReportProgress(0, new StandardProgress(false, "", false, "", true, " *** LAST MODIFIED DATE IS IDENTICAL BUT FILE SIZES DON'T MATCH ***  Source Len: " + CompareResults.SourceSize.ToString() + ", Dest Len: " + CompareResults.DestSize + " - " + strSourcepath));
                            }
                        }
                    }
                    else
                    {
                        if (Directory.Exists(strSourceRoot) == false)
                        {
                            _usercancelled = true;
                            bw.ReportProgress(0, new StandardProgress(false, "", false, "", true, " *** FILE LISTING ERROR - SOURCE NO LONGER AVAILABLE ***  " + strSourceRoot));
                            return;
                        }
                    }
                }

                for (int d = 0; d < dirs.Length; d++)
                {
                    if (_listerror == true)
                    {
                        if (Directory.Exists(strSourceRoot) == false)
                        {
                            _usercancelled = true;
                            bw.ReportProgress(0, new StandardProgress(false, "", false, "", true, " *** DIR LIST ERROR - SOURCE NO LONGER AVAILABLE ***  " + strSourceRoot));
                            return;
                        }

                    }

                    if (IsFolderExcluded(dirs[d]) == false)
                    {
                        //MessageBox.Show(dirs[d].Replace(strSourceRoot, strDest));
                        //MessageBox.Show(dirs[d].Replace(strDest, strSourceRoot));
                        ListDirsUsingServer(dirs[d], strSourceRoot, strServerAccount, strUsertoken, bw, lowpriority, jobname);
                    }
                }
            }
            catch (Exception ex)
            {
                //bw.ReportProgress(0, new FileCompareProgress(ex.Message, new FileCompareResults()));
                _listerror = true;
                bw.ReportProgress(0, new StandardProgress(false, "", true, ex.Message, true, " *** LIST ERROR ***  " + ex.Message));
                _numerrors++;

            }
        }

        void _servercommunications_OnSendCompleted()
        {       
            NextFileSendAsync();
        }

        void _servercommunications_OnSendProgressUpdate(SendWorkerProgress progressinfo)
        {
            if (OnIndividualSendFileProgress != null)
            {
                OnIndividualSendFileProgress(progressinfo);
            }
        }

        private void PerformDifferentialAnalysis()
        {
            _differentialworker.RunWorkerAsync();

        }

        private bool IsFileExtensionIncluded()
        {
            // Grab the file extension of the current file
            string fileext = Path.GetExtension(_currentfilecopyentry.SourceFile);

            // Now check if this extension is in the inclusion list
            foreach (string item in _dfafileextinclusions)
            {
                if (item.ToUpper() == fileext.ToUpper())
                {
                    return true;
                }
            }

            return false;
        }

        private bool IsFileExtensionExcluded()
        {
            // Grab the file extension of the current file
            string fileext = Path.GetExtension(_currentfilecopyentry.SourceFile);

            // Now check if this extension is in the exclusion list
            foreach (string item in _dfafileextexclusions)
            {
                if (item.ToUpper() == fileext.ToUpper())
                {
                    return true;
                }
            }

            return false;
        }

        private bool ShouldPerformDFA()
        {
            // DFA is differential file analysis.
            // This method basically checks if it is appropriate to perform differential file analysis.

            // Firstly, is the current file copy entry an updated file, or a new file, DFA can only be performed on updated files
            // as the server needs a copy at it's side.
            if (_dfaenabled == false)
            {
                return false;
            }


            if (_currentfilecopyentry.UpdatedFile == true)
            {
                // First convert the number of megabytes in the minimum file size variable to bytes
                long minimumfilesizebytes = _dfaminimumfilesizemb * 1024 * 1024;

                if (_currentfilecopyentry.FileSize >= minimumfilesizebytes)
                {
                    // Now check the exclusion / inclusion lists.
                    if (_usingdfainclusions)
                    {
                        if (IsFileExtensionIncluded() == true)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }

                    if (_usingdfaexclusions)
                    {
                        if (IsFileExtensionExcluded() == true)
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }

                    // If not using anything then return false
                    return false;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public void SendFilesAsync(string useraccount, string authtoken)
        {
            if (_copyentries.Count == 0)
            {
                throw new Exception("There are no file copy entries listed to send to the server.");
            }

            if (_servercommunications == null)
            {
                throw new Exception("The Server Communications property has not been set. Please set this property.");
            }

            _currentcopyentryindex = 0;
            _currentfilecopyentry = _copyentries[_currentcopyentryindex];
            _currentfilecopyuseraccount = useraccount;
            _currentfilecopyauthtoken = authtoken;
            _currentfilecopyjobname = _currentfilecopyentry.JobName;


            if (OnSendFilesProgressUpdate != null)
            {
                SendFilesProgressInformation progressinfo = new SendFilesProgressInformation();
                progressinfo.currentfilepath = _currentfilecopyentry.SourceFile;
                progressinfo.numfilessent = _currentcopyentryindex;
                progressinfo.totalfiles = _copyentries.Count;
                progressinfo.percentprogress = (_currentcopyentryindex * 100) / (int)_copyentries.Count;

                OnSendFilesProgressUpdate(progressinfo);
            }


            if (ShouldPerformDFA () == true)
            {
                PerformDifferentialAnalysis();
            }
            else
            {
                _servercommunications.SendFileAsync(_currentfilecopyentry.SourceFile, _currentfilecopyentry.DestFile, useraccount, authtoken, _currentfilecopyentry.JobName);
            }
            
        }

        private void NextFileSendAsync()
        {
            if (_currentcopyentryindex < _copyentries.Count-1)
            {
                _currentcopyentryindex++;
                _currentfilecopyentry = _copyentries[_currentcopyentryindex];

                if (OnSendFilesProgressUpdate != null)
                {
                    SendFilesProgressInformation progressinfo = new SendFilesProgressInformation();
                    progressinfo.currentfilepath = _currentfilecopyentry.SourceFile;
                    progressinfo.numfilessent = _currentcopyentryindex;
                    progressinfo.totalfiles = _copyentries.Count;
                    progressinfo.percentprogress = (_currentcopyentryindex * 100) / (int)_copyentries.Count;

                    OnSendFilesProgressUpdate(progressinfo);

                    if (ShouldPerformDFA() == true)
                    {
                        PerformDifferentialAnalysis();
                    }
                    else
                    {
                        _servercommunications.SendFileAsync(_currentfilecopyentry.SourceFile, _currentfilecopyentry.DestFile, _currentfilecopyuseraccount, _currentfilecopyauthtoken, _currentfilecopyentry.JobName);
                    }

                    
                }
            }
            else
            {
                if (OnSendFilesCompleted != null)
                {
                    OnSendFilesCompleted();
                }
            }
        }

        void ListDirs(String strSource, String strSourceRoot, String strDest, BackgroundWorker bw, bool lowpriority)
        {
            String strHeader = "Checking for added and updated files...";

            if (bw.CancellationPending == true)
            {
                return;
            }

            if (_listerror == true)
            {
                if (Directory.Exists(strSourceRoot) == false)
                {
                    _usercancelled = true;
                    bw.ReportProgress(0, new StandardProgress(false, "", false, "", true, " *** BEGIN LIST ERROR - SOURCE NO LONGER AVAILABLE ***  " + strSourceRoot));
                    return;
                }

                if (Directory.Exists(strDest) == false)
                {
                    _usercancelled = true;
                    bw.ReportProgress(0, new StandardProgress(false, "", false, "", true, " *** BEGIN LIST ERROR - DESTINATION NO LONGER AVAILABLE ***  " + strDest));
                    return;
                }
            }

            try
            {
                string[] dirs;
                string[] files;

                dirs = Directory.GetDirectories(strSource);
                files = Directory.GetFiles(strSource);
                FileInfo finfo;

                _currentfileentries.Add(new FileListDBEntry(strSource, true));

                for (int f = 0; f < files.Length; f++)
                {
                    //Thread.Sleep(1);
                    //Thread.Sleep(5);
                    if (lowpriority == true)
                    {
                        Thread.Sleep(100);
                    }

                    if (bw.CancellationPending == true)
                    {
                        _usercancelled = true;
                        return;
                    }

                    if (_listerror == false)
                    {
                        String strSourcepath = (String)files[f];

                        if (IsFileExcluded(strSourcepath) == false)
                        {
                            _currentfileentries.Add(new FileListDBEntry(strSourcepath, false, File.GetLastWriteTime(strSourcepath)));

                            String strDestpath = strSourcepath.Replace(strSourceRoot, strDest);

                            FileCompareResults CompareResults = CompareFile(strSourcepath, strDestpath);

                            bw.ReportProgress(0, new StandardProgress(true, strHeader, true, FormatPath(strSourcepath, 100)));

                            if (CompareResults.bToBeUpdated == true) // This is a newer/updated file
                            {
                                _numtobeupdated++;
                                finfo = new FileInfo(strSourcepath);
                                _sizetobeupdated += finfo.Length;

                                bw.ReportProgress(0, new StandardProgress(false, "", false, "", true, " *** UPDATED FILE ***  " + strSourcepath));
                            }

                            if (CompareResults.bToBeCopied == true) // This is an added file
                            {
                                _numtobecopied++;
                                finfo = new FileInfo(strSourcepath);
                                _sizetobecopied += finfo.Length;
                                bw.ReportProgress(0, new StandardProgress(false, "", false, "", true, " *** NEW FILE ***  " + strSourcepath));
                            }

                            if (CompareResults.bTimeSpanFiltered == true) // This file was updated but filtered because it was not new enough.
                            {
                                //bw.ReportProgress(0, new StandardProgress(false, "", false, "", true, " *** UPDATED BUT NOT NEW ENOUGH ***  Diff: " + CompareResults.bTimeSpanAmount.ToString () + " - " + strSourcepath));
                            }

                            if (CompareResults.FileSizeInconsistent == true) // The files have the same modified date, but the sizes don't match - flag it
                            {
                                _numwarnings++;
                                bw.ReportProgress(0, new StandardProgress(false, "", false, "", true, " *** LAST MODIFIED DATE IS IDENTICAL BUT FILE SIZES DON'T MATCH ***  Source Len: " + CompareResults.SourceSize.ToString() + ", Dest Len: " + CompareResults.DestSize + " - " + strSourcepath));
                            }
                        }
                    }
                    else
                    {
                        if (Directory.Exists(strSourceRoot) == false)
                        {
                            _usercancelled = true;
                            bw.ReportProgress(0, new StandardProgress(false, "", false, "", true, " *** FILE LISTING ERROR - SOURCE NO LONGER AVAILABLE ***  " + strSourceRoot));
                            return;
                        }

                        if (Directory.Exists(strDest) == false)
                        {
                            _usercancelled = true;
                            bw.ReportProgress(0, new StandardProgress(false, "", false, "", true, " *** FILE LISTING ERROR - DESTINATION NO LONGER AVAILABLE ***  " + strDest));
                            return;
                        }
                    }
                }

                for (int d = 0; d < dirs.Length; d++)
                {
                    if (_listerror == true)
                    {
                        if (Directory.Exists(strSourceRoot) == false)
                        {
                            _usercancelled = true;
                            bw.ReportProgress(0, new StandardProgress(false, "", false, "", true, " *** DIR LIST ERROR - SOURCE NO LONGER AVAILABLE ***  " + strSourceRoot));
                            return;
                        }

                        if (Directory.Exists(strDest) == false)
                        {
                            _usercancelled = true;
                            bw.ReportProgress(0, new StandardProgress(false, "", false, "", true, " *** DIR LIST ERROR - DESTINATION NO LONGER AVAILABLE ***  " + strDest));
                            return;
                        }
                    }

                    if (IsFolderExcluded(dirs[d]) == false && IsFolderExcluded(dirs[d].Replace(strSourceRoot, strDest)) == false && IsFolderExcluded(dirs[d].Replace(strDest, strSourceRoot)) == false)
                    {
                        //MessageBox.Show(dirs[d].Replace(strSourceRoot, strDest));
                        //MessageBox.Show(dirs[d].Replace(strDest, strSourceRoot));
                        ListDirs(dirs[d], strSourceRoot, strDest, bw, lowpriority);
                    }
                }
            }
            catch (Exception ex)
            {
                //bw.ReportProgress(0, new FileCompareProgress(ex.Message, new FileCompareResults()));
                _listerror = true;
                bw.ReportProgress(0, new StandardProgress(false, "", true, ex.Message, true, " *** LIST ERROR ***  " + ex.Message));
                _numerrors++;

            }
        }

        private String FormatFileSize(long filesize)
        {
            if (filesize > 0 && filesize <= 1000) return Convert.ToString(filesize) + "b";
            if (filesize > 1000 && filesize <= 1000000) return Convert.ToString(filesize / 1000) + "Kb";
            if (filesize > 1000000 && filesize <= 1000000000) return Convert.ToString(filesize / 1000000) + "MB";
            if (filesize > 1000000000) return Convert.ToString(filesize / 1000000000) + "GB";

            return Convert.ToString(filesize / 1000000) + "MB";
        }

        public void ListFilesNeedingCopy(string strSource, string strDest, bool lowpriority)
        {
            ResetListDirCounters();

            ListWorkerParams listparams = new ListWorkerParams();
            listparams.Source = strSource;
            listparams.Destination = strDest;
            listparams.LowPriority = lowpriority;
            listparams.UseServerCommunications = false;

            _listworker.RunWorkerAsync(listparams);

        }


        public void ListFilesNeedingCopyUsingServer(string strSource, bool lowpriority, string serveraccount, string usertoken, bool reset, string jobname)
        {
            if (reset == true)
            {
                ResetListDirCounters();
            }

            ListWorkerParams listparams = new ListWorkerParams();
            listparams.Source = strSource;
            listparams.UseServerCommunications = true;
            listparams.ServerAccount = serveraccount;
            listparams.UserToken = usertoken;
            listparams.LowPriority = lowpriority;
            listparams.JobName = jobname;


            _listworker.RunWorkerAsync(listparams);

        }


        FileCompareResults CompareFile(String strSource, String strDest)
        {
            // Remember, strDest is a theoretical destination as it has just been constructed given the 2
            // paths to compare. strSource is the real dir. Also this is a push update function only
            // Which means updates are pushed from Source to dest, but not the other way, it is up to 
            // danhead to call the list function in the reverse direction.

            FileInfo finfo;
            FileCompareResults res = new FileCompareResults();

            if (File.Exists(strDest) == false)
            {
                // File doesn't exist, so it needs to be copied.
                res.bToBeCopied = true;

                FileInfo fi1 = new FileInfo(strSource);

                _copyentries.Add(new FileCopyEntry(strSource, strDest, "File does not exist at destination", "Default", false, fi1.Length));
                

            }
            else
            {
                DateTime dtfs = File.GetLastWriteTime(strSource);
                DateTime dtSource = new DateTime(dtfs.Year, dtfs.Month, dtfs.Day, dtfs.Hour, dtfs.Minute, dtfs.Second);

                DateTime dtfd = File.GetLastWriteTime(strDest);
                DateTime dtDest = new DateTime(dtfd.Year, dtfd.Month, dtfd.Day, dtfd.Hour, dtfd.Minute, dtfd.Second);

                FileInfo fi1 = new FileInfo(strSource);
                FileInfo fi2 = new FileInfo(strDest);


                if (dtSource > dtDest)
                {

                    TimeSpan ts = new TimeSpan();
                    ts = dtSource - dtDest;

                    if (ts.TotalSeconds > 5)
                    {
                        res.bToBeUpdated = true;
                        
                        _copyentries.Add(new FileCopyEntry(strSource, strDest, "Source file is newer", "Default", true, fi1.Length));
                        
                    }
                    else
                    {
                        res.bTimeSpanFiltered = true;
                        res.bTimeSpanAmount = (int)ts.TotalSeconds;

                        if (fi1.Length != fi2.Length)
                        {
                            res.FileSizeInconsistent = true;
                            res.SourceSize = fi1.Length;
                            res.DestSize = fi2.Length;
                        }
                    }
                }
                else
                {
                    if (dtSource == dtDest)
                    {
                        if (fi1.Length != fi2.Length)
                        {
                            res.FileSizeInconsistent = true;
                            res.SourceSize = fi1.Length;
                            res.DestSize = fi2.Length;
                        }
                    }
                }
            }

            _numcompared++;

            finfo = new FileInfo(strSource);
            _sizecompared += finfo.Length;

            return res;
        }

        FileCompareResults CompareServerFile(String straccountname, String usertoken, String strSource, String strDest, String jobName)
        {
            // Remember, strDest is a theoretical destination as it has just been constructed given the 2
            // paths to compare. strSource is the real dir. Also this is a push update function only
            // Which means updates are pushed from Source to dest, but not the other way, it is up to 
            // danhead to call the list function in the reverse direction.

            FileInfo finfo;
            FileCompareResults res = new FileCompareResults();

            if (_servercommunications.DoesServerFileExist_Linux (straccountname, usertoken, strDest, jobName) == false)
            //if (File.Exists(strDest) == false)
            {
                // File doesn't exist, so it needs to be copied.
                res.bToBeCopied = true;

                FileInfo fi1 = new FileInfo(strSource);
                

                _copyentries.Add(new FileCopyEntry(strSource, strDest, "File does not exist at server destination", jobName, false, fi1.Length));

            }
            else
            {
                DateTime dtfs = File.GetLastWriteTime(strSource);
                DateTime dtSource = new DateTime(dtfs.Year, dtfs.Month, dtfs.Day, dtfs.Hour, dtfs.Minute, dtfs.Second);
                
                DateTime dtfd = _servercommunications.GetServerFileLastWriteTime_Linux(straccountname, usertoken, strDest, jobName); 
                DateTime dtDest = new DateTime(dtfd.Year, dtfd.Month, dtfd.Day, dtfd.Hour, dtfd.Minute, dtfd.Second);

                FileInfo fi1 = new FileInfo(strSource);
                long serverfilelength = _servercommunications.GetServerFileLength_Linux(straccountname, usertoken, strDest, jobName);

                if (dtSource > dtDest)
                {

                    TimeSpan ts = new TimeSpan();
                    ts = dtSource - dtDest;

                    if (ts.TotalSeconds > 5)
                    {
                        res.bToBeUpdated = true;

                        _copyentries.Add(new FileCopyEntry(strSource, strDest, "Source file is newer", jobName, true, fi1.Length));

                    }
                    else
                    {
                        res.bTimeSpanFiltered = true;
                        res.bTimeSpanAmount = (int)ts.TotalSeconds;

                        if (fi1.Length != serverfilelength)
                        {
                            res.FileSizeInconsistent = true;
                            res.SourceSize = fi1.Length;
                            res.DestSize = serverfilelength;
                        }
                    }
                }
                else
                {
                    if (dtSource == dtDest)
                    {
                        if (fi1.Length != serverfilelength)
                        {
                            res.FileSizeInconsistent = true;
                            res.SourceSize = fi1.Length;
                            res.DestSize = serverfilelength;
                        }
                    }
                }
            }

            _numcompared++;

            finfo = new FileInfo(strSource);
            _sizecompared += finfo.Length;

            return res;
        }

        private bool IsFolderExcluded(String strFolderpath)
        {
            for (int a = 0; a < _Folderexclusions.Count; a++)
            {
                if ((String)_Folderexclusions[a].ToString().ToUpper() == strFolderpath.ToUpper())
                {
                    return true;
                }
            }
            return false;
        }

        private bool IsFileExcluded(String strFilepath)
        {
            int a = 0;
            String curExc = "";

            if (Path.GetExtension(strFilepath.ToUpper()).Contains("SBAK") == true)
            {
                return true;
            }

            for (a = 0; a < _Exclusions.Count; a++)
            {
                curExc = (String)_Exclusions[a];

                if (strFilepath.ToUpper().Contains(curExc.ToUpper()) == true)
                {
                    return true;
                }
            }

            return false;
        }

        private String FormatPath(String inPath, int MaxLength)
        {
            if (inPath.Length > MaxLength)
            {
                String strFormatted = inPath.Substring(0, 15) + "....." + inPath.Substring(inPath.Length - (MaxLength - 20));
                return strFormatted;
            }
            else
            {
                return inPath;
            }
        }
    }

    public class ListProgressInformation
    {
        public string NumFilesCompared;
        public string NumFilesToBeCopied;
        public string NumFilesToBeUpdated;
        public string NumFilesToBeDeleted;
        public string NumFilesActuallyDeleted;
        public string NumErrorsOccured;
        public string NumWarningsOccured;

        public string SizeCompared;
        public string SizeToBeUpdated;
        public string SizeToBeCopied;
        public string SizeToBeDeleted;

        public string SizeActuallyDeleted;

        public string ProgressHeading;
        public string ProgressSubHeading;
        public string ProgressListOutput;

        public bool UsingHeading;
        public bool UsingSubHeading;
        public bool UsingListOutput;
    }

    public class SendFilesProgressInformation
    {
        public string currentfilepath;
        public int numfilessent;
        public int totalfiles;
        public int percentprogress;
    }

    public class DifferentialProgressInformation
    {
        public string currentoperation;
        public string currentfilepath;
        public long filelength;
    }

    public class ListWorkerParams
    {
        public String Source;
        public String Destination;
        public bool LowPriority;
        public bool UseServerCommunications;
        public String ServerAccount;
        public String UserToken;
        public String JobName;
    }

    public class FileCopyEntry
    {
        public String SourceFile;
        public String DestFile;
        public String CopyReason;
        public String JobName;
        public bool UpdatedFile;
        public long FileSize;

        public FileCopyEntry(String Src, String Dest, String Reason, String jobname, bool updated, long filesize)
        {
            SourceFile = Src;
            DestFile = Dest;
            JobName = jobname;
            CopyReason = Reason;
            UpdatedFile = updated;
            FileSize = filesize;
        }

        public FileCopyEntry()
        {
            SourceFile = "";
            DestFile = "";
            CopyReason = "";
            JobName = "";
            UpdatedFile = false;
            FileSize = 0;
        }
    }

    public class StandardProgress
    {
        public String Heading;
        public String Subheading;
        public String ListOutput;

        public bool useHeading;
        public bool useSubheading;
        public bool useListoutput;

        public StandardProgress()
        {

        }

        public StandardProgress(bool useheading, String heading)
        {
            useHeading = useheading;
            Heading = heading;
        }

        public StandardProgress(bool useheading, String heading, bool usesubheading, String subheading)
        {
            useHeading = useheading;
            Heading = heading;
            useSubheading = usesubheading;
            Subheading = subheading;
        }

        public StandardProgress(bool useheading, String heading, bool usesubheading, String subheading, bool uselistoutput, String listoutput)
        {
            useHeading = useheading;
            Heading = heading;
            useSubheading = usesubheading;
            Subheading = subheading;
            useListoutput = uselistoutput;
            ListOutput = listoutput;
        }
    }

    public class FileListDBEntry
    {
        // This is the object that is stored in the XML file
        // for checking deleted files and directories, and for conflicting files (files that have been modified on both sides).
        // All we need is the file path, the type, and the modified date

        public String FilePath;
        public bool IsDir;
        public DateTime ModifiedDate;

        public FileListDBEntry()
        {
            FilePath = "";
            IsDir = false;
            ModifiedDate = new DateTime();
        }

        public FileListDBEntry(String pFilePath, bool pIsDir, DateTime pModifiedDate)
        {
            FilePath = pFilePath;
            IsDir = pIsDir;
            ModifiedDate = pModifiedDate;
        }

        public FileListDBEntry(String pFilePath, bool pIsDir)
        {
            FilePath = pFilePath;
            IsDir = pIsDir;
        }
    }

    public class FileCompareResults
    {
        public bool bToBeUpdated = false;
        public bool bToBeCopied = false;
        public bool bToBeDeleted = false;
        public bool bDeleteActioned = false;
        public bool bTimeSpanFiltered = false;
        public int bTimeSpanAmount = 0;
        public bool FileSizeInconsistent = false;
        public long SourceSize = 0;
        public long DestSize = 0;

        public FileCompareResults()
        {
            bToBeUpdated = false;
            bToBeCopied = false;
            bToBeDeleted = false;
        }

        public FileCompareResults(bool Updated, bool Copied, bool Deleted)
        {
            bToBeUpdated = Updated;
            bToBeCopied = Copied;
            bToBeDeleted = Deleted;
        }

        public FileCompareResults(bool Updated, bool Copied, bool Deleted, bool DeleteActioned)
        {
            bToBeUpdated = Updated;
            bToBeCopied = Copied;
            bToBeDeleted = Deleted;
            bDeleteActioned = DeleteActioned;
        }
    }
}
