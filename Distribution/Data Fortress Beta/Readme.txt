DATA FORTRESS CLIENT & SERVER BETA

Contained in this package is the Data Fortress Server and the Data Fortress Desktop Client. DataFortress is a multi-client TCP/IP based File and Folder Backup solution. The server is a multithreaded server capable of handling multiple client connections simultaneously backing up data to the server. The primary use for this solution is the backing up of files and folders to a remote server over the internet using purely a TCP/IP connection.

RECOMMENDED SETUP

The DataFortress Server is designed to be the server which will accept the connections from clients so it is recommended that this server has enough disk space (or access to) to store all of the data created by Client Backup jobs.

Steps to Installation

1) Install the Server on your Windows Server machine

2) Using the Fortress Server Manager, create one or more Client Accounts which will allow a Desktop Client to 
   authenticate and logon to the server. IMPORTANT: When creating a Client Account please ensure the Clients
   server root path is pointing to a folder that exists on the server. This folder is the place where all of the
   Desktops Clients data will be stored when a backup is initiated by the client.


3) Install the Client on a designated Client machine.

4) Enter the Server details in the Client Settings. The ServerName and Port must be known. By Default the 
   Data Fortress Server listens on Port 10116. For the username and password, enter the username and password
   you specified when creating the account on the server.

5) Create one or more backup jobs. 

6) Click Backup. The client will then initiate a connection to the server and start checking which files
   and folders need to be backed up and sent to the server.




KNOWN ISSUES
------------

Please note that Bit Level Differencing of individual files has not yet been implemented. This will be implemented
in an upcoming version. For the moment, changes in files will result in the entire file being sent to the server
and overwritten on the server side.


Max Allowed MB currently is not implemented. This currently means that Clients have unlimited data storage on
the server side. This will be implemented in an upcoming version.

