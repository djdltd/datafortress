﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace DiffEngine
{
    class Program
    {
        //static int _chunksize = 16384;
        static int _chunksize = 16384;

        static void Main(string[] args)
        {
            string originalfile = "C:\\Temp\\IFC\\NicePicture.jpg";
            string modifiedfile = "C:\\Temp\\IFC\\NicePictureMod.jpg";
            string newfile = "C:\\Temp\\IFC\\NicePictureNewMod.jpg";

            //string originalfile = "C:\\Temp\\IFC\\Original.avi";
            //string modifiedfile = "C:\\Temp\\IFC\\Modified.avi";
            //string newfile = "C:\\Temp\\IFC\\NewModified.avi";

           

            //string originalfile = "C:\\Temp\\IFC\\SourceText.txt";
            //string modifiedfile = "C:\\Temp\\IFC\\ModifiedText.txt";
            //string newfile = "C:\\Temp\\IFC\\NewModified.txt";

            List<FileChunkEntry> chunklist = BuildFileChunkTable(originalfile);
            //F:\Mon01-08\X1700022.MOV
            //ListSmallChunkCounts(chunklist);

            //Console.WriteLine("Press enter to continue...");
            //Console.ReadLine();

            //List<FileChunkEntry> chunklist = BuildFileChunkTable("F:\\Mon01-08\\X1700022.MOV");
            //List<FileChunkEntry> chunklist = BuildFileChunkTable("F:\\X1700177.MOV");

            byte[] serializedhunklist = Serialize(chunklist);

            Console.WriteLine("Size of serialized byte array: " + serializedhunklist.Length.ToString());

            // FInd out how big in bytes a single FileChunkEntry is
            if (chunklist.Count > 0)
            {
                FileChunkEntry entry = chunklist[0];

                byte[] serializedentry = Serialize(entry);

                Console.WriteLine("Size of single entry: " + serializedentry.Length.ToString());
            }

            //int md5collisions = CheckMD5Collisions(chunklist);

            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();
            int nummismatches = 0;

            List<FileChunkEntry> completedfiletable = MapChunkTableToFileEx(modifiedfile, chunklist, out nummismatches);

            if (nummismatches == 0)
            {
                Console.WriteLine("There were not any mismatches in the compared files. Files are identical.");
                Console.WriteLine("Press enter to exit...");
                Console.ReadLine();
                return;
            }
            else
            {
                Console.WriteLine("There were " + nummismatches.ToString() + " in the modified file.");
            }

            if (completedfiletable.Count == 0)
            {
                Console.WriteLine("Files were completely different. No similarities remain in the new file. Sending of entire file is needed.");
                Console.WriteLine("Press enter to exit...");
                Console.ReadLine();
                return;
            }

            ListCompletedFileTable(completedfiletable);

            byte[] serializedfiletable = Serialize(completedfiletable);

            Console.WriteLine("Size of serialized completion table: " + serializedfiletable.Length.ToString());


            BuildCompletedFile(originalfile, newfile, chunklist, completedfiletable);

            CompareFiles(modifiedfile, newfile);

            Console.WriteLine("Press enter to exit...");
            Console.ReadLine();

        }

        public static byte[] Serialize(Object myObject)
        {
            BinaryFormatter bin = new BinaryFormatter();

            MemoryStream mem = new MemoryStream();

            bin.Serialize(mem, myObject);

            return mem.GetBuffer();
        }

        //Console.WriteLine("This is a test man\n\nand another test\n\n");
        public void ReadFile()
        {

        }

        public static void ListCompletedFileTable(List<FileChunkEntry> completedtable)
        {
            foreach (FileChunkEntry entry in completedtable)
            {
                if (entry.dataincluded == true)
                {
                    Console.WriteLine("OFF: " + entry.offset.ToString() + " LEN: " + entry.length.ToString() + " HASH: " + entry.primaryhash + " SRVIDX: " + entry.index.ToString() + " DATA: " + entry.dataincluded.ToString());

                    Console.WriteLine("Data Length: " + entry.data.Length.ToString());
                
                }
            }
        }

        public static bool DoesListStringExist(List<string> stringlist, string stringvalue)
        {
            foreach (string currentstring in stringlist)
            {
                if (stringvalue == currentstring)
                {
                    return true;
                }
            }

            return false;
        }

        public static int CheckMD5Collisions(List<FileChunkEntry> chunklist)
        {
            Console.WriteLine("Checking for MD5 Collisions....");

            int md5collisioncount = 0;
            List<string> uniquemd5list = new List<string>();

            foreach (FileChunkEntry entry in chunklist)
            {
                if (DoesListStringExist(uniquemd5list, entry.primaryhash) == false)
                {
                    uniquemd5list.Add(entry.primaryhash);
                }
                else
                {
                    md5collisioncount++;
                    Console.WriteLine("MD5 Collision!!! - Hash " + entry.primaryhash + " is not unique!!");
                }
            }

            Console.WriteLine("MD5 Collision Check Complete. There were " + md5collisioncount.ToString() + " collisions.");

            return md5collisioncount;
        }


        public static List<FileChunkEntry> BuildFileChunkTable(string filepath)
        {
            List<FileChunkEntry> chunklist = new List<FileChunkEntry>();

            byte[] buffer;
            byte[] smallchunkbuffer;

            FileStream fileStream = new FileStream(filepath, FileMode.Open, FileAccess.Read);

            int chunksize = _chunksize;
            int smallchunksize = 8192;
            long smallchunkpointer = 0;
            int numsmallstartingchunks = 100;

            // When we divide the file length by the chunk size it rounds the number down to the smallest non decimal number.
            // so we might miss some stuff at the end of the file, so we do a final check to make sure than when the number of chunks
            // are multiplied by the chunk size we check the calculated size. If the calculated size is smaller, then we need another chunk.
            long numchunks = fileStream.Length / chunksize;

            long calcsize = numchunks * chunksize;

            if (fileStream.Length > calcsize)
            {
                numchunks++;
            }


            long filepointer = 0;
            long workingchunksize = 0;
            
            buffer = new byte[chunksize];

            for (int a = 0; a < numchunks; a++)
            {
                if ((fileStream.Length - filepointer) >= chunksize)
                {
                    workingchunksize = chunksize;
                    

                    fileStream.Read(buffer, 0, chunksize);
                }
                else
                {
                    long lastchunksize = fileStream.Length - filepointer;
                    workingchunksize = lastchunksize;

                    buffer = new byte[lastchunksize];

                    fileStream.Read(buffer, 0, (int)lastchunksize);
                }

                // At this point we have our buffer array populated with the current chunk (even the last one)
                FileChunkEntry newentry = new FileChunkEntry();

                newentry.primaryhash = CalculateMD5(buffer);

                // Now add the first smaller chunks
                // For each primary hash we store a small number of smaller chunks which serve has hash position markers so that when the
                // mapping function does it's incremental search it can use 8K smaller chunks for increased performance.
                smallchunkbuffer = new byte[smallchunksize];
                smallchunkpointer = 0;

                newentry.offset = filepointer;
                newentry.length = (int)workingchunksize;
                newentry.index = a;

                Console.WriteLine("Offset: " + newentry.offset.ToString() + "\tCS: " + newentry.length.ToString() + " HASH: " + newentry.primaryhash + " IDX: " + newentry.index.ToString());

                chunklist.Add(newentry);

                filepointer += workingchunksize;
            }


            return chunklist;
        }

        public static FileChunkEntry GetChunkEntry(List<FileChunkEntry> chunktable, int possibleindex, string hashtosearch)
        {
            // This function looks up the given hash in the supplied chunktable. But first it checks if the hastosearch matches
            // the possible index given, to save having to run through the entire list.

            // First look up the possible index.
            if (possibleindex >= 0)
            {
                if (possibleindex < chunktable.Count)
                {
                    FileChunkEntry possibleentry = chunktable[possibleindex];

                    if (hashtosearch == possibleentry.primaryhash)
                    {
                        // Great! The possible index given and hash matched immedietely, so return this - no need to do a full search
                        return possibleentry;
                    }
                }
            }

            // If we got here then the possible index was not matched or the possible index was outside the count of chunktable items
            // so we have to do a full search

            foreach (FileChunkEntry entry in chunktable)
            {
                if (entry.primaryhash == hashtosearch)
                {
                    return entry;
                }
            }

            // Now if we got here then we couldn't find the hash, so return null
            return null;
        }

        public static FileChunkEntry FindSmallChunkAtIndex(List<FileChunkEntry> chunktable, string smallhashtosearch, int smallchunkindex)
        {
            // This function searches the smaller chunk list for each chunk entry in the chunk table and matches the one given in the
            // small chunk index.

            foreach (FileChunkEntry entry in chunktable)
            {
                //if (entry.starthashlist != null && entry.starthashlist.Count > smallchunkindex)
                //{
                    //if (entry.starthashlist[smallchunkindex] == smallhashtosearch)
                    //{
                        //return entry;
                    //}
                //}
            }

            return null;
        }

        public static long ReadFileChunk(FileStream fileStream, long filepointer, int chunksize, byte[] buffer)
        {
            long workingchunksize = 0;

            if ((fileStream.Length - filepointer) >= chunksize)
            {
                workingchunksize = chunksize;

                fileStream.Seek(filepointer, SeekOrigin.Begin);
                fileStream.Read(buffer, 0, chunksize);

                //ByteArrayToFile("C:\\Temp\\IFC\\BufferFileOffset" + a.ToString() + ".dat", buffer);
            }
            else
            {
                long lastchunksize = fileStream.Length - filepointer;
                workingchunksize = lastchunksize;

                buffer = new byte[lastchunksize];

                fileStream.Seek(filepointer, SeekOrigin.Begin);
                fileStream.Read(buffer, 0, (int)lastchunksize);

                //ByteArrayToFile("C:\\Temp\\IFC\\LastBufferFileOffset" + a.ToString() + ".dat", buffer);
            }

            return workingchunksize;
        }

        
        public static bool ConfirmSmallHashMatch(FileStream fileStream, long filepointer, int chunksize, List<FileChunkEntry> sourcechunktable, int numsmallchunks, FileChunkEntry foundchunkmatch)
        {
            byte[] smallchunkbuffer;
            long workingchunksize = 0;
            long localfilepointer = filepointer;

            smallchunkbuffer = new byte[chunksize];

            for (int c = 0; c < numsmallchunks; c++)
            {
                workingchunksize = ReadFileChunk(fileStream, localfilepointer, chunksize, smallchunkbuffer);

                string currenthash = CalculateMD5(smallchunkbuffer);

                try
                {
                    /*
                    //FileChunkEntry smallchunkentry = FindSmallChunkAtIndex(sourcechunktable, currenthash, c);
                    if (foundchunkmatch.starthashlist[c] == currenthash)
                    {
                        // Found it. Keep confirming.
                        localfilepointer += chunksize;
                    }
                    else
                    {
                        return false;
                    }
                     */
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Confirmation generated exception: " + ex.Message);
                    return false;
                }

                /*
                if (smallchunkentry != null)
                {
                    // Found it. Keep confirming.
                    localfilepointer += chunksize;
                }
                else
                {
                    // Didn't find it, confirmation failed.
                    return false;
                }
                 */
            }

            return true;
            
        }

        private static long GetSkipSize(long filelengthremaining)
        {
            // Calculate 30% of the file length remaining.

            long mult = filelengthremaining * 30;
            long skipresult = mult / 100;

            if (skipresult > 2048000)
            {
                skipresult = 2048000; // We cap the skip result at 2MB. We can't be skipping over chunks of greater sizes.
            }

            return skipresult;
        }

        public static List<FileChunkEntry> MapChunkTableToFileEx(string filetomap, List<FileChunkEntry> sourcechunktable, out int numfilemismatches)
        {
            // This method always operates on the modified file. The source chunk table must come from the
            // unmodified file.

            byte[] buffer;
            byte[] smallchunkbuffer;

            FileStream fileStream = new FileStream(filetomap, FileMode.Open, FileAccess.Read);

            int chunksize = _chunksize;
            int smallchunksize = 8192;
            long filepointer = 0;
            long possibleindex = 0;
            bool incrementalmode = false;
            long mismatchfilepointer = 0;
            long diffsize = 0;
            int skipcount = 0;
            int nummismatches = 0;

            List<FileChunkEntry> completedfiletable = new List<FileChunkEntry>();

            long workingchunksize = 0;
            buffer = new byte[chunksize];
            smallchunkbuffer = new byte[smallchunksize];

            while (filepointer < fileStream.Length)
            {
                if (incrementalmode == false) // Normal mode operating on 16K chunks
                {
                    // Grab a chunk of data from the modified file.
                    if ((fileStream.Length - filepointer) >= chunksize)
                    {
                        workingchunksize = chunksize;

                        fileStream.Seek(filepointer, SeekOrigin.Begin);
                        fileStream.Read(buffer, 0, chunksize);                        
                    }
                    else
                    {
                        long lastchunksize = fileStream.Length - filepointer;
                        workingchunksize = lastchunksize;

                        buffer = new byte[lastchunksize];

                        fileStream.Seek(filepointer, SeekOrigin.Begin);
                        fileStream.Read(buffer, 0, (int)lastchunksize);
                    }


                    Console.WriteLine("WORKINGCHUNKSIZE: " + workingchunksize);   

                    string currenthash = CalculateMD5(buffer);

                    // The possible index just aids in finding chunk entries in the source table, basically
                    // it means we don't have to search the whole table from start to finish, as we kinda know where to look
                    if ((filepointer % chunksize) == 0) // This means the file pointer is easily divisible by the chunk size and we can obtain a possible index
                    {
                        possibleindex = filepointer / chunksize;
                        Console.WriteLine("Possible IDX: " + possibleindex.ToString());
                    }
                    else
                    {
                        possibleindex = -1;
                    }

                    // See if the buffer (hash) we've just grabbed from our modified file exists in the source table
                    // Basically do we have this chunk of file in the source file.
                    FileChunkEntry chunkentry = GetChunkEntry(sourcechunktable, (int)possibleindex, currenthash);

                    if (chunkentry != null)
                    {
                        // Found it
                        Console.WriteLine("CHUNK ENTRY MATCH! OFF: " + filepointer.ToString() + " SOURCE IDX: " + chunkentry.index.ToString() + " SOURCE OFF: " + chunkentry.offset.ToString());

                        // Add this meta chunk to the completed file table as it's the same as the source
                        FileChunkEntry completedentry = new FileChunkEntry();
                        completedentry.offset = filepointer;
                        completedentry.length = chunkentry.length;
                        completedentry.primaryhash = chunkentry.primaryhash;
                        completedentry.index = chunkentry.index;
                        completedentry.dataincluded = false;

                        completedfiletable.Add(completedentry);

                        filepointer += chunksize;
                    }
                    else
                    {
                        // Did not find it, this means the file we are scanning has come across a mismatch on the source file, this means
                        // our file has changed and at this point no longer matches the source file at the same place, so we need to do incremental
                        // file mapping by advancing the pointer one byte at a time instead of a whole chunk at a time.
                        Console.WriteLine("CHUNK ENTRY MISMATCH!");
                        mismatchfilepointer = filepointer;
                        diffsize = 0;
                        skipcount = 0;
                        nummismatches++;

                        //filepointer++;
                        incrementalmode = true;
                    }

                    //Console.WriteLine("NON INCREMENTAL HASH: " + currenthash + " OFF: " + filepointer.ToString() + " LEN: " + fileStream.Length.ToString());
                }


                if (incrementalmode == true) // Incremental mode searching for the next match in the source file to see where the file lines up again
                {

                    if ((fileStream.Length - filepointer) >= chunksize)
                    {
                        workingchunksize = chunksize;

                        fileStream.Seek(filepointer, SeekOrigin.Begin);
                        fileStream.Read(buffer, 0, chunksize);
                    }
                    else
                    {
                        long lastchunksize = fileStream.Length - filepointer;
                        workingchunksize = lastchunksize;

                        buffer = new byte[lastchunksize];

                        fileStream.Seek(filepointer, SeekOrigin.Begin);
                        fileStream.Read(buffer, 0, (int)lastchunksize);
                    }

                    string currenthash = CalculateMD5(buffer);

                    FileChunkEntry chunkentry = GetChunkEntry(sourcechunktable, -1, currenthash);

                    if (chunkentry != null)
                    {
                        // Found it
                        long changeddatalength = filepointer - mismatchfilepointer;

                        byte[] changeddata = new byte[changeddatalength];

                        ReadFileChunk(fileStream, mismatchfilepointer, (int)changeddatalength, changeddata);

                        FileChunkEntry completedentry = new FileChunkEntry();
                        completedentry.offset = mismatchfilepointer;
                        completedentry.length = (int)changeddatalength;
                        completedentry.primaryhash = null;
                        completedentry.index = -1;
                        completedentry.dataincluded = true;
                        completedentry.data = new byte[changeddatalength];
                        completedentry.data = changeddata;

                        completedfiletable.Add(completedentry);

                        Console.WriteLine("SEARCH MATCH FOUND! OFF: " + filepointer.ToString() + " SOURCE IDX: " + chunkentry.index.ToString() + " SOURCE OFF: " + chunkentry.offset.ToString());
                        incrementalmode = false;
                        skipcount = 0;
                    }
                    else
                    {
                        Console.WriteLine("SEARCH MODE. INCREMENTAL HASH: " + currenthash + " FILE OFFSET: " + filepointer.ToString() + " DIFF SIZE: " + (filepointer - mismatchfilepointer).ToString() + " ACTUAL DIFF: " + diffsize.ToString ());
                        diffsize++;

                        if (diffsize > _chunksize)
                        {
                            diffsize = 0;
                            skipcount++;

                            long lengthremaining = fileStream.Length - filepointer;

                            long skipsize = GetSkipSize(lengthremaining);

                            if ((filepointer * skipcount) < lengthremaining)
                            {
                                filepointer += (skipsize * skipcount); // When we skip over chunks of file we start small then if there are still
                                // differences then we skip over larger chunks. There are pros and cons to doing this. The advantage is that we'll
                                // be done checking the file quite quickly. But the disadvantage is that the differencing precision will not be
                                // as good as it could be if we didn't skip. This basically means we'll be sending more data over to complete
                                // the destination file than we really have to which results in more data being sent. But hey, it's my call
                                // and I've called it - sacrifice differencing precision for speed, we can't have the differencing taking longer
                                // than it would actually take to send the file now can we!
                            }
                            else
                            {
                                filepointer += skipsize; // Skip over a chunk of the file otherwise we will be incrementally searching for ages.
                            } 
                        }
                        
                        filepointer++;
                    }
                }               
            }

            fileStream.Close();
            numfilemismatches = nummismatches;

            return completedfiletable;
        }

        public static void CompareFiles(string file1path, string file2path)
        {
            FileStream file1Stream = new FileStream(file1path, FileMode.Open, FileAccess.Read);
            FileStream file2Stream = new FileStream(file2path, FileMode.Open, FileAccess.Read);

            if (file1Stream.Length != file2Stream.Length)
            {
                Console.WriteLine("File Sizes are different. Files are not the same.");
                return;
            }

            long chunksize = _chunksize;
            long workingchunksize = 0;
            long filepointer = 0;
            byte[] buffer1;
            byte[] buffer2;
            long numdifferences = 0;

            while (filepointer < file1Stream.Length)
            {
                Console.WriteLine("Comparing File offset: " + filepointer.ToString());

                if ((file1Stream.Length - filepointer) >= chunksize)
                {
                    workingchunksize = chunksize;

                    buffer1 = new byte[workingchunksize];
                    buffer2 = new byte[workingchunksize];

                    file1Stream.Read(buffer1, 0, (int)workingchunksize);
                    file2Stream.Read(buffer2, 0, (int)workingchunksize);

                    // Compare the buffers
                    for (int b = 0; b < buffer1.Length; b++)
                    {
                        byte b1 = buffer1[b];
                        byte b2 = buffer2[b];

                        if (b1 != b2)
                        {
                            numdifferences++;
                        }
                    }

                    filepointer += workingchunksize;
                }
                else
                {
                    workingchunksize = file1Stream.Length - filepointer;

                    buffer1 = new byte[workingchunksize];
                    buffer2 = new byte[workingchunksize];

                    file1Stream.Read(buffer1, 0, (int)workingchunksize);
                    file2Stream.Read(buffer2, 0, (int)workingchunksize);

                    // Compare the buffers
                    for (int b = 0; b < buffer1.Length; b++)
                    {
                        byte b1 = buffer1[b];
                        byte b2 = buffer2[b];

                        if (b1 != b2)
                        {
                            numdifferences++;
                        }
                    }

                    filepointer += workingchunksize;
                }
                
                
            }

            Console.WriteLine("Comparison Complete. There were " + numdifferences.ToString() + " differences.");

            file1Stream.Close();
            file2Stream.Close();
        }


        public static void BuildCompletedFile(string originalfilepath, string newfilepath, List<FileChunkEntry> sourcechunktable, List<FileChunkEntry> completedfiletable)
        {
            // First delete the destination file if it exists
            if (File.Exists(newfilepath))
            {
                File.Delete(newfilepath);
            }

            FileStream inputfileStream = new FileStream(originalfilepath, FileMode.Open, FileAccess.Read);
            FileStream outputfileStream = new FileStream(newfilepath, FileMode.Append, FileAccess.Write);

            byte[] inputbuffer;

            foreach (FileChunkEntry entry in completedfiletable)
            {
                if (entry.dataincluded == false)
                {
                    // First get the server entry referred to by the index
                    FileChunkEntry sourceentry = sourcechunktable[(int)entry.index];

                    inputbuffer = new byte[sourceentry.length];

                    // We're reading an existing chunk from the original file, so read it from the original file and write it to the new file
                    inputfileStream.Seek(sourceentry.offset, SeekOrigin.Begin);
                    inputfileStream.Read(inputbuffer, 0, sourceentry.length);

                    // Write this read chunk to the outputfile
                    outputfileStream.Write(inputbuffer, 0, sourceentry.length);
                }

                if (entry.dataincluded == true)
                {
                    // This time we write data which has been provided by the completed file table
                    outputfileStream.Write(entry.data, 0, entry.data.Length);
                }

                Console.WriteLine("Writing new file...OFF: " + entry.offset.ToString());
            }

            Console.WriteLine("Done writing new file.");
            outputfileStream.Close();
            inputfileStream.Close();
        }


        /// <summary>
        /// Calculates a MD5 hash from the given string and uses the given
        /// encoding.
        /// </summary>
        /// <param name="Input">Input string</param>
        /// <param name="UseEncoding">Encoding method</param>
        /// <returns>MD5 computed string</returns>
        public static string CalculateMD5(byte[] Input, Encoding UseEncoding)
        {
            //System.Security.Cryptography.MD5CryptoServiceProvider CryptoService;
            //CryptoService = new System.Security.Cryptography.MD5CryptoServiceProvider();

            System.Security.Cryptography.SHA1CryptoServiceProvider CryptoService;
            CryptoService = new System.Security.Cryptography.SHA1CryptoServiceProvider();

            byte[] OutputBytes;
            OutputBytes = CryptoService.ComputeHash(Input);
            return BitConverter.ToString(OutputBytes).Replace("-", "");
        }

        /// <summary>
        /// Calculates a MD5 hash from the given string. 
        /// (By using the default encoding)
        /// </summary>
        /// <param name="Input">Input string</param>
        /// <returns>MD5 computed string</returns>
        public static string CalculateMD5(byte[] Input)
        {
            // That's just a shortcut to the base method
            return CalculateMD5(Input, System.Text.Encoding.Default);
        }
    }

    [Serializable]
    public class FileChunkEntry
    {
        public string primaryhash; // 2 meg hash of the whole chunk
        //public List<String> starthashlist; // At the beginning of each chunk we store 10 or so 8K chunk hashes for incremental analysis performance.
        public long offset;
        public int length;
        public long index;
        public bool dataincluded;
        public byte[] data;
    }
}
