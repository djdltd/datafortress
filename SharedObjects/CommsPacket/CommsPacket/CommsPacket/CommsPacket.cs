﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace CommsPacket
{
    public enum MessageTypes
    {
        PrepareServerForFileTransfer = 100,
        FileChunk = 101,
        FileExists = 102,
        LastWriteTime = 103,
        FileLength = 104,
        LargeDataTest = 105,
        AuthenticateAdmin = 106,
        ListAdminAccounts = 107,
        NewAdminAccount = 108,
        UpdateAdminAccount = 109,
        DeleteAdminAccount = 110,
        ChangeAdminPassword = 111,
        ListClientAccounts = 112,
        NewClientAccount = 113,
        UpdateClientAccount = 114,
        DeleteClientAccount = 115,
        ChangeClientPassword = 116,
        AuthenticateClient = 117,
        BuildFileChunkTable = 118,
        FileNotFound = 119,
        CompletedFileTable = 120,
        AuthenticateAdmin_Linux = 10005,
        LinuxSignatureOne = 47577,
        LinuxSignatureTwo = 77777,
        LinuxAuthAdminOk = 10006,
        LinuxAuthAdminDenied = 10007,
        LinuxListAdminAccounts = 10008,
        LinuxAdminAccountList = 10009,
        LinuxSizeString = 1024,
        LinuxListClientAccounts = 10010,
        LinuxClientAccountList = 10011,
        LinuxNewClientAccount = 10012,
        LinuxUpdateClientAccount = 10013,
        LinuxDeleteClientAccount = 10014,
        LinuxChangeClientPassword = 10015,
        LinuxNewAdminAccount = 10016,
        LinuxUpdateAdminAccount = 10017,
        LinuxDeleteAdminAccount = 10018,
        LinuxChangeAdminPassword = 10019,
        AuthenticateClient_Linux = 10020,
        LinuxAuthClientOk = 10021,
        LinuxAuthClientDenied = 10022,
        LinuxFileExists = 10023,
        LinuxResponseFileExists = 10024,
        LinuxResponseFileNotFound = 10025,
        LinuxLastWriteTime = 10026,
        LinuxResponseLastWriteTime = 10027,
        LinuxFileLength = 10028,
        LinuxResponseFileLength = 10029,
        LinuxPrepareFileTransfer = 10030,
        LinuxFileTransferReady = 10031
    }

    [Serializable]
    public class CommsPacket
    {

        public int iMessagetype;
        public string filename;
        public long filelength;
        public string useraccount;
        public string passwordhash;
        public string usertoken;
        public string sourcepath;
        public string destpath;
        public string jobname;

        public string Somemessage;
        public byte[] somebuffer;
        public string somethingelse;

        public static int GetLinuxAdminAccountSize()
        {
            return (sizeof(int)*2) + (((int)MessageTypes.LinuxSizeString) * 5);
        }

        public static int GetLinuxClientAccountSize()
        {
            return (sizeof(int) * 3) + (((int)MessageTypes.LinuxSizeString) * 7);
        }

        public byte[] Serialize()
        {
            BinaryFormatter bin = new BinaryFormatter();
            
            MemoryStream mem = new MemoryStream();

            bin.Serialize(mem, this);

            return mem.GetBuffer();
        }

        public static byte[] SerializeObject(Object objecttoserialise)
        {
            BinaryFormatter bin = new BinaryFormatter();

            MemoryStream mem = new MemoryStream();

            bin.Serialize(mem, objecttoserialise);

            return mem.GetBuffer();
        }

        public static Object DeSerializeObject(byte[] serializedobject)
        {
            BinaryFormatter bin = new BinaryFormatter();
            MemoryStream mem = new MemoryStream();

            mem.Write(serializedobject, 0, serializedobject.Length);
            mem.Seek(0, 0);

            return bin.Deserialize(mem);
        }

        public byte[] Encapsulate(byte[] serialisedbytes)
        {
            // This takes the already serialised byte buffer, and encapsulates it with some extra stuff
            // we need for reliable comms. In this case we just add some magic bytes to the end of the buffer
            // so that the receiver knows when all the data for this packet has been received.

            // Set up our signature
            string signature = "23882379482482ABSHDJAHSBDHDBDHDBDHDB777777777777398749283END OF PACKET39248977777777777777777777732842038END OF PACKET 382482749872948723977777777777777777777777777748729348723984END OF PACKET 3874982749384729777777777777777777734872938472934 END OF PACKET";
            byte[] sigbytes = Encoding.ASCII.GetBytes(signature);

            byte[] newbuffer = new byte[serialisedbytes.Length + 256];

            serialisedbytes.CopyTo(newbuffer, 0);
            sigbytes.CopyTo(newbuffer, serialisedbytes.Length);

            return newbuffer;
        }

        public byte[] Decapsulate(byte[] encapsulatedbytes)
        {
            if (encapsulatedbytes.Length > 256)
            {
                byte[] newbuffer = new byte[encapsulatedbytes.Length - 256];

                for (long b = 0; b < newbuffer.Length; b++)
                {
                    newbuffer[b] = encapsulatedbytes[b];
                }

                return newbuffer;
            }
            else
            {
                return new byte[0];
            }
        }

        public bool ContainsSignature(byte[] encapsulatedbytes)
        {
            string wantedsignature = "23882379482482ABSHDJAHSBDHDBDHDBDHDB777777777777398749283END OF PACKET39248977777777777777777777732842038END OF PACKET 382482749872948723977777777777777777777777777748729348723984END OF PACKET 3874982749384729777777777777777777734872938472934 END OF PACKET";
            

            if (encapsulatedbytes.Length > 256)
            {
                byte[] sigbytes = new byte[256];

                long startingindex = encapsulatedbytes.Length - 256;

                for (int b = 0; b < 256; b++)
                {
                    sigbytes[b] = encapsulatedbytes[startingindex];
                    startingindex++;
                }

                string signature = Encoding.ASCII.GetString(sigbytes);

                if (wantedsignature == signature)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public CommsPacket DeSerialize(byte[] dataBuffer)
        {

            BinaryFormatter bin = new BinaryFormatter();
            MemoryStream mem = new MemoryStream();

            mem.Write(dataBuffer, 0, dataBuffer.Length);
            mem.Seek(0, 0);

            return (CommsPacket)bin.Deserialize(mem);

        }

    }

    [Serializable]
    public class CommsResponse
    {
        public int iResponsetype;
        public string Message;
        public byte[] bytebuffer;
        public bool FileExists;
        public DateTime LastWriteTime;
        public long FileLength;
        public string token;
        public bool authenticated;


        public byte[] Serialize()
        {
            BinaryFormatter bin = new BinaryFormatter();

            MemoryStream mem = new MemoryStream();

            bin.Serialize(mem, this);

            return mem.GetBuffer();
        }

        public CommsResponse DeSerialize(byte[] dataBuffer)
        {

            BinaryFormatter bin = new BinaryFormatter();
            MemoryStream mem = new MemoryStream();

            mem.Write(dataBuffer, 0, dataBuffer.Length);
            mem.Seek(0, 0);

            return (CommsResponse)bin.Deserialize(mem);

        }
    }

    [Serializable]
    public class AdminAccount
    {
        public string Username;
        public string PasswordHash;
        public string Firstname;
        public string Lastname;
        public string Description;
    }


    [Serializable]
    public class ClientAccount
    {
        public string Username;
        public string PasswordHash;
        public string Firstname;
        public string Lastname;
        public string Company;
        public string RootPath; // Where this clients backup data will be stored physically on the server side
        public string Description;
        public int MaxAllowedMegaBytes;
    }

    [Serializable]
    public class FileChunkEntry
    {
        public string primaryhash; // 2 meg hash of the whole chunk
        //public List<String> starthashlist; // At the beginning of each chunk we store 10 or so 8K chunk hashes for incremental analysis performance.
        public long offset;
        public int length;
        public long index;
        public bool dataincluded;
        public byte[] data;
    }
}
