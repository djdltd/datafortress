﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.IO;

namespace FortressManager
{
    public class ApplicationSettings
    {
        public List<ServerDetails> serverlogonlist = new List<ServerDetails>();
        public int LastSelectedIndex;

        public void SaveXMLSettings()
        {
            XmlSerializer mySerializer = new XmlSerializer(typeof(ApplicationSettings));
            StreamWriter myWriter = new StreamWriter(CommonFunctions.GetAppSettingsDir() + "FortressManagerSettings.xml");

            mySerializer.Serialize(myWriter, this);
            myWriter.Close();
        }

        private ApplicationSettings LoadSettings(string filepath)
        {
            ApplicationSettings loadedsettings = new ApplicationSettings();

            XmlSerializer mySerializer = new XmlSerializer(typeof(ApplicationSettings));

            // To read the xml file, create a FileStream.
            FileStream myFileStream = new FileStream(filepath, FileMode.Open);

            // Call the Deserialize method and cast to the object type.
            loadedsettings = (ApplicationSettings)mySerializer.Deserialize(myFileStream);

            myFileStream.Close();

            return loadedsettings;
        }

        public ApplicationSettings LoadXMLSettings()
        {
            string filename = "FortressManagerSettings.xml";
            string usersettingspath = CommonFunctions.GetAppSettingsDir() + filename;
            string programsettingpath = Path.Combine (CommonFunctions.GetProgramSettingsDir () , filename);

            if (File.Exists(usersettingspath) == true)
            {
                return LoadSettings(usersettingspath);
            }
            else
            {
                if (File.Exists (programsettingpath) == true) {
                    return LoadSettings(programsettingpath);
                }
            }

            return new ApplicationSettings();
        }
    }

}
