﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace FortressManager
{
    public partial class AdminAccountControl : UserControl
    {
        public delegate void NewAccountCreated(CommsPacket.AdminAccount newaccount);
        public event NewAccountCreated OnNewAccountCreated;

        public delegate void ExistingAccountChanged(CommsPacket.AdminAccount newaccountdetails);
        public event ExistingAccountChanged OnExistingAccountChanged;

        public delegate void AccountDeleted(string adminaccountusername);
        public event AccountDeleted OnAccountDeleted;

        public delegate void AccountPasswordChanged(string username, string newpasswordhash);
        public event AccountPasswordChanged OnAccountPasswordChanged;

        List<CommsPacket.AdminAccount> _accountlist = new List<CommsPacket.AdminAccount>();
        private string _selectedusername;
        private int _selectedindex;
        private bool _gridrefreshing = false;

        public AdminAccountControl()
        {
            InitializeComponent();
        }

        public void RefreshAdminAccountList(List<CommsPacket.AdminAccount> accountlist)
        {
            _gridrefreshing = true;
            _accountlist = accountlist;

            grdAdminAccounts.Columns.Clear();

            grdAdminAccounts.Columns.Add("Username", "Username");
            grdAdminAccounts.Columns.Add("FirstName", "FirstName");
            grdAdminAccounts.Columns.Add("LastName", "LastName");
            grdAdminAccounts.Columns.Add("Description", "Description");

            for (int c = 0; c < accountlist.Count; c++)
            {
                CommsPacket.AdminAccount currentAccount = new CommsPacket.AdminAccount();
                currentAccount = (CommsPacket.AdminAccount)accountlist[c];

                grdAdminAccounts.Rows.Add();

                grdAdminAccounts.Rows[c].Cells[0].Value = currentAccount.Username;
                grdAdminAccounts.Rows[c].Cells[1].Value = currentAccount.Firstname;
                grdAdminAccounts.Rows[c].Cells[2].Value = currentAccount.Lastname;
                grdAdminAccounts.Rows[c].Cells[3].Value = currentAccount.Description;

            }

            grdAdminAccounts.AutoResizeColumns();

            if (_accountlist.Count > 0)
            {
                _selectedusername = _accountlist[0].Username;
                _selectedindex = 0;
            }

            _gridrefreshing = false;
        }

        private int GetSelectedIndex(string username)
        {

            for (int c = 0; c < _accountlist.Count; c++)
            {
                CommsPacket.AdminAccount account = (CommsPacket.AdminAccount)_accountlist[c];

                if (account.Username.ToUpper() == username.ToUpper())
                {
                    return c;
                }
            }

            return -1;
        }

        private void btnNewAccount_Click(object sender, EventArgs e)
        {
            NewAdminAccount newadmindialog = new NewAdminAccount();

            newadmindialog.OnNewAccountEntered += new NewAdminAccount.NewAccountEntered(newadmindialog_OnNewAccountEntered);
            newadmindialog.ShowDialog();
        }

        void newadmindialog_OnNewAccountEntered(CommsPacket.AdminAccount newadminaccount)
        {
            //throw new NotImplementedException();
            if (OnNewAccountCreated != null)
            {
                OnNewAccountCreated(newadminaccount);
            }
        }

        private void btnEditAccount_Click(object sender, EventArgs e)
        {
            EditAdminAccount editadmindialog = new EditAdminAccount();
            editadmindialog.SetAccountDetails(_accountlist[_selectedindex]);

            editadmindialog.OnAccountChanged += new EditAdminAccount.AccountChanged(editadmindialog_OnAccountChanged);
            editadmindialog.ShowDialog();
        }

        void editadmindialog_OnAccountChanged(CommsPacket.AdminAccount newadminaccount)
        {
            //throw new NotImplementedException();

            if (OnExistingAccountChanged != null)
            {
                OnExistingAccountChanged(newadminaccount);
            }
        }

        private void grdAdminAccounts_CurrentCellChanged(object sender, EventArgs e)
        {
            if (_gridrefreshing == false)
            {
                if (grdAdminAccounts.CurrentRow.Cells[0].Value != null)
                {
                    _selectedusername = grdAdminAccounts.CurrentRow.Cells[0].Value.ToString();
                    _selectedindex = GetSelectedIndex(_selectedusername);
                }
            }
        }

        private void btnDeleteAccount_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you wish to delete the Admin Account: " + _selectedusername + " ?", "Delete Account?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (OnAccountDeleted != null)
                {
                    OnAccountDeleted(_selectedusername);
                }
            }
        }

        private void btnChangePassword_Click(object sender, EventArgs e)
        {
            ChangeAdminPassword newpassworddialog = new ChangeAdminPassword();

            newpassworddialog.SetAccountDetails(_accountlist[_selectedindex]);
            newpassworddialog.OnPasswordChanged += new ChangeAdminPassword.PasswordChanged(newpassworddialog_OnPasswordChanged);
            newpassworddialog.ShowDialog();
        }

        void newpassworddialog_OnPasswordChanged(string adminusername, string newpasswordhash)
        {
            if (OnAccountPasswordChanged != null)
            {
                OnAccountPasswordChanged(adminusername, newpasswordhash);
            }
            //throw new NotImplementedException();
        }
    }
}
