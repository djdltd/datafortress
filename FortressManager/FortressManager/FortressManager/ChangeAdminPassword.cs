﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace FortressManager
{
    public partial class ChangeAdminPassword : Form
    {
        CommsPacket.AdminAccount _existingaccount = new CommsPacket.AdminAccount();
        CommsPacket.ClientAccount _existingclientaccount = new CommsPacket.ClientAccount();

        public delegate void PasswordChanged(string adminusername, string newpasswordhash);
        public event PasswordChanged OnPasswordChanged;

        bool _clientmode = false;

        public ChangeAdminPassword()
        {
            InitializeComponent();
        }

        public void SetAccountDetails(CommsPacket.AdminAccount existingaccount)
        {
            _clientmode = false;
            _existingaccount = existingaccount;
            txtUsername.Text = _existingaccount.Username;
        }

        public void SetClientAccountDetails(CommsPacket.ClientAccount existingclientaccount)
        {
            _clientmode = true;
            _existingclientaccount = existingclientaccount;
            txtUsername.Text = _existingclientaccount.Username;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (txtPassword.Text != txtConfirmPassword.Text)
            {
                MessageBox.Show("Passwords do not match.", "Different Passwords", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {

                string passwordhash = CommonFunctions.CreateMD5HashFromString(txtPassword.Text);

                if (OnPasswordChanged != null)
                {
                    if (_clientmode == true)
                    {
                        OnPasswordChanged(_existingclientaccount.Username, passwordhash);
                    }
                    else
                    {
                        OnPasswordChanged(_existingaccount.Username, passwordhash);
                    }                    
                }

                Hide();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Hide();
        }
    }
}
