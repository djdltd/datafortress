﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace FortressManager
{
    public partial class ClientAccountControl : UserControl
    {
        List<CommsPacket.ClientAccount> _clientaccountlist = new List<CommsPacket.ClientAccount>();
        private string _selectedusername;
        private int _selectedindex = -1;
        private bool _gridrefreshing = false;

        public delegate void NewClientAccountCreated(CommsPacket.ClientAccount newclientaccount);
        public event NewClientAccountCreated OnNewClientAccountCreated;

        public delegate void ExistingClientAccountChanged(CommsPacket.ClientAccount newclientaccountdetails);
        public event ExistingClientAccountChanged OnExistingClientAccountChanged;

        public delegate void ClientAccountDeleted(string clientaccountusername);
        public event ClientAccountDeleted OnClientAccountDeleted;

        public delegate void ClientAccountPasswordChanged(string username, string newpasswordhash);
        public event ClientAccountPasswordChanged OnClientAccountPasswordChanged;

        public ClientAccountControl()
        {
            InitializeComponent();
        }

        public void RefreshClientAccountList(List<CommsPacket.ClientAccount> clientaccountlist)
        {
            _gridrefreshing = true;
            _clientaccountlist = clientaccountlist;

            grdClientAccounts.Columns.Clear();

            grdClientAccounts.Columns.Add("Username", "Username");
            grdClientAccounts.Columns.Add("FirstName", "FirstName");
            grdClientAccounts.Columns.Add("LastName", "LastName");
            grdClientAccounts.Columns.Add("Company", "Company");
            grdClientAccounts.Columns.Add("ServerRootPath", "ServerRootPath");
            grdClientAccounts.Columns.Add("Description", "Description");
            grdClientAccounts.Columns.Add("MaxAllowedMB", "MaxAllowedMB");

            for (int c = 0; c < clientaccountlist.Count; c++)
            {
                CommsPacket.ClientAccount currentAccount = new CommsPacket.ClientAccount();
                currentAccount = (CommsPacket.ClientAccount)clientaccountlist[c];

                grdClientAccounts.Rows.Add();

                grdClientAccounts.Rows[c].Cells[0].Value = currentAccount.Username;
                grdClientAccounts.Rows[c].Cells[1].Value = currentAccount.Firstname;
                grdClientAccounts.Rows[c].Cells[2].Value = currentAccount.Lastname;
                grdClientAccounts.Rows[c].Cells[3].Value = currentAccount.Company;
                grdClientAccounts.Rows[c].Cells[4].Value = currentAccount.RootPath;
                grdClientAccounts.Rows[c].Cells[5].Value = currentAccount.Description;
                grdClientAccounts.Rows[c].Cells[6].Value = currentAccount.MaxAllowedMegaBytes;

            }

            grdClientAccounts.AutoResizeColumns();

            if (_clientaccountlist.Count > 0)
            {
                _selectedusername = _clientaccountlist[0].Username;
                _selectedindex = 0;
            }

            _gridrefreshing = false;
        }


        private int GetSelectedIndex(string username)
        {
            for (int c = 0; c < _clientaccountlist.Count; c++)
            {
                CommsPacket.ClientAccount account = (CommsPacket.ClientAccount)_clientaccountlist[c];

                if (account.Username.ToUpper() == username.ToUpper())
                {
                    return c;
                }
            }

            return -1;
        }

        private void btnNewAccount_Click(object sender, EventArgs e)
        {
            ClientAccountDetails accountdialog = new ClientAccountDetails();

            accountdialog.OnNewClientAccountEntered += new ClientAccountDetails.NewClientAccountEntered(accountdialog_OnNewClientAccountEntered);
            accountdialog.ShowDialog();

        }

        void accountdialog_OnNewClientAccountEntered(CommsPacket.ClientAccount newclientaccount)
        {
            //throw new NotImplementedException();
            if (OnNewClientAccountCreated != null)
            {
                OnNewClientAccountCreated(newclientaccount);
            }
        }

        private void btnEditAccount_Click(object sender, EventArgs e)
        {
            if (_selectedindex >= 0)
            {
                ClientAccountDetails accountdialog = new ClientAccountDetails();
                accountdialog.SetClientAccountDetails(_clientaccountlist[_selectedindex]);
                accountdialog.OnClientAccountChanged += new ClientAccountDetails.ClientAccountChanged(accountdialog_OnClientAccountChanged);
                accountdialog.ShowDialog();
            }

        }

        void accountdialog_OnClientAccountChanged(CommsPacket.ClientAccount newclientaccount)
        {
            //throw new NotImplementedException();
            if (OnExistingClientAccountChanged != null)
            {
                OnExistingClientAccountChanged(newclientaccount);
            }
        }

        private void btnChangePassword_Click(object sender, EventArgs e)
        {
            ChangeAdminPassword newpassworddialog = new ChangeAdminPassword();

            if (_selectedindex >= 0)
            {
                newpassworddialog.SetClientAccountDetails(_clientaccountlist[_selectedindex]);
                newpassworddialog.OnPasswordChanged += new ChangeAdminPassword.PasswordChanged(newpassworddialog_OnPasswordChanged);
                newpassworddialog.ShowDialog();
            }
        }

        void newpassworddialog_OnPasswordChanged(string adminusername, string newpasswordhash)
        {
            // The admin username passed back to us is really the client username because we set the client account details above.
            if (OnClientAccountPasswordChanged != null)
            {
                OnClientAccountPasswordChanged(adminusername, newpasswordhash);
            }
        }

        private void btnDeleteAccount_Click(object sender, EventArgs e)
        {
            if (_selectedindex >= 0)
            {
                if (MessageBox.Show("Are you sure you wish to delete the Client Account: " + _selectedusername + " ?", "Delete Account?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (OnClientAccountDeleted != null)
                    {
                        OnClientAccountDeleted(_selectedusername);
                    }
                }
            }
        }

        private void grdClientAccounts_CurrentCellChanged(object sender, EventArgs e)
        {
            if (_gridrefreshing == false)
            {
                if (grdClientAccounts.CurrentRow.Cells[0].Value != null)
                {
                    _selectedusername = grdClientAccounts.CurrentRow.Cells[0].Value.ToString();
                    _selectedindex = GetSelectedIndex(_selectedusername);
                }
            }
        }


    }
}
