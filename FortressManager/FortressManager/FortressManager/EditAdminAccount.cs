﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace FortressManager
{
    public partial class EditAdminAccount : Form
    {
        public delegate void AccountChanged(CommsPacket.AdminAccount newadminaccount);
        public event AccountChanged OnAccountChanged;
        public string _existingpasswordhash;

        public EditAdminAccount()
        {
            InitializeComponent();
        }

        public void SetAccountDetails(CommsPacket.AdminAccount existingaccount)
        {
            _existingpasswordhash = existingaccount.PasswordHash;
            txtUsername.Text = existingaccount.Username;
            txtPassword.Text = existingaccount.PasswordHash;
            txtFirstname.Text = existingaccount.Firstname;
            txtLastname.Text = existingaccount.Lastname;
            txtDescription.Text = existingaccount.Description;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (OnAccountChanged != null)
            {

                CommsPacket.AdminAccount newaccount = new CommsPacket.AdminAccount();

                newaccount.Username = txtUsername.Text;

                if (txtPassword.Text == _existingpasswordhash)
                {
                    newaccount.PasswordHash = _existingpasswordhash;
                }
                else
                {
                    newaccount.PasswordHash = CommonFunctions.CreateMD5HashFromString(txtPassword.Text);
                }

                newaccount.Firstname = txtFirstname.Text;
                newaccount.Lastname = txtLastname.Text;
                newaccount.Description = txtDescription.Text;

                OnAccountChanged(newaccount);
            }

            Hide();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Hide();
        }
    }
}
