﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace FortressManager
{
    public partial class ServerLogon : Form
    {
        public delegate void ServerConnect(ServerDetails logondetails, bool savedetails);
        public event ServerConnect OnServerConnect;
        public bool _passwordishash = false;

        public ServerLogon()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Hide ();
        }

        public void SetServerDetails(ServerDetails details)
        {
            txtServer.Text = details.ServerName;
            txtPort.Text = details.Port;
            txtUsername.Text = details.Username;
            txtPassword.Text = details.Password;
            _passwordishash = true;
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (OnServerConnect != null)
            {
                ServerDetails logondetails = new ServerDetails();
                logondetails.ServerName = txtServer.Text;
                logondetails.Port = txtPort.Text;
                logondetails.Username = txtUsername.Text;

                if (_passwordishash)
                {
                    logondetails.Password = txtPassword.Text;
                }
                else
                {
                    logondetails.Password = CommonFunctions.CreateMD5HashFromString(txtPassword.Text);
                }
                

                OnServerConnect(logondetails, chkSavelogin.Checked);
            }

            Hide();
        }

        private void txtPassword_TextChanged(object sender, EventArgs e)
        {
            _passwordishash = false;
        }
    }

    [Serializable]
    public class ServerDetails
    {
        public string ServerName;
        public string Port;
        public string Username;
        public string Password;
    }
}
