﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace FortressManager
{
    public partial class NewAdminAccount : Form
    {
        public delegate void NewAccountEntered(CommsPacket.AdminAccount newadminaccount);
        public event NewAccountEntered OnNewAccountEntered;

        public NewAdminAccount()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (OnNewAccountEntered != null)
            {

                CommsPacket.AdminAccount newaccount = new CommsPacket.AdminAccount();

                newaccount.Username = txtUsername.Text;
                newaccount.PasswordHash = CommonFunctions.CreateMD5HashFromString(txtPassword.Text);
                newaccount.Firstname = txtFirstname.Text;
                newaccount.Lastname = txtLastname.Text;
                newaccount.Description = txtDescription.Text;

                OnNewAccountEntered(newaccount);

            }
            Hide();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Hide();
        }
    }
}
