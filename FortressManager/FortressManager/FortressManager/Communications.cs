﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.IO;
using System.ComponentModel;
using System.Runtime.Serialization.Formatters.Binary;

namespace FortressManager
{
    public class Communications
    {
        private TcpClient _tcpClient = new TcpClient();
        NetworkStream _networkStream = null;
        BackgroundWorker _filesendworker = new BackgroundWorker();

        public delegate void SendProgressUpdate(SendWorkerProgress progressinfo);
        public event SendProgressUpdate OnSendProgressUpdate;

        public delegate void SendCompleted();
        public event SendCompleted OnSendCompleted;

        // Receive large data variables
        const long _maxbuffersize = 2048000;
        MemoryStream memStream;
        BinaryWriter binBuffer;
        private byte[] bytes; 		// Data buffer for incoming data.
        CommsPacket.CommsPacket _largereceivedobject = null;

        //private const int _portNum = 10116;

        public Communications()
        {
            _filesendworker.WorkerSupportsCancellation = true;
            _filesendworker.WorkerReportsProgress = true;

            _filesendworker.ProgressChanged += new ProgressChangedEventHandler(_filesendworker_ProgressChanged);
            _filesendworker.DoWork += new DoWorkEventHandler(_filesendworker_DoWork);
            _filesendworker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(_filesendworker_RunWorkerCompleted);

        }

        void _filesendworker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (OnSendCompleted != null)
            {
                OnSendCompleted();
            }
        }

        void _filesendworker_DoWork(object sender, DoWorkEventArgs e)
        {
            SendWorkerParams workerparams = (SendWorkerParams)e.Argument;

            SendFile(workerparams.sourcepath, workerparams.destpath, _filesendworker);
        }

        void _filesendworker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            SendWorkerProgress workerprogress = (SendWorkerProgress)e.UserState;

            if (OnSendProgressUpdate != null)
            {
                OnSendProgressUpdate(workerprogress);
            }
            //throw new NotImplementedException();
        }

        public void SendFileAsync(string filepath, string destfilepath)
        {
            SendWorkerParams sendparams = new SendWorkerParams();
            sendparams.sourcepath = filepath;
            sendparams.destpath = destfilepath;

            if (_filesendworker.IsBusy == false)
            {
                _filesendworker.RunWorkerAsync(sendparams);
            }
            else
            {
                throw new Exception("There is already a file send operation in progress. Please wait for that to finish before starting another file send.");
            }
        }

        public bool Connect(string serveraddress, int portnum, out string reason)
        {
            try
            {
                _tcpClient = new TcpClient();
                _tcpClient.Connect(serveraddress, portnum);

                _networkStream = _tcpClient.GetStream();

                if (_networkStream.CanWrite && _networkStream.CanRead)
                {
                    reason = "Connection Successful";
                    return true;
                }
                else
                {
                    reason = "Cannot write or read from network stream.";
                    _tcpClient.Close();
                    return false;
                }

            }
            catch (SocketException sockex)
            {
                reason = sockex.Message;
                return false;
            }
            catch (System.IO.IOException ioex)
            {
                reason = ioex.Message;
                return false;
            }
            catch (Exception e)
            {
                reason = e.Message;
                return false;
            }
        }

        public void Disconnect()
        {
            try
            {
                _networkStream.Close();
                _tcpClient.Close();
                //_tcpClient.Close();
            }
            catch (Exception ex)
            {

            }
        }

        private void SendFile(string filepath, string destfilepath, BackgroundWorker bw)
        {
            byte[] chunkbuffer;

            FileStream fileStream = new FileStream(filepath, FileMode.Open, FileAccess.Read);

            //FileStream writeStream = new FileStream(filepath + "write", FileMode.Append, FileAccess.Write);

            int chunksize = 262144;

            long numchunks = fileStream.Length / chunksize;

            long calcsize = numchunks * chunksize;

            if (fileStream.Length > calcsize)
            {
                numchunks++;
            }

            long filepointer = 0;
            long workingchunksize = 0;

            if (PrepareServerForFileTransfer("danny", new Guid().ToString(), Path.GetFileName(filepath), fileStream.Length, destfilepath) == false)
            {
                throw new Exception("Unable to prepare server for file transfer");
            }

            for (int a = 0; a < numchunks; a++)
            {

                if ((fileStream.Length - filepointer) >= chunksize)
                {
                    workingchunksize = chunksize;
                    chunkbuffer = new byte[workingchunksize];

                    fileStream.Read(chunkbuffer, 0, (int)workingchunksize);
                }
                else
                {
                    long lastchunksize = fileStream.Length - filepointer;
                    workingchunksize = lastchunksize;
                    chunkbuffer = new byte[lastchunksize];

                    fileStream.Read(chunkbuffer, 0, (int)workingchunksize);
                }


                //writeStream.Write(chunkbuffer, 0, chunkbuffer.Length);
                SendFileChunk(chunkbuffer, filepath, fileStream.Length, "danny", "someguid");

                filepointer += workingchunksize;

                //System.Threading.Thread.Sleep(1000);
                SendWorkerProgress workerprogress = new SendWorkerProgress();
                workerprogress.sourcepath = filepath;
                workerprogress.bytessent = filepointer;
                workerprogress.totalbytes = fileStream.Length;
                workerprogress.bytesremaining = (fileStream.Length - filepointer);
                int percentprogress = (a * 100) / (int)numchunks;
                workerprogress.percentcompleted = percentprogress;

                bw.ReportProgress(percentprogress, workerprogress);

                System.Diagnostics.Debug.WriteLine("File Chunk has been sent. Filepointer: " + filepointer.ToString() + " File Length: " + fileStream.Length.ToString());

            }

            //writeStream.Close();
        }



        public DateTime GetServerFileLastWriteTime(string accountname, string usertoken, string strpath)
        {
            // NOTE: strpath needs to looklike $$$ACCOUNTROOT$$$\SomeFolder\SubFolder\File.txt

            CommsPacket.CommsPacket myObject = new CommsPacket.CommsPacket();
            myObject.iMessagetype = (int)CommsPacket.MessageTypes.LastWriteTime;
            myObject.filename = strpath;
            myObject.useraccount = accountname;
            myObject.usertoken = usertoken;

            if (_networkStream != null)
            {
                Byte[] sendBytes = myObject.Serialize();
                Byte[] encbytes = myObject.Encapsulate(sendBytes);

                _networkStream.Write(encbytes, 0, encbytes.Length);

                //Read the reply
                byte[] bytes = new byte[_tcpClient.ReceiveBufferSize];
                int BytesRead = _networkStream.Read(bytes, 0, (int)_tcpClient.ReceiveBufferSize);

                CommsPacket.CommsResponse response = new CommsPacket.CommsResponse();
                response = response.DeSerialize(bytes);

                if (response.Message.Contains("I received the packet") == false)
                {
                    throw new Exception("Did not receive packet acknowledgement");
                }

                return response.LastWriteTime;

            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
        }

        public bool SendBytes(byte[] bytes)
        {
            if (_networkStream != null)
            {
                _networkStream.Write(bytes, 0, bytes.Length);
                return true;
            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
        }

        public long GetServerFileLength(string accountname, string usertoken, string strpath)
        {
            // NOTE: strpath needs to looklike $$$ACCOUNTROOT$$$\SomeFolder\SubFolder\File.txt

            CommsPacket.CommsPacket myObject = new CommsPacket.CommsPacket();
            myObject.iMessagetype = (int)CommsPacket.MessageTypes.FileLength;
            myObject.filename = strpath;
            myObject.useraccount = accountname;
            myObject.usertoken = usertoken;

            if (_networkStream != null)
            {
                Byte[] sendBytes = myObject.Serialize();
                Byte[] encbytes = myObject.Encapsulate(sendBytes);

                _networkStream.Write(encbytes, 0, encbytes.Length);

                //Read the reply
                byte[] bytes = new byte[_tcpClient.ReceiveBufferSize];
                int BytesRead = _networkStream.Read(bytes, 0, (int)_tcpClient.ReceiveBufferSize);

                CommsPacket.CommsResponse response = new CommsPacket.CommsResponse();
                response = response.DeSerialize(bytes);

                if (response.Message.Contains("I received the packet") == false)
                {
                    throw new Exception("Did not receive packet acknowledgement");
                }

                return response.FileLength;
            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
        }

        public bool AuthenticateAdminAccount(string accountname, string passwordhash, out string authtoken)
        {
            CommsPacket.CommsPacket myObject = new CommsPacket.CommsPacket();
            myObject.iMessagetype = (int)CommsPacket.MessageTypes.AuthenticateAdmin;
            myObject.useraccount = accountname;
            myObject.passwordhash = passwordhash;

            if (_networkStream != null)
            {
                Byte[] sendBytes = myObject.Serialize();
                Byte[] encbytes = myObject.Encapsulate(sendBytes);

                _networkStream.Write(encbytes, 0, encbytes.Length);

                //Read the reply
                byte[] bytes = new byte[_tcpClient.ReceiveBufferSize];
                int BytesRead = _networkStream.Read(bytes, 0, (int)_tcpClient.ReceiveBufferSize);

                CommsPacket.CommsResponse response = new CommsPacket.CommsResponse();
                response = response.DeSerialize(bytes);

                if (response.Message.Contains("I received the packet") == false)
                {
                    throw new Exception("Did not receive packet acknowledgement");
                }

                if (response.authenticated == true)
                {
                    authtoken = response.token;
                    return true;
                }
                else
                {
                    authtoken = "";
                    return false;
                }
            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
        }

        public bool SignatureValid(int sig1, int sig2)
        {
            if (sig1 == (int)CommsPacket.MessageTypes.LinuxSignatureOne  && sig2 == (int)CommsPacket.MessageTypes.LinuxSignatureTwo)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool AuthenticateAdminAccount_Linux(string accountname, string passwordhash, out string authtoken)
        {
            int signatureone = (int)CommsPacket.MessageTypes.LinuxSignatureOne;
            int signaturetwo = (int)CommsPacket.MessageTypes.LinuxSignatureTwo;
            int messageidentifier = (int)CommsPacket.MessageTypes.AuthenticateAdmin_Linux;
            int bulkmessagelength = sizeof(int) + sizeof(int) + accountname.Length + sizeof(int) + passwordhash.Length;

            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms);
            UTF8Encoding enc = new UTF8Encoding();            

            bw.Write(signatureone);
            bw.Write(signaturetwo);
            bw.Write(bulkmessagelength);
            bw.Write(messageidentifier);
            bw.Write(accountname.Length);
            bw.Write(enc.GetBytes (accountname));
            bw.Write(passwordhash.Length);
            bw.Write(enc.GetBytes(passwordhash));

            SendBytes(ms.ToArray());

            //Read the reply
            byte[] bytes = new byte[_tcpClient.ReceiveBufferSize];
            int BytesRead = _networkStream.Read(bytes, 0, (int)_tcpClient.ReceiveBufferSize);

            int ipointer = 0;

            int sig1 = BitConverter.ToInt32(bytes, ipointer);
            ipointer += sizeof(int);

            int sig2 = BitConverter.ToInt32(bytes, ipointer);
            ipointer += sizeof(int);

            int msgident = BitConverter.ToInt32(bytes, ipointer);
            ipointer += sizeof(int);

            if (SignatureValid(sig1, sig2))
            {
                if (msgident == (int)CommsPacket.MessageTypes.LinuxAuthAdminOk)
                {
                    authtoken = "";
                    return true;
                }

                if (msgident == (int)CommsPacket.MessageTypes.LinuxAuthAdminDenied)
                {
                    authtoken = "";
                    return false;
                }
            }

            authtoken = "";

            //FileStream fs = new FileStream("C:\\Temp\\AuthAdminLinux.dat", FileMode.CreateNew);
            //fs.Write(ms.ToArray (), 0, (int)ms.Length);
            //fs.Close();

            
            // If we got here then the message identifier returned from the server did not match or the signatures did not match
            return false;
        }

        public List<CommsPacket.AdminAccount> ListAdminAccounts(string adminaccountname, string authtoken)
        {
            if (_networkStream != null)
            {
                CommsPacket.CommsPacket myObject = new CommsPacket.CommsPacket();

                myObject.iMessagetype = (int)CommsPacket.MessageTypes.ListAdminAccounts;

                myObject.Somemessage = "ListAdminAccounts";
                myObject.somethingelse = "ListAdminAccounts";

                byte[] sendbytes = myObject.Serialize();
                byte[] encbytes = myObject.Encapsulate(sendbytes);

                _networkStream.Write(encbytes, 0, encbytes.Length);

                // Now process the large bit of data we're gonna receive
                BeginReceivingLargeData();

                // By the time we get to here our _largereceivedobject should be populated with stuff

                if (_largereceivedobject != null)
                {
                    //System.Windows.Forms.MessageBox.Show("Large Buffer Length: " + _largereceivedobject.somebuffer.Length.ToString());

                    BinaryFormatter bin = new BinaryFormatter();
                    MemoryStream mem = new MemoryStream();

                    mem.Write(_largereceivedobject.somebuffer, 0, _largereceivedobject.somebuffer.Length);
                    mem.Seek(0, 0);

                    return (List<CommsPacket.AdminAccount>)bin.Deserialize(mem);
                }
                else
                {
                    throw new Exception("Large Received Object was NULL, when listing Admin Accounts.");
                }
            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
        }

        private void SaveBytesToFile(string filename, byte[] bytearray)
        {
            FileStream fs = new FileStream(filename, FileMode.Create, FileAccess.Write);
            fs.Write(bytearray, 0, bytearray.Length);
            fs.Close();
        }

        private byte[] GetZeroTerminatedByteString(byte[] inputbytes, int offset, int length)
        {
            List<byte> resultbytes = new List<byte>();

            for (int b = offset; b < (offset + length); b++)
            {
                if (inputbytes[b] != 0)
                {
                    resultbytes.Add(inputbytes[b]);
                }
                else
                {
                    break;
                }
            }

            return resultbytes.ToArray();
        }

        public List<CommsPacket.AdminAccount> ListAdminAccounts_Linux(string adminaccountname, string authtoken)
        {
            if (_networkStream != null)
            {
                int signatureone = (int)CommsPacket.MessageTypes.LinuxSignatureOne;
                int signaturetwo = (int)CommsPacket.MessageTypes.LinuxSignatureTwo;
                int messageidentifier = (int)CommsPacket.MessageTypes.LinuxListAdminAccounts;
                int bulkmessagelength = sizeof (int);
                List<CommsPacket.AdminAccount> adminaccountlist = new List<CommsPacket.AdminAccount>();


                MemoryStream ms = new MemoryStream();
                BinaryWriter bw = new BinaryWriter(ms);
                UTF8Encoding enc = new UTF8Encoding();

                bw.Write(signatureone);
                bw.Write(signaturetwo);
                bw.Write(bulkmessagelength);
                bw.Write(messageidentifier);

                SendBytes(ms.ToArray());

                //Read the reply
                BeginReceivingLargeData_Linux();

                /*
                byte[] bytes = new byte[_tcpClient.ReceiveBufferSize];
                int BytesRead = _networkStream.Read(bytes, 0, (int)_tcpClient.ReceiveBufferSize);

                 */


                byte[] membuffer = memStream.GetBuffer(); // This buffer will contain more bytes than we expect, as it pads the end out with zero bytes.
                byte[] trimmedbuffer = new byte[memStream.Length];
                Array.Copy(membuffer, trimmedbuffer, memStream.Length);

                //SaveBytesToFile("C:\\Temp\\AdminAccounts_Linux.dat", membuffer);

                int ipointer = 0;

                int sig1 = BitConverter.ToInt32(membuffer, ipointer);
                ipointer += sizeof(int);

                int sig2 = BitConverter.ToInt32(membuffer, ipointer);
                ipointer += sizeof(int);

                int msgident = BitConverter.ToInt32(membuffer, ipointer);
                ipointer += sizeof(int);

                if (SignatureValid(sig1, sig2))
                {
                    if (msgident == (int)CommsPacket.MessageTypes.LinuxAdminAccountList)
                    {
                        // Manually deserialize the buffer into a list of admin account objects we can return
                        while (ipointer <= (membuffer.Length - CommsPacket.CommsPacket.GetLinuxAdminAccountSize()))
                        {
                            int accsig1 = BitConverter.ToInt32(membuffer, ipointer);
                            ipointer += sizeof(int);

                            int accsig2 = BitConverter.ToInt32(membuffer, ipointer);
                            ipointer += sizeof(int);

                            if (SignatureValid(accsig1, accsig2))
                            {
                                CommsPacket.AdminAccount newaccount = new CommsPacket.AdminAccount();

                                //string strFirstname = enc.GetString(membuffer, ipointer, (int)CommsPacket.MessageTypes.LinuxSizeString);

                                string strFirstname = enc.GetString(GetZeroTerminatedByteString(membuffer, ipointer, (int)CommsPacket.MessageTypes.LinuxSizeString));
                                ipointer += (int)CommsPacket.MessageTypes.LinuxSizeString;

                                string strLastname = enc.GetString(GetZeroTerminatedByteString(membuffer, ipointer, (int)CommsPacket.MessageTypes.LinuxSizeString));
                                ipointer += (int)CommsPacket.MessageTypes.LinuxSizeString;

                                string strPasswordhash = enc.GetString(GetZeroTerminatedByteString(membuffer, ipointer, (int)CommsPacket.MessageTypes.LinuxSizeString));
                                ipointer += (int)CommsPacket.MessageTypes.LinuxSizeString;

                                string strUsername = enc.GetString(GetZeroTerminatedByteString(membuffer, ipointer, (int)CommsPacket.MessageTypes.LinuxSizeString));
                                ipointer += (int)CommsPacket.MessageTypes.LinuxSizeString;

                                string strDescription = enc.GetString(GetZeroTerminatedByteString(membuffer, ipointer, (int)CommsPacket.MessageTypes.LinuxSizeString));
                                ipointer += (int)CommsPacket.MessageTypes.LinuxSizeString;

                                newaccount.Firstname = strFirstname;
                                newaccount.Lastname = strLastname;
                                newaccount.PasswordHash = strPasswordhash;
                                newaccount.Username = strUsername;
                                newaccount.Description = strDescription;

                                adminaccountlist.Add(newaccount);
                            }
                        }

                        if (adminaccountlist.Count == 0)
                        {
                            throw new Exception("Admin account list is empty!");
                        }

                        return adminaccountlist;
                    }
                    else
                    {
                        throw new Exception("Message identifier did not match what was expected.");
                    }
                }
                else
                {
                    //throw new Exception("Signature was not valid!");
                    System.Diagnostics.Debug.WriteLine("About to do a recursive call of ListAdminAccounts_Linux - signature was not valid.");
                    return ListAdminAccounts_Linux(adminaccountname, authtoken);
                }


                // If we got here then the message identifier returned from the server did not match or the signatures did not match
                return adminaccountlist; // Will return an empty list
            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
        }

        public List<CommsPacket.ClientAccount> ListClientAccounts_Linux(string adminaccountname, string authtoken)
        {
            if (_networkStream != null)
            {
                int signatureone = (int)CommsPacket.MessageTypes.LinuxSignatureOne;
                int signaturetwo = (int)CommsPacket.MessageTypes.LinuxSignatureTwo;
                int messageidentifier = (int)CommsPacket.MessageTypes.LinuxListClientAccounts;
                int bulkmessagelength = sizeof(int);
                List<CommsPacket.ClientAccount> clientaccountlist = new List<CommsPacket.ClientAccount>();


                MemoryStream ms = new MemoryStream();
                BinaryWriter bw = new BinaryWriter(ms);
                UTF8Encoding enc = new UTF8Encoding();

                bw.Write(signatureone);
                bw.Write(signaturetwo);
                bw.Write(bulkmessagelength);
                bw.Write(messageidentifier);

                SendBytes(ms.ToArray());

                //Read the reply
                BeginReceivingLargeData_Linux();

                /*
                byte[] bytes = new byte[_tcpClient.ReceiveBufferSize];
                int BytesRead = _networkStream.Read(bytes, 0, (int)_tcpClient.ReceiveBufferSize);

                 */


                byte[] membuffer = memStream.GetBuffer(); // This buffer will contain more bytes than we expect, as it pads the end out with zero bytes.
                byte[] trimmedbuffer = new byte[memStream.Length];
                Array.Copy(membuffer, trimmedbuffer, memStream.Length);

                //SaveBytesToFile("C:\\Temp\\AdminAccounts_Linux.dat", membuffer);

                int ipointer = 0;

                int sig1 = BitConverter.ToInt32(membuffer, ipointer);
                ipointer += sizeof(int);

                int sig2 = BitConverter.ToInt32(membuffer, ipointer);
                ipointer += sizeof(int);

                int msgident = BitConverter.ToInt32(membuffer, ipointer);
                ipointer += sizeof(int);

                if (SignatureValid(sig1, sig2))
                {
                    if (msgident == (int)CommsPacket.MessageTypes.LinuxClientAccountList)
                    {
                        // Manually deserialize the buffer into a list of admin account objects we can return
                        while (ipointer <= (membuffer.Length - CommsPacket.CommsPacket.GetLinuxClientAccountSize()))
                        {
                            int accsig1 = BitConverter.ToInt32(membuffer, ipointer);
                            ipointer += sizeof(int);

                            int accsig2 = BitConverter.ToInt32(membuffer, ipointer);
                            ipointer += sizeof(int);

                            if (SignatureValid(accsig1, accsig2))
                            {
                                CommsPacket.ClientAccount newaccount = new CommsPacket.ClientAccount();

                                //string strFirstname = enc.GetString(membuffer, ipointer, (int)CommsPacket.MessageTypes.LinuxSizeString);

                                string strCompany = enc.GetString(GetZeroTerminatedByteString(membuffer, ipointer, (int)CommsPacket.MessageTypes.LinuxSizeString));
                                ipointer += (int)CommsPacket.MessageTypes.LinuxSizeString;

                                string strDescription = enc.GetString(GetZeroTerminatedByteString(membuffer, ipointer, (int)CommsPacket.MessageTypes.LinuxSizeString));
                                ipointer += (int)CommsPacket.MessageTypes.LinuxSizeString;

                                string strFirstname = enc.GetString(GetZeroTerminatedByteString(membuffer, ipointer, (int)CommsPacket.MessageTypes.LinuxSizeString));
                                ipointer += (int)CommsPacket.MessageTypes.LinuxSizeString;

                                string strLastname = enc.GetString(GetZeroTerminatedByteString(membuffer, ipointer, (int)CommsPacket.MessageTypes.LinuxSizeString));
                                ipointer += (int)CommsPacket.MessageTypes.LinuxSizeString;

                                int maxallowedmb = BitConverter.ToInt32(membuffer, ipointer);
                                ipointer += sizeof(int);

                                string strPasswordhash = enc.GetString(GetZeroTerminatedByteString(membuffer, ipointer, (int)CommsPacket.MessageTypes.LinuxSizeString));
                                ipointer += (int)CommsPacket.MessageTypes.LinuxSizeString;

                                string strRootpath = enc.GetString(GetZeroTerminatedByteString(membuffer, ipointer, (int)CommsPacket.MessageTypes.LinuxSizeString));
                                ipointer += (int)CommsPacket.MessageTypes.LinuxSizeString;

                                string strUsername = enc.GetString(GetZeroTerminatedByteString(membuffer, ipointer, (int)CommsPacket.MessageTypes.LinuxSizeString));
                                ipointer += (int)CommsPacket.MessageTypes.LinuxSizeString;

                                newaccount.Company = strCompany;
                                newaccount.Description = strDescription;
                                newaccount.Firstname = strFirstname;
                                newaccount.Lastname = strLastname;
                                newaccount.MaxAllowedMegaBytes = maxallowedmb;
                                newaccount.PasswordHash = strPasswordhash;
                                newaccount.RootPath = strRootpath;
                                newaccount.Username = strUsername;

                                clientaccountlist.Add(newaccount);
                            }
                        }

                        if (clientaccountlist.Count == 0)
                        {
                            throw new Exception("Client Account list was empty!");
                        }

                        return clientaccountlist;
                    }
                    else
                    {
                        throw new Exception("Message Identifier did not match what was expected.");
                    }
                }
                else
                {
                    //throw new Exception("Received signature was not valid.");
                    // Try and request the list again 
                    System.Diagnostics.Debug.WriteLine("About to do a recursive call of ListClientAccounts_Linux - signature was not valid.");
                    return ListClientAccounts_Linux(adminaccountname, authtoken);
                }


                // If we got here then the message identifier returned from the server did not match or the signatures did not match
                return clientaccountlist; // Will return an empty list
            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
        }

        public List<CommsPacket.ClientAccount> ListClientAccounts(string clientaccountname, string authtoken)
        {
            if (_networkStream != null)
            {
                CommsPacket.CommsPacket myObject = new CommsPacket.CommsPacket();

                myObject.iMessagetype = (int)CommsPacket.MessageTypes.ListClientAccounts;

                myObject.Somemessage = "ListClientAccounts";
                myObject.somethingelse = "ListClientAccounts";

                byte[] sendbytes = myObject.Serialize();
                byte[] encbytes = myObject.Encapsulate(sendbytes);

                _networkStream.Write(encbytes, 0, encbytes.Length);

                // Now process the large bit of data we're gonna receive
                BeginReceivingLargeData();

                // By the time we get to here our _largereceivedobject should be populated with stuff

                if (_largereceivedobject != null)
                {
                    //System.Windows.Forms.MessageBox.Show("Large Buffer Length: " + _largereceivedobject.somebuffer.Length.ToString());

                    BinaryFormatter bin = new BinaryFormatter();
                    MemoryStream mem = new MemoryStream();

                    mem.Write(_largereceivedobject.somebuffer, 0, _largereceivedobject.somebuffer.Length);
                    mem.Seek(0, 0);

                    return (List<CommsPacket.ClientAccount>)bin.Deserialize(mem);
                }
                else
                {
                    throw new Exception("Large Received Object was NULL, when listing Client Accounts.");
                }
            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
        }

        public bool DoesServerFileExist(string accountname, string usertoken, string strpath)
        {
            // NOTE: strpath needs to looklike $$$ACCOUNTROOT$$$\SomeFolder\SubFolder\File.txt

            CommsPacket.CommsPacket myObject = new CommsPacket.CommsPacket();
            myObject.iMessagetype = (int)CommsPacket.MessageTypes.FileExists;
            myObject.filename = strpath;
            myObject.useraccount = accountname;
            myObject.usertoken = usertoken;

            if (_networkStream != null)
            {
                Byte[] sendBytes = myObject.Serialize();
                Byte[] encbytes = myObject.Encapsulate(sendBytes);

                _networkStream.Write(encbytes, 0, encbytes.Length);

                //Read the reply
                byte[] bytes = new byte[_tcpClient.ReceiveBufferSize];
                int BytesRead = _networkStream.Read(bytes, 0, (int)_tcpClient.ReceiveBufferSize);

                CommsPacket.CommsResponse response = new CommsPacket.CommsResponse();
                response = response.DeSerialize(bytes);

                if (response.Message.Contains("I received the packet") == false)
                {
                    throw new Exception("Did not receive packet acknowledgement");
                }

                if (response.FileExists == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
        }



        public bool PrepareServerForFileTransfer(string useraccountname, string usertoken, string localfilename, long filelength, string destfilepath)
        {
            CommsPacket.CommsPacket myObject = new CommsPacket.CommsPacket();
            myObject.iMessagetype = (int)CommsPacket.MessageTypes.PrepareServerForFileTransfer;
            myObject.filename = Path.GetFileName(localfilename);
            myObject.filelength = filelength;
            myObject.sourcepath = localfilename;
            myObject.destpath = destfilepath;
            myObject.useraccount = useraccountname;
            myObject.usertoken = usertoken;


            myObject.Somemessage = "PrepareFileTransfer";
            myObject.somethingelse = "PrepareFileTransfer";
            myObject.somebuffer = new byte[10];

            if (_networkStream != null)
            {
                Byte[] sendBytes = myObject.Serialize();
                Byte[] encbytes = myObject.Encapsulate(sendBytes);

                _networkStream.Write(encbytes, 0, encbytes.Length);


                // Reads the NetworkStream into a byte buffer.
                byte[] bytes = new byte[_tcpClient.ReceiveBufferSize];
                int BytesRead = _networkStream.Read(bytes, 0, (int)_tcpClient.ReceiveBufferSize);

                CommsPacket.CommsResponse response = new CommsPacket.CommsResponse();
                response = response.DeSerialize(bytes);


                if (response.Message.Contains("I received the packet") == false)
                {
                    throw new Exception("Did not receive packet acknowledgement");
                }

                return true;
            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
        }

        public bool SendFileChunk(byte[] chunk, string filepath, long filelength, string useraccount, string usertoken)
        {
            if (_networkStream != null)
            {
                CommsPacket.CommsPacket myObject = new CommsPacket.CommsPacket();

                myObject.iMessagetype = (int)CommsPacket.MessageTypes.FileChunk;
                myObject.filename = Path.GetFileName(filepath);
                myObject.sourcepath = filepath;
                myObject.filelength = filelength;
                myObject.useraccount = useraccount;
                myObject.usertoken = usertoken;

                myObject.Somemessage = "FileChunk";
                myObject.somethingelse = "FileChunk";
                myObject.somebuffer = chunk;

                byte[] sendbytes = myObject.Serialize();
                byte[] encbytes = myObject.Encapsulate(sendbytes);

                _networkStream.Write(encbytes, 0, encbytes.Length);

                // Reads the NetworkStream into a byte buffer.
                byte[] bytes = new byte[_tcpClient.ReceiveBufferSize];
                int BytesRead = _networkStream.Read(bytes, 0, (int)_tcpClient.ReceiveBufferSize);

                CommsPacket.CommsResponse response = new CommsPacket.CommsResponse();
                response = response.DeSerialize(bytes);

                if (response.Message.Contains("I received the packet") == false)
                {
                    throw new Exception("Did not receive packet acknowledgement");
                }

                return true;
            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
        }


        public bool SendNewAdminAccount(CommsPacket.AdminAccount newadminaccount, string adminusername, string authtoken)
        {
            if (_networkStream != null)
            {
                CommsPacket.CommsPacket myObject = new CommsPacket.CommsPacket();

                myObject.iMessagetype = (int)CommsPacket.MessageTypes.NewAdminAccount;
                myObject.useraccount = adminusername;
                myObject.usertoken = authtoken;

                myObject.Somemessage = "NewAdminAccount";
                myObject.somethingelse = "NewAdminAccount";
                myObject.somebuffer = CommonFunctions.ObjectToByteArray(newadminaccount, typeof(CommsPacket.AdminAccount));

                byte[] sendbytes = myObject.Serialize();
                byte[] encbytes = myObject.Encapsulate(sendbytes);

                _networkStream.Write(encbytes, 0, encbytes.Length);

                // Reads the NetworkStream into a byte buffer.
                byte[] bytes = new byte[_tcpClient.ReceiveBufferSize];
                int BytesRead = _networkStream.Read(bytes, 0, (int)_tcpClient.ReceiveBufferSize);

                CommsPacket.CommsResponse response = new CommsPacket.CommsResponse();
                response = response.DeSerialize(bytes);

                if (response.Message.Contains("I received the packet") == false)
                {
                    throw new Exception("Did not receive packet acknowledgement");
                }

                return true;
            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
        }

        public bool SendNewAdminAccount_Linux(CommsPacket.AdminAccount newadminaccount, string adminusername, string authtoken)
        {
            if (_networkStream != null)
            {
                int signatureone = (int)CommsPacket.MessageTypes.LinuxSignatureOne;
                int signaturetwo = (int)CommsPacket.MessageTypes.LinuxSignatureTwo;
                int messageidentifier = (int)CommsPacket.MessageTypes.LinuxNewAdminAccount;

                int bulkmessagelength = sizeof(int); // First int size is the message identifier

                bulkmessagelength += sizeof(int); // string length
                bulkmessagelength += newadminaccount.Firstname.Length;

                bulkmessagelength += sizeof(int); // string length
                bulkmessagelength += newadminaccount.Lastname.Length;
                
                bulkmessagelength += sizeof(int); // string length
                bulkmessagelength += newadminaccount.PasswordHash.Length;
                
                bulkmessagelength += sizeof(int); // string length
                bulkmessagelength += newadminaccount.Username.Length;
                
                bulkmessagelength += sizeof(int); // string length
                bulkmessagelength += newadminaccount.Description.Length;
                
                MemoryStream ms = new MemoryStream();
                BinaryWriter bw = new BinaryWriter(ms);
                UTF8Encoding enc = new UTF8Encoding();

                bw.Write(signatureone);
                bw.Write(signaturetwo);
                bw.Write(bulkmessagelength);
                bw.Write(messageidentifier);

                bw.Write(newadminaccount.Firstname.Length);
                bw.Write(enc.GetBytes(newadminaccount.Firstname));
                bw.Write(newadminaccount.Lastname.Length);
                bw.Write(enc.GetBytes(newadminaccount.Lastname));
                bw.Write(newadminaccount.PasswordHash.Length);
                bw.Write(enc.GetBytes(newadminaccount.PasswordHash));
                bw.Write(newadminaccount.Username.Length);
                bw.Write(enc.GetBytes(newadminaccount.Username));
                bw.Write(newadminaccount.Description.Length);
                bw.Write(enc.GetBytes(newadminaccount.Description));
                
                // Send the new client to the server
                SendBytes(ms.ToArray());

                return true;
            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
        }

        public bool SendUpdatedAdminAccount(CommsPacket.AdminAccount newadminaccount, string adminusername, string authtoken)
        {
            if (_networkStream != null)
            {
                CommsPacket.CommsPacket myObject = new CommsPacket.CommsPacket();

                myObject.iMessagetype = (int)CommsPacket.MessageTypes.UpdateAdminAccount;
                myObject.useraccount = adminusername;
                myObject.usertoken = authtoken;

                myObject.Somemessage = "UpdateAdminAccount";
                myObject.somethingelse = "UpdateAdminAccount";
                myObject.somebuffer = CommonFunctions.ObjectToByteArray(newadminaccount, typeof(CommsPacket.AdminAccount));

                byte[] sendbytes = myObject.Serialize();
                byte[] encbytes = myObject.Encapsulate(sendbytes);

                _networkStream.Write(encbytes, 0, encbytes.Length);

                // Reads the NetworkStream into a byte buffer.
                byte[] bytes = new byte[_tcpClient.ReceiveBufferSize];
                int BytesRead = _networkStream.Read(bytes, 0, (int)_tcpClient.ReceiveBufferSize);

                CommsPacket.CommsResponse response = new CommsPacket.CommsResponse();
                response = response.DeSerialize(bytes);

                if (response.Message.Contains("I received the packet") == false)
                {
                    throw new Exception("Did not receive packet acknowledgement");
                }

                return true;
            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
        }

        public bool SendUpdatedAdminAccount_Linux(CommsPacket.AdminAccount newadminaccount, string adminusername, string authtoken)
        {
            if (_networkStream != null)
            {
                int signatureone = (int)CommsPacket.MessageTypes.LinuxSignatureOne;
                int signaturetwo = (int)CommsPacket.MessageTypes.LinuxSignatureTwo;
                int messageidentifier = (int)CommsPacket.MessageTypes.LinuxUpdateAdminAccount;

                int bulkmessagelength = sizeof(int); // First int size is the message identifier

                bulkmessagelength += sizeof(int); // string length
                bulkmessagelength += newadminaccount.Firstname.Length;
                bulkmessagelength += sizeof(int); // string length
                bulkmessagelength += newadminaccount.Lastname.Length;
                bulkmessagelength += sizeof(int); // string length
                bulkmessagelength += newadminaccount.PasswordHash.Length;
                bulkmessagelength += sizeof(int); // string length
                bulkmessagelength += newadminaccount.Username.Length;
                bulkmessagelength += sizeof(int); // string length
                bulkmessagelength += newadminaccount.Description.Length;

                MemoryStream ms = new MemoryStream();
                BinaryWriter bw = new BinaryWriter(ms);
                UTF8Encoding enc = new UTF8Encoding();

                bw.Write(signatureone);
                bw.Write(signaturetwo);
                bw.Write(bulkmessagelength);
                bw.Write(messageidentifier);

                bw.Write(newadminaccount.Firstname.Length);
                bw.Write(enc.GetBytes(newadminaccount.Firstname));
                bw.Write(newadminaccount.Lastname.Length);
                bw.Write(enc.GetBytes(newadminaccount.Lastname));
                bw.Write(newadminaccount.PasswordHash.Length);
                bw.Write(enc.GetBytes(newadminaccount.PasswordHash));
                bw.Write(newadminaccount.Username.Length);
                bw.Write(enc.GetBytes(newadminaccount.Username));
                bw.Write(newadminaccount.Description.Length);
                bw.Write(enc.GetBytes(newadminaccount.Description));

                // Send the new client to the server
                SendBytes(ms.ToArray());

                return true;
            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
        }

        public bool SendDeleteAdminAccount (string accountusername, string adminusername, string authtoken)
        {
            if (_networkStream != null)
            {
                CommsPacket.CommsPacket myObject = new CommsPacket.CommsPacket();

                myObject.iMessagetype = (int)CommsPacket.MessageTypes.DeleteAdminAccount;
                myObject.useraccount = adminusername;
                myObject.usertoken = authtoken;

                myObject.Somemessage = "DeleteAdminAccount";
                myObject.somethingelse = accountusername;
                
                byte[] sendbytes = myObject.Serialize();
                byte[] encbytes = myObject.Encapsulate(sendbytes);

                _networkStream.Write(encbytes, 0, encbytes.Length);

                // Reads the NetworkStream into a byte buffer.
                byte[] bytes = new byte[_tcpClient.ReceiveBufferSize];
                int BytesRead = _networkStream.Read(bytes, 0, (int)_tcpClient.ReceiveBufferSize);

                CommsPacket.CommsResponse response = new CommsPacket.CommsResponse();
                response = response.DeSerialize(bytes);

                if (response.Message.Contains("I received the packet") == false)
                {
                    throw new Exception("Did not receive packet acknowledgement");
                }

                return true;
            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
        }

        public bool SendDeleteAdminAccount_Linux(string accountusername, string adminusername, string authtoken)
        {
            if (_networkStream != null)
            {
                int signatureone = (int)CommsPacket.MessageTypes.LinuxSignatureOne;
                int signaturetwo = (int)CommsPacket.MessageTypes.LinuxSignatureTwo;
                int messageidentifier = (int)CommsPacket.MessageTypes.LinuxDeleteAdminAccount;

                int bulkmessagelength = sizeof(int); // First int size is the message identifier

                bulkmessagelength += sizeof(int); // string length
                bulkmessagelength += accountusername.Length;

                MemoryStream ms = new MemoryStream();
                BinaryWriter bw = new BinaryWriter(ms);
                UTF8Encoding enc = new UTF8Encoding();

                bw.Write(signatureone);
                bw.Write(signaturetwo);
                bw.Write(bulkmessagelength);
                bw.Write(messageidentifier);

                bw.Write(accountusername.Length);
                bw.Write(enc.GetBytes(accountusername));

                // Send the new client to the server
                SendBytes(ms.ToArray());

                return true;
            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
        }

        public bool SendChangeAdminPassword (string accountusername, string newpasswordhash, string adminusername, string authtoken)
        {
            if (_networkStream != null)
            {
                CommsPacket.CommsPacket myObject = new CommsPacket.CommsPacket();

                myObject.iMessagetype = (int)CommsPacket.MessageTypes.ChangeAdminPassword;
                myObject.useraccount = adminusername;
                myObject.usertoken = authtoken;

                myObject.Somemessage = newpasswordhash;
                myObject.somethingelse = accountusername;

                byte[] sendbytes = myObject.Serialize();
                byte[] encbytes = myObject.Encapsulate(sendbytes);

                _networkStream.Write(encbytes, 0, encbytes.Length);

                // Reads the NetworkStream into a byte buffer.
                byte[] bytes = new byte[_tcpClient.ReceiveBufferSize];
                int BytesRead = _networkStream.Read(bytes, 0, (int)_tcpClient.ReceiveBufferSize);

                CommsPacket.CommsResponse response = new CommsPacket.CommsResponse();
                response = response.DeSerialize(bytes);

                if (response.Message.Contains("I received the packet") == false)
                {
                    throw new Exception("Did not receive packet acknowledgement");
                }

                return true;
            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
        }


        public bool SendChangeAdminPassword_Linux(string accountusername, string newpasswordhash, string adminusername, string authtoken)
        {
            if (_networkStream != null)
            {
                int signatureone = (int)CommsPacket.MessageTypes.LinuxSignatureOne;
                int signaturetwo = (int)CommsPacket.MessageTypes.LinuxSignatureTwo;
                int messageidentifier = (int)CommsPacket.MessageTypes.LinuxChangeAdminPassword;

                int bulkmessagelength = sizeof(int); // First int size is the message identifier

                bulkmessagelength += sizeof(int); // string length
                bulkmessagelength += accountusername.Length;
                bulkmessagelength += sizeof(int); // string length
                bulkmessagelength += newpasswordhash.Length;

                MemoryStream ms = new MemoryStream();
                BinaryWriter bw = new BinaryWriter(ms);
                UTF8Encoding enc = new UTF8Encoding();

                bw.Write(signatureone);
                bw.Write(signaturetwo);
                bw.Write(bulkmessagelength);
                bw.Write(messageidentifier);

                bw.Write(accountusername.Length);
                bw.Write(enc.GetBytes(accountusername));

                bw.Write(newpasswordhash.Length);
                bw.Write(enc.GetBytes(newpasswordhash));

                // Send the new client to the server
                SendBytes(ms.ToArray());

                return true;
            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
        }



        public bool SendNewClientAccount(CommsPacket.ClientAccount newclientaccount, string adminusername, string authtoken)
        {
            if (_networkStream != null)
            {
                CommsPacket.CommsPacket myObject = new CommsPacket.CommsPacket();

                myObject.iMessagetype = (int)CommsPacket.MessageTypes.NewClientAccount;
                myObject.useraccount = adminusername;
                myObject.usertoken = authtoken;

                myObject.Somemessage = "NewClientAccount";
                myObject.somethingelse = "NewClientAccount";
                myObject.somebuffer = CommonFunctions.ObjectToByteArray(newclientaccount, typeof(CommsPacket.ClientAccount));

                byte[] sendbytes = myObject.Serialize();
                byte[] encbytes = myObject.Encapsulate(sendbytes);

                _networkStream.Write(encbytes, 0, encbytes.Length);

                // Reads the NetworkStream into a byte buffer.
                byte[] bytes = new byte[_tcpClient.ReceiveBufferSize];
                int BytesRead = _networkStream.Read(bytes, 0, (int)_tcpClient.ReceiveBufferSize);

                CommsPacket.CommsResponse response = new CommsPacket.CommsResponse();
                response = response.DeSerialize(bytes);

                if (response.Message.Contains("I received the packet") == false)
                {
                    throw new Exception("Did not receive packet acknowledgement");
                }

                return true;
            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
        }

        public bool SendNewClientAccount_Linux(CommsPacket.ClientAccount newclientaccount, string adminusername, string authtoken)
        {
            if (_networkStream != null)
            {
                int signatureone = (int)CommsPacket.MessageTypes.LinuxSignatureOne;
                int signaturetwo = (int)CommsPacket.MessageTypes.LinuxSignatureTwo;
                int messageidentifier = (int)CommsPacket.MessageTypes.LinuxNewClientAccount;
                
                int bulkmessagelength = sizeof(int); // First int size is the message identifier

                bulkmessagelength += sizeof(int); // string length
                bulkmessagelength += newclientaccount.Company.Length;
                bulkmessagelength += sizeof(int); // string length
                bulkmessagelength += newclientaccount.Description.Length;
                bulkmessagelength += sizeof(int); // string length
                bulkmessagelength += newclientaccount.Firstname.Length;
                bulkmessagelength += sizeof(int); // string length
                bulkmessagelength += newclientaccount.Lastname.Length;
                bulkmessagelength += sizeof(int); // max allowed mb
                bulkmessagelength += sizeof(int); // string length
                bulkmessagelength += newclientaccount.PasswordHash.Length;
                bulkmessagelength += sizeof(int); // string length
                bulkmessagelength += newclientaccount.RootPath.Length;
                bulkmessagelength += sizeof(int); // string length
                bulkmessagelength += newclientaccount.Username.Length;

                MemoryStream ms = new MemoryStream();
                BinaryWriter bw = new BinaryWriter(ms);
                UTF8Encoding enc = new UTF8Encoding();

                bw.Write(signatureone);
                bw.Write(signaturetwo);
                bw.Write(bulkmessagelength);
                bw.Write(messageidentifier);

                bw.Write(newclientaccount.Company.Length);
                bw.Write(enc.GetBytes (newclientaccount.Company));
                bw.Write (newclientaccount.Description.Length);
                bw.Write (enc.GetBytes (newclientaccount.Description));
                bw.Write(newclientaccount.Firstname.Length);
                bw.Write(enc.GetBytes(newclientaccount.Firstname));
                bw.Write(newclientaccount.Lastname.Length);
                bw.Write(enc.GetBytes(newclientaccount.Lastname));
                bw.Write(newclientaccount.MaxAllowedMegaBytes);
                bw.Write(newclientaccount.PasswordHash.Length);
                bw.Write(enc.GetBytes(newclientaccount.PasswordHash));
                bw.Write(newclientaccount.RootPath.Length);
                bw.Write(enc.GetBytes(newclientaccount.RootPath));
                bw.Write(newclientaccount.Username.Length);
                bw.Write(enc.GetBytes(newclientaccount.Username));

                // Send the new client to the server
                SendBytes(ms.ToArray());

                return true;
            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
        }

        public bool SendUpdatedClientAccount(CommsPacket.ClientAccount newclientaccount, string adminusername, string authtoken)
        {
            if (_networkStream != null)
            {
                CommsPacket.CommsPacket myObject = new CommsPacket.CommsPacket();

                myObject.iMessagetype = (int)CommsPacket.MessageTypes.UpdateClientAccount;
                myObject.useraccount = adminusername;
                myObject.usertoken = authtoken;

                myObject.Somemessage = "UpdateClientAccount";
                myObject.somethingelse = "UpdateClientAccount";
                myObject.somebuffer = CommonFunctions.ObjectToByteArray(newclientaccount, typeof(CommsPacket.ClientAccount));

                byte[] sendbytes = myObject.Serialize();
                byte[] encbytes = myObject.Encapsulate(sendbytes);

                _networkStream.Write(encbytes, 0, encbytes.Length);

                // Reads the NetworkStream into a byte buffer.
                byte[] bytes = new byte[_tcpClient.ReceiveBufferSize];
                int BytesRead = _networkStream.Read(bytes, 0, (int)_tcpClient.ReceiveBufferSize);

                CommsPacket.CommsResponse response = new CommsPacket.CommsResponse();
                response = response.DeSerialize(bytes);

                if (response.Message.Contains("I received the packet") == false)
                {
                    throw new Exception("Did not receive packet acknowledgement");
                }

                return true;
            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
        }

        public bool SendUpdatedClientAccount_Linux(CommsPacket.ClientAccount updatedclientaccount, string adminusername, string authtoken)
        {
            if (_networkStream != null)
            {
                int signatureone = (int)CommsPacket.MessageTypes.LinuxSignatureOne;
                int signaturetwo = (int)CommsPacket.MessageTypes.LinuxSignatureTwo;
                int messageidentifier = (int)CommsPacket.MessageTypes.LinuxUpdateClientAccount;

                int bulkmessagelength = sizeof(int); // First int size is the message identifier

                bulkmessagelength += sizeof(int); // string length
                bulkmessagelength += updatedclientaccount.Company.Length;
                bulkmessagelength += sizeof(int); // string length
                bulkmessagelength += updatedclientaccount.Description.Length;
                bulkmessagelength += sizeof(int); // string length
                bulkmessagelength += updatedclientaccount.Firstname.Length;
                bulkmessagelength += sizeof(int); // string length
                bulkmessagelength += updatedclientaccount.Lastname.Length;
                bulkmessagelength += sizeof(int); // max allowed mb
                bulkmessagelength += sizeof(int); // string length
                bulkmessagelength += updatedclientaccount.PasswordHash.Length;
                bulkmessagelength += sizeof(int); // string length
                bulkmessagelength += updatedclientaccount.RootPath.Length;
                bulkmessagelength += sizeof(int); // string length
                bulkmessagelength += updatedclientaccount.Username.Length;

                MemoryStream ms = new MemoryStream();
                BinaryWriter bw = new BinaryWriter(ms);
                UTF8Encoding enc = new UTF8Encoding();

                bw.Write(signatureone);
                bw.Write(signaturetwo);
                bw.Write(bulkmessagelength);
                bw.Write(messageidentifier);

                bw.Write(updatedclientaccount.Company.Length);
                bw.Write(enc.GetBytes(updatedclientaccount.Company));
                bw.Write(updatedclientaccount.Description.Length);
                bw.Write(enc.GetBytes(updatedclientaccount.Description));
                bw.Write(updatedclientaccount.Firstname.Length);
                bw.Write(enc.GetBytes(updatedclientaccount.Firstname));
                bw.Write(updatedclientaccount.Lastname.Length);
                bw.Write(enc.GetBytes(updatedclientaccount.Lastname));
                bw.Write(updatedclientaccount.MaxAllowedMegaBytes);
                bw.Write(updatedclientaccount.PasswordHash.Length);
                bw.Write(enc.GetBytes(updatedclientaccount.PasswordHash));
                bw.Write(updatedclientaccount.RootPath.Length);
                bw.Write(enc.GetBytes(updatedclientaccount.RootPath));
                bw.Write(updatedclientaccount.Username.Length);
                bw.Write(enc.GetBytes(updatedclientaccount.Username));

                // Send the new client to the server
                SendBytes(ms.ToArray());

                return true;
            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
        }

        public bool SendDeleteClientAccount_Linux(string accountusername, string adminusername, string authtoken)
        {
            if (_networkStream != null)
            {
                int signatureone = (int)CommsPacket.MessageTypes.LinuxSignatureOne;
                int signaturetwo = (int)CommsPacket.MessageTypes.LinuxSignatureTwo;
                int messageidentifier = (int)CommsPacket.MessageTypes.LinuxDeleteClientAccount;

                int bulkmessagelength = sizeof(int); // First int size is the message identifier

                bulkmessagelength += sizeof(int); // string length
                bulkmessagelength += accountusername.Length;
                
                MemoryStream ms = new MemoryStream();
                BinaryWriter bw = new BinaryWriter(ms);
                UTF8Encoding enc = new UTF8Encoding();

                bw.Write(signatureone);
                bw.Write(signaturetwo);
                bw.Write(bulkmessagelength);
                bw.Write(messageidentifier);

                bw.Write(accountusername.Length);
                bw.Write(enc.GetBytes(accountusername));
                
                // Send the new client to the server
                SendBytes(ms.ToArray());

                return true;
            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
        }

        public bool SendDeleteClientAccount(string accountusername, string adminusername, string authtoken)
        {
            if (_networkStream != null)
            {
                CommsPacket.CommsPacket myObject = new CommsPacket.CommsPacket();

                myObject.iMessagetype = (int)CommsPacket.MessageTypes.DeleteClientAccount;
                myObject.useraccount = adminusername;
                myObject.usertoken = authtoken;

                myObject.Somemessage = "DeleteClientAccount";
                myObject.somethingelse = accountusername;

                byte[] sendbytes = myObject.Serialize();
                byte[] encbytes = myObject.Encapsulate(sendbytes);

                _networkStream.Write(encbytes, 0, encbytes.Length);

                // Reads the NetworkStream into a byte buffer.
                byte[] bytes = new byte[_tcpClient.ReceiveBufferSize];
                int BytesRead = _networkStream.Read(bytes, 0, (int)_tcpClient.ReceiveBufferSize);

                CommsPacket.CommsResponse response = new CommsPacket.CommsResponse();
                response = response.DeSerialize(bytes);

                if (response.Message.Contains("I received the packet") == false)
                {
                    throw new Exception("Did not receive packet acknowledgement");
                }

                return true;
            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
        }

        public bool SendChangeClientPassword(string accountusername, string newpasswordhash, string adminusername, string authtoken)
        {
            if (_networkStream != null)
            {
                CommsPacket.CommsPacket myObject = new CommsPacket.CommsPacket();

                myObject.iMessagetype = (int)CommsPacket.MessageTypes.ChangeClientPassword;
                myObject.useraccount = adminusername;
                myObject.usertoken = authtoken;

                myObject.Somemessage = newpasswordhash;
                myObject.somethingelse = accountusername;

                byte[] sendbytes = myObject.Serialize();
                byte[] encbytes = myObject.Encapsulate(sendbytes);

                _networkStream.Write(encbytes, 0, encbytes.Length);

                // Reads the NetworkStream into a byte buffer.
                byte[] bytes = new byte[_tcpClient.ReceiveBufferSize];
                int BytesRead = _networkStream.Read(bytes, 0, (int)_tcpClient.ReceiveBufferSize);

                CommsPacket.CommsResponse response = new CommsPacket.CommsResponse();
                response = response.DeSerialize(bytes);

                if (response.Message.Contains("I received the packet") == false)
                {
                    throw new Exception("Did not receive packet acknowledgement");
                }

                return true;
            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
        }


        public bool SendChangeClientPassword_Linux(string accountusername, string newpasswordhash, string adminusername, string authtoken)
        {
            if (_networkStream != null)
            {
                int signatureone = (int)CommsPacket.MessageTypes.LinuxSignatureOne;
                int signaturetwo = (int)CommsPacket.MessageTypes.LinuxSignatureTwo;
                int messageidentifier = (int)CommsPacket.MessageTypes.LinuxChangeClientPassword;

                int bulkmessagelength = sizeof(int); // First int size is the message identifier

                bulkmessagelength += sizeof(int); // string length
                bulkmessagelength += accountusername.Length;
                bulkmessagelength += sizeof(int); // string length
                bulkmessagelength += newpasswordhash.Length;

                MemoryStream ms = new MemoryStream();
                BinaryWriter bw = new BinaryWriter(ms);
                UTF8Encoding enc = new UTF8Encoding();

                bw.Write(signatureone);
                bw.Write(signaturetwo);
                bw.Write(bulkmessagelength);
                bw.Write(messageidentifier);

                bw.Write(accountusername.Length);
                bw.Write(enc.GetBytes(accountusername));

                bw.Write(newpasswordhash.Length);
                bw.Write(enc.GetBytes(newpasswordhash));

                // Send the new client to the server
                SendBytes(ms.ToArray());

                return true;
            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
        }



        public bool SendLargeDataRequest()
        {
            if (_networkStream != null)
            {
                CommsPacket.CommsPacket myObject = new CommsPacket.CommsPacket();

                myObject.iMessagetype = (int)CommsPacket.MessageTypes.LargeDataTest;

                myObject.Somemessage = "LargeDataRequest";
                myObject.somethingelse = "LargeDataRequest";

                byte[] sendbytes = myObject.Serialize();
                byte[] encbytes = myObject.Encapsulate(sendbytes);

                _networkStream.Write(encbytes, 0, encbytes.Length);

                // Now process the large bit of data we're gonna receive
                BeginReceivingLargeData();

                // By the time we get to here our _largereceivedobject should be populated with stuff

                if (_largereceivedobject != null)
                {
                    System.Windows.Forms.MessageBox.Show("Large Buffer Length: " + _largereceivedobject.somebuffer.Length.ToString());
                }
                else
                {
                    throw new Exception("Large Received Object was NULL.");
                }

                return true;
            }
            else
            {
                throw new Exception("The Network Connection has not been initialised. Use Connect() to initialise and connect to the server before trying to send data.");
            }
        }


        private void BeginReceivingLargeData()
        {
            // This resets the global variables and kicks of the method that actually appends to the buffer of large data
            memStream = new MemoryStream();
            binBuffer = new BinaryWriter(memStream);

            ReceiveLargeData();
        }

        private void BeginReceivingLargeData_Linux()
        {
            memStream = new MemoryStream();
            binBuffer = new BinaryWriter(memStream);

            ReceiveLargeData_Linux();
        }

        private void ReceiveLargeData_Linux()
        {

            bool continuereceiving = true;

            while (continuereceiving)
            {
                try
                {
                    if (_networkStream.DataAvailable == true)
                    {
                        bytes = new byte[_maxbuffersize];

                        int dataavailable = _tcpClient.Available;

                        int BytesRead = _networkStream.Read(bytes, 0, dataavailable);

                        if (BytesRead > 0)
                        {
                            byte[] actualbytes = new byte[dataavailable];
                            Array.Copy(bytes, actualbytes, dataavailable);

                            // There might be more data, so store the data received so far.
                            binBuffer.Write(actualbytes);
                        }

                        // Copy the received buffer to a trimmed buffer. Why? Because memStream.GetBuffer() is a bit deceiving. Basically
                        // it gives you the buffer thats been received yes, but the size of the buffer it gives you is the maximum capacity
                        // of the buffer, not the actual length of data received, and this is what we need.
                        byte[] membuffer = memStream.GetBuffer();
                        byte[] trimmedbuffer = new byte[memStream.Length];
                        Array.Copy(membuffer, trimmedbuffer, memStream.Length);

                        if (_networkStream.DataAvailable == true)
                        {
                            continuereceiving = true;
                        }
                        else
                        {
                            continuereceiving = false;

                        }
                    }
                }
                catch (IOException)
                {

                }
                catch (SocketException)
                {

                }

                System.Threading.Thread.Sleep(5);
            }
        }

        private void ReceiveLargeData()
        {
            CommsPacket.CommsPacket myPacket = new CommsPacket.CommsPacket();
            bool continuereceiving = true;

            while (continuereceiving)
            {
                try
                {
                    if (_networkStream.DataAvailable == true)
                    {
                        bytes = new byte[_maxbuffersize];

                        int dataavailable = _tcpClient.Available;

                        int BytesRead = _networkStream.Read(bytes, 0, dataavailable);

                        if (BytesRead > 0)
                        {
                            byte[] actualbytes = new byte[dataavailable];
                            Array.Copy(bytes, actualbytes, dataavailable);

                            // There might be more data, so store the data received so far.
                            binBuffer.Write(actualbytes);
                        }

                        // Copy the received buffer to a trimmed buffer. Why? Because memStream.GetBuffer() is a bit deceiving. Basically
                        // it gives you the buffer thats been received yes, but the size of the buffer it gives you is the maximum capacity
                        // of the buffer, not the actual length of data received, and this is what we need.
                        byte[] membuffer = memStream.GetBuffer();
                        byte[] trimmedbuffer = new byte[memStream.Length];
                        Array.Copy(membuffer, trimmedbuffer, memStream.Length);

                        if (myPacket.ContainsSignature(trimmedbuffer) == true)
                        {
                            // We've found an end of packet signature, so process it!!
                            //System.Diagnostics.Debug.WriteLine("We've found a signature. Received data length: " + memStream.GetBuffer().Length.ToString());
                            continuereceiving = false;
                            ProcessLargeData(trimmedbuffer);
                        }
                        else
                        {
                            //System.Diagnostics.Debug.WriteLine("No signature found. Waiting for more." + memStream.GetBuffer().Length.ToString());
                        }


                        if (_networkStream.DataAvailable == true)
                        {
                            continuereceiving = true;
                        }
                        else
                        {

                        }
                    }
                }
                catch (IOException)
                {

                }
                catch (SocketException)
                {

                }

                System.Threading.Thread.Sleep(5);
            }
        }


        private void ProcessLargeData(byte[] trimmedbuffer)
        {
            if (memStream.Length > 0)
            {
                _largereceivedobject = new CommsPacket.CommsPacket();

                byte[] decapsulatedbytes = _largereceivedobject.Decapsulate(trimmedbuffer);

                try
                {
                    _largereceivedobject = _largereceivedobject.DeSerialize(decapsulatedbytes);
                }
                catch (Exception ex)
                {
                    _largereceivedobject = null;
                }
            }
            else
            {
                _largereceivedobject = null;
            }
        }


        public bool SendSampleData(string messagetosend)
        {
            CommsPacket.CommsPacket myObject = new CommsPacket.CommsPacket();
            myObject.iMessagetype = 100;
            myObject.Somemessage = messagetosend;
            myObject.somethingelse = "Another message";
            myObject.somebuffer = new byte[20000];


            //Byte[] sendBytes = Encoding.ASCII.GetBytes(DataToSend);
            byte[] sendBytes = myObject.Serialize();

            if (myObject.ContainsSignature(sendBytes) == true)
            {
                System.Windows.Forms.MessageBox.Show("send bytes contains the signature!");
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("send bytes DOES NOT contain the signature!");
            }



            byte[] encbytes = myObject.Encapsulate(sendBytes);


            if (myObject.ContainsSignature(encbytes) == true)
            {
                System.Windows.Forms.MessageBox.Show("encbytes contains the signature!");
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("encbytes DOES NOT contain the signature!");
            }


            // Try and decapsulate
            byte[] decapsulatedbytes = myObject.Decapsulate(encbytes);



            return true;

        }
    }

    public class SendWorkerParams
    {
        public string sourcepath;
        public string destpath;
    }

    public class SendWorkerProgress
    {
        public string sourcepath;
        public long bytessent;
        public long totalbytes;
        public long bytesremaining;
        public int percentcompleted;
    }
}
