﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace FortressManager
{
    public partial class Form1 : Form
    {
        Communications _comms = new Communications();
        string _authenticatedtoken;
        string _adminusername;
        bool _firstrun = false;

        AdminAccountControl _adminaccountcontrol = new AdminAccountControl();
        ClientAccountControl _clientaccountcontrol = new ClientAccountControl();
        ApplicationSettings _appsettings = new ApplicationSettings();

        private const string _administratoraccountsstring = "Administrator Accounts";
        private const string _clientaccountsstring = "Client Accounts";

        public Form1()
        {
            InitializeComponent();
        }

        private void btnServerConnect_Click(object sender, EventArgs e)
        {
            ServerLogon logonform = new ServerLogon();
            logonform.OnServerConnect += new ServerLogon.ServerConnect(logonform_OnServerConnect);

            bool serverdetailsset = false;

            if (tvServers.SelectedNode != null)
            {
                if (tvServers.SelectedNode.Tag != null && tvServers.SelectedNode.Tag.GetType() == typeof(ServerDetails))
                {
                    logonform.SetServerDetails((ServerDetails)tvServers.SelectedNode.Tag);
                    serverdetailsset = true;
                }
            }

            if (serverdetailsset == false)
            {
                if (_appsettings.serverlogonlist.Count > 0)
                {
                    ServerDetails defaultdetails = _appsettings.serverlogonlist[0];
                    logonform.SetServerDetails(defaultdetails);
                }
            }
            
            logonform.ShowDialog();
        }

        public void InitialiseSelectionPane()
        {
            lstSettings.Items.Add(_administratoraccountsstring);
            lstSettings.Items.Add(_clientaccountsstring);   
        }

        public void RefreshServerList()
        {
            tvServers.Nodes.Clear();

            TreeNode servernode = new TreeNode("Servers");

            foreach (ServerDetails serverdetails in _appsettings.serverlogonlist)
            {
                TreeNode connectnode = new TreeNode(serverdetails.ServerName);
                connectnode.Tag = serverdetails;
                servernode.Nodes.Add(connectnode);
            }

            tvServers.Nodes.Add(servernode);
        }

        private void UpdateOrAdd(ServerDetails logondetails)
        {
            foreach (ServerDetails details in _appsettings.serverlogonlist)
            {
                if (details.ServerName.ToUpper() == logondetails.ServerName.ToUpper() && details.Port.ToUpper() == logondetails.Port.ToUpper())
                {
                    details.Username = logondetails.Username;
                    details.Password = logondetails.Password;
                    return;
                }
            }

            // Nothing found so add a new entry
            _appsettings.serverlogonlist.Add(logondetails);
        }

        void logonform_OnServerConnect(ServerDetails logondetails, bool savedetails)
        {
            //MessageBox.Show("Now need to attempt to connect to server: " + logondetails.ServerName);
            //throw new NotImplementedException();
            string connecterror;

            if (_comms.Connect(logondetails.ServerName, Int32.Parse(logondetails.Port), out connecterror) == true)
            {
                string authenticatedtoken;
                if (_comms.AuthenticateAdminAccount_Linux(logondetails.Username, logondetails.Password, out authenticatedtoken) == true)
                {
                    _authenticatedtoken = authenticatedtoken;
                    _adminusername = logondetails.Username;
                    

                    if (savedetails)
                    {
                        UpdateOrAdd(logondetails);
                        _appsettings.SaveXMLSettings();
                        RefreshServerList();
                    }

                    RequestAdminAccountList();
                    RequestClientAccountList();
                }
                else
                {
                    MessageBox.Show("Incorrect Username or Password. Access Denied.", "Access Denied", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    _comms.Disconnect();
                }
            }
            else
            {
                MessageBox.Show("Connection to server " + logondetails.ServerName + " failed. Reason: " + connecterror, "Connection Failed.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                _comms.Disconnect();
            }
        }

        public void RequestAdminAccountList()
        {
            // First start by listing the admin accounts;

            List<CommsPacket.AdminAccount> adminaccountlist = _comms.ListAdminAccounts_Linux (_adminusername, _authenticatedtoken);

            //MessageBox.Show("Admin Accounts Listed.");

            _adminaccountcontrol.RefreshAdminAccountList(adminaccountlist);
        }

        public void RequestClientAccountList()
        {
            List<CommsPacket.ClientAccount> clientaccountlist = _comms.ListClientAccounts_Linux(_adminusername, _authenticatedtoken);

            _clientaccountcontrol.RefreshClientAccountList(clientaccountlist);
        }

        public void ShowAdminAccountControl()
        {
            _adminaccountcontrol.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Right;
            _adminaccountcontrol.Size = pnlParent.Size;
            pnlParent.Controls.Clear();
            pnlParent.Controls.Add(_adminaccountcontrol);
        }

        public void ShowClientAccountControl()
        {
            _clientaccountcontrol.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Right;
            _clientaccountcontrol.Size = pnlParent.Size;
            pnlParent.Controls.Clear();
            pnlParent.Controls.Add(_clientaccountcontrol);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _adminaccountcontrol.OnNewAccountCreated += new AdminAccountControl.NewAccountCreated(_adminaccountcontrol_OnNewAccountCreated);
            _adminaccountcontrol.OnExistingAccountChanged += new AdminAccountControl.ExistingAccountChanged(_adminaccountcontrol_OnExistingAccountChanged);
            _adminaccountcontrol.OnAccountDeleted += new AdminAccountControl.AccountDeleted(_adminaccountcontrol_OnAccountDeleted);
            _adminaccountcontrol.OnAccountPasswordChanged += new AdminAccountControl.AccountPasswordChanged(_adminaccountcontrol_OnAccountPasswordChanged);

            _clientaccountcontrol.OnNewClientAccountCreated += new ClientAccountControl.NewClientAccountCreated(_clientaccountcontrol_OnNewClientAccountCreated);
            _clientaccountcontrol.OnExistingClientAccountChanged += new ClientAccountControl.ExistingClientAccountChanged(_clientaccountcontrol_OnExistingClientAccountChanged);
            _clientaccountcontrol.OnClientAccountDeleted += new ClientAccountControl.ClientAccountDeleted(_clientaccountcontrol_OnClientAccountDeleted);
            _clientaccountcontrol.OnClientAccountPasswordChanged += new ClientAccountControl.ClientAccountPasswordChanged(_clientaccountcontrol_OnClientAccountPasswordChanged);

            CommonFunctions.ExecutablePath = Application.ExecutablePath;

            InitialiseSelectionPane();
            ShowAdminAccountControl();
            _appsettings = _appsettings.LoadXMLSettings();
            RefreshServerList();
            //btnServerConnect_Click(sender, e);
        }

        void _clientaccountcontrol_OnClientAccountPasswordChanged(string username, string newpasswordhash)
        {
            //throw new NotImplementedException();
            _comms.SendChangeClientPassword_Linux(username, newpasswordhash, _adminusername, _authenticatedtoken);
            RequestClientAccountList();
        }

        void _clientaccountcontrol_OnClientAccountDeleted(string clientaccountusername)
        {
            //throw new NotImplementedException();
            _comms.SendDeleteClientAccount_Linux(clientaccountusername, _adminusername, _authenticatedtoken);
            RequestClientAccountList();
        }

        void _clientaccountcontrol_OnExistingClientAccountChanged(CommsPacket.ClientAccount newclientaccountdetails)
        {
            //throw new NotImplementedException();
            _comms.SendUpdatedClientAccount_Linux(newclientaccountdetails, _adminusername, _authenticatedtoken);
            RequestClientAccountList();
        }

        void _clientaccountcontrol_OnNewClientAccountCreated(CommsPacket.ClientAccount newclientaccount)
        {

            //throw new NotImplementedException();
            _comms.SendNewClientAccount_Linux(newclientaccount, _adminusername, _authenticatedtoken);
            RequestClientAccountList();
        }

        void _adminaccountcontrol_OnAccountPasswordChanged(string username, string newpasswordhash)
        {
            //throw new NotImplementedException();
            //MessageBox.Show("Need to change the passord on the server");
            _comms.SendChangeAdminPassword_Linux(username, newpasswordhash, _adminusername, _authenticatedtoken);
            RequestAdminAccountList();
        }

        void _adminaccountcontrol_OnAccountDeleted(string adminaccountusername)
        {
            //throw new NotImplementedException();
            //MessageBox.Show("Need to Delete the account on the server");
            _comms.SendDeleteAdminAccount_Linux(adminaccountusername, _adminusername, _authenticatedtoken);
            RequestAdminAccountList();
        }

        void _adminaccountcontrol_OnExistingAccountChanged(CommsPacket.AdminAccount newaccountdetails)
        {
            //throw new NotImplementedException();
            //MessageBox.Show("Need to edit the account on the server.");
            _comms.SendUpdatedAdminAccount_Linux (newaccountdetails, _adminusername, _authenticatedtoken);
            RequestAdminAccountList();
        }

        void _adminaccountcontrol_OnNewAccountCreated(CommsPacket.AdminAccount newaccount)
        {
            //throw new NotImplementedException();
            //MessageBox.Show("Need to create the account on the server");
            _comms.SendNewAdminAccount_Linux(newaccount, _adminusername, _authenticatedtoken);
            RequestAdminAccountList();

        }

        private void Form1_Activated(object sender, EventArgs e)
        {
            if (_firstrun == false)
            {
                _firstrun = true;
                btnServerConnect_Click(sender, e);
            }
        }

        private void lstSettings_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstSettings.SelectedItem.ToString() == _administratoraccountsstring)
            {
                ShowAdminAccountControl();
            }

            if (lstSettings.SelectedItem.ToString() == _clientaccountsstring)
            {
                ShowClientAccountControl();
            }

            
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
