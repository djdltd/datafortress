﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace FortressManager
{
    public partial class ClientAccountDetails : Form
    {
        public delegate void NewClientAccountEntered(CommsPacket.ClientAccount newclientaccount);
        public event NewClientAccountEntered OnNewClientAccountEntered;

        public delegate void ClientAccountChanged(CommsPacket.ClientAccount newclientaccount);
        public event ClientAccountChanged OnClientAccountChanged;

        bool _editmode = false;
        public string _existingpasswordhash;

        public ClientAccountDetails()
        {
            InitializeComponent();
        }

        public void SetClientAccountDetails(CommsPacket.ClientAccount existingaccount)
        {
            _editmode = true;
            txtUsername.ReadOnly = true;
            _existingpasswordhash = existingaccount.PasswordHash;
            txtUsername.Text = existingaccount.Username;
            txtPassword.Text = existingaccount.PasswordHash;
            txtFirstname.Text = existingaccount.Firstname;
            txtLastname.Text = existingaccount.Lastname;
            txtCompany.Text = existingaccount.Company;
            txtServerPath.Text = existingaccount.RootPath;
            txtMaxAllowedMB.Text = existingaccount.MaxAllowedMegaBytes.ToString();
            txtDescription.Text = existingaccount.Description;
        }

        private void ClientAccountDetails_Load(object sender, EventArgs e)
        {

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (_editmode == false) // We are in new account mode
            {
                if (OnNewClientAccountEntered != null)
                {

                    CommsPacket.ClientAccount newclientaccount = new CommsPacket.ClientAccount();

                    newclientaccount.Username = txtUsername.Text;
                    newclientaccount.PasswordHash = CommonFunctions.CreateMD5HashFromString(txtPassword.Text);
                    newclientaccount.Firstname = txtFirstname.Text;
                    newclientaccount.Lastname = txtLastname.Text;
                    newclientaccount.Company = txtCompany.Text;
                    newclientaccount.RootPath = txtServerPath.Text;
                    newclientaccount.MaxAllowedMegaBytes = Int32.Parse(txtMaxAllowedMB.Text);
                    newclientaccount.Description = txtDescription.Text;

                    OnNewClientAccountEntered(newclientaccount);

                }
            }

            if (_editmode == true)
            {
                if (OnClientAccountChanged != null)
                {
                    CommsPacket.ClientAccount newclientaccount = new CommsPacket.ClientAccount();

                    newclientaccount.Username = txtUsername.Text;

                    if (txtPassword.Text == _existingpasswordhash)
                    {
                        newclientaccount.PasswordHash = _existingpasswordhash;
                    }
                    else
                    {
                        newclientaccount.PasswordHash = CommonFunctions.CreateMD5HashFromString(txtPassword.Text);
                    }

                    newclientaccount.Firstname = txtFirstname.Text;
                    newclientaccount.Lastname = txtLastname.Text;
                    newclientaccount.Company = txtCompany.Text;
                    newclientaccount.RootPath = txtServerPath.Text;
                    newclientaccount.MaxAllowedMegaBytes = Int32.Parse (txtMaxAllowedMB.Text);
                    newclientaccount.Description = txtDescription.Text;

                    OnClientAccountChanged(newclientaccount);
                }
            }

            Hide();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Hide();
        }
    }
}
