/* This is a basic hello world C program */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <fstream>
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>
#include <unistd.h>
#include "MemoryBuffer.h"
#include "ClientAccount.h"
#include "AdminAccount.h"



#define BUFSIZE (64*1024)

#define SIZE_STRING		1024

#define SIGNATUREONE	47577
#define SIGNATURETWO	77777

#define TEST_MESSAGE	10001
#define TEST_REPLY		10002

#define BEGINFILESEND	10004
#define AUTHENTICATEADMIN		10005
#define AUTHADMINOK			10006
#define AUTHADMINDENIED		10007
#define REQUESTADMINACCOUNTS	10008
#define ADMINACCOUNTLIST		10009
#define REQUESTCLIENTACCOUNTS	10010
#define CLIENTACCOUNTLIST		10011
#define NEWCLIENTACCOUNT		10012
#define UPDATECLIENTACCOUNT	10013
#define DELETECLIENTACCOUNT	10014
#define CLIENTPASSWORDCHANGE	10015
#define NEWADMINACCOUNT		10016
#define UPDATEADMINACCOUNT	10017
#define DELETEADMINACCOUNT	10018
#define ADMINPASSWORDCHANGE	10019
#define AUTHENTICATECLIENT		10020
#define AUTHCLIENTOK			10021
#define AUTHCLIENTDENIED		10022
#define FILEEXISTS				10023
#define RESPONSEFILEEXISTS		10024
#define RESPONSEFILENOTFOUND	10025
#define LASTWRITETIME			10026
#define RESPONSELASTWRITETIME	10027
#define FILELENGTH			10028
#define RESPONSEFILELENGTH	10029
#define PREPARE_FILETRANSFER	10030
#define FILETRANSFERREADY		10031

int StartServer ();
void StartDaemon ();
void FileCopy ();
void AddClientAccount ();
void AddClientAccount (ClientAccount *newclientaccount);
bool DoesClientAccountExist (char *szUsername, char *szPasswordhash);
void AddDefaultAdminAccount ();
bool DoesFileExist (char *szFilename);
bool DoesAdminUserExist (char *szUsername);
bool DoesAdminAccountExist (char *szUsername, char *szPasswordHash);
long GetFileLength (char *szFilename);
bool ReadAdminAccountList (MemoryBuffer *memDest);
bool ListAllClientAccounts ();
int FindString (char *szStrtolook, char *szStrtofind);
int ReplaceString (char *szStrtolook, char *szStrtofind, char *szReplacement, char *szOutput);
void TestFindString ();
void TestReplaceString ();
void TestGetClientAccountRootPath ();
void TestCombinePath ();
void TestGetFileLastWriteTime ();
int mkpath(const char *path, mode_t mode);
static int do_mkdir(const char *path, mode_t mode);
bool GetDirectoryName (char *szFullfilepath, char *szOutdirectoryname);
void TestGetDirectoryName ();

using namespace std;

// Global Variables
char _szClientaccountpath[SIZE_STRING];
char _szAdminaccountpath[SIZE_STRING];

main()
{
	//DoesClientAccountExist ("ddraper", "MyPasswordHash");
	//AddClientAccount ();
	
	
	// Set up the client and admin file account paths - the places where we store the client accounts and admin accounts
	char szExepath[SIZE_STRING];
	bzero (szExepath, SIZE_STRING);
	
	readlink ("/proc/self/exe", szExepath, SIZE_STRING);
	
	char szExedir[SIZE_STRING];
	bzero (szExedir, SIZE_STRING);
	
	GetDirectoryName (szExepath, szExedir);
	
	syslog (LOG_DEBUG, "DataFortressServer: Executable path is: %s\n", szExepath);
	syslog (LOG_DEBUG, "DataFortressServer: Executable directory is: %s\n", szExedir);
	
	// Populate the client and admin file account paths
	bzero (_szClientaccountpath, SIZE_STRING);
	bzero (_szAdminaccountpath, SIZE_STRING);
	
	strcat (_szClientaccountpath, szExedir);
	strcat (_szClientaccountpath, "/clientaccounts.dat");
	
	strcat (_szAdminaccountpath, szExedir);
	strcat (_szAdminaccountpath, "/adminaccounts.dat");
	
	syslog (LOG_DEBUG, "DataFortressServer: Client Account path is: %s\n", _szClientaccountpath);
	syslog (LOG_DEBUG, "DataFortressServer: Admin Account path is: %s\n", _szAdminaccountpath);
	
	
	//StartServer ();
	StartDaemon ();
	
	//bool GetDirectoryName (char *szFullfilepath, char *szOutdirectoryname)
	//TestGetDirectoryName ();
	
	
	//mkpath ("/home/danny/somedir/subdir/sub2/mysubdirectory", 0777);
	
	//syslog (LOG_DEBUG, "DataFortressServer: Sizeof a long is: %i\n", sizeof (unsigned long long));
	
	//TestGetFileLastWriteTime ();
	//ListAllClientAccounts ();
	//FileCopy ();
	//AddDefaultAdminAccount ();
	//TestFindString ();
	//TestReplaceString ();
	//TestGetClientAccountRootPath ();
	//TestCombinePath ();
	
	/*
	MemoryBuffer memDest;
	
	if (ReadAdminAccountList (&memDest) == true) {
		syslog (LOG_DEBUG, "DataFortressServer: Admin account list read ok. %i bytes read into memory.\n", memDest.GetSize ());
	} else {
		syslog (LOG_DEBUG, "DataFortressServer: Admin account failed to read.\n");
	}
	*/
	
	//long filelength = GetFileLength (_szAdminaccountpath);
	
	//syslog (LOG_DEBUG, "DataFortressServer: File length was: %i", filelength);
	
	/*
	if (DoesAdminAccountExist ("admin", "admin") == true) {
		syslog (LOG_DEBUG, "DataFortressServer: Admin account exists.\n");
	} else {
		syslog (LOG_DEBUG, "DataFortressServer: Admin account does NOT exist\n");
	}
	*/
	
	/*
	printf("This is a socket server program created by Danny Draper!\n");
	
	const int listenPortNumber = 7009;

	const int listenSocketFile = socket(AF_INET, SOCK_STREAM, 0);
	
	if (listenSocketFile < 0) 
		error("ERROR opening server socket");

	struct sockaddr_in listenAddress;
	bzero((char *) &listenAddress, sizeof(listenAddress));

	listenAddress.sin_family = AF_INET;
	listenAddress.sin_addr.s_addr = INADDR_ANY;
	listenAddress.sin_port = htons(listenPortNumber);

	const int bindResult = bind(listenSocketFile, (struct sockaddr *) &listenAddress, sizeof(listenAddress));

	if (bindResult<0) 
		error("ERROR on binding");

	listen(listenSocketFile,5);

	struct sockaddr_in transferAddress;
	socklen_t sizeofTransferAddress = sizeof(transferAddress);

	const int transferSocketFile = accept(listenSocketFile, (struct sockaddr *) &transferAddress, &sizeofTransferAddress);

	if (transferSocketFile<0) 
		error("ERROR on accept");

	const int bufferLength = 256;
	char buffer[bufferLength];

	const int bytesRead = read(transferSocketFile,buffer,(bufferLength-1));
	
	if (bytesRead<0) 
		error("ERROR reading from socket");

	// Pete- On some systems the buffer past the read bytes may be altered, so make sure the string is zero terminated
	buffer[bytesRead] = '\0';

	printf("Here is the message: %s\n",buffer);

	const char* outputMessage = "I got your message";
	const int outputMessageLength = strlen(outputMessage);
	const int bytesWritten = write(transferSocketFile, outputMessage, outputMessageLength);

	if (bytesWritten<0) 
		error("ERROR writing to socket");
		
	close(transferSocketFile);
	
	close(listenSocketFile);

	return 0;
	*/
}

static int do_mkdir(const char *path, mode_t mode)
{
    struct stat            st;
    int             status = 0;

    if (stat(path, &st) != 0)
    {
        /* Directory does not exist */
        if (mkdir(path, mode) != 0)
            status = -1;
    }
    else if (!S_ISDIR(st.st_mode))
    {
        errno = ENOTDIR;
        status = -1;
    }

    return(status);
}

int mkpath(const char *path, mode_t mode)
{
    char           *pp;
    char           *sp;
    int             status;
    char           *copypath = strdup(path);

    status = 0;
    pp = copypath;
    while (status == 0 && (sp = strchr(pp, '/')) != 0)
    {
        if (sp != pp)
        {
            /* Neither root nor double slash in path */
            *sp = '\0';
            status = do_mkdir(copypath, mode);
            *sp = '/';
        }
        pp = sp + 1;
    }
    if (status == 0)
        status = do_mkdir(path, mode);
    free(copypath);
    return (status);
}

bool GetDirectoryName (char *szFullfilepath, char *szOutdirectoryname)
{
	char szCurchar[SIZE_TINYSTRING];
	bzero (szCurchar, SIZE_TINYSTRING);
	
	int index = strlen (szFullfilepath)-1;
	int lastsepindex = strlen (szFullfilepath)-1;
	
	while (index > 0) {
	
		bzero (szCurchar, SIZE_TINYSTRING);
		strncpy (szCurchar, szFullfilepath+index, 1);
		
		if (strcmp (szCurchar, "/") == 0) {
			lastsepindex = index;
			break;
		}
		
		index--;
	}
	
	strncpy (szOutdirectoryname, szFullfilepath, lastsepindex);
}

void TestGetDirectoryName ()
{
	char szPath[SIZE_STRING];
	bzero (szPath, SIZE_STRING);
	
	char szOutdir[SIZE_STRING];
	bzero (szOutdir, SIZE_STRING);
	
	strcpy (szPath, "/home/danny/desktop/somedir/somepath/myfile.txt");
	
	GetDirectoryName (szPath, szOutdir);
	
	syslog (LOG_DEBUG, "DataFortressServer: szOutdir is: %s\n", szOutdir);
}

int FindString (char *szStrtolook, char *szStrtofind)
{
	char szTemp[SIZE_LARGESTRING];
	int i = 0;
	if (strlen (szStrtolook) >= strlen (szStrtofind)) {		
		if (strlen (szStrtolook) > 0 && strlen (szStrtofind) > 0) {
		
			for (i=0;i<(strlen (szStrtolook) - strlen (szStrtofind))+1;i++) {
				
				bzero (szTemp, SIZE_LARGESTRING);
				strncpy (szTemp, szStrtolook+i, strlen (szStrtofind));
				
				if (strcmp (szTemp, szStrtofind) == 0) {
					// The string has been found.
					return i;
				}				
			}			
		}
	}
	return -1;
}

int ReplaceString (char *szStrtolook, char *szStrtofind, char *szReplacement, char *szOutput)
{
	int index = FindString (szStrtolook, szStrtofind);
	
	if (index != -1) {
		
		char szTemp[SIZE_LARGESTRING];
		bzero (szTemp, SIZE_LARGESTRING);
		strncpy (szTemp, szStrtolook, index);
		
		char szAfter[SIZE_LARGESTRING];
		bzero (szAfter, SIZE_LARGESTRING);
		strcpy (szAfter, szStrtolook+(index+strlen (szStrtofind)));
		
		strcpy (szOutput, szTemp);
		strcat (szOutput, szReplacement);
		strcat (szOutput, szAfter);
		
		//syslog (LOG_DEBUG, "DataFortressServer: The string was: %s\n", szTemp);
		//syslog (LOG_DEBUG, "DataFortressServer: The string after was: %s\n", szAfter);
		
	} else {
	
		// Copy the str to look into the output
		strcpy (szOutput, szStrtolook);
	}
}

int ReplaceStringEx (char *szStrtolook, char *szStrtofind, char *szReplacement, char *szOutput)
{
	// A replacement replace string method which this time finds all matches of an occurance in a string and replaces
	// ALL occurances with the replacement, not just the first find.
	
	int i = 0;
	
	char szCurrent[SIZE_LARGESTRING];
	
	//for (i=0;i<=(strlen (szStrtolook) - strlen (szStrtofind));i++) {
	while (i<=strlen (szStrtolook)) {
		bzero (szCurrent, SIZE_LARGESTRING);
		
		if ((strlen (szStrtolook)-i) >= strlen (szStrtofind)) {
			strncpy (szCurrent, szStrtolook+i, strlen (szStrtofind));
		}
		
		if (strcmp (szCurrent, szStrtofind) == 0) {
			strcat (szOutput, szReplacement);
			i+=strlen (szStrtofind);
		} else {
			strncat (szOutput, szStrtolook+i, 1);
			i++;
		}
	}
}

void TestFindString ()
{
	// Method to test the find string method, ensure it is working properly.
	char szStrtolook[SIZE_STRING];
	bzero (szStrtolook, SIZE_STRING);
	strcpy (szStrtolook, "This is the string to look for a test value which can test this findstring method.");
	
	char szStrtofind[SIZE_STRING];
	bzero (szStrtofind, SIZE_STRING);
	strcpy (szStrtofind, "which");
	
	char szStrtonotfind[SIZE_STRING];
	bzero (szStrtonotfind, SIZE_STRING);
	strcpy (szStrtonotfind, "moosehead");
	
	int index = FindString (szStrtolook, szStrtofind);
	int noindex = FindString (szStrtolook, szStrtonotfind);
	
	syslog (LOG_DEBUG, "DataFortressServer: First FindString (find success test) was looking for: \n%s\nIn the string:\n%s\n and returned index: %i\n", szStrtofind, szStrtolook, index);
	syslog (LOG_DEBUG, "DataFortressServer: Second FindString (find fail test) was looking for: \n%s\nIn the string:\n%s\n and returned index: %i\n", szStrtonotfind, szStrtolook, noindex);
	
}

void TestReplaceString ()
{
	char szStrtolook[SIZE_STRING];
	bzero (szStrtolook, SIZE_STRING);
	strcpy (szStrtolook, "This is the string to look for a test value which can test this findstring method.");
	
	char szStrtofind[SIZE_STRING];
	bzero (szStrtofind, SIZE_STRING);
	strcpy (szStrtofind, "t");
	
	char szStrtoreplace[SIZE_STRING];
	bzero (szStrtoreplace, SIZE_STRING);
	strcpy (szStrtoreplace, "r");
	
	char szOutput[SIZE_STRING];
	bzero (szOutput, SIZE_STRING);
	
	ReplaceStringEx (szStrtolook, szStrtofind, szStrtoreplace, szOutput);
	
	printf("Original: \n%s\n", szStrtolook);
	printf("Replace Output: \n%s\n", szOutput);	
}

void AddClientAccount ()
{
	ClientAccount acct;
	
	/*
		char szCompany[SIZE_STRING];
		char szDescription[SIZE_STRING];
		char szFirstname[SIZE_STRING];
		char szLastname[SIZE_STRING];
		int iMaxAllowedMegaBytes;
		char szPasswordHash[SIZE_STRING];
		char szRootPath[SIZE_STRING];
		char szUsername[SIZE_STRING];
	*/
	
	acct.signatureone = SIGNATUREONE;
	acct.signaturetwo = SIGNATURETWO;
	
	bzero (acct.szCompany, SIZE_STRING);
	strcpy (acct.szCompany, "My Company");
	
	bzero (acct.szDescription, SIZE_STRING);
	strcpy (acct.szDescription, "My Description");
	
	bzero (acct.szFirstname, SIZE_STRING);
	strcpy (acct.szFirstname, "Danny");
	
	bzero (acct.szLastname, SIZE_STRING);
	strcpy (acct.szLastname, "Draper");
	
	acct.iMaxAllowedMegaBytes = 25;
	
	bzero (acct.szPasswordHash, SIZE_STRING);
	strcpy (acct.szPasswordHash, "MyPasswordHash");
	
	bzero (acct.szRootPath, SIZE_STRING);
	strcpy (acct.szRootPath, "/somedir/someotherdir/somepath");
	
	bzero (acct.szUsername, SIZE_STRING);
	strcpy (acct.szUsername, "ddraper");
	
	int hWriteFile;
	mode_t filePerms;
	filePerms = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;
	
	if (DoesFileExist (_szClientaccountpath) == true) {
		hWriteFile = open (_szClientaccountpath, O_WRONLY | O_APPEND, filePerms);
	} else {
		hWriteFile = open (_szClientaccountpath,  O_CREAT | O_WRONLY | O_APPEND, filePerms);
	}
	
	write (hWriteFile, &acct, sizeof (acct));	
	close (hWriteFile);
}

void AddClientAccount (ClientAccount *newclientaccount)
{
	ClientAccount acct;
	
	/*
		char szCompany[SIZE_STRING];
		char szDescription[SIZE_STRING];
		char szFirstname[SIZE_STRING];
		char szLastname[SIZE_STRING];
		int iMaxAllowedMegaBytes;
		char szPasswordHash[SIZE_STRING];
		char szRootPath[SIZE_STRING];
		char szUsername[SIZE_STRING];
	*/
	
	acct.signatureone = SIGNATUREONE;
	acct.signaturetwo = SIGNATURETWO;
	
	bzero (acct.szCompany, SIZE_STRING);
	strncpy (acct.szCompany, newclientaccount->szCompany, SIZE_STRING);
	
	bzero (acct.szDescription, SIZE_STRING);
	strncpy (acct.szDescription, newclientaccount->szDescription, SIZE_STRING);
	
	bzero (acct.szFirstname, SIZE_STRING);
	strncpy (acct.szFirstname, newclientaccount->szFirstname, SIZE_STRING);
	
	bzero (acct.szLastname, SIZE_STRING);
	strncpy (acct.szLastname, newclientaccount->szLastname, SIZE_STRING);
	
	acct.iMaxAllowedMegaBytes = newclientaccount->iMaxAllowedMegaBytes;
	
	bzero (acct.szPasswordHash, SIZE_STRING);
	strncpy (acct.szPasswordHash, newclientaccount->szPasswordHash, SIZE_STRING);
	
	bzero (acct.szRootPath, SIZE_STRING);
	strncpy (acct.szRootPath, newclientaccount->szRootPath, SIZE_STRING);
	
	bzero (acct.szUsername, SIZE_STRING);
	strncpy (acct.szUsername, newclientaccount->szUsername, SIZE_STRING);
	
	int hWriteFile;
	mode_t filePerms;
	filePerms = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;
	
	hWriteFile = open (_szClientaccountpath, O_WRONLY | O_APPEND, filePerms);
	
	write (hWriteFile, &acct, sizeof (acct));
	
	close (hWriteFile);
}

void AddAdminAccount (AdminAccount *newadminaccount)
{
	AdminAccount acct;
	
	acct.signatureone = SIGNATUREONE;
	acct.signaturetwo = SIGNATURETWO;
	
	bzero (acct.szFirstname, SIZE_STRING);
	strncpy (acct.szFirstname, newadminaccount->szFirstname, SIZE_STRING);
	
	bzero (acct.szLastname, SIZE_STRING);
	strncpy (acct.szLastname, newadminaccount->szLastname, SIZE_STRING);
	
	bzero (acct.szPasswordHash, SIZE_STRING);
	strncpy (acct.szPasswordHash, newadminaccount->szPasswordHash, SIZE_STRING);
	
	bzero (acct.szUsername, SIZE_STRING);
	strncpy (acct.szUsername, newadminaccount->szUsername, SIZE_STRING);
	
	bzero (acct.szDescription, SIZE_STRING);
	strncpy (acct.szDescription, newadminaccount->szDescription, SIZE_STRING);
	
	int hWriteFile;
	mode_t filePerms;
	filePerms = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;
	
	if (DoesFileExist (_szAdminaccountpath) == true) {
		hWriteFile = open (_szAdminaccountpath,  O_WRONLY | O_APPEND, filePerms);
	} else {
		hWriteFile = open (_szAdminaccountpath,  O_CREAT | O_WRONLY | O_APPEND, filePerms);
	}
	
	write (hWriteFile, &acct, sizeof (acct));
	
	close (hWriteFile);
	syslog (LOG_DEBUG, "DataFortressServer: New Admin Account has been created with username: %s.\n", acct.szUsername);	
}

bool DoesFileExist (char *szFilename)
{
	ifstream my_file (szFilename);
	
	if (my_file.good ()) {
		return true;
	} else {
		return false;
	}
}

long GetFileLength (char *szFilename)
{
	struct stat statbuf;

	if (stat(szFilename, &statbuf) != -1) {
		return statbuf.st_size;
	}

	return 0;
}

struct tm* GetFileLastWriteTime (char *szFilename)
{
	struct stat statbuf;

	if (stat(szFilename, &statbuf) != -1) {
		printf("last write time is: %s\n", ctime (&statbuf.st_mtime));
		
		return localtime (&statbuf.st_mtime);
	}
}

void TestGetFileLastWriteTime ()
{
	struct tm tmtime;
	
	memcpy (&tmtime, GetFileLastWriteTime (_szClientaccountpath), sizeof (tmtime));
	
	syslog (LOG_DEBUG, "DataFortressServer: Seconds: %i\n", tmtime.tm_sec);
	syslog (LOG_DEBUG, "DataFortressServer: Minutes: %i\n", tmtime.tm_min);
	syslog (LOG_DEBUG, "DataFortressServer: Hour: %i\n", tmtime.tm_hour);
	syslog (LOG_DEBUG, "DataFortressServer: Day of Month: %i\n", tmtime.tm_mday);
	syslog (LOG_DEBUG, "DataFortressServer: Month: %i\n", tmtime.tm_mon);
	syslog (LOG_DEBUG, "DataFortressServer: Year: %i\n", tmtime.tm_year+1900);
	syslog (LOG_DEBUG, "DataFortressServer: Day of Week: %i\n", tmtime.tm_wday);
	syslog (LOG_DEBUG, "DataFortressServer: Day in the Year: %i\n", tmtime.tm_yday);
	syslog (LOG_DEBUG, "DataFortressServer: Daylight saving time: %i\n", tmtime.tm_isdst);
	
	GetFileLastWriteTime (_szClientaccountpath);
	
}

void AddDefaultAdminAccount ()
{
	/*
			int signatureone;
		int signaturetwo;
		char szFirstname[SIZE_STRING];
		char szLastname[SIZE_STRING];
		char szPasswordHash[SIZE_STRING];
		char szUsername[SIZE_STRING];
		char szDescription[SIZE_STRING];
	*/
	
	// Only do this if the admin accounts file does not exist and there is not
	// admin account in an already existing admin account file
	if (DoesFileExist (_szAdminaccountpath) == true) {
		if (DoesAdminUserExist ("admin") == true) {
			printf("No need to create default admin account as it already exists.\n");
			return;
		}
	}
	
	
	AdminAccount acct;
	
	acct.signatureone = SIGNATUREONE;
	acct.signaturetwo = SIGNATURETWO;
	strcpy (acct.szFirstname, "Default Admin");
	strcpy (acct.szLastname, "Default Admin");
	strcpy (acct.szPasswordHash, "admin");
	strcpy (acct.szUsername, "admin");
	strcpy (acct.szDescription, "Default Administrator Account");
	
	int hWriteFile;
	mode_t filePerms;
	filePerms = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;
	
	if (DoesFileExist (_szAdminaccountpath) == true) {
		hWriteFile = open (_szAdminaccountpath,  O_WRONLY | O_APPEND, filePerms);
	} else {
		hWriteFile = open (_szAdminaccountpath,  O_CREAT | O_WRONLY | O_APPEND, filePerms);
	}
	
	
	write (hWriteFile, &acct, sizeof (acct));
	
	close (hWriteFile);
	syslog (LOG_DEBUG, "DataFortressServer: Default Admin Account has been created.\n");
	
}

void AddAdminAccount (char *szUsername, char *szPasswordHash)
{
	AdminAccount acct;
	
	acct.signatureone = SIGNATUREONE;
	acct.signaturetwo = SIGNATURETWO;
	
	bzero (acct.szFirstname, SIZE_STRING);
	strcpy (acct.szFirstname, "Default Admin");
	
	bzero (acct.szLastname, SIZE_STRING);
	strcpy (acct.szLastname, "Default Admin");
	
	bzero (acct.szPasswordHash, SIZE_STRING);
	strcpy (acct.szPasswordHash, szPasswordHash);
	
	bzero (acct.szUsername, SIZE_STRING);
	strcpy (acct.szUsername, szUsername);
	
	bzero (acct.szDescription, SIZE_STRING);
	strcpy (acct.szDescription, "Administrator Account");
	
	int hWriteFile;
	mode_t filePerms;
	filePerms = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;
	
	if (DoesFileExist (_szAdminaccountpath) == true) {
		hWriteFile = open (_szAdminaccountpath,  O_WRONLY | O_APPEND, filePerms);
	} else {
		hWriteFile = open (_szAdminaccountpath,  O_CREAT | O_WRONLY | O_APPEND, filePerms);
	}
	
	
	write (hWriteFile, &acct, sizeof (acct));
	
	close (hWriteFile);
	syslog (LOG_DEBUG, "DataFortressServer: New Admin Account has been created.\n");	
}

bool DoesClientAccountExist (char *szUsername, char *szPasswordhash)
{
	int hReadFile;
	hReadFile = open (_szClientaccountpath, O_RDONLY);
	
	MemoryBuffer readbuffer;
	readbuffer.SetSize (sizeof (ClientAccount));
	
	ssize_t bytes_read;
	
	while (1)
	{
		bytes_read = read (hReadFile, readbuffer.GetBuffer(), sizeof (ClientAccount));
		
		if (bytes_read == sizeof (ClientAccount))
		{
			ClientAccount currentaccount;
			memcpy (&currentaccount, readbuffer.GetBuffer(), sizeof (ClientAccount));
			
			if (strcmp (szUsername, currentaccount.szUsername) == 0) {
				
				if (strcmp (szPasswordhash, currentaccount.szPasswordHash) == 0) {
					readbuffer.Clear ();
					close (hReadFile);
					syslog (LOG_DEBUG, "DataFortressServer: Client account found, username: %s, password: %s\n", szUsername, szPasswordhash);
					return true;
				}
				
			}
			
			syslog (LOG_DEBUG, "DataFortressServer: Current firstname: %s\n", currentaccount.szFirstname);
		} else {
			break;
		}
	}
	
	close (hReadFile);
	readbuffer.Clear ();
	
	return false;
}

bool GetClientAccountRootPath (char *szUsername, char *szOutput)
{
	int hReadFile;
	hReadFile = open (_szClientaccountpath, O_RDONLY);
	
	MemoryBuffer readbuffer;
	readbuffer.SetSize (sizeof (ClientAccount));
	
	ssize_t bytes_read;
	
	while (1)
	{
		bytes_read = read (hReadFile, readbuffer.GetBuffer(), sizeof (ClientAccount));
		
		if (bytes_read == sizeof (ClientAccount))
		{
			ClientAccount currentaccount;
			memcpy (&currentaccount, readbuffer.GetBuffer(), sizeof (ClientAccount));
			
			if (strcmp (szUsername, currentaccount.szUsername) == 0) {
				
				syslog (LOG_DEBUG, "DataFortressServer: Client account found, username: %s, account root path: %s\n", szUsername, currentaccount.szRootPath);
				
				bzero (szOutput, SIZE_STRING);
				strcpy (szOutput, currentaccount.szRootPath);
				
				close (hReadFile);
				readbuffer.Clear ();
				
				return true;
				
			}
			
			syslog (LOG_DEBUG, "DataFortressServer: Current username: %s\n", currentaccount.szUsername);
		} else {
			break;
		}
	}
	
	close (hReadFile);
	readbuffer.Clear ();
	
	return false;
}

void TestGetClientAccountRootPath ()
{
	
	char szUsername[SIZE_STRING];
	bzero (szUsername, SIZE_STRING);
	strcpy (szUsername, "ddraper");
	
	char szOutput[SIZE_STRING];
	
	GetClientAccountRootPath (szUsername, szOutput);
	
	printf("GetClientAccountRootPath returned: %s\n", szOutput);
	
}

bool CombinePath (char *szPath1, char *szPath2, char *szOutput)
{
	char sz1End[SIZE_STRING];
	bzero (sz1End, SIZE_STRING);
	
	char sz2Beg[SIZE_STRING];
	bzero (sz2Beg, SIZE_STRING);
	
	strncpy (sz1End, szPath1+(strlen (szPath1)-1), 1);
	strncpy (sz2Beg, szPath2, 1);
	
	//syslog (LOG_DEBUG, "DataFortressServer: End of path 1 is: %s\n", sz1End);
	//syslog (LOG_DEBUG, "DataFortressServer: Beginning of path 2 is: %s\n", sz2Beg);
	
	bool hasseparator = (strcmp (sz1End, "/") == 0 || strcmp (sz2Beg, "/") == 0);
	
	if (hasseparator == true) {
		strcat (szOutput, szPath1);
		strcat (szOutput, szPath2);
	} else {
		strcat (szOutput, szPath1);
		strcat (szOutput, "/");
		strcat (szOutput, szPath2);
	}
	
	return true;
}

void TestCombinePath ()
{
	char szPath1[SIZE_STRING];
	bzero (szPath1, SIZE_STRING);
	strcpy (szPath1, "");
	
	char szPath2[SIZE_STRING];
	bzero (szPath2, SIZE_STRING);
	strcpy (szPath2, "");
	
	char szResult[SIZE_STRING];
	bzero (szResult, SIZE_STRING);
	
	CombinePath (szPath1, szPath2, szResult);
	
	syslog (LOG_DEBUG, "DataFortressServer: Result: %s\n", szResult);
}

bool DoesAdminUserExist (char *szUsername)
{
	int hReadFile;
	hReadFile = open (_szAdminaccountpath, O_RDONLY);
	
	MemoryBuffer readbuffer;
	readbuffer.SetSize (sizeof (AdminAccount));
	
	ssize_t bytes_read;
	
	while (1)
	{
		bytes_read = read (hReadFile, readbuffer.GetBuffer(), sizeof (AdminAccount));
		
		if (bytes_read == sizeof (AdminAccount))
		{
			AdminAccount currentaccount;
			memcpy (&currentaccount, readbuffer.GetBuffer(), sizeof (AdminAccount));
			
			if (strcmp (szUsername, currentaccount.szUsername) == 0) {
				readbuffer.Clear ();
				close (hReadFile);
				printf("Admin account already exists.\n");
				return true;
			}
		} else {
			break;
		}
	}
	
	close (hReadFile);
	readbuffer.Clear ();
	return false;
}

bool GetAllAdminAccounts (MemoryBuffer *outBuffer)
{
	unsigned long filelength = GetFileLength (_szAdminaccountpath);
	
	int hReadFile;
	
	hReadFile = open (_szAdminaccountpath, O_RDONLY);
	
	MemoryBuffer readbuffer;
	readbuffer.SetSize (sizeof (AdminAccount));
	
	ssize_t bytes_read;
	
	outBuffer->SetSize (filelength);
	
	while (1)
	{
		bytes_read = read (hReadFile, readbuffer.GetBuffer(), sizeof (AdminAccount));
		
		if (bytes_read == sizeof (AdminAccount))
		{
			AdminAccount currentaccount;
			memcpy (&currentaccount, readbuffer.GetBuffer(), sizeof (AdminAccount));
			
			outBuffer->Append (&currentaccount, sizeof (AdminAccount));
			
		} else {
			break;
		}
	}
	
	close (hReadFile);
	readbuffer.Clear ();
	return true;
}


bool ListAllClientAccounts ()
{
	unsigned long filelength = GetFileLength (_szClientaccountpath);
	
	int hReadFile;
	
	hReadFile = open (_szClientaccountpath, O_RDONLY);
	
	MemoryBuffer readbuffer;
	readbuffer.SetSize (sizeof (ClientAccount));
	
	ssize_t bytes_read;
	
	while (1)
	{
		bytes_read = read (hReadFile, readbuffer.GetBuffer(), sizeof (ClientAccount));
		
		if (bytes_read == sizeof (ClientAccount)) {
			
			ClientAccount currentaccount;
			memcpy (&currentaccount, readbuffer.GetBuffer(), sizeof (ClientAccount));
			
			syslog (LOG_DEBUG, "DataFortressServer: Username: %s Firstname: %s, Lastname: %s\n", currentaccount.szUsername, currentaccount.szFirstname, currentaccount.szLastname);
			
		} else {
			break;
		}
	}
	
	close (hReadFile);
	readbuffer.Clear ();
	return true;
}

bool GetAllClientAccounts (MemoryBuffer *outBuffer)
{
	unsigned long filelength = GetFileLength (_szClientaccountpath);
	
	int hReadFile;
	
	hReadFile = open (_szClientaccountpath, O_RDONLY);
	
	MemoryBuffer readbuffer;
	readbuffer.SetSize (sizeof (ClientAccount));
	
	ssize_t bytes_read;
	
	outBuffer->SetSize (filelength);
	
	while (1)
	{
		bytes_read = read (hReadFile, readbuffer.GetBuffer(), sizeof (ClientAccount));
		
		if (bytes_read == sizeof (ClientAccount)) {
			
			ClientAccount currentaccount;
			memcpy (&currentaccount, readbuffer.GetBuffer(), sizeof (ClientAccount));
			
			outBuffer->Append (&currentaccount, sizeof (ClientAccount));
			
		} else {
			break;
		}
	}
	
	close (hReadFile);
	readbuffer.Clear ();
	return true;
}

bool UpdateClientAccount (ClientAccount *updatedaccount)
{
	// When updating client accounts, we have to remember the client account update process is keyed on the username.
	
	// Firstly, take a copy of the client account we've been supplied with
	ClientAccount acct;
	
	/*
		char szCompany[SIZE_STRING];
		char szDescription[SIZE_STRING];
		char szFirstname[SIZE_STRING];
		char szLastname[SIZE_STRING];
		int iMaxAllowedMegaBytes;
		char szPasswordHash[SIZE_STRING];
		char szRootPath[SIZE_STRING];
		char szUsername[SIZE_STRING];
	*/
	
	acct.signatureone = SIGNATUREONE;
	acct.signaturetwo = SIGNATURETWO;
	
	bzero (acct.szCompany, SIZE_STRING);
	strncpy (acct.szCompany, updatedaccount->szCompany, SIZE_STRING);
	
	bzero (acct.szDescription, SIZE_STRING);
	strncpy (acct.szDescription, updatedaccount->szDescription, SIZE_STRING);
	
	bzero (acct.szFirstname, SIZE_STRING);
	strncpy (acct.szFirstname, updatedaccount->szFirstname, SIZE_STRING);
	
	bzero (acct.szLastname, SIZE_STRING);
	strncpy (acct.szLastname, updatedaccount->szLastname, SIZE_STRING);
	
	acct.iMaxAllowedMegaBytes = updatedaccount->iMaxAllowedMegaBytes;
	
	bzero (acct.szPasswordHash, SIZE_STRING);
	strncpy (acct.szPasswordHash, updatedaccount->szPasswordHash, SIZE_STRING);
	
	bzero (acct.szRootPath, SIZE_STRING);
	strncpy (acct.szRootPath, updatedaccount->szRootPath, SIZE_STRING);
	
	bzero (acct.szUsername, SIZE_STRING);
	strncpy (acct.szUsername, updatedaccount->szUsername, SIZE_STRING);
		
	unsigned long filelength = GetFileLength (_szClientaccountpath);
	
	int hReadFile;
	
	hReadFile = open (_szClientaccountpath, O_RDONLY);
	
	MemoryBuffer readbuffer;
	MemoryBuffer completedbuffer;
	
	readbuffer.SetSize (sizeof (ClientAccount));
	completedbuffer.SetSize (filelength);
	
	ssize_t bytes_read;
	
	while (1)
	{
		bytes_read = read (hReadFile, readbuffer.GetBuffer(), sizeof (ClientAccount));
		
		if (bytes_read == sizeof (ClientAccount)) {
			
			ClientAccount currentaccount;
			memcpy (&currentaccount, readbuffer.GetBuffer(), sizeof (ClientAccount));
		
			if (strcmp (currentaccount.szUsername, acct.szUsername) == 0) {
				completedbuffer.Append (&acct, sizeof (ClientAccount)); // Substitute the modified account
			} else {
				completedbuffer.Append (&currentaccount, sizeof (ClientAccount));
			}
			
		} else {
			break;
		}
	}
	
	close (hReadFile);
	readbuffer.Clear ();
	
	// Now we have to save the completed buffer back to the original file.	
	int hWriteFile;
	mode_t filePerms;
	filePerms = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;
	hWriteFile = open (_szClientaccountpath,  O_TRUNC | O_WRONLY, filePerms);
	write (hWriteFile, completedbuffer.GetBuffer (), completedbuffer.GetSize ());
	
	close (hWriteFile);
	syslog (LOG_DEBUG, "DataFortressServer: Client account with username: %s has been updated.\n", acct.szUsername);	
	
	return true;
}

bool UpdateAdminAccount (AdminAccount *updatedaccount)
{
	AdminAccount acct;
	
	acct.signatureone = SIGNATUREONE;
	acct.signaturetwo = SIGNATURETWO;
	
	bzero (acct.szFirstname, SIZE_STRING);
	strncpy (acct.szFirstname, updatedaccount->szFirstname, SIZE_STRING);
	
	bzero (acct.szLastname, SIZE_STRING);
	strncpy (acct.szLastname, updatedaccount->szLastname, SIZE_STRING);
	
	bzero (acct.szPasswordHash, SIZE_STRING);
	strncpy (acct.szPasswordHash, updatedaccount->szPasswordHash, SIZE_STRING);
	
	bzero (acct.szUsername, SIZE_STRING);
	strncpy (acct.szUsername, updatedaccount->szUsername, SIZE_STRING);
	
	bzero (acct.szDescription, SIZE_STRING);
	strncpy (acct.szDescription, updatedaccount->szDescription, SIZE_STRING);
	
	unsigned long filelength = GetFileLength (_szAdminaccountpath);
	
	int hReadFile;
	
	hReadFile = open (_szAdminaccountpath, O_RDONLY);
	
	MemoryBuffer readbuffer;
	MemoryBuffer completedbuffer;
	
	readbuffer.SetSize (sizeof (AdminAccount));
	completedbuffer.SetSize (filelength);
	
	ssize_t bytes_read;
	
	while (1)
	{
		bytes_read = read (hReadFile, readbuffer.GetBuffer(), sizeof (AdminAccount));
		
		if (bytes_read == sizeof (AdminAccount)) {
			
			AdminAccount currentaccount;
			memcpy (&currentaccount, readbuffer.GetBuffer(), sizeof (AdminAccount));
		
			if (strcmp (currentaccount.szUsername, acct.szUsername) == 0) {
				completedbuffer.Append (&acct, sizeof (AdminAccount)); // Substitute the modified account
			} else {
				completedbuffer.Append (&currentaccount, sizeof (AdminAccount));
			}
			
		} else {
			break;
		}
	}
	
	close (hReadFile);
	readbuffer.Clear ();
	
	// Now we have to save the completed buffer back to the original file.	
	int hWriteFile;
	mode_t filePerms;
	filePerms = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;
	hWriteFile = open (_szAdminaccountpath,  O_TRUNC | O_WRONLY, filePerms);
	write (hWriteFile, completedbuffer.GetBuffer (), completedbuffer.GetSize ());
	
	close (hWriteFile);
	syslog (LOG_DEBUG, "DataFortressServer: Admin account with username: %s has been updated.\n", acct.szUsername);	
	
	return true;
}

bool UpdateClientPassword (char *szAccount, char *szPasswordhash)
{
	// When updating client accounts, we have to remember the client account update process is keyed on the username.
	unsigned long filelength = GetFileLength (_szClientaccountpath);
	
	int hReadFile;
	
	hReadFile = open (_szClientaccountpath, O_RDONLY);
	
	MemoryBuffer readbuffer;
	MemoryBuffer completedbuffer;
	
	readbuffer.SetSize (sizeof (ClientAccount));
	completedbuffer.SetSize (filelength);
	
	ssize_t bytes_read;
	
	while (1)
	{
		bytes_read = read (hReadFile, readbuffer.GetBuffer(), sizeof (ClientAccount));
		
		if (bytes_read == sizeof (ClientAccount)) {
			
			ClientAccount currentaccount;
			memcpy (&currentaccount, readbuffer.GetBuffer(), sizeof (ClientAccount));
		
			if (strcmp (currentaccount.szUsername, szAccount) == 0) {
				
				strcpy (currentaccount.szPasswordHash, szPasswordhash);
				completedbuffer.Append (&currentaccount, sizeof (ClientAccount)); // Substitute the modified account
			} else {
				completedbuffer.Append (&currentaccount, sizeof (ClientAccount));
			}
			
		} else {
			break;
		}
	}
	
	close (hReadFile);
	readbuffer.Clear ();
	
	// Now we have to save the completed buffer back to the original file.	
	int hWriteFile;
	mode_t filePerms;
	filePerms = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;
	hWriteFile = open (_szClientaccountpath,  O_TRUNC | O_WRONLY, filePerms);
	write (hWriteFile, completedbuffer.GetBuffer (), completedbuffer.GetSize ());
	
	close (hWriteFile);
	syslog (LOG_DEBUG, "DataFortressServer: Client account with username: %s has been password hash updated with password hash: %s.\n", szAccount, szPasswordhash);	
	
	return true;
}

bool UpdateAdminPassword (char *szAccount, char *szPasswordhash)
{	
	unsigned long filelength = GetFileLength (_szAdminaccountpath);
	
	int hReadFile;
	
	hReadFile = open (_szAdminaccountpath, O_RDONLY);
	
	MemoryBuffer readbuffer;
	MemoryBuffer completedbuffer;
	
	readbuffer.SetSize (sizeof (AdminAccount));
	completedbuffer.SetSize (filelength);
	
	ssize_t bytes_read;
	
	while (1)
	{
		bytes_read = read (hReadFile, readbuffer.GetBuffer(), sizeof (AdminAccount));
		
		if (bytes_read == sizeof (AdminAccount)) {
			
			AdminAccount currentaccount;
			memcpy (&currentaccount, readbuffer.GetBuffer(), sizeof (AdminAccount));
		
			if (strcmp (currentaccount.szUsername, szAccount) == 0) {
				
				strcpy (currentaccount.szPasswordHash, szPasswordhash);
				completedbuffer.Append (&currentaccount, sizeof (AdminAccount));
			} else {
				completedbuffer.Append (&currentaccount, sizeof (AdminAccount));
			}
			
		} else {
			break;
		}
	}
	
	close (hReadFile);
	readbuffer.Clear ();
	
	// Now we have to save the completed buffer back to the original file.	
	int hWriteFile;
	mode_t filePerms;
	filePerms = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;
	hWriteFile = open (_szAdminaccountpath,  O_TRUNC | O_WRONLY, filePerms);
	write (hWriteFile, completedbuffer.GetBuffer (), completedbuffer.GetSize ());
	
	close (hWriteFile);
	syslog (LOG_DEBUG, "DataFortressServer: Admin account with username: %s has been password hash updated.\n", szAccount);	
	
	return true;
}

bool DeleteClientAccount (char *szAccount)
{
	// When updating client accounts, we have to remember the client account update process is keyed on the username.
		
	unsigned long filelength = GetFileLength (_szClientaccountpath);
	
	int hReadFile;
	
	hReadFile = open (_szClientaccountpath, O_RDONLY);
	
	MemoryBuffer readbuffer;
	MemoryBuffer completedbuffer;
	
	readbuffer.SetSize (sizeof (ClientAccount));
	completedbuffer.SetSize (filelength);
	
	ssize_t bytes_read;
	
	while (1)
	{
		bytes_read = read (hReadFile, readbuffer.GetBuffer(), sizeof (ClientAccount));
		
		if (bytes_read == sizeof (ClientAccount)) {
			
			ClientAccount currentaccount;
			memcpy (&currentaccount, readbuffer.GetBuffer(), sizeof (ClientAccount));
		
			if (strcmp (currentaccount.szUsername, szAccount) != 0) {
				completedbuffer.Append (&currentaccount, sizeof (ClientAccount)); // Substitute the modified account
			}
			
		} else {
			break;
		}
	}
	
	close (hReadFile);
	readbuffer.Clear ();
	
	// Now we have to save the completed buffer back to the original file.	
	int hWriteFile;
	mode_t filePerms;
	filePerms = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;
	hWriteFile = open (_szClientaccountpath,  O_TRUNC | O_WRONLY, filePerms);
	write (hWriteFile, completedbuffer.GetBuffer (), completedbuffer.GetSize ());
	
	close (hWriteFile);
	syslog (LOG_DEBUG, "DataFortressServer: Client account with username: %s has been deleted.\n", szAccount);	
	
	return true;
}

bool DeleteAdminAccount (char *szAccount)
{
	unsigned long filelength = GetFileLength (_szAdminaccountpath);
	
	int hReadFile;
	
	hReadFile = open (_szAdminaccountpath, O_RDONLY);
	
	MemoryBuffer readbuffer;
	MemoryBuffer completedbuffer;
	
	readbuffer.SetSize (sizeof (AdminAccount));
	completedbuffer.SetSize (filelength);
	
	ssize_t bytes_read;
	
	while (1)
	{
		bytes_read = read (hReadFile, readbuffer.GetBuffer(), sizeof (AdminAccount));
		
		if (bytes_read == sizeof (AdminAccount)) {
			
			AdminAccount currentaccount;
			memcpy (&currentaccount, readbuffer.GetBuffer(), sizeof (AdminAccount));
		
			if (strcmp (currentaccount.szUsername, szAccount) != 0) {
				completedbuffer.Append (&currentaccount, sizeof (AdminAccount));
			}
			
		} else {
			break;
		}
	}
	
	close (hReadFile);
	readbuffer.Clear ();
	
	// Now we have to save the completed buffer back to the original file.	
	int hWriteFile;
	mode_t filePerms;
	filePerms = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;
	hWriteFile = open (_szAdminaccountpath,  O_TRUNC | O_WRONLY, filePerms);
	write (hWriteFile, completedbuffer.GetBuffer (), completedbuffer.GetSize ());
	
	close (hWriteFile);
	syslog (LOG_DEBUG, "DataFortressServer: Admin account with username: %s has been deleted.\n", szAccount);	
	
	return true;
}

bool DoesAdminAccountExist (char *szUsername, char *szPasswordHash)
{
	int hReadFile;
	hReadFile = open (_szAdminaccountpath, O_RDONLY);
	
	MemoryBuffer readbuffer;
	readbuffer.SetSize (sizeof (AdminAccount));
	
	ssize_t bytes_read;
	
	while (1)
	{
		bytes_read = read (hReadFile, readbuffer.GetBuffer(), sizeof (AdminAccount));
		
		if (bytes_read == sizeof (AdminAccount))
		{
			AdminAccount currentaccount;
			memcpy (&currentaccount, readbuffer.GetBuffer(), sizeof (AdminAccount));
			
			if (strcmp (szUsername, currentaccount.szUsername) == 0) {
				if (strcmp (szPasswordHash, currentaccount.szPasswordHash) == 0) {
					readbuffer.Clear ();
					close (hReadFile);
					printf("Admin account  exists. Username: %s, Password: %s\n", szUsername, szPasswordHash);
					return true;
				}
			}
			
		} else {
			break;
		}
	}
	
	close (hReadFile);
	readbuffer.Clear ();
	return false;
}

bool ReadAdminAccountList (MemoryBuffer *memDest)
{
	ssize_t bytes_read;
	int hReadFile;
	MemoryBuffer readbuffer;
	
	if (DoesFileExist (_szAdminaccountpath) == false) {
		return false;
	}
	
	long filelength = GetFileLength (_szAdminaccountpath);
	
	memDest->SetSize (filelength);
	readbuffer.SetSize (sizeof (AdminAccount));
	
	while(1)
	{
		bytes_read = read (hReadFile, readbuffer.GetBuffer(), sizeof (AdminAccount));
		
		if (bytes_read == sizeof (AdminAccount)) {
		
			memDest->Append (readbuffer.GetBuffer(), sizeof (AdminAccount));
		} else {
			break;
		}
	}
	
	close (hReadFile);
	readbuffer.Clear ();
	return true;
}

void *receive_cmds_ex (void* arg)
{
	printf("Client ex thread created.\n");
	
	bool bbulkmode = false;
	int current_client = *(int *)arg;
	
	int res;
	
	//				SIG1		   SIG2			SIZE
	int initbufsize = sizeof (int) + sizeof (int) + sizeof (unsigned long);
	
	MemoryBuffer initialBuffer;
	initialBuffer.SetSize (initbufsize);
	
	MemoryBuffer bulkBuffer;
	unsigned long bulkpointer = 0;
	unsigned long bulksize = 0;
	MemoryBuffer bulkStorage;
	
	int hWriteFile;
	mode_t filePerms;
	filePerms = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;
	unsigned long long receivefilepointer;
	unsigned long long receivefilesize;
	MemoryBuffer replyBuffer;
	bool bfiletransfermode = false;
	MemoryBuffer fileTransferBuffer;
	unsigned long ltempsize = 1000000;
	
	while (1)
	{
		if (bfiletransfermode == true) {
			
			res = read(current_client, fileTransferBuffer.GetBuffer(), fileTransferBuffer.GetSize());
			
			if (res == -1) {
				printf("There was a socket error with id: %s\n", strerror (errno));
				close (current_client);
				break;
			}
			
			if (res == 0) {
				printf("There was an error receiving from the client. Zero bytes received.\n");
				close (current_client);
				break;
			}
			
			bool bTestwrite;
			write (hWriteFile, fileTransferBuffer.GetBuffer(), res);
			
			receivefilepointer+=res;
			
			if (receivefilepointer == receivefilesize) {
				printf("Complete file received.\n");
				close (hWriteFile);
				bfiletransfermode = false;
			}
			
			if (receivefilepointer > receivefilesize) {
				printf("File received, but file pointer is greater than file size.\n");
				close (hWriteFile);
			}
			
			
		} else { // bfiletransfermode
			
			if (bbulkmode == false) {
			
				// Non bulk mode receive - we're only expecting a signature and bulk buffer size at this stage
				res = read (current_client, initialBuffer.GetBuffer(), initialBuffer.GetSize());
				
				// Here we're expecting a message in the following format
				// int	int	    int			whatever
				// SIG1 | SIG2 | upcomingsize | data
				
				
				if (res == -1) {
					printf("There was a socket error with id: %s\n", strerror (errno));
					close (current_client);
					break;
				}
				
				if (res == 0) {
					printf("There was an error receiving from the client. Zero bytes received.\n");
					close (current_client);
					break;
				}
				
				int sig1 = 0;
				int sig2 = 0;
				unsigned long finalbufsize = 0;
				
				unsigned long ipointer  = 0;
				memcpy (&sig1, (char *) initialBuffer.GetBuffer() + ipointer, sizeof (int));
				ipointer +=sizeof (int);
				
				memcpy (&sig2, (char *) initialBuffer.GetBuffer() + ipointer, sizeof (int));
				ipointer +=sizeof (int);
				
				memcpy (&finalbufsize, (char *) initialBuffer.GetBuffer() + ipointer, sizeof (unsigned long));
				ipointer +=sizeof (unsigned long);
				
				syslog (LOG_DEBUG, "DataFortressServer: Res is: %i\n", res);
				syslog (LOG_DEBUG, "DataFortressServer: Signature 1: %i\n", sig1);
				syslog (LOG_DEBUG, "DataFortressServer: Signature 2: %i\n", sig2);
				
				if (sig1 == SIGNATUREONE && sig2 == SIGNATURETWO) {
					printf("Signature is valid.\n");
					printf("Bulk Buffer Size: %i\n", (int)finalbufsize);
					
					bulkpointer = 0;
					bulksize = finalbufsize;
					bulkBuffer.SetSize (finalbufsize);
					bulkStorage.SetSize (finalbufsize);
					bbulkmode = true;
				}
				
			} else {
			
				// We're in bulk mode - receive everything to the already set up buffer which should have been done in non-bulk mode
				res = read (current_client, bulkBuffer.GetBuffer(), bulkBuffer.GetSize ()); // Receive the data
				
				bulkStorage.Append (bulkBuffer.GetBuffer(), res);
				bulkpointer += res;
				
				syslog (LOG_DEBUG, "DataFortressServer: Received bulk chunk. %i bytes. Pointer is %i of %i\n", res, (int) bulkpointer, (int) bulksize);
				
				if (bulkpointer == bulksize) {
					// We've received all the data
					// Preprepared signatures for the response
					int sig1 = SIGNATUREONE;
					int sig2 = SIGNATURETWO;
					int msgident;
					
					printf("Entire bulk buffer received. Back to non-bulk mode.\n");
					
					int msg;
					int ipointer = 0;
					
					memcpy (&msg, (char *) bulkStorage.GetBuffer() + ipointer, sizeof (int));
					ipointer += sizeof (int);
					
					if (msg == TEST_MESSAGE) {
						printf("Test Message Identifier has been received.\n");
					}
					
					if (msg == PREPARE_FILETRANSFER) {
						
						syslog (LOG_DEBUG, "DataFortressServer: Prepare file transfer message received Client wants to send a file....\n");
						
						int useraccountlen = 0;
						memcpy (&useraccountlen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: User account len is: %i\n", useraccountlen);
						
						char szUseraccount[SIZE_STRING];
						bzero(szUseraccount, SIZE_STRING);
						strncpy (szUseraccount, (char *) bulkStorage.GetBuffer () + ipointer, useraccountlen);
						ipointer+=useraccountlen;
						syslog (LOG_DEBUG, "DataFortressServer: User Account: %s\n", szUseraccount);
						
						
						int usertokenlen = 0;
						memcpy (&usertokenlen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: User token len is: %i\n", usertokenlen);
						
						char szUsertoken[SIZE_STRING];
						bzero(szUsertoken, SIZE_STRING);
						strncpy (szUsertoken, (char *) bulkStorage.GetBuffer () + ipointer, usertokenlen);
						ipointer+=usertokenlen;
						syslog (LOG_DEBUG, "DataFortressServer: User Token: %s\n", szUsertoken);
						
						
						int localfilenamelen = 0;
						memcpy (&localfilenamelen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Local filename len is: %i\n", localfilenamelen);
						
						char szLocalfilename[SIZE_STRING];
						bzero(szLocalfilename, SIZE_STRING);
						strncpy (szLocalfilename, (char *) bulkStorage.GetBuffer () + ipointer, localfilenamelen);
						ipointer+=localfilenamelen;
						syslog (LOG_DEBUG, "DataFortressServer: Local filename: %s\n", szLocalfilename);
						
						
						unsigned long long filesize = 0;
						memcpy (&filesize, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (unsigned long long));
						ipointer+=sizeof (unsigned long long);
						syslog (LOG_DEBUG, "DataFortressServer: Filesize is: %i\n", filesize);
						
						
						int destfilepathlen = 0;
						memcpy (&destfilepathlen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Dest file path len is: %i\n", destfilepathlen);
						
						char szDestfilepath[SIZE_STRING];
						bzero(szDestfilepath, SIZE_STRING);
						strncpy (szDestfilepath, (char *) bulkStorage.GetBuffer () + ipointer, destfilepathlen);
						ipointer+=destfilepathlen;
						syslog (LOG_DEBUG, "DataFortressServer: Dest file path: %s\n", szDestfilepath);
						
						
						int jobnamelen = 0;
						memcpy (&jobnamelen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Job name len is: %i\n", jobnamelen);
						
						char szJobname[SIZE_STRING];
						bzero(szJobname, SIZE_STRING);
						strncpy (szJobname, (char *) bulkStorage.GetBuffer () + ipointer, jobnamelen);
						ipointer+=jobnamelen;
						syslog (LOG_DEBUG, "DataFortressServer: Job Name: %s\n", szJobname);
						
						// Get the client account path
						char szClientrootpath[SIZE_STRING];
						bzero (szClientrootpath, SIZE_STRING);
						
						if (GetClientAccountRootPath (szUseraccount, szClientrootpath) == true) {
							
							char szAccountrootpath[SIZE_STRING];
							bzero (szAccountrootpath, SIZE_STRING);
							
							CombinePath (szClientrootpath, szJobname, szAccountrootpath);
							
							if (FindString (szDestfilepath, "$$$ACCOUNTROOT$$$") != -1) {
								
								// Servepath will contain the localised path to look for the file on our side.
								char szServerpath[SIZE_STRING];
								bzero (szServerpath, SIZE_STRING);
								
								ReplaceStringEx (szDestfilepath, "$$$ACCOUNTROOT$$$", szAccountrootpath, szServerpath);
								
								char szConvertedserverpath[SIZE_STRING];
								bzero (szConvertedserverpath, SIZE_STRING);
								
								ReplaceStringEx (szServerpath, "\\", "/", szConvertedserverpath); // This will take windows backslashes which windows likes and convert them to linux forward slashes for path separators.
								
								syslog (LOG_DEBUG, "DataFortressServer: Localised path is %s. Creating file for write...\n", szConvertedserverpath);
								
								// Now we need to ensure the directory path for the destination file exists, if not then create it
								//bool GetDirectoryName (char *szFullfilepath, char *szOutdirectoryname)
								
								char szDirectory[SIZE_STRING];
								bzero (szDirectory, SIZE_STRING);
								
								GetDirectoryName (szConvertedserverpath, szDirectory);
								
								syslog (LOG_DEBUG, "DataFortressServer: Directory of localised path is: %s\n", szDirectory);
								
								syslog (LOG_DEBUG, "DataFortressServer: Creating directory tree...\n");
								// Now ensure the path exists - attempt to create the tree
								mkpath (szDirectory, 0777);
								syslog (LOG_DEBUG, "DataFortressServer: Directory tree created.\n");
								
								// Now create and open the file for writing
								hWriteFile = open (szConvertedserverpath, O_WRONLY | O_CREAT | O_TRUNC, filePerms);
								
								if (hWriteFile == -1) {
									syslog (LOG_DEBUG, "DataFortressServer: There was a problem opening the new file for writing. %s\n", strerror (errno));
								}
								
								
								receivefilepointer = 0;
								receivefilesize = filesize;
								
								fileTransferBuffer.Clear ();
								fileTransferBuffer.SetSize (ltempsize);
								bfiletransfermode = true;
								
								// Send the client a reply so that it can trigger the file sending
								int sig1 = SIGNATUREONE;
								int sig2 = SIGNATURETWO;
								int msgident = FILETRANSFERREADY;
								
								replyBuffer.Clear ();
								replyBuffer.SetSize (1000);
								replyBuffer.Append (&sig1, sizeof (int));
								replyBuffer.Append (&sig2, sizeof (int));
								replyBuffer.Append (&msgident, sizeof (int));
								
								usleep (100);
								
								write(current_client, replyBuffer.GetBuffer(), replyBuffer.GetSize());
								
								printf("Client Reply sent and File Transfer mode has been set - awaiting data...\n");
								
								
								
							} else {
								syslog (LOG_DEBUG, "DataFortressServer: Prepare file transfer message was received, but no account root tag was found in the supplied path.\n");
							}
							
						} else {
							syslog (LOG_DEBUG, "DataFortressServer: Unable to get the client root path for user: %s\n", szUseraccount);
						}
						
						/*
						hWriteFile = open ("/tmp/receivefile.avi", O_WRONLY | O_CREAT | O_TRUNC, filePerms);
						
						receivefilepointer = 0;
						receivefilesize = filesize;
						
						fileTransferBuffer.Clear ();
						fileTransferBuffer.SetSize (ltempsize);
						
						bfiletransfermode = true;
						
						if (hWriteFile == -1) {
							syslog (LOG_DEBUG, "DataFortressServer: There was a problem opening the new file for writing. %s\n", strerror (errno));
						}
						
						// Send the client a reply so that it can trigger the file sending
						int sig1 = SIGNATUREONE;
						int sig2 = SIGNATURETWO;
						int msgident = BEGINFILESEND;
						
						replyBuffer.Clear ();
						replyBuffer.SetSize (1000);
						replyBuffer.Append (&sig1, sizeof (int));
						replyBuffer.Append (&sig2, sizeof (int));
						replyBuffer.Append (&msgident, sizeof (int));
						
						sleep (10);
						
						write(current_client, replyBuffer.GetBuffer(), replyBuffer.GetSize());
						
						printf("Client Reply sent and File Transfer mode has been set - awaiting data...\n");
						*/
						
					}
					
					if (msg == AUTHENTICATEADMIN)
					{
						printf("Authenticate Admin Message received.\n");
						
						int accountnamelen = 0;
						memcpy (&accountnamelen, (char *) bulkStorage.GetBuffer() + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						printf("Accountname len is: %i\n", accountnamelen);
						
						char szAccountname[SIZE_STRING];
						bzero (szAccountname, SIZE_STRING);
						strncpy (szAccountname, (char *) bulkStorage.GetBuffer() + ipointer, accountnamelen);
						ipointer+=accountnamelen;
						printf("Account Name: %s\n", szAccountname);
						
						int passwordhashlen = 0;
						memcpy (&passwordhashlen, (char *) bulkStorage.GetBuffer() + ipointer, sizeof (int));
						ipointer += sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Password hash len is: %i\n", passwordhashlen);
						
						char szPasswordhash[SIZE_STRING];
						bzero (szPasswordhash, SIZE_STRING);
						strncpy (szPasswordhash, (char *) bulkStorage.GetBuffer() + ipointer, passwordhashlen);
						ipointer +=passwordhashlen;
						syslog (LOG_DEBUG, "DataFortressServer: Password hash: %s\n", szPasswordhash);
						
						// Time to see if the admin accounts exists
						if (DoesAdminAccountExist (szAccountname, szPasswordhash) == true) {
							// great we can respond with authenticated
							// Send the client a reply so that it can trigger the file sending
							
							/*
							#define AUTHADMINOK			10006
							#define AUTHADMINDENIED		10007
							*/
							
							msgident = AUTHADMINOK;
						
							replyBuffer.Clear ();
							replyBuffer.SetSize ((sizeof (int)*3));
							replyBuffer.Append (&sig1, sizeof (int));
							replyBuffer.Append (&sig2, sizeof (int));
							replyBuffer.Append (&msgident, sizeof (int));
						
							write(current_client, replyBuffer.GetBuffer(), replyBuffer.GetSize());
						
							printf("Admin Auth Ok. Auth Admin Ok message sent - awaiting request for admin account list...\n");
							
						} else {
							// no good, couldn't find the account so respond with access denied.
							
							// Temporarily add the account to the list of admin accounts that can authenticate
							//AddAdminAccount (szAccountname, szPasswordhash);
							
							msgident = AUTHADMINDENIED;
						
							replyBuffer.Clear ();
							replyBuffer.SetSize ((sizeof (int)*3));
							replyBuffer.Append (&sig1, sizeof (int));
							replyBuffer.Append (&sig2, sizeof (int));
							replyBuffer.Append (&msgident, sizeof (int));
						
							write(current_client, replyBuffer.GetBuffer(), replyBuffer.GetSize());
							
							printf("Admin Auth Failed. Auth Admin Failed message sent...\n");
						}
						
					}
					
					
					if (msg == AUTHENTICATECLIENT)
					{
						printf("Authenticate Client Message received.\n");
						
						int accountnamelen = 0;
						memcpy (&accountnamelen, (char *) bulkStorage.GetBuffer() + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						printf("Accountname len is: %i\n", accountnamelen);
						
						char szAccountname[SIZE_STRING];
						bzero (szAccountname, SIZE_STRING);
						strncpy (szAccountname, (char *) bulkStorage.GetBuffer() + ipointer, accountnamelen);
						ipointer+=accountnamelen;
						printf("Account Name: %s\n", szAccountname);
						
						int passwordhashlen = 0;
						memcpy (&passwordhashlen, (char *) bulkStorage.GetBuffer() + ipointer, sizeof (int));
						ipointer += sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Password hash len is: %i\n", passwordhashlen);
						
						char szPasswordhash[SIZE_STRING];
						bzero (szPasswordhash, SIZE_STRING);
						strncpy (szPasswordhash, (char *) bulkStorage.GetBuffer() + ipointer, passwordhashlen);
						ipointer +=passwordhashlen;
						syslog (LOG_DEBUG, "DataFortressServer: Password hash: %s\n", szPasswordhash);
						
						// Time to see if the admin accounts exists
						if (DoesClientAccountExist (szAccountname, szPasswordhash) == true) {
							// great we can respond with authenticated
							// Send the client a reply so that it can trigger the file sending
							
							/*
							#define AUTHADMINOK			10006
							#define AUTHADMINDENIED		10007
							*/
							
							msgident = AUTHCLIENTOK;
						
							replyBuffer.Clear ();
							replyBuffer.SetSize ((sizeof (int)*3));
							replyBuffer.Append (&sig1, sizeof (int));
							replyBuffer.Append (&sig2, sizeof (int));
							replyBuffer.Append (&msgident, sizeof (int));
						
							write(current_client, replyBuffer.GetBuffer(), replyBuffer.GetSize());
						
							printf("Client Auth Ok. Auth Client Ok message sent...\n");
							
						} else {
							// no good, couldn't find the account so respond with access denied.
							
							// Temporarily add the account to the list of admin accounts that can authenticate
							//AddAdminAccount (szAccountname, szPasswordhash);
							
							msgident = AUTHCLIENTDENIED;
						
							replyBuffer.Clear ();
							replyBuffer.SetSize ((sizeof (int)*3));
							replyBuffer.Append (&sig1, sizeof (int));
							replyBuffer.Append (&sig2, sizeof (int));
							replyBuffer.Append (&msgident, sizeof (int));
						
							write(current_client, replyBuffer.GetBuffer(), replyBuffer.GetSize());
							
							printf("Client Auth Failed. Auth Client Failed message sent...\n");
						}
						
					}
					
					
					if (msg == REQUESTADMINACCOUNTS)
					{
						syslog (LOG_DEBUG, "DataFortressServer: Request Admin Account list message received.\n");
						
						MemoryBuffer adminaccounts;
						GetAllAdminAccounts (&adminaccounts);
						
						msgident = ADMINACCOUNTLIST;
						
						replyBuffer.Clear ();
						replyBuffer.SetSize (adminaccounts.GetSize () + (sizeof (int)*3));
						replyBuffer.Append (&sig1, sizeof (int));
						replyBuffer.Append (&sig2, sizeof (int));
						replyBuffer.Append (&msgident, sizeof (int));
						replyBuffer.Append (adminaccounts.GetBuffer (), adminaccounts.GetSize ());
						
						write (current_client, replyBuffer.GetBuffer(), replyBuffer.GetSize ());
						
						syslog (LOG_DEBUG, "DataFortressServer: Admin Account list has been sent. Size of entire list was: %i\n", adminaccounts.GetSize ());
						
						
					}
					
					if (msg == REQUESTCLIENTACCOUNTS)
					{
						syslog (LOG_DEBUG, "DataFortressServer: Request Client Account list message received.\n");
						
						MemoryBuffer clientaccounts;
						GetAllClientAccounts (&clientaccounts);
						
						msgident = CLIENTACCOUNTLIST;
						
						replyBuffer.Clear ();
						replyBuffer.SetSize (clientaccounts.GetSize () + (sizeof (int)*3));
						replyBuffer.Append (&sig1, sizeof (int));
						replyBuffer.Append (&sig2, sizeof (int));
						replyBuffer.Append (&msgident, sizeof (int));
						replyBuffer.Append (clientaccounts.GetBuffer (), clientaccounts.GetSize ());
						
						write (current_client, replyBuffer.GetBuffer(), replyBuffer.GetSize ());
						
						syslog (LOG_DEBUG, "DataFortressServer: Client list has been sent. Size of entire list was: %i\n", clientaccounts.GetSize ());
					}
					
					if (msg == NEWCLIENTACCOUNT) {
						
						syslog (LOG_DEBUG, "DataFortressServer: New Client Account message received.\n");
						
						int companylen = 0;
						memcpy (&companylen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Company len is: %i\n", companylen);
						
						char szCompany [SIZE_STRING];
						bzero (szCompany, SIZE_STRING);
						strncpy (szCompany, (char *) bulkStorage.GetBuffer () + ipointer, companylen);
						ipointer+=companylen;
						syslog (LOG_DEBUG, "DataFortressServer: Company: %s\n", szCompany);
						
						
						int descriptionlen = 0;
						memcpy (&descriptionlen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Description len is: %i\n", descriptionlen);
						
						char szDescription [SIZE_STRING];
						bzero (szDescription, SIZE_STRING);
						strncpy (szDescription, (char *) bulkStorage.GetBuffer () + ipointer, descriptionlen);
						ipointer+=descriptionlen;
						syslog (LOG_DEBUG, "DataFortressServer: Description: %s\n", szDescription);
						
						
						int firstnamelen = 0;
						memcpy (&firstnamelen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Firstname len is: %i\n", firstnamelen);
						
						char szFirstname [SIZE_STRING];
						bzero (szFirstname, SIZE_STRING);
						strncpy (szFirstname, (char *) bulkStorage.GetBuffer () + ipointer, firstnamelen);
						ipointer+=firstnamelen;
						syslog (LOG_DEBUG, "DataFortressServer: Firstname: %s\n", szFirstname);
						
						
						int lastnamelen = 0;
						memcpy (&lastnamelen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Lastname len is: %i\n", lastnamelen);
						
						char szLastname [SIZE_STRING];
						bzero (szLastname, SIZE_STRING);
						strncpy (szLastname, (char *) bulkStorage.GetBuffer () + ipointer, lastnamelen);
						ipointer+=lastnamelen;
						syslog (LOG_DEBUG, "DataFortressServer: Lastname: %s\n", szLastname);
						
						
						int maxallowedmb = 0;
						memcpy (&maxallowedmb, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Max Allowed MB is: %i\n", maxallowedmb);
						
						
						int passwordhashlen = 0;
						memcpy (&passwordhashlen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Password Hash len is: %i\n", passwordhashlen);
						
						char szPasswordhash [SIZE_STRING];
						bzero (szPasswordhash, SIZE_STRING);
						strncpy (szPasswordhash, (char *) bulkStorage.GetBuffer () + ipointer, passwordhashlen);
						ipointer+=passwordhashlen;
						syslog (LOG_DEBUG, "DataFortressServer: Password Hash is: %s\n", szPasswordhash);
						
						
						int rootpathlen = 0;
						memcpy (&rootpathlen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: RootPath len is: %i\n", rootpathlen);
						
						char szRootPath [SIZE_STRING];
						bzero (szRootPath, SIZE_STRING);
						strncpy (szRootPath, (char *) bulkStorage.GetBuffer () + ipointer, rootpathlen);
						ipointer+=rootpathlen;
						syslog (LOG_DEBUG, "DataFortressServer: Root Path is: %s\n", szRootPath);
						
						
						int usernamelen = 0;
						memcpy (&usernamelen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Username len is: %i\n", usernamelen);
						
						char szUsername [SIZE_STRING];
						bzero (szUsername, SIZE_STRING);
						strncpy (szUsername, (char *) bulkStorage.GetBuffer () + ipointer, usernamelen);
						ipointer+=usernamelen;
						syslog (LOG_DEBUG, "DataFortressServer: Username is: %s\n", szUsername);
						
						ClientAccount newclientaccount;
						
						bzero (newclientaccount.szCompany, SIZE_STRING);
						strncpy (newclientaccount.szCompany, szCompany, companylen);
						
						bzero (newclientaccount.szDescription, SIZE_STRING);
						strncpy (newclientaccount.szDescription, szDescription, descriptionlen);
						
						bzero (newclientaccount.szFirstname, SIZE_STRING);
						strncpy (newclientaccount.szFirstname, szFirstname, firstnamelen);
						
						bzero (newclientaccount.szLastname, SIZE_STRING);
						strncpy (newclientaccount.szLastname, szLastname, lastnamelen);
						
						newclientaccount.iMaxAllowedMegaBytes = maxallowedmb;
						
						bzero (newclientaccount.szPasswordHash, SIZE_STRING);
						strncpy (newclientaccount.szPasswordHash, szPasswordhash, passwordhashlen);
						
						bzero (newclientaccount.szRootPath, SIZE_STRING);
						strncpy (newclientaccount.szRootPath, szRootPath, rootpathlen);
						
						bzero (newclientaccount.szUsername, SIZE_STRING);
						strncpy (newclientaccount.szUsername, szUsername, usernamelen);
						
						AddClientAccount (&newclientaccount);
					}
					
					
					
					if (msg == UPDATECLIENTACCOUNT) {
						syslog (LOG_DEBUG, "DataFortressServer: Update Client Account message received.\n");
						
						int companylen = 0;
						memcpy (&companylen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Company len is: %i\n", companylen);
						
						char szCompany [SIZE_STRING];
						bzero (szCompany, SIZE_STRING);
						strncpy (szCompany, (char *) bulkStorage.GetBuffer () + ipointer, companylen);
						ipointer+=companylen;
						syslog (LOG_DEBUG, "DataFortressServer: Company: %s\n", szCompany);
						
						
						int descriptionlen = 0;
						memcpy (&descriptionlen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Description len is: %i\n", descriptionlen);
						
						char szDescription [SIZE_STRING];
						bzero (szDescription, SIZE_STRING);
						strncpy (szDescription, (char *) bulkStorage.GetBuffer () + ipointer, descriptionlen);
						ipointer+=descriptionlen;
						syslog (LOG_DEBUG, "DataFortressServer: Description: %s\n", szDescription);
						
						
						int firstnamelen = 0;
						memcpy (&firstnamelen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Firstname len is: %i\n", firstnamelen);
						
						char szFirstname [SIZE_STRING];
						bzero (szFirstname, SIZE_STRING);
						strncpy (szFirstname, (char *) bulkStorage.GetBuffer () + ipointer, firstnamelen);
						ipointer+=firstnamelen;
						syslog (LOG_DEBUG, "DataFortressServer: Firstname: %s\n", szFirstname);
						
						
						int lastnamelen = 0;
						memcpy (&lastnamelen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Lastname len is: %i\n", lastnamelen);
						
						char szLastname [SIZE_STRING];
						bzero (szLastname, SIZE_STRING);
						strncpy (szLastname, (char *) bulkStorage.GetBuffer () + ipointer, lastnamelen);
						ipointer+=lastnamelen;
						syslog (LOG_DEBUG, "DataFortressServer: Lastname: %s\n", szLastname);
						
						
						int maxallowedmb = 0;
						memcpy (&maxallowedmb, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Max Allowed MB is: %i\n", maxallowedmb);
						
						
						int passwordhashlen = 0;
						memcpy (&passwordhashlen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Password Hash len is: %i\n", passwordhashlen);
						
						char szPasswordhash [SIZE_STRING];
						bzero (szPasswordhash, SIZE_STRING);
						strncpy (szPasswordhash, (char *) bulkStorage.GetBuffer () + ipointer, passwordhashlen);
						ipointer+=passwordhashlen;
						syslog (LOG_DEBUG, "DataFortressServer: Password Hash is: %s\n", szPasswordhash);
						
						
						int rootpathlen = 0;
						memcpy (&rootpathlen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: RootPath len is: %i\n", rootpathlen);
						
						char szRootPath [SIZE_STRING];
						bzero (szRootPath, SIZE_STRING);
						strncpy (szRootPath, (char *) bulkStorage.GetBuffer () + ipointer, rootpathlen);
						ipointer+=rootpathlen;
						syslog (LOG_DEBUG, "DataFortressServer: Root Path is: %s\n", szRootPath);
						
						
						int usernamelen = 0;
						memcpy (&usernamelen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Username len is: %i\n", usernamelen);
						
						char szUsername [SIZE_STRING];
						bzero (szUsername, SIZE_STRING);
						strncpy (szUsername, (char *) bulkStorage.GetBuffer () + ipointer, usernamelen);
						ipointer+=usernamelen;
						syslog (LOG_DEBUG, "DataFortressServer: Username is: %s\n", szUsername);
						
						ClientAccount updatedclientaccount;
						
						bzero (updatedclientaccount.szCompany, SIZE_STRING);
						strncpy (updatedclientaccount.szCompany, szCompany, companylen);
						
						bzero (updatedclientaccount.szDescription, SIZE_STRING);
						strncpy (updatedclientaccount.szDescription, szDescription, descriptionlen);
						
						bzero (updatedclientaccount.szFirstname, SIZE_STRING);
						strncpy (updatedclientaccount.szFirstname, szFirstname, firstnamelen);
						
						bzero (updatedclientaccount.szLastname, SIZE_STRING);
						strncpy (updatedclientaccount.szLastname, szLastname, lastnamelen);
						
						updatedclientaccount.iMaxAllowedMegaBytes = maxallowedmb;
						
						bzero (updatedclientaccount.szPasswordHash, SIZE_STRING);
						strncpy (updatedclientaccount.szPasswordHash, szPasswordhash, passwordhashlen);
						
						bzero (updatedclientaccount.szRootPath, SIZE_STRING);
						strncpy (updatedclientaccount.szRootPath, szRootPath, rootpathlen);
						
						bzero (updatedclientaccount.szUsername, SIZE_STRING);
						strncpy (updatedclientaccount.szUsername, szUsername, usernamelen);
						
						UpdateClientAccount (&updatedclientaccount);
						
					}
					
					if (msg == DELETECLIENTACCOUNT) {
						syslog (LOG_DEBUG, "DataFortressServer: Delete Client Account message received.\n");
						
						int clientaccountlen = 0;
						memcpy (&clientaccountlen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Client account len is: %i\n", clientaccountlen);
						
						char szClientaccount [SIZE_STRING];
						bzero (szClientaccount, SIZE_STRING);
						strncpy (szClientaccount, (char *) bulkStorage.GetBuffer () + ipointer, clientaccountlen);
						ipointer+=clientaccountlen;
						syslog (LOG_DEBUG, "DataFortressServer: Client Account: %s\n", szClientaccount);
						
						DeleteClientAccount (szClientaccount);
					}
					
					if (msg == CLIENTPASSWORDCHANGE) {
						
						syslog (LOG_DEBUG, "DataFortressServer: Client Password change message received.\n");
						
						int clientaccountlen = 0;
						memcpy (&clientaccountlen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Client account len is: %i\n", clientaccountlen);
						
						char szClientaccount [SIZE_STRING];
						bzero (szClientaccount, SIZE_STRING);
						strncpy (szClientaccount, (char *) bulkStorage.GetBuffer () + ipointer, clientaccountlen);
						ipointer+=clientaccountlen;
						syslog (LOG_DEBUG, "DataFortressServer: Client Account: %s\n", szClientaccount);
						
						int passwordhashlen = 0;
						memcpy (&passwordhashlen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Passwordhash len is: %i\n", passwordhashlen);
						
						char szPasswordhash [SIZE_STRING];
						bzero (szPasswordhash, SIZE_STRING);
						strncpy (szPasswordhash, (char *) bulkStorage.GetBuffer () + ipointer, passwordhashlen);
						ipointer+=passwordhashlen;
						syslog (LOG_DEBUG, "DataFortressServer: Password hash: %s\n", szPasswordhash);
						
						// Need to update the client account here
						UpdateClientPassword (szClientaccount, szPasswordhash);
					}
					
					if (msg == NEWADMINACCOUNT) {
						
						syslog (LOG_DEBUG, "DataFortressServer: New Admin account message received.\n");
						
						int firstnamelen = 0;
						memcpy (&firstnamelen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Firstname len is: %i\n", firstnamelen);
						
						char szFirstname [SIZE_STRING];
						bzero (szFirstname, SIZE_STRING);
						strncpy (szFirstname, (char *) bulkStorage.GetBuffer () + ipointer, firstnamelen);
						ipointer+=firstnamelen;
						syslog (LOG_DEBUG, "DataFortressServer: Firstname: %s\n", szFirstname);
						
						
						int lastnamelen = 0;
						memcpy (&lastnamelen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Lastname len is: %i\n", lastnamelen);
						
						char szLastname [SIZE_STRING];
						bzero (szLastname, SIZE_STRING);
						strncpy (szLastname, (char *) bulkStorage.GetBuffer () + ipointer, lastnamelen);
						ipointer+=lastnamelen;
						syslog (LOG_DEBUG, "DataFortressServer: Lastname: %s\n", szLastname);
						
						
						int passwordhashlen = 0;
						memcpy (&passwordhashlen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Password Hash len is: %i\n", passwordhashlen);
						
						char szPasswordhash [SIZE_STRING];
						bzero (szPasswordhash, SIZE_STRING);
						strncpy (szPasswordhash, (char *) bulkStorage.GetBuffer () + ipointer, passwordhashlen);
						ipointer+=passwordhashlen;
						syslog (LOG_DEBUG, "DataFortressServer: Password Hash is: %s\n", szPasswordhash);
						
						
						int usernamelen = 0;
						memcpy (&usernamelen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Username len is: %i\n", usernamelen);
						
						char szUsername [SIZE_STRING];
						bzero (szUsername, SIZE_STRING);
						strncpy (szUsername, (char *) bulkStorage.GetBuffer () + ipointer, usernamelen);
						ipointer+=usernamelen;
						syslog (LOG_DEBUG, "DataFortressServer: Username is: %s\n", szUsername);
						
						
						int descriptionlen = 0;
						memcpy (&descriptionlen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Description len is: %i\n", descriptionlen);
						
						char szDescription [SIZE_STRING];
						bzero (szDescription, SIZE_STRING);
						strncpy (szDescription, (char *) bulkStorage.GetBuffer () + ipointer, descriptionlen);
						ipointer+=descriptionlen;
						syslog (LOG_DEBUG, "DataFortressServer: Description: %s\n", szDescription);
						
						AdminAccount newadminaccount;
						
						bzero (newadminaccount.szFirstname, SIZE_STRING);
						strncpy (newadminaccount.szFirstname, szFirstname, firstnamelen);
						
						bzero (newadminaccount.szLastname, SIZE_STRING);
						strncpy (newadminaccount.szLastname, szLastname, lastnamelen);
						
						bzero (newadminaccount.szPasswordHash, SIZE_STRING);
						strncpy (newadminaccount.szPasswordHash, szPasswordhash, passwordhashlen);
						
						bzero (newadminaccount.szUsername, SIZE_STRING);
						strncpy (newadminaccount.szUsername, szUsername, usernamelen);
						
						bzero (newadminaccount.szDescription, SIZE_STRING);
						strncpy (newadminaccount.szDescription, szDescription, descriptionlen);
						
						// Add admin account
						AddAdminAccount (&newadminaccount);
					}
					
					if (msg == UPDATEADMINACCOUNT) {
						
						syslog (LOG_DEBUG, "DataFortressServer: Update Admin Account message received.\n");
						
						int firstnamelen = 0;
						memcpy (&firstnamelen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Firstname len is: %i\n", firstnamelen);
						
						char szFirstname [SIZE_STRING];
						bzero (szFirstname, SIZE_STRING);
						strncpy (szFirstname, (char *) bulkStorage.GetBuffer () + ipointer, firstnamelen);
						ipointer+=firstnamelen;
						syslog (LOG_DEBUG, "DataFortressServer: Firstname: %s\n", szFirstname);
						
						
						int lastnamelen = 0;
						memcpy (&lastnamelen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Lastname len is: %i\n", lastnamelen);
						
						char szLastname [SIZE_STRING];
						bzero (szLastname, SIZE_STRING);
						strncpy (szLastname, (char *) bulkStorage.GetBuffer () + ipointer, lastnamelen);
						ipointer+=lastnamelen;
						syslog (LOG_DEBUG, "DataFortressServer: Lastname: %s\n", szLastname);
						
						
						int passwordhashlen = 0;
						memcpy (&passwordhashlen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Password Hash len is: %i\n", passwordhashlen);
						
						char szPasswordhash [SIZE_STRING];
						bzero (szPasswordhash, SIZE_STRING);
						strncpy (szPasswordhash, (char *) bulkStorage.GetBuffer () + ipointer, passwordhashlen);
						ipointer+=passwordhashlen;
						syslog (LOG_DEBUG, "DataFortressServer: Password Hash is: %s\n", szPasswordhash);
						
						
						int usernamelen = 0;
						memcpy (&usernamelen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Username len is: %i\n", usernamelen);
						
						char szUsername [SIZE_STRING];
						bzero (szUsername, SIZE_STRING);
						strncpy (szUsername, (char *) bulkStorage.GetBuffer () + ipointer, usernamelen);
						ipointer+=usernamelen;
						syslog (LOG_DEBUG, "DataFortressServer: Username is: %s\n", szUsername);
						
						
						int descriptionlen = 0;
						memcpy (&descriptionlen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Description len is: %i\n", descriptionlen);
						
						char szDescription [SIZE_STRING];
						bzero (szDescription, SIZE_STRING);
						strncpy (szDescription, (char *) bulkStorage.GetBuffer () + ipointer, descriptionlen);
						ipointer+=descriptionlen;
						syslog (LOG_DEBUG, "DataFortressServer: Description: %s\n", szDescription);
						
						AdminAccount updatedadminaccount;
						
						bzero (updatedadminaccount.szFirstname, SIZE_STRING);
						strncpy (updatedadminaccount.szFirstname, szFirstname, firstnamelen);
						
						bzero (updatedadminaccount.szLastname, SIZE_STRING);
						strncpy (updatedadminaccount.szLastname, szLastname, lastnamelen);
						
						bzero (updatedadminaccount.szPasswordHash, SIZE_STRING);
						strncpy (updatedadminaccount.szPasswordHash, szPasswordhash, passwordhashlen);
						
						bzero (updatedadminaccount.szUsername, SIZE_STRING);
						strncpy (updatedadminaccount.szUsername, szUsername, usernamelen);
						
						bzero (updatedadminaccount.szDescription, SIZE_STRING);
						strncpy (updatedadminaccount.szDescription, szDescription, descriptionlen);
						
						// Update admin account
						UpdateAdminAccount (&updatedadminaccount);
						
						
					}
					
					if (msg == DELETEADMINACCOUNT) {
						syslog (LOG_DEBUG, "DataFortressServer: Delete Admin Account message received.\n");
						
						int adminaccountlen = 0;
						memcpy (&adminaccountlen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Admin account len is: %i\n", adminaccountlen);
						
						char szAdminaccount [SIZE_STRING];
						bzero (szAdminaccount, SIZE_STRING);
						strncpy (szAdminaccount, (char *) bulkStorage.GetBuffer () + ipointer, adminaccountlen);
						ipointer+=adminaccountlen;
						syslog (LOG_DEBUG, "DataFortressServer: Admin Account: %s\n", szAdminaccount);
						
						DeleteAdminAccount (szAdminaccount);
					}
					
					if (msg == ADMINPASSWORDCHANGE) {
						
						syslog (LOG_DEBUG, "DataFortressServer: Admin password change message received.\n");
						
						int adminaccountlen = 0;
						memcpy (&adminaccountlen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Admin account len is: %i\n", adminaccountlen);
						
						char szAdminaccount [SIZE_STRING];
						bzero (szAdminaccount, SIZE_STRING);
						strncpy (szAdminaccount, (char *) bulkStorage.GetBuffer () + ipointer, adminaccountlen);
						ipointer+=adminaccountlen;
						syslog (LOG_DEBUG, "DataFortressServer: Admin Account: %s\n", szAdminaccount);
						
						int passwordhashlen = 0;
						memcpy (&passwordhashlen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Passwordhash len is: %i\n", passwordhashlen);
						
						char szPasswordhash [SIZE_STRING];
						bzero (szPasswordhash, SIZE_STRING);
						strncpy (szPasswordhash, (char *) bulkStorage.GetBuffer () + ipointer, passwordhashlen);
						ipointer+=passwordhashlen;
						syslog (LOG_DEBUG, "DataFortressServer: Password hash: %s\n", szPasswordhash);
						
						// Need to update the admin account here
						UpdateAdminPassword (szAdminaccount, szPasswordhash);
					}
					
					if (msg == FILEEXISTS) {
						
						syslog (LOG_DEBUG, "DataFortressServer: File Exists message has been received.\n");
						
						int useraccountlen = 0;
						memcpy (&useraccountlen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: User account len is: %i\n", useraccountlen);
						
						char szUseraccount[SIZE_STRING];
						bzero(szUseraccount, SIZE_STRING);
						strncpy (szUseraccount, (char *) bulkStorage.GetBuffer () + ipointer, useraccountlen);
						ipointer+=useraccountlen;
						syslog (LOG_DEBUG, "DataFortressServer: User Account: %s\n", szUseraccount);
						
						int filepathlen = 0;
						memcpy (&filepathlen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Filepath len is: %i\n", filepathlen);
						
						char szFilepath[SIZE_STRING];
						bzero(szFilepath, SIZE_STRING);
						strncpy (szFilepath, (char *) bulkStorage.GetBuffer () + ipointer, filepathlen);
						ipointer+=filepathlen;
						syslog (LOG_DEBUG, "DataFortressServer: File path is: %s\n", szFilepath);
						
						int jobnamelen = 0;
						memcpy (&jobnamelen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Job name len is: %i\n", jobnamelen);
						
						char szJobname[SIZE_STRING];
						bzero(szJobname, SIZE_STRING);
						strncpy (szJobname, (char *) bulkStorage.GetBuffer () + ipointer, jobnamelen);
						ipointer+=jobnamelen;
						syslog (LOG_DEBUG, "DataFortressServer: Jobname is: %s\n", szJobname);
						
						// Get the client account path
						char szClientrootpath[SIZE_STRING];
						bzero (szClientrootpath, SIZE_STRING);
						
						if (GetClientAccountRootPath (szUseraccount, szClientrootpath) == true) {
							
							char szAccountrootpath[SIZE_STRING];
							bzero (szAccountrootpath, SIZE_STRING);
							
							CombinePath (szClientrootpath, szJobname, szAccountrootpath);
							
							if (FindString (szFilepath, "$$$ACCOUNTROOT$$$") != -1) {
								
								// Servepath will contain the localised path to look for the file on our side.
								char szServerpath[SIZE_STRING];
								bzero (szServerpath, SIZE_STRING);
								
								ReplaceStringEx (szFilepath, "$$$ACCOUNTROOT$$$", szAccountrootpath, szServerpath);
								
								char szConvertedserverpath[SIZE_STRING];
								bzero (szConvertedserverpath, SIZE_STRING);
								
								ReplaceStringEx (szServerpath, "\\", "/", szConvertedserverpath); // This will take windows backslashes which windows likes and convert them to linux forward slashes for path separators.
								
								if (DoesFileExist (szConvertedserverpath) == true) {
								
									//#define RESPONSEFILEEXISTS		10024
									//#define RESPONSEFILENOTFOUND	10025
									
									syslog (LOG_DEBUG, "DataFortressServer: Localised path %s exists. Sending response back to client.\n", szConvertedserverpath);
									
									msgident = RESPONSEFILEEXISTS;
						
									replyBuffer.Clear ();
									replyBuffer.SetSize ((sizeof (int)*3));
									replyBuffer.Append (&sig1, sizeof (int));
									replyBuffer.Append (&sig2, sizeof (int));
									replyBuffer.Append (&msgident, sizeof (int));
								
									write(current_client, replyBuffer.GetBuffer(), replyBuffer.GetSize());
								
									printf("ResponseFileExists message sent...\n");
									
								} else {
								
									syslog (LOG_DEBUG, "DataFortressServer: Localised path %s not found. Sending response back to client.\n", szConvertedserverpath);
									
									msgident = RESPONSEFILENOTFOUND;
						
									replyBuffer.Clear ();
									replyBuffer.SetSize ((sizeof (int)*3));
									replyBuffer.Append (&sig1, sizeof (int));
									replyBuffer.Append (&sig2, sizeof (int));
									replyBuffer.Append (&msgident, sizeof (int));
								
									write(current_client, replyBuffer.GetBuffer(), replyBuffer.GetSize());
								
									printf("ResponseFileNotFound message sent...\n");
									
								}
								
							} else {
								syslog (LOG_DEBUG, "DataFortressServer: File Exists message was received, but no account root tag was found in the supplied path.\n");
							}
							
						} else {
							syslog (LOG_DEBUG, "DataFortressServer: Unable to get the client root path for user: %s\n", szUseraccount);
						}
					}
					
					if (msg == LASTWRITETIME) {
						
						syslog (LOG_DEBUG, "DataFortressServer: Last Write Time message has been received.\n");
						
						int useraccountlen = 0;
						memcpy (&useraccountlen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: User account len is: %i\n", useraccountlen);
						
						char szUseraccount[SIZE_STRING];
						bzero(szUseraccount, SIZE_STRING);
						strncpy (szUseraccount, (char *) bulkStorage.GetBuffer () + ipointer, useraccountlen);
						ipointer+=useraccountlen;
						syslog (LOG_DEBUG, "DataFortressServer: User Account: %s\n", szUseraccount);
						
						int filepathlen = 0;
						memcpy (&filepathlen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Filepath len is: %i\n", filepathlen);
						
						char szFilepath[SIZE_STRING];
						bzero(szFilepath, SIZE_STRING);
						strncpy (szFilepath, (char *) bulkStorage.GetBuffer () + ipointer, filepathlen);
						ipointer+=filepathlen;
						syslog (LOG_DEBUG, "DataFortressServer: File path is: %s\n", szFilepath);
						
						int jobnamelen = 0;
						memcpy (&jobnamelen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Job name len is: %i\n", jobnamelen);
						
						char szJobname[SIZE_STRING];
						bzero(szJobname, SIZE_STRING);
						strncpy (szJobname, (char *) bulkStorage.GetBuffer () + ipointer, jobnamelen);
						ipointer+=jobnamelen;
						syslog (LOG_DEBUG, "DataFortressServer: Jobname is: %s\n", szJobname);
						
						// Get the client account path
						char szClientrootpath[SIZE_STRING];
						bzero (szClientrootpath, SIZE_STRING);
						
						if (GetClientAccountRootPath (szUseraccount, szClientrootpath) == true) {
							
							char szAccountrootpath[SIZE_STRING];
							bzero (szAccountrootpath, SIZE_STRING);
							
							CombinePath (szClientrootpath, szJobname, szAccountrootpath);
							
							if (FindString (szFilepath, "$$$ACCOUNTROOT$$$") != -1) {
								
								// Servepath will contain the localised path to look for the file on our side.
								char szServerpath[SIZE_STRING];
								bzero (szServerpath, SIZE_STRING);
								
								ReplaceStringEx (szFilepath, "$$$ACCOUNTROOT$$$", szAccountrootpath, szServerpath);
								
								char szConvertedserverpath[SIZE_STRING];
								bzero (szConvertedserverpath, SIZE_STRING);
								
								ReplaceStringEx (szServerpath, "\\", "/", szConvertedserverpath); // This will take windows backslashes which windows likes and convert them to linux forward slashes for path separators.
								
								if (DoesFileExist (szConvertedserverpath) == true) {								
									
									syslog (LOG_DEBUG, "DataFortressServer: Localised path %s exists. Sending last write time back to client.\n", szConvertedserverpath);
									
									struct tm tmtime;
	
									memcpy (&tmtime, GetFileLastWriteTime (szConvertedserverpath), sizeof (tmtime));
	
									int seconds = tmtime.tm_sec;
									int minutes = tmtime.tm_min;
									int hour = tmtime.tm_hour;
									int dayofmonth = tmtime.tm_mday;
									int month = tmtime.tm_mon;
									int year = tmtime.tm_year+1900;
									int dayofweek = tmtime.tm_wday;
									int dayinyear = tmtime.tm_yday;
									int isdaylightsaving = tmtime.tm_isdst;																		
									
									msgident = RESPONSELASTWRITETIME;
						
									replyBuffer.Clear ();
									replyBuffer.SetSize ((sizeof (int)*12));
									replyBuffer.Append (&sig1, sizeof (int));
									replyBuffer.Append (&sig2, sizeof (int));
									replyBuffer.Append (&msgident, sizeof (int));
									replyBuffer.Append (&seconds, sizeof (int));
									replyBuffer.Append (&minutes, sizeof (int));
									replyBuffer.Append (&hour, sizeof (int));
									replyBuffer.Append (&dayofmonth, sizeof (int));
									replyBuffer.Append (&month, sizeof (int));
									replyBuffer.Append (&year, sizeof (int));
									replyBuffer.Append (&dayofweek, sizeof (int));
									replyBuffer.Append (&dayinyear, sizeof (int));
									replyBuffer.Append (&isdaylightsaving, sizeof (int));
								
									write(current_client, replyBuffer.GetBuffer(), replyBuffer.GetSize());
								
									printf("Response File Last Write Time message sent...\n");
									
								} else {
								
									syslog (LOG_DEBUG, "DataFortressServer: Localised path %s not found. Sending response back to client.\n", szConvertedserverpath);
									
									msgident = RESPONSEFILENOTFOUND;
						
									replyBuffer.Clear ();
									replyBuffer.SetSize ((sizeof (int)*3));
									replyBuffer.Append (&sig1, sizeof (int));
									replyBuffer.Append (&sig2, sizeof (int));
									replyBuffer.Append (&msgident, sizeof (int));
								
									write(current_client, replyBuffer.GetBuffer(), replyBuffer.GetSize());
								
									printf("ResponseFileNotFound message sent...\n");
									
								}
								
							} else {
								syslog (LOG_DEBUG, "DataFortressServer: Last Write time message was received, but no account root tag was found in the supplied path.\n");
							}
							
						} else {
							syslog (LOG_DEBUG, "DataFortressServer: Unable to get the client root path for user: %s\n", szUseraccount);
						}
						
					}
					
					if (msg == FILELENGTH) {
						
						syslog (LOG_DEBUG, "DataFortressServer: File Length message has been received.\n");
						
						int useraccountlen = 0;
						memcpy (&useraccountlen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: User account len is: %i\n", useraccountlen);
						
						char szUseraccount[SIZE_STRING];
						bzero(szUseraccount, SIZE_STRING);
						strncpy (szUseraccount, (char *) bulkStorage.GetBuffer () + ipointer, useraccountlen);
						ipointer+=useraccountlen;
						syslog (LOG_DEBUG, "DataFortressServer: User Account: %s\n", szUseraccount);
						
						int filepathlen = 0;
						memcpy (&filepathlen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Filepath len is: %i\n", filepathlen);
						
						char szFilepath[SIZE_STRING];
						bzero(szFilepath, SIZE_STRING);
						strncpy (szFilepath, (char *) bulkStorage.GetBuffer () + ipointer, filepathlen);
						ipointer+=filepathlen;
						syslog (LOG_DEBUG, "DataFortressServer: File path is: %s\n", szFilepath);
						
						int jobnamelen = 0;
						memcpy (&jobnamelen, (char *) bulkStorage.GetBuffer () + ipointer, sizeof (int));
						ipointer+=sizeof (int);
						syslog (LOG_DEBUG, "DataFortressServer: Job name len is: %i\n", jobnamelen);
						
						char szJobname[SIZE_STRING];
						bzero(szJobname, SIZE_STRING);
						strncpy (szJobname, (char *) bulkStorage.GetBuffer () + ipointer, jobnamelen);
						ipointer+=jobnamelen;
						syslog (LOG_DEBUG, "DataFortressServer: Jobname is: %s\n", szJobname);
						
						// Get the client account path
						char szClientrootpath[SIZE_STRING];
						bzero (szClientrootpath, SIZE_STRING);
						
						if (GetClientAccountRootPath (szUseraccount, szClientrootpath) == true) {
							
							char szAccountrootpath[SIZE_STRING];
							bzero (szAccountrootpath, SIZE_STRING);
							
							CombinePath (szClientrootpath, szJobname, szAccountrootpath);
							
							if (FindString (szFilepath, "$$$ACCOUNTROOT$$$") != -1) {
								
								// Servepath will contain the localised path to look for the file on our side.
								char szServerpath[SIZE_STRING];
								bzero (szServerpath, SIZE_STRING);
								
								ReplaceStringEx (szFilepath, "$$$ACCOUNTROOT$$$", szAccountrootpath, szServerpath);
								
								char szConvertedserverpath[SIZE_STRING];
								bzero (szConvertedserverpath, SIZE_STRING);
								
								ReplaceStringEx (szServerpath, "\\", "/", szConvertedserverpath); // This will take windows backslashes which windows likes and convert them to linux forward slashes for path separators.
								
								if (DoesFileExist (szConvertedserverpath) == true) {								
									
									syslog (LOG_DEBUG, "DataFortressServer: Localised path %s exists. Sending file length back to client.\n", szConvertedserverpath);
									
									long filelength = GetFileLength (szConvertedserverpath);
									
									msgident = RESPONSEFILELENGTH;
						
									replyBuffer.Clear ();
									replyBuffer.SetSize ((sizeof (int)*3) + sizeof (long));
									replyBuffer.Append (&sig1, sizeof (int));
									replyBuffer.Append (&sig2, sizeof (int));
									replyBuffer.Append (&msgident, sizeof (int));
									replyBuffer.Append (&filelength, sizeof (long));
									
									write(current_client, replyBuffer.GetBuffer(), replyBuffer.GetSize());
								
									printf("Response File Length message sent...\n");
									
								} else {
								
									syslog (LOG_DEBUG, "DataFortressServer: Localised path %s not found. Sending response back to client.\n", szConvertedserverpath);
									
									msgident = RESPONSEFILENOTFOUND;
						
									replyBuffer.Clear ();
									replyBuffer.SetSize ((sizeof (int)*3));
									replyBuffer.Append (&sig1, sizeof (int));
									replyBuffer.Append (&sig2, sizeof (int));
									replyBuffer.Append (&msgident, sizeof (int));
								
									write(current_client, replyBuffer.GetBuffer(), replyBuffer.GetSize());
								
									printf("ResponseFileNotFound message sent...\n");
									
								}
								
							} else {
								syslog (LOG_DEBUG, "DataFortressServer: File Length message was received, but no account root tag was found in the supplied path.\n");
							}
							
						} else {
							syslog (LOG_DEBUG, "DataFortressServer: Unable to get the client root path for user: %s\n", szUseraccount);
						}
						
					}
					
					bulkBuffer.Clear ();
					bulkStorage.Clear ();
					bulkpointer = 0;
					
					bbulkmode = false;
					
				}
			}
			
		}
	} // While
}

int StartServer ()
{
	printf("Starting up TCP Server....\n");
	int sock;
	pthread_t clientthread;
	
	MemoryBuffer buffer;
	
	buffer.SetSize (sizeof (int));
	
	int test = 123;
	buffer.Append (&test, sizeof (int));
	
	int testread = 0;
	
	memcpy (&testread, buffer.GetBuffer(), sizeof (int));
	syslog (LOG_DEBUG, "DataFortressServer: Test read value is: %i\n", testread);
	
	struct sockaddr_in server;
	bzero ((char *) &server, sizeof (server));
	
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port=htons(7009);
	
	sock = socket (AF_INET, SOCK_STREAM, 0);
	
	if (sock == -1) {
		printf("There was an error creating the socket. Errno was %i\n", errno);
		return -1;
	}
	
	if (bind (sock, (struct sockaddr *) &server, sizeof (server)) != 0) {
		printf("There was an error binding the socket. Errno was %s\n", strerror (errno));
		return -1;
	}
	
	if (listen (sock, 5) != 0) {
		printf("There was an error listening. Errno was %i\n", errno);
		return -1;
	}
	
	int client;
	struct sockaddr_in from;
	socklen_t fromlen = sizeof (from);
	
	while (1)
	{
		client = accept (sock, (struct sockaddr *) &from, &fromlen);
		syslog (LOG_DEBUG, "DataFortressServer: Client connected.\n");		
		if (pthread_create (&clientthread, NULL, receive_cmds_ex, &client) != 0) {
			printf("Error creating client thread.\n");
		} else {
			pthread_detach (clientthread);
		}
	}
	
	close (sock);
	return 0;
}

void StartDaemon ()
{
	 /* Our process ID and Session ID */
        pid_t pid, sid;
        
        /* Fork off the parent process */
        pid = fork();
        if (pid < 0) {
                exit(EXIT_FAILURE);
        }
        /* If we got a good PID, then
           we can exit the parent process. */
        if (pid > 0) {
                exit(EXIT_SUCCESS);
        }

        /* Change the file mode mask */
        umask(0);
                
        /* Open any logs here */        
                
        /* Create a new SID for the child process */
        sid = setsid();
        if (sid < 0) {
                /* Log the failure */
                exit(EXIT_FAILURE);
        }
        

        
        /* Change the current working directory */
        //if ((chdir("/")) < 0) {
                /* Log the failure */
                //exit(EXIT_FAILURE);
        //}
        
        /* Close out the standard file descriptors */
        close(STDIN_FILENO);
        close(STDOUT_FILENO);
        close(STDERR_FILENO);
        
        /* Daemon-specific initialization goes here */
        
        /* The Big Loop */
        //while (1) {
           /* Do some task here ... */
           
           //sleep(30); /* wait 30 seconds */
        //}
	
	StartServer ();
	
   exit(EXIT_SUCCESS);
}

void FileCopy ()
{
	char buffer[BUFSIZE];
	int in = open ("/tmp/testfile.avi", O_RDONLY);
	mode_t filePerms;
	
	if (in == -1) {
		printf("There was a problem opening the source file for reading. %s\n", strerror (errno));
		return;
	}
	
	filePerms = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;
	
	int out = open ("/tmp/destfile.avi", O_WRONLY | O_CREAT | O_TRUNC, filePerms);
	
	if (out == -1) {
		printf("There was a problem opening the destination file for writing. %s\n", strerror (errno));
		return;
	}
	
	// Get the input file size
	struct stat st;
	fstat (in, &st);
	
	
	printf("Copying file....\n");
	ssize_t bytes_read;
	while (1)
	{
		bytes_read = read (in, buffer, BUFSIZE);
		
		if (bytes_read > 0) {
			write (out, buffer, bytes_read);
		} else {
			break;
		}
	}
	printf("Completed.\n");
	
	close (in);
	close (out);
}
