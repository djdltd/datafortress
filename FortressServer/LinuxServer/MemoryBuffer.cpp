// A memory buffer class. Written by Danny Draper (c) 2007. Provides safe memory buffer storage.

#include "MemoryBuffer.h"

MemoryBuffer::MemoryBuffer ()
{
	m_bCleared = true;
	m_bSizeset = false;
	m_iBuffersize = 0;
	m_iAppendpointer = 0;
}

MemoryBuffer::~MemoryBuffer ()
{
}

void MemoryBuffer::SetSize (unsigned long iSize)
{
	if (m_bSizeset == false) {
		m_bSizeset = true;
		m_bCleared = false;
		m_buffer = (char *) calloc (iSize, 1);
		m_iBuffersize = iSize;
	} else {
		m_buffer = (char *) realloc (m_buffer, iSize);
		m_iBuffersize = iSize;
		m_iAppendpointer = 0;
		m_bCleared = false;
		bzero (m_buffer, iSize);
	}
}

unsigned long MemoryBuffer::GetSize ()
{
	return m_iBuffersize;
}

char MemoryBuffer::GetByte (unsigned long lIndex)
{
	if (m_bSizeset == true) {
		if (m_iAppendpointer > 0) {
			if (lIndex >=0 && lIndex < m_iAppendpointer) {
				return m_buffer[lIndex];
			} else {
				return 0;
			}
		} else {
			if (lIndex >=0 && lIndex < m_iBuffersize) {
				return m_buffer[lIndex];
			} else {
				return 0;
			}
		}
	} else {
		return 0;
	}
}

void MemoryBuffer::SetByte (unsigned long lIndex, char bByte)
{
	if (m_bSizeset == true) {
		if (m_iAppendpointer > 0) {
			if (lIndex >=0 && lIndex < m_iAppendpointer) {
				m_buffer[lIndex] = bByte;
			}
		} else {
			if (lIndex >=0 && lIndex < m_iBuffersize) {
				m_buffer[lIndex] = bByte;
			}
		}
	}
}

bool MemoryBuffer::Write (void *pItem, unsigned long iOffset, unsigned long itemSize)
{
	if (m_bSizeset == true) { // Make sure memory is allocated
		if (iOffset >=0 && iOffset < m_iBuffersize) { // Make sure the offset is within bounds.
			if (itemSize <= (m_iBuffersize-iOffset)) { // Make sure the item size will fit within the space available
				memcpy (m_buffer+iOffset, pItem, itemSize);
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	} else {
		return false;
	}
}

bool MemoryBuffer::Append (void *pItem, unsigned long itemSize)
{
	if (m_bSizeset == true) { // Make sure memory is allocated
		if (itemSize <= (m_iBuffersize-m_iAppendpointer)) {
			memcpy (m_buffer+m_iAppendpointer, pItem, itemSize);
			m_iAppendpointer+=itemSize;
			return true;
		} else {
			return false;
		}
	} else {
		return false;	
	}
}

unsigned long MemoryBuffer::GetAppendPointer ()
{
	return m_iAppendpointer;
}

void *MemoryBuffer::GetBuffer ()
{
	return (void *) m_buffer;
}

bool MemoryBuffer::IsSizeset ()
{
	return m_bSizeset;
}

void MemoryBuffer::Clear ()
{
	if (m_bCleared == false) {
		m_bCleared = true;
		m_bSizeset = false;
		m_iBuffersize = 0;
		m_iAppendpointer = 0;
		// Free the buffer
		free (m_buffer);	
	}
}
