#include <malloc.h>
#include <stdio.h>
#include <string.h>


#define SIZE_STRING		1024

class MemoryBuffer {
	
	public:
		MemoryBuffer ();
		~MemoryBuffer ();		
		bool Write (void *pItem, unsigned long iOffset, unsigned long itemSize);
		bool Append (void *pItem, unsigned long itemSize);
		void *GetBuffer ();
		void SetSize (unsigned long iSize);
		unsigned long GetSize ();
		void Clear ();
		char GetByte (unsigned long lIndex);
		void SetByte (unsigned long lIndex, char bByte);


		unsigned long GetAppendPointer ();

		bool IsSizeset ();

		//bool StartSharing (char *szSharename, unsigned long lMaxsize);
		//void StopSharing ();
		//bool Share ();
		//bool ReadSharedMemory (char *szSharename, unsigned long lMaxsize);
	private:

		char *m_buffer;
		bool m_bCleared;
		bool m_bSizeset;
		unsigned long m_iBuffersize;
		unsigned long m_iAppendpointer;
		unsigned long m_lsharedmemsize;
};
