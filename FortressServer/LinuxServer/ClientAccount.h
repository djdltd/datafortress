#include <malloc.h>
#include <stdio.h>
#include <string.h>
#include "resource.h"

class ClientAccount {
	
	public:
		ClientAccount ();
		~ClientAccount ();		
		
		int signatureone;
		int signaturetwo;
		char szCompany[SIZE_STRING];
		char szDescription[SIZE_STRING];
		char szFirstname[SIZE_STRING];
		char szLastname[SIZE_STRING];
		int iMaxAllowedMegaBytes;
		char szPasswordHash[SIZE_STRING];
		char szRootPath[SIZE_STRING];
		char szUsername[SIZE_STRING];
	
	private:

};
