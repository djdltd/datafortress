﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using System.Collections;
using System.Threading;
using System.Net.Sockets;
using System.IO;
using System.Net;
using CommsPacket;

namespace DataFortressServer
{

    public partial class FortressServer : ServiceBase
    {
        public FortressServer()
        {
            InitializeComponent();
        }

        Thread _rootThread;
        private const int portNum = 10116;
        private static ManualResetEvent ServiceStopEvent = new ManualResetEvent(false);
        public static ManualResetEvent allDone = new ManualResetEvent(false);
        private AdminAccountManager _adminaccountmanager = new AdminAccountManager();
        private ClientAccountManager _clientaccountmanager = new ClientAccountManager();
        private TokenManager _tokenmanager = new TokenManager();
        private bool _serverinitialised = false;

       

        static void ClientWorkerThread(Object stateInfo)
        {
            WorkerThreadState workerstate = (WorkerThreadState)stateInfo;

            //TcpClient clientconnection = (TcpClient)stateInfo;
            TcpClient clientconnection = workerstate.TCPClient;
            AdminAccountManager adminaccountmanager = workerstate.AdminAccountManager;
            TokenManager tokenmanager = workerstate.TokenManager;
            ClientAccountManager clientaccountmanager = workerstate.ClientAccountManager;

            ClientHandler clienthandler = new ClientHandler(clientconnection, adminaccountmanager, tokenmanager, clientaccountmanager);
        
            while (true)
            {
                if (clienthandler.Process() == true)
                {
                    if (WaitHandle.WaitAny(new WaitHandle[] { ServiceStopEvent }, 5) != WaitHandle.WaitTimeout)
                    {
                        break;
                    }
                }
                else
                {
                    if (WaitHandle.WaitAny(new WaitHandle[] { ServiceStopEvent }, 100) != WaitHandle.WaitTimeout)
                    {
                        break;
                    }
                }
            }
        }

        public void InitialiseServer()
        {
            if (_serverinitialised == false)
            {
                _serverinitialised = true;
                _adminaccountmanager.Initialise();
                _clientaccountmanager.Initialise();
            }
        }

        public void StartListeningEx()
        {
            //IPAddress localAddr = IPAddress.Parse("127.0.0.1");

            TcpListener listener = new TcpListener(IPAddress.Any, portNum);

            listener.Start();

            while (true)
            {
                TcpClient newclient = listener.AcceptTcpClient();

                InitialiseServer();

                WorkerThreadState workerstate = new WorkerThreadState();
                workerstate.TCPClient = newclient;
                workerstate.AdminAccountManager = _adminaccountmanager;
                workerstate.TokenManager = _tokenmanager;
                workerstate.ClientAccountManager = _clientaccountmanager;

                //ThreadPool.QueueUserWorkItem(ClientWorkerThread, newclient);
                ThreadPool.QueueUserWorkItem(ClientWorkerThread, workerstate);
            }
        }

        public void RootWorkerThread()
        {
            try
            {
                StartListeningEx();
            }
            catch (Exception ex)
            {
            }
        }



        protected override void OnStart(string[] args)
        {

            _rootThread = new Thread(new ThreadStart(RootWorkerThread));
            _rootThread.Start();
        }

        protected override void OnStop()
        {
            ServiceStopEvent.Set();
            _rootThread.Abort();
        }
    }

    public class WorkerThreadState
    {
        public TcpClient TCPClient;
        public AdminAccountManager AdminAccountManager;
        public ClientAccountManager ClientAccountManager;
        public TokenManager TokenManager;
    }



    public class ClientHandler
    {

        private TcpClient ClientSocket;
        private NetworkStream networkStream;
        bool ContinueProcess = false;
        private byte[] bytes; 		// Data buffer for incoming data.
        private StringBuilder sb = new StringBuilder(); // Received data string.
        private string data = null; // Incoming data from the client.
        MemoryStream memStream;
        BinaryWriter binBuffer;
        bool _filetransfermode = false;
        string _filetransferfilename;
        string _filetransferuseraccount;
        string _filetransfersourcepath;
        string _filetransferdestpath;
        long _filetransferfilelength;
        long _filetransferreceivedpointer;
        string _filetransferjobname;
        //string _rootpath = "C:\\Temp\\IFC\\Server";
        const long _maxbuffersize = 2048000;

        // Byte level differencing vars
        const int _chunksize = 16384;
        List<FileChunkEntry> _chunklist = new List<FileChunkEntry>();

        FileStream _filetransferwritestream;
        AdminAccountManager _adminaccountmanager; // Points to the admin account manager instantiated by the service
        ClientAccountManager _clientaccountmanager; // Points to the client account manager instantiated by the service
        TokenManager _tokenmanager; // Points to the token manager for authenticating requests



        public ClientHandler(TcpClient ClientSocket, AdminAccountManager adminaccountmanager, TokenManager tokenmanager, ClientAccountManager clientaccountmanager)
        {
            _adminaccountmanager = adminaccountmanager;
            _clientaccountmanager = clientaccountmanager;
            _tokenmanager = tokenmanager;
            ClientSocket.ReceiveTimeout = 100; // 1000 miliseconds
            this.ClientSocket = ClientSocket;
            networkStream = ClientSocket.GetStream();
            bytes = new byte[ClientSocket.ReceiveBufferSize];
            ContinueProcess = true;
            memStream = new MemoryStream();
            binBuffer = new BinaryWriter(memStream);
            
        }

        public void RenewBuffer()
        {
            binBuffer.Flush();
            binBuffer.Close();
            memStream.Close();
            memStream = new MemoryStream();
            binBuffer = new BinaryWriter(memStream);
            
        }

        public bool Process()
        {
            CommsPacket.CommsPacket myPacket = new CommsPacket.CommsPacket();

            try
            {
                if (networkStream.DataAvailable == true)
                {
                    //System.Diagnostics.Debug.WriteLine("Data is available." + ClientSocket.Available);

                    bytes = new byte[_maxbuffersize];

                    int dataavailable = ClientSocket.Available;

                    int BytesRead = networkStream.Read(bytes, 0, dataavailable);                   


                    if (BytesRead > 0)
                    {
                        byte[] actualbytes = new byte[dataavailable];
                        Array.Copy(bytes, actualbytes, dataavailable);

                        // There might be more data, so store the data received so far.
                        binBuffer.Write(actualbytes);
                    }

                    // Copy the received buffer to a trimmed buffer. Why? Because memStream.GetBuffer() is a bit deceiving. Basically
                    // it gives you the buffer thats been received yes, but the size of the buffer it gives you is the maximum capacity
                    // of the buffer, not the actual length of data received, and this is what we need.
                    byte[] membuffer = memStream.GetBuffer();
                    byte[] trimmedbuffer = new byte[memStream.Length];
                    Array.Copy(membuffer, trimmedbuffer, memStream.Length); 


                    if (myPacket.ContainsSignature(trimmedbuffer) == true)
                    {
                        // We've found an end of packet signature, so process it!!
                        //System.Diagnostics.Debug.WriteLine("We've found a signature. Received data length: " + memStream.GetBuffer().Length.ToString());

                        if (ProcessDataReceived(trimmedbuffer) == true)
                        {
                            RenewBuffer();
                        }
                    }
                    else
                    {

                        //System.Diagnostics.Debug.WriteLine("No signature found. Waiting for more." + memStream.GetBuffer().Length.ToString());
                        
                    }


                    // Process again, just incase there is more data and we don't depend on our parent thread to kick us off.

                    if (networkStream.DataAvailable == true)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
                else
                {
                    //System.Diagnostics.Debug.WriteLine("No more data available." + memStream.GetBuffer().Length.ToString());

                    RenewBuffer();
                }
            }
            catch (IOException)
            {
                // All the data has arrived; put it in response.
                //ProcessDataReceived();
                //RenewBuffer();
            }
            catch (SocketException)
            {
                networkStream.Close();
                ClientSocket.Close();
                ContinueProcess = false;
                Console.WriteLine("Conection is broken!");
            }


            return false;
        }  // Process()

        public static string CalculateMD5(byte[] Input, Encoding UseEncoding)
        {
            //System.Security.Cryptography.MD5CryptoServiceProvider CryptoService;
            //CryptoService = new System.Security.Cryptography.MD5CryptoServiceProvider();

            System.Security.Cryptography.SHA1CryptoServiceProvider CryptoService;
            CryptoService = new System.Security.Cryptography.SHA1CryptoServiceProvider();

            byte[] OutputBytes;
            OutputBytes = CryptoService.ComputeHash(Input);
            return BitConverter.ToString(OutputBytes).Replace("-", "");
        }

        public static string CalculateMD5(byte[] Input)
        {
            // That's just a shortcut to the base method
            return CalculateMD5(Input, System.Text.Encoding.Default);
        }

        public static List<FileChunkEntry> BuildFileChunkTable(string filepath)
        {
            List<FileChunkEntry> chunklist = new List<FileChunkEntry>();

            byte[] buffer;
            byte[] smallchunkbuffer;

            FileStream fileStream = new FileStream(filepath, FileMode.Open, FileAccess.Read);

            int chunksize = _chunksize;
            int smallchunksize = 8192;
            long smallchunkpointer = 0;
            int numsmallstartingchunks = 100;

            // When we divide the file length by the chunk size it rounds the number down to the smallest non decimal number.
            // so we might miss some stuff at the end of the file, so we do a final check to make sure than when the number of chunks
            // are multiplied by the chunk size we check the calculated size. If the calculated size is smaller, then we need another chunk.
            long numchunks = fileStream.Length / chunksize;

            long calcsize = numchunks * chunksize;

            if (fileStream.Length > calcsize)
            {
                numchunks++;
            }


            long filepointer = 0;
            long workingchunksize = 0;

            buffer = new byte[chunksize];

            for (int a = 0; a < numchunks; a++)
            {
                if ((fileStream.Length - filepointer) >= chunksize)
                {
                    workingchunksize = chunksize;


                    fileStream.Read(buffer, 0, chunksize);
                }
                else
                {
                    long lastchunksize = fileStream.Length - filepointer;
                    workingchunksize = lastchunksize;

                    buffer = new byte[lastchunksize];

                    fileStream.Read(buffer, 0, (int)lastchunksize);
                }

                // At this point we have our buffer array populated with the current chunk (even the last one)
                FileChunkEntry newentry = new FileChunkEntry();

                newentry.primaryhash = CalculateMD5(buffer);

                // Now add the first smaller chunks
                // For each primary hash we store a small number of smaller chunks which serve has hash position markers so that when the
                // mapping function does it's incremental search it can use 8K smaller chunks for increased performance.
                smallchunkbuffer = new byte[smallchunksize];
                smallchunkpointer = 0;

                newentry.offset = filepointer;
                newentry.length = (int)workingchunksize;
                newentry.index = a;

                Console.WriteLine("Offset: " + newentry.offset.ToString() + "\tCS: " + newentry.length.ToString() + " HASH: " + newentry.primaryhash + " IDX: " + newentry.index.ToString());

                chunklist.Add(newentry);

                filepointer += workingchunksize;
            }


            return chunklist;
        }

        public void BuildCompletedFile(string originalfilepath, string newfilepath, List<FileChunkEntry> sourcechunktable, List<FileChunkEntry> completedfiletable)
        {
            // First delete the destination file if it exists
            if (File.Exists(newfilepath))
            {
                File.Delete(newfilepath);
            }

            FileStream inputfileStream = new FileStream(originalfilepath, FileMode.Open, FileAccess.Read);
            FileStream outputfileStream = new FileStream(newfilepath, FileMode.Append, FileAccess.Write);

            byte[] inputbuffer;

            foreach (FileChunkEntry entry in completedfiletable)
            {
                if (entry.dataincluded == false)
                {
                    // First get the server entry referred to by the index
                    FileChunkEntry sourceentry = sourcechunktable[(int)entry.index];

                    inputbuffer = new byte[sourceentry.length];

                    // We're reading an existing chunk from the original file, so read it from the original file and write it to the new file
                    inputfileStream.Seek(sourceentry.offset, SeekOrigin.Begin);
                    inputfileStream.Read(inputbuffer, 0, sourceentry.length);

                    // Write this read chunk to the outputfile
                    outputfileStream.Write(inputbuffer, 0, sourceentry.length);
                }

                if (entry.dataincluded == true)
                {
                    // This time we write data which has been provided by the completed file table
                    outputfileStream.Write(entry.data, 0, entry.data.Length);
                }

                Console.WriteLine("Writing new file...OFF: " + entry.offset.ToString());
            }

            Console.WriteLine("Done writing new file.");
            outputfileStream.Close();
            inputfileStream.Close();
        }

        private bool ProcessDataReceived(byte[] trimmedbuffer)
        {
            CommsPacket.CommsResponse commsresponse = new CommsPacket.CommsResponse();
            CommsPacket.CommsPacket largecommsresponse = new CommsPacket.CommsPacket();
            commsresponse.iResponsetype = -100;
            bool result = true;
            bool largeresponsemode = false;

            if (memStream.Length > 0)
            {
                if (_filetransfermode == false)
                {
                    // If we are not in file transfer mode then the object we receive will be a comms packet which 
                    // needs to be deserialised.
                    CommsPacket.CommsPacket receivedObject = new CommsPacket.CommsPacket();

                    byte[] decapsulatedbytes = receivedObject.Decapsulate(trimmedbuffer);

                    try
                    {
                        receivedObject = receivedObject.DeSerialize(decapsulatedbytes);
                    }
                    catch (Exception ex)
                    {
                        result = false;
                        return result;
                    }

                    memStream.Seek(0, SeekOrigin.Begin);
                    memStream.SetLength(0);

                    if (receivedObject.iMessagetype == (int)CommsPacket.MessageTypes.PrepareServerForFileTransfer)
                    {
                        //_filetransfermode = true;
                        _filetransferfilename = receivedObject.filename;
                        _filetransfersourcepath = receivedObject.sourcepath;
                        _filetransferuseraccount = receivedObject.useraccount;
                        _filetransferfilelength = receivedObject.filelength;
                        _filetransferdestpath = receivedObject.destpath;
                        _filetransferjobname = receivedObject.jobname;

                        string accountrootpath = Path.Combine (_clientaccountmanager.GetAccountRootPath(_filetransferuseraccount), _filetransferjobname);
                        
                        if (_filetransferdestpath.Contains("$$$ACCOUNTROOT$$$"))
                        {
                            _filetransferdestpath = _filetransferdestpath.Replace("$$$ACCOUNTROOT$$$", accountrootpath);
                        }

                        // Now make sure the directory structure is created for the destination path
                        CommonFunctions.BuildDestFileDirs(_filetransferdestpath);

                        _filetransferreceivedpointer = 0;

                        // If the file already exists then we need to delete it, otherwise we will be just adding to this already existing file
                        if (File.Exists(_filetransferdestpath))
                        {
                            File.Delete(_filetransferdestpath);
                        }

                        // Open a file ready to append the chunks we're about to receive
                        _filetransferwritestream = new FileStream(_filetransferdestpath, FileMode.Append, FileAccess.Write);

                        
                        // Echo the message type back to the client in the response
                        commsresponse.iResponsetype = (int)CommsPacket.MessageTypes.PrepareServerForFileTransfer;

                        System.Diagnostics.Debug.WriteLine("File Server Preparation request received. Server prepared.");
                        System.Diagnostics.Debug.WriteLine("File Length: " + _filetransferfilelength);
                    }

                    if (receivedObject.iMessagetype == (int)CommsPacket.MessageTypes.FileChunk)
                    {
                        string chunkfilename = receivedObject.filename;
                        string chunksourcepath = receivedObject.sourcepath;
                        string chunkuseraccount = receivedObject.useraccount;
                        string chunkusertoken = receivedObject.usertoken;
                        long chunkfilelength = receivedObject.filelength;

                        byte[] chunkbuffer = receivedObject.somebuffer;

                        _filetransferreceivedpointer += chunkbuffer.Length;

                        _filetransferwritestream.Write(chunkbuffer, 0, chunkbuffer.Length);


                        if (_filetransferreceivedpointer >= _filetransferfilelength)
                        {
                            System.Diagnostics.Debug.WriteLine("All File Chunks received.");
                            _filetransferwritestream.Close();

                        }

                        System.Diagnostics.Debug.WriteLine("File Receive Pointer: " + _filetransferreceivedpointer + " File Length: " + _filetransferfilelength);
                    }

                    if (receivedObject.iMessagetype == (int)CommsPacket.MessageTypes.FileExists)
                    {
                        string usertoken = receivedObject.usertoken;
                        string useraccount = receivedObject.useraccount;
                        string strpath = receivedObject.filename;
                        string jobname = receivedObject.jobname;

                        string accountrootpath = Path.Combine (_clientaccountmanager.GetAccountRootPath(useraccount), jobname);

                        if (strpath.Contains("$$$ACCOUNTROOT$$$"))
                        {
                            strpath = strpath.Replace("$$$ACCOUNTROOT$$$", accountrootpath);

                            if (File.Exists(strpath))
                            {
                                commsresponse.FileExists = true;
                            }
                            else
                            {
                                commsresponse.FileExists = false;
                            }
                        }
                        else
                        {
                            throw new Exception("File Exists message was sent, but no account root tag was found in the given path.");
                        }
                    }


                    if (receivedObject.iMessagetype == (int)CommsPacket.MessageTypes.LastWriteTime)
                    {
                        string usertoken = receivedObject.usertoken;
                        string useraccount = receivedObject.useraccount;
                        string strpath = receivedObject.filename;
                        string jobname = receivedObject.jobname;

                        string accountrootpath = Path.Combine(_clientaccountmanager.GetAccountRootPath(useraccount), jobname);                     

                        if (strpath.Contains("$$$ACCOUNTROOT$$$"))
                        {
                            strpath = strpath.Replace("$$$ACCOUNTROOT$$$", accountrootpath);

                            if (File.Exists(strpath))
                            {
                                commsresponse.LastWriteTime = File.GetLastWriteTime(strpath);
                            }
                            else
                            {
                                throw new Exception("Server was asked for a Last Write Time on a file that does not exist.");
                            }
                        }
                        else
                        {
                            throw new Exception("File Exists message was sent, but no account root tag was found in the given path.");
                        }
                    }


                    if (receivedObject.iMessagetype == (int)CommsPacket.MessageTypes.FileLength)
                    {
                        string usertoken = receivedObject.usertoken;
                        string useraccount = receivedObject.useraccount;
                        string strpath = receivedObject.filename;
                        string jobname = receivedObject.jobname;

                        string accountrootpath = Path.Combine(_clientaccountmanager.GetAccountRootPath(useraccount), jobname); 

                        if (strpath.Contains("$$$ACCOUNTROOT$$$"))
                        {
                            strpath = strpath.Replace("$$$ACCOUNTROOT$$$", accountrootpath);

                            if (File.Exists(strpath))
                            {
                                FileInfo fi = new FileInfo(strpath);
                                commsresponse.FileLength = fi.Length;
                            }
                            else
                            {
                                throw new Exception("Server was asked for a Length on a file that does not exist.");
                            }
                        }
                        else
                        {
                            throw new Exception("File Exists message was sent, but no account root tag was found in the given path.");
                        }
                    }

                    if (receivedObject.iMessagetype == (int)CommsPacket.MessageTypes.AuthenticateAdmin)
                    {
                        // An Admin has requested to be logged on. We need to authenticate their username and password hash.

                        string username = receivedObject.useraccount;
                        string passwordhash = receivedObject.passwordhash;

                        if (_adminaccountmanager.DoesAdminAccountExist(username, passwordhash) == true)
                        {
                            string newtoken = _tokenmanager.CreateNewAuthenticatedToken(username, AccountType.Admin);

                            commsresponse.authenticated = true;
                            commsresponse.token = newtoken;
                        }
                        else
                        {
                            commsresponse.authenticated = false;
                            commsresponse.token = "";
                        }
                    }

                    if (receivedObject.iMessagetype == (int)CommsPacket.MessageTypes.AuthenticateClient)
                    {
                        // A client has requested to be logged on. We need to authenticate their username and password hash.

                        string username = receivedObject.useraccount;
                        string passwordhash = receivedObject.passwordhash;

                        if (_clientaccountmanager.DoesClientAccountExist(username, passwordhash) == true)
                        {
                            string newtoken = _tokenmanager.CreateNewAuthenticatedToken(username, AccountType.Client);

                            commsresponse.authenticated = true;
                            commsresponse.token = newtoken;
                        }
                        else
                        {
                            commsresponse.authenticated = false;
                            commsresponse.token = "";
                        }
                    }


                    if (receivedObject.iMessagetype == (int)CommsPacket.MessageTypes.LargeDataTest)
                    {
                        string strlargefilepath = "C:\\Temp\\IFC\\NicePicture.jpg";

                        FileStream fs = new FileStream(strlargefilepath, FileMode.Open, FileAccess.Read);

                        byte[] filebuffer = new byte[fs.Length];

                        fs.Read(filebuffer, 0, (int) fs.Length);

                        fs.Close();

                        largeresponsemode = true; // This tells the routine below not to send a usual server response.

                        largecommsresponse = new CommsPacket.CommsPacket();

                        largecommsresponse.iMessagetype = (int)CommsPacket.MessageTypes.LargeDataTest;
                        largecommsresponse.somebuffer = filebuffer;

                        byte[] sendbytes = largecommsresponse.Serialize();
                        byte[] encbytes = largecommsresponse.Encapsulate(sendbytes);

                        networkStream.Write(encbytes, 0, encbytes.Length);
                                       
                    }

                    if (receivedObject.iMessagetype == (int)CommsPacket.MessageTypes.ListAdminAccounts)
                    {
                        // Serialise the admin account list, and send it
                        byte[] listbuffer = _adminaccountmanager.Serialize();

                        largeresponsemode = true;

                        largecommsresponse = new CommsPacket.CommsPacket();
                        largecommsresponse.iMessagetype = (int)CommsPacket.MessageTypes.ListAdminAccounts;
                        largecommsresponse.somebuffer = listbuffer;

                        byte[] sendbytes = largecommsresponse.Serialize();
                        byte[] encbytes = largecommsresponse.Encapsulate(sendbytes);

                        networkStream.Write(encbytes, 0, encbytes.Length);
                    }

                    if (receivedObject.iMessagetype == (int)CommsPacket.MessageTypes.NewAdminAccount)
                    {
                        // New admin account to be added
                        string username = receivedObject.useraccount;
                        string authtoken = receivedObject.usertoken;

                        CommsPacket.AdminAccount newaccount = (CommsPacket.AdminAccount) CommonFunctions.ByteArrayToObject(receivedObject.somebuffer);

                        _adminaccountmanager.AddNewAdminAccount(newaccount);
                    }

                    if (receivedObject.iMessagetype == (int)CommsPacket.MessageTypes.UpdateAdminAccount)
                    {
                        // Admin account to be updated
                        string username = receivedObject.useraccount;
                        string authtoken = receivedObject.usertoken;

                        CommsPacket.AdminAccount newaccount = (CommsPacket.AdminAccount)CommonFunctions.ByteArrayToObject(receivedObject.somebuffer);

                        _adminaccountmanager.UpdateAdminAccount(newaccount);
                    }

                    if (receivedObject.iMessagetype == (int)CommsPacket.MessageTypes.DeleteAdminAccount)
                    {
                        // Admin account to be updated
                        string username = receivedObject.useraccount;
                        string authtoken = receivedObject.usertoken;

                        string accountusername = receivedObject.somethingelse;

                        _adminaccountmanager.DeleteAdminAccount(accountusername);
                    }

                    if (receivedObject.iMessagetype == (int)CommsPacket.MessageTypes.ChangeAdminPassword)
                    {
                        // Admin account to be updated
                        string username = receivedObject.useraccount;
                        string authtoken = receivedObject.usertoken;

                        string accountusername = receivedObject.somethingelse;
                        string newpasswordhash = receivedObject.Somemessage;

                        _adminaccountmanager.ChangeAdminAccountPassword(accountusername, newpasswordhash);
                    }

                    if (receivedObject.iMessagetype == (int)CommsPacket.MessageTypes.ListClientAccounts)
                    {
                        // Serialise the admin account list, and send it
                        byte[] listbuffer = _clientaccountmanager.Serialize();

                        largeresponsemode = true;

                        largecommsresponse = new CommsPacket.CommsPacket();
                        largecommsresponse.iMessagetype = (int)CommsPacket.MessageTypes.ListClientAccounts;
                        largecommsresponse.somebuffer = listbuffer;

                        byte[] sendbytes = largecommsresponse.Serialize();
                        byte[] encbytes = largecommsresponse.Encapsulate(sendbytes);

                        networkStream.Write(encbytes, 0, encbytes.Length);
                    }

                    if (receivedObject.iMessagetype == (int)CommsPacket.MessageTypes.NewClientAccount)
                    {
                        // New admin account to be added
                        string username = receivedObject.useraccount;
                        string authtoken = receivedObject.usertoken;

                        CommsPacket.ClientAccount newaccount = (CommsPacket.ClientAccount)CommonFunctions.ByteArrayToObject(receivedObject.somebuffer);

                        _clientaccountmanager.AddNewClientAccount(newaccount);
                    }

                    if (receivedObject.iMessagetype == (int)CommsPacket.MessageTypes.UpdateClientAccount)
                    {
                        // Admin account to be updated
                        string username = receivedObject.useraccount;
                        string authtoken = receivedObject.usertoken;

                        CommsPacket.ClientAccount newaccount = (CommsPacket.ClientAccount)CommonFunctions.ByteArrayToObject(receivedObject.somebuffer);

                        _clientaccountmanager.UpdateClientAccount(newaccount);
                    }

                    if (receivedObject.iMessagetype == (int)CommsPacket.MessageTypes.DeleteClientAccount)
                    {
                        // Admin account to be updated
                        string username = receivedObject.useraccount;
                        string authtoken = receivedObject.usertoken;

                        string accountusername = receivedObject.somethingelse;

                        _clientaccountmanager.DeleteClientAccount(accountusername);
                    }

                    if (receivedObject.iMessagetype == (int)CommsPacket.MessageTypes.ChangeClientPassword)
                    {
                        // Admin account to be updated
                        string username = receivedObject.useraccount;
                        string authtoken = receivedObject.usertoken;

                        string accountusername = receivedObject.somethingelse;
                        string newpasswordhash = receivedObject.Somemessage;

                        _clientaccountmanager.ChangeClientAccountPassword(accountusername, newpasswordhash);
                    }

                    if (receivedObject.iMessagetype == (int)CommsPacket.MessageTypes.CompletedFileTable)
                    {
                        // We have received a completed file table from the client. The client has figured
                        // out what has changed on it's side and has sent just those changes in the
                        // completed file table. We now need to peice together the completed file by using
                        // the source chunk list we already have an the completed file table sent from the client

                        string useraccount = receivedObject.useraccount;
                        string authtoken = receivedObject.usertoken;
                        string jobname = receivedObject.jobname;
                        string strpath = receivedObject.destpath;

                        string accountrootpath = Path.Combine(_clientaccountmanager.GetAccountRootPath(useraccount), jobname);

                        if (strpath.Contains("$$$ACCOUNTROOT$$$"))
                        {
                            strpath = strpath.Replace("$$$ACCOUNTROOT$$$", accountrootpath);

                            byte[] serializedfiletable = receivedObject.somebuffer;

                            // Now need to deserialize the byte array into a list of completed file chunks.
                            List<FileChunkEntry> completedfiletable = (List<FileChunkEntry>)CommsPacket.CommsPacket.DeSerializeObject(serializedfiletable);

                            // Now we have enough information to build the completed file.
                            BuildCompletedFile(strpath, strpath + ".new", _chunklist, completedfiletable);
                            
                            // Now delete the old file
                            File.Delete(strpath);

                            // Now move the .new file in place of the old one
                            File.Move(strpath + ".new", strpath);
                        }
                        else
                        {
                            throw new Exception("BuildFileChunkTable message was sent, but no account root tag was found in the given path.");
                        }
                    }

                    if (receivedObject.iMessagetype == (int)CommsPacket.MessageTypes.BuildFileChunkTable)
                    {
                        // A byte level differencing operation is set to begin, it starts with the client
                        // asking us for a hash table of the equivalent file on our side.
                        string useraccount = receivedObject.useraccount;
                        string authtoken = receivedObject.usertoken;
                        string jobname = receivedObject.jobname;
                        string strpath = receivedObject.filename;

                        string accountrootpath = Path.Combine(_clientaccountmanager.GetAccountRootPath(useraccount), jobname);

                        if (strpath.Contains("$$$ACCOUNTROOT$$$"))
                        {
                            strpath = strpath.Replace("$$$ACCOUNTROOT$$$", accountrootpath);

                            if (File.Exists(strpath))
                            {
                                _chunklist = BuildFileChunkTable(strpath);

                                // Now we ned to serialise this chunklist into a byte array for sending back to the client
                                byte[] serializedchunklist = CommsPacket.CommsPacket.SerializeObject(_chunklist);

                                // Now prepare the large object to send back to the client
                                largeresponsemode = true;

                                largecommsresponse = new CommsPacket.CommsPacket();
                                largecommsresponse.iMessagetype = (int)CommsPacket.MessageTypes.BuildFileChunkTable;
                                largecommsresponse.somebuffer = serializedchunklist;

                                byte[] sendbytes = largecommsresponse.Serialize();
                                byte[] encbytes = largecommsresponse.Encapsulate(sendbytes);

                                networkStream.Write(encbytes, 0, encbytes.Length);
                            }
                            else
                            {
                                // If the file does not exist, we need to reply with an appropriate message
                                // Now prepare the large object to send back to the client, but send a file not found response.
                                largeresponsemode = true;

                                largecommsresponse = new CommsPacket.CommsPacket();
                                largecommsresponse.iMessagetype = (int)CommsPacket.MessageTypes.FileNotFound;
                                largecommsresponse.somebuffer = null;

                                byte[] sendbytes = largecommsresponse.Serialize();
                                byte[] encbytes = largecommsresponse.Encapsulate(sendbytes);

                                networkStream.Write(encbytes, 0, encbytes.Length);
                            }
                        }
                        else
                        {
                            throw new Exception("BuildFileChunkTable message was sent, but no account root tag was found in the given path.");
                        }
                    }
                }

                /*
                else
                {
                    // If we are in file transfer mode then what we've received is a direct file chunk, so append it
                    // to the file details we've received 

                    byte[] receivedchunk = memStream.GetBuffer();

                    memStream.Seek(0, SeekOrigin.Begin);
                    memStream.SetLength(0);
                    

                    // Append the chunk to the previously specified file when the preparation request was sent
                    _filetransferwritestream.Write(receivedchunk, 0, receivedchunk.Length);

                    _filetransferreceivedpointer += receivedchunk.Length;

                    if (_filetransferreceivedpointer >= _filetransferfilelength)
                    {
                        _filetransferwritestream.Close();
                        _filetransfermode = false;
                        System.Diagnostics.Debug.WriteLine("All File Chunks received. File Transfer mode deactivated.");
                    }


                    System.Diagnostics.Debug.WriteLine("File Receive Pointer: " + _filetransferreceivedpointer + " File Length: " + _filetransferfilelength);
                }
                */
                 
                
                
                Console.WriteLine("Data received from client:");
                bool bQuit = false;

                if (largeresponsemode == false)
                {
                    commsresponse.Message = "I received the packet at: " + DateTime.Now.ToString();

                    // Send the response back to the client
                    byte[] sendBytes = commsresponse.Serialize();
                    networkStream.Write(sendBytes, 0, sendBytes.Length);
                }
                // Client stop processing  
                if (bQuit)
                {
                    networkStream.Close();
                    ClientSocket.Close();
                    ContinueProcess = false;
                }
            }

            return result;
        }

        public void Close()
        {
            networkStream.Close();
            ClientSocket.Close();
        }

        public bool Alive
        {
            get
            {
                return ContinueProcess;
            }
        }

    } // class ClientHandler 





    
}
