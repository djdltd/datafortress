﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;


namespace DataFortressServer
{
    [RunInstaller(true)]
    public partial class FortressServerInstaller : System.Configuration.Install.Installer
    {
        public FortressServerInstaller()
        {
            InitializeComponent();
        }
    }
}
