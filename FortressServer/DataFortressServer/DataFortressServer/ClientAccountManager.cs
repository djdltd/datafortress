﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace DataFortressServer
{
    public class ClientAccountManager
    {

        private string _clientaccountsxml = "ClientAccounts.xml";
        private List<CommsPacket.ClientAccount> _clientaccountlist = new List<CommsPacket.ClientAccount>();
        private string _clientaccountxmlfullpath;

        public void Initialise()
        {
            string clientaccountxmlpath = Path.Combine(Path.Combine(Environment.GetEnvironmentVariable("APPDATA"), "DataFortressServer"), _clientaccountsxml);

            CommonFunctions.BuildDestFileDirs(clientaccountxmlpath);

            _clientaccountxmlfullpath = clientaccountxmlpath;

            if (File.Exists(clientaccountxmlpath) == false)
            {
                // Create the xml file with the default account username and password
                CommsPacket.ClientAccount defaultaccount = CreateDefaultAccount();
                _clientaccountlist.Add(defaultaccount);
                SaveXML();
            }
            else
            {
                // Load the existing one into memory
                LoadXML();
            }
        }

        public void AddNewClientAccount(CommsPacket.ClientAccount newaccount)
        {
            _clientaccountlist.Add(newaccount);
            SaveXML();
        }

        public void UpdateClientAccount(CommsPacket.ClientAccount changedaccount)
        {
            // Client accounts are keyed on username. You can't change the username when it's created
            // only option is to delete and recreate if you want a different username

            foreach (CommsPacket.ClientAccount clientaccount in _clientaccountlist)
            {
                if (changedaccount.Username.ToUpper() == clientaccount.Username.ToUpper())
                {
                    clientaccount.PasswordHash = changedaccount.PasswordHash;
                    clientaccount.Firstname = changedaccount.Firstname;
                    clientaccount.Lastname = changedaccount.Lastname;
                    clientaccount.Description = changedaccount.Description;
                    clientaccount.Company = changedaccount.Company;
                    clientaccount.RootPath = changedaccount.RootPath;
                    clientaccount.MaxAllowedMegaBytes = changedaccount.MaxAllowedMegaBytes;
                }
            }

            SaveXML();
        }

        public void DeleteClientAccount(string clientaccountusername)
        {
            // Delete the client account
            for (int c = 0; c < _clientaccountlist.Count; c++)
            {
                CommsPacket.ClientAccount account = (CommsPacket.ClientAccount)_clientaccountlist[c];

                if (account.Username.ToUpper() == clientaccountusername.ToUpper())
                {
                    _clientaccountlist.RemoveAt(c);
                    return;
                }
            }

            SaveXML();
        }

        public void ChangeClientAccountPassword(string username, string newpasswordhash)
        {
            foreach (CommsPacket.ClientAccount account in _clientaccountlist)
            {
                if (account.Username.ToUpper() == username.ToUpper())
                {
                    account.PasswordHash = newpasswordhash;
                    return;
                }
            }

            SaveXML();
        }

        public void SaveXML()
        {
            if (_clientaccountlist.Count > 0)
            {

                XmlSerializer mySerializer = new XmlSerializer(typeof(List<CommsPacket.ClientAccount>));
                StreamWriter myWriter = new StreamWriter(_clientaccountxmlfullpath);

                mySerializer.Serialize(myWriter, _clientaccountlist);
                myWriter.Close();
            }
            else
            {
                if (File.Exists(_clientaccountxmlfullpath) == true)
                {
                    File.Delete(_clientaccountxmlfullpath);
                }
            }
        }

        public void LoadXML()
        {
            _clientaccountlist.Clear();

            if (File.Exists(_clientaccountxmlfullpath) == true)
            {

                XmlSerializer mySerializer = new XmlSerializer(typeof(List<CommsPacket.ClientAccount>));

                // To read the xml file, create a FileStream.
                FileStream myFileStream = new FileStream(_clientaccountxmlfullpath, FileMode.Open);

                // Call the Deserialize method and cast to the object type.
                _clientaccountlist = (List<CommsPacket.ClientAccount>)mySerializer.Deserialize(myFileStream);

                myFileStream.Close();
            }
        }

        private CommsPacket.ClientAccount CreateDefaultAccount()
        {
            CommsPacket.ClientAccount defaultaccount = new CommsPacket.ClientAccount();

            defaultaccount.Username = "defaultclient";
            defaultaccount.PasswordHash = CommonFunctions.CreateMD5HashFromString("password");
            defaultaccount.Firstname = "Default Client Account";
            defaultaccount.Lastname = "Default Client Account";
            defaultaccount.Company = "Default Company";
            defaultaccount.RootPath = "C:\\Temp";
            defaultaccount.MaxAllowedMegaBytes = 2048; // 2 GB
            defaultaccount.Description = "This is the default account that is created when no client accounts exist.";

            return defaultaccount;
        }

        public bool DoesClientAccountExist(string username, string passwordhash)
        {
            foreach (CommsPacket.ClientAccount account in _clientaccountlist)
            {
                if (account.Username.ToUpper() == username.ToUpper())
                {
                    if (account.PasswordHash.ToUpper() == passwordhash.ToUpper())
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public string GetAccountRootPath(string username)
        {
            foreach (CommsPacket.ClientAccount account in _clientaccountlist)
            {
                if (account.Username.ToUpper() == username.ToUpper())
                {
                    return account.RootPath;
                }
            }

            return "";
        }

        public byte[] Serialize()
        {
            BinaryFormatter bin = new BinaryFormatter();

            MemoryStream mem = new MemoryStream();

            bin.Serialize(mem, _clientaccountlist);

            return mem.GetBuffer();
        }

    }
}
