﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace DataFortressServer
{
    public class AdminAccountManager
    {
        private string _adminaccountsxml = "AdminAccounts.xml";
        private List<CommsPacket.AdminAccount> _adminaccountlist = new List<CommsPacket.AdminAccount>();
        private string _adminaccountxmlfullpath;

        public void Initialise()
        {
            string adminaccountxmlpath = Path.Combine(Path.Combine(Environment.GetEnvironmentVariable("APPDATA"), "DataFortressServer"), _adminaccountsxml);

            CommonFunctions.BuildDestFileDirs(adminaccountxmlpath);

            _adminaccountxmlfullpath = adminaccountxmlpath;

            if (File.Exists(adminaccountxmlpath) == false)
            {
                // Create the xml file with the default account username and password
                CommsPacket.AdminAccount defaultaccount = CreateDefaultAccount();
                _adminaccountlist.Add(defaultaccount);
                SaveXML();
            }
            else
            {
                // Load the existing one into memory
                LoadXML();
            }
        }

        public void AddNewAdminAccount(CommsPacket.AdminAccount newaccount)
        {
            _adminaccountlist.Add(newaccount);
            SaveXML();
        }

        public void UpdateAdminAccount(CommsPacket.AdminAccount changedaccount)
        {
            // Admin accounts are keyed on username. You can't change the username when it's created
            // only option is to delete and recreate if you want a different username

            foreach (CommsPacket.AdminAccount adminaccount in _adminaccountlist)
            {
                if (changedaccount.Username.ToUpper() == adminaccount.Username.ToUpper())
                {
                    adminaccount.PasswordHash = changedaccount.PasswordHash;
                    adminaccount.Firstname = changedaccount.Firstname;
                    adminaccount.Lastname = changedaccount.Lastname;
                    adminaccount.Description = changedaccount.Description;

                }
            }

            SaveXML();
        }

        public void DeleteAdminAccount(string adminaccountusername)
        {
            // Delete the admin account
            for (int c = 0; c < _adminaccountlist.Count; c++)
            {
                CommsPacket.AdminAccount account = (CommsPacket.AdminAccount)_adminaccountlist[c];

                if (account.Username.ToUpper() == adminaccountusername.ToUpper())
                {
                    _adminaccountlist.RemoveAt(c);
                    return;
                }
            }

            SaveXML();
        }

        public void ChangeAdminAccountPassword(string username, string newpasswordhash)
        {
            foreach (CommsPacket.AdminAccount account in _adminaccountlist)
            {
                if (account.Username.ToUpper() == username.ToUpper())
                {
                    account.PasswordHash = newpasswordhash;
                    return;
                }
            }

            SaveXML();
        }

        public void SaveXML()
        {
            if (_adminaccountlist.Count > 0)
            {

                XmlSerializer mySerializer = new XmlSerializer(typeof(List<CommsPacket.AdminAccount>));
                StreamWriter myWriter = new StreamWriter(_adminaccountxmlfullpath);

                mySerializer.Serialize(myWriter, _adminaccountlist);
                myWriter.Close();
            }
            else
            {
                if (File.Exists(_adminaccountxmlfullpath) == true)
                {
                    File.Delete(_adminaccountxmlfullpath);
                }
            }
        }

        public void LoadXML()
        {
            _adminaccountlist.Clear();

            if (File.Exists(_adminaccountxmlfullpath) == true)
            {

                XmlSerializer mySerializer = new XmlSerializer(typeof(List<CommsPacket.AdminAccount>));

                // To read the xml file, create a FileStream.
                FileStream myFileStream = new FileStream(_adminaccountxmlfullpath, FileMode.Open);

                // Call the Deserialize method and cast to the object type.
                _adminaccountlist = (List<CommsPacket.AdminAccount>)mySerializer.Deserialize(myFileStream);

                myFileStream.Close();
            }
        }

        private CommsPacket.AdminAccount CreateDefaultAccount()
        {
            CommsPacket.AdminAccount defaultaccount = new CommsPacket.AdminAccount();

            defaultaccount.Username = "admin";
            defaultaccount.PasswordHash = CommonFunctions.CreateMD5HashFromString("admin");
            defaultaccount.Firstname = "Default Admin Account";
            defaultaccount.Lastname = "Default Admin Accounts";
            defaultaccount.Description = "This is the default account that is created when no admin accounts exist.";

            return defaultaccount;
        }

        public bool DoesAdminAccountExist(string username, string passwordhash)
        {
            foreach (CommsPacket.AdminAccount account in _adminaccountlist)
            {
                if (account.Username.ToUpper() == username.ToUpper())
                {
                    if (account.PasswordHash.ToUpper() == passwordhash.ToUpper())
                    {
                        return true;
                    } 
                }
            }

            return false;
        }

        public byte[] Serialize()
        {
            BinaryFormatter bin = new BinaryFormatter();

            MemoryStream mem = new MemoryStream();

            bin.Serialize(mem, _adminaccountlist);

            return mem.GetBuffer();
        }


    }
}
