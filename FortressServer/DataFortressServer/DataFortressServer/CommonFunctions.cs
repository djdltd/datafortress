﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using System.Runtime.Serialization.Formatters.Binary;

namespace DataFortressServer
{
    public class CommonFunctions
    {
        public static void BuildDestFileDirs(String strDestfile)
        {
            LinkedList<String> pathlist = new LinkedList<String>();
            LinkedListNode<String> curpath;

            String strFullpath = strDestfile;

            String strFilteredpath = Path.GetDirectoryName(strFullpath);

            while (Directory.Exists(strFilteredpath) == false)
            {
                pathlist.AddLast(new LinkedListNode<string>(strFilteredpath));
                strFilteredpath = Path.GetDirectoryName(strFilteredpath);
            }

            curpath = pathlist.Last;
            for (int a = 0; a < pathlist.Count; a++)
            {
                Directory.CreateDirectory(curpath.Value);
                curpath = curpath.Previous;
            }
        }

        public static string CreateMD5HashFromString (string input)
        {
            // Use input string to calculate MD5 hash
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hashBytes = md5.ComputeHash(inputBytes);

            // Convert the byte array to hexadecimal string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++)
            {
                sb.Append(hashBytes[i].ToString("X2"));
                // To force the hex string to lower-case letters instead of
                // upper-case, use he following line instead:
                // sb.Append(hashBytes[i].ToString("x2")); 
            }
            return sb.ToString();
        }

        public static byte[] ObjectToByteArray(Object myobject, Type mytype)
        {
            BinaryFormatter bin = new BinaryFormatter();

            MemoryStream mem = new MemoryStream();

            bin.Serialize(mem, myobject);

            return mem.GetBuffer();
        }

        public static Object ByteArrayToObject(byte[] buffer)
        {
            BinaryFormatter bin = new BinaryFormatter();
            MemoryStream mem = new MemoryStream();

            mem.Write(buffer, 0, buffer.Length);
            mem.Seek(0, 0);

            return bin.Deserialize(mem);
        }
    }
}
