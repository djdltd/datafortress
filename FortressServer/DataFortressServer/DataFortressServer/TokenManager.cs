﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataFortressServer
{
    public class TokenManager
    {
        private List<AuthenticatedToken> _authtokenlist = new List<AuthenticatedToken>();

        public string CreateNewAuthenticatedToken(string Username, AccountType accounttype)
        {
            AuthenticatedToken newtoken = new AuthenticatedToken();

            newtoken.Username = Username;
            newtoken.accounttype = accounttype;
            newtoken.token = Guid.NewGuid().ToString();
            _authtokenlist.Add(newtoken);

            return newtoken.token;
        }

        public bool DoesAccountTokenExist(string Username, AccountType accounttype, string Token)
        {
            foreach (AuthenticatedToken authtoken in _authtokenlist)
            {
                if (authtoken.accounttype == accounttype)
                {
                    if (authtoken.Username.ToUpper() == Username.ToUpper())
                    {
                        if (authtoken.token.ToUpper() == Token.ToUpper())
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }
    }

    public enum AccountType
    {
        Admin,
        Client
    }

    public class AuthenticatedToken
    {
        public string Username;
        public AccountType accounttype;
        public string token;
    }
}
