﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace DataFortress
{
    public partial class Form1 : Form
    {
        SqlConnection _myConnection = null;
        List<string> _md5list = null;

        public enum ChunkSizes
        {
            Maximum = 200000000,
            Medium = 20000000,
            Small = 2000000,
            Invalid = 0
        }

        public Form1()
        {
            InitializeComponent();
        }

        public void OpenDatabaseConnection()
        {
            _myConnection = new SqlConnection("Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=DataFortress;Data Source=DANNY-DESKTOP\\SQLEXPRESS");

            _myConnection.Open();

        }

        public void CloseDatabaseConnection()
        {
            _myConnection.Close();
        }

        public void StoreFileChunk(FileChunk chunk)
        {
            SqlCommand myCommand = _myConnection.CreateCommand();

            myCommand.CommandText = "AddFileChunk";

            myCommand.CommandType = CommandType.StoredProcedure;

            myCommand.Parameters.Add(new SqlParameter("file_path", chunk.filepath));
            myCommand.Parameters.Add(new SqlParameter("chunk_md5", chunk.chunkmd5));
            myCommand.Parameters.Add(new SqlParameter("chunk_modified_date", chunk.chunkmodifieddate));                        
            myCommand.Parameters.Add(new SqlParameter("chunk_offset", chunk.chunkoffset));
            myCommand.Parameters.Add(new SqlParameter("chunk_length", chunk.chunklength));
            myCommand.Parameters.Add(new SqlParameter("chunk_parent_id", chunk.chunk_parent_id));
            myCommand.Parameters.Add(new SqlParameter("chunk_data", chunk.chunkdata));

            myCommand.ExecuteNonQuery();
        }

        public void StoreFileInfo(FileInfo fileinfo)
        {
            SqlCommand myCommand = _myConnection.CreateCommand();

            myCommand.CommandText = "AddFile";

            myCommand.CommandType = CommandType.StoredProcedure;

            myCommand.Parameters.Add(new SqlParameter("file_name", fileinfo.filename));
            myCommand.Parameters.Add(new SqlParameter("file_path", fileinfo.filepath));
            myCommand.Parameters.Add(new SqlParameter("file_modified_date", fileinfo.filemodifieddate));
            myCommand.Parameters.Add(new SqlParameter("file_size", fileinfo.filesize));

            myCommand.ExecuteNonQuery();
        }

        public int StoreChunkInfo(FileChunk chunk)
        {
            // Returns the chunk id of the added chunk info
            SqlCommand myCommand = _myConnection.CreateCommand();

            myCommand.CommandText = "AddChunkInfo";

            myCommand.CommandType = CommandType.StoredProcedure;

            myCommand.Parameters.Add(new SqlParameter("file_path", chunk.filepath));
            myCommand.Parameters.Add(new SqlParameter("chunk_md5", chunk.chunkmd5));
            myCommand.Parameters.Add(new SqlParameter("chunk_modified_date", chunk.chunkmodifieddate));
            myCommand.Parameters.Add(new SqlParameter("chunk_offset", chunk.chunkoffset));
            myCommand.Parameters.Add(new SqlParameter("chunk_length", chunk.chunklength));

            if (chunk.chunk_parent_id == null)
            {
                myCommand.Parameters.Add(new SqlParameter("chunk_parent_id", DBNull.Value));
            }
            else
            {
                myCommand.Parameters.Add(new SqlParameter("chunk_parent_id", chunk.chunk_parent_id));
            }
            

            SqlParameter paramchunkid = new SqlParameter("chunk_id", SqlDbType.Int);
            paramchunkid.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(paramchunkid);

            myCommand.ExecuteNonQuery();

            return (int) paramchunkid.Value;
        }

        public void ConnectToDatabase()
        {

            OpenDatabaseConnection();

            FileChunk newchunk = new FileChunk();
            newchunk.chunkmd5 = "1276476AKJHDKJH";
            newchunk.chunkmodifieddate = DateTime.Now;
            newchunk.filepath = "My FilePath";
            newchunk.chunkoffset = 1234;
            newchunk.chunklength = 123;
            newchunk.chunkdata = new byte[100];

            StoreFileChunk(newchunk);

            CloseDatabaseConnection();

            MessageBox.Show ("Done");

        }

        public int CalcRootChunkSize(long filelength)
        {
            int rootchunksize = 0;

            int maxsize = 200000000;
            int mediumsize = 20000000;
            int smallsize = 2000000;

            if (filelength >= maxsize)
            {
                rootchunksize = maxsize;
            }
            else
            {
                if (filelength >= mediumsize)
                {
                    rootchunksize = mediumsize;
                }
                else
                {
                    rootchunksize = smallsize;
                }
            }

            return rootchunksize;
        }

        public ChunkSizes GetChunkSizeClass(int chunksize)
        {
            int maxsize = 200000000;
            int mediumsize = 20000000;
            int smallsize = 2000000;

            if (chunksize == maxsize)
            {
                return ChunkSizes.Maximum;
            }

            if (chunksize == mediumsize)
            {
                return ChunkSizes.Medium;
            }

            if (chunksize == smallsize)
            {
                return ChunkSizes.Small;
            }

            return ChunkSizes.Invalid;
        }

        public void StoreIndividualChunks(string filepath, int parentchunkid, byte[] chunkbuffer)
        {
            byte[] buffer;

            long datachunksize = 8000;

            long numchunks = chunkbuffer.Length / datachunksize;

            long calcsize = numchunks * datachunksize;

            if (chunkbuffer.Length > calcsize)
            {
                numchunks++;
            }

            long filepointer = 0;
            long workingchunksize = 0;

            for (int a = 0; a < numchunks; a++)
            {
                if ((chunkbuffer.Length - filepointer) >= datachunksize)
                {
                    workingchunksize = datachunksize;
                    buffer = new byte[datachunksize];

                    Array.Copy(chunkbuffer, filepointer, buffer, 0, datachunksize);

                }
                else
                {
                    long lastchunksize = chunkbuffer.Length - filepointer;
                    workingchunksize = lastchunksize;

                    buffer = new byte[lastchunksize];

                    Array.Copy(chunkbuffer, filepointer, buffer, 0, lastchunksize);
                }

                FileChunk rootchunk = new FileChunk();

                rootchunk.chunkmd5 = CalculateMD5(buffer);
                rootchunk.chunkmodifieddate = File.GetLastWriteTime(filepath);
                rootchunk.filepath = filepath;
                rootchunk.chunkoffset = filepointer;
                rootchunk.chunklength = (int)workingchunksize;
                rootchunk.chunk_parent_id = parentchunkid; // our parent chunk id
                rootchunk.chunkdata = buffer;

                StoreFileChunk(rootchunk);

                filepointer += workingchunksize;
            }
        }

        public void ProcessSmallChunks(string filepath, int parentchunkid, byte[] chunkbuffer)
        {
            byte[] buffer;

            long smallchunksize = 2000000;

            long numchunks = chunkbuffer.Length / smallchunksize;

            long calcsize = numchunks * smallchunksize;

            if (chunkbuffer.Length > calcsize)
            {
                numchunks++;
            }

            long filepointer = 0;
            long workingchunksize = 0;

            for (int a = 0; a < numchunks; a++)
            {
                if ((chunkbuffer.Length - filepointer) >= smallchunksize)
                {
                    workingchunksize = smallchunksize;
                    buffer = new byte[smallchunksize];

                    Array.Copy(chunkbuffer, filepointer, buffer, 0, smallchunksize);

                }
                else
                {
                    long lastchunksize = chunkbuffer.Length - filepointer;
                    workingchunksize = lastchunksize;

                    buffer = new byte[lastchunksize];

                    Array.Copy(chunkbuffer, filepointer, buffer, 0, lastchunksize);
                }

                FileChunk rootchunk = new FileChunk();

                rootchunk.chunkmd5 = CalculateMD5(buffer);
                rootchunk.chunkmodifieddate = File.GetLastWriteTime(filepath);
                rootchunk.filepath = filepath;
                rootchunk.chunkoffset = filepointer;
                rootchunk.chunklength = (int)workingchunksize;
                rootchunk.chunk_parent_id = parentchunkid; // our parent chunk id

                int returnedchunkid = StoreChunkInfo(rootchunk);

                StoreIndividualChunks(filepath, returnedchunkid, buffer);

                filepointer += workingchunksize;
            }
        }

        public void ProcessMediumChunks(string filepath, int parentchunkid, byte[] chunkbuffer)
        {
            byte[] buffer;

            long medchunksize = 20000000;

            long numchunks = chunkbuffer.Length / medchunksize;

            long calcsize = numchunks * medchunksize;

            if (chunkbuffer.Length > calcsize)
            {
                numchunks++;
            }

            long filepointer = 0;
            long workingchunksize = 0;

            for (int a = 0; a < numchunks; a++)
            {
                if ((chunkbuffer.Length - filepointer) >= medchunksize)
                {
                    workingchunksize = medchunksize;
                    buffer = new byte[medchunksize];

                    Array.Copy(chunkbuffer, filepointer, buffer, 0, medchunksize);

                }
                else
                {
                    long lastchunksize = chunkbuffer.Length - filepointer;
                    workingchunksize = lastchunksize;

                    buffer = new byte[lastchunksize];

                    Array.Copy(chunkbuffer, filepointer, buffer, 0, lastchunksize);
                }

                FileChunk rootchunk = new FileChunk();

                rootchunk.chunkmd5 = CalculateMD5(buffer);
                rootchunk.chunkmodifieddate = File.GetLastWriteTime(filepath);
                rootchunk.filepath = filepath;
                rootchunk.chunkoffset = filepointer;
                rootchunk.chunklength = (int)workingchunksize;
                rootchunk.chunk_parent_id = parentchunkid; // our parent chunk id

                int returnedchunkid = StoreChunkInfo(rootchunk);

                // Now go through and process the smaller chunks of this buffer
                ProcessSmallChunks(filepath, returnedchunkid, buffer);

                filepointer += workingchunksize;
            }
        }


        public List<FileChunkEntry> BuildFileChunkTable(string filepath)
        {
            List<FileChunkEntry> chunklist = new List<FileChunkEntry>();

            byte[] buffer;

            FileStream fileStream = new FileStream(filepath, FileMode.Open, FileAccess.Read);

            int chunksize = 8000;

            // When we divide the file length by the chunk size it rounds the number down to the smallest non decimal number.
            // so we might miss some stuff at the end of the file, so we do a final check to make sure than when the number of chunks
            // are multiplied by the chunk size we check the calculated size. If the calculated size is smaller, then we need another chunk.
            long numchunks = fileStream.Length / chunksize;

            long calcsize = numchunks * chunksize;

            if (fileStream.Length > calcsize)
            {
                numchunks++;
            }


            long filepointer = 0;
            long workingchunksize = 0;

            for (int a = 0; a < numchunks; a++)
            {
                if ((fileStream.Length - filepointer) >= chunksize)
                {
                    workingchunksize = chunksize;
                    buffer = new byte[chunksize];

                    fileStream.Read(buffer, 0, chunksize);
                }
                else
                {
                    long lastchunksize = fileStream.Length - filepointer;
                    workingchunksize = lastchunksize;

                    buffer = new byte[lastchunksize];

                    fileStream.Read(buffer, 0, (int)lastchunksize);
                }

                // At this point we have our buffer array populated with the current chunk (even the last one)
                FileChunkEntry newentry = new FileChunkEntry();

                newentry.hash = CalculateMD5(buffer);
                newentry.offset = filepointer;
                newentry.length = (int)workingchunksize;
                newentry.index = a;

                chunklist.Add(newentry);


                filepointer += workingchunksize;
            }


            return chunklist;
        }

        public FileChunkEntry GetChunkEntry(List<FileChunkEntry> chunktable, int possibleindex, string hashtosearch)
        {
            // This function looks up the given hash in the supplied chunktable. But first it checks if the hastosearch matches
            // the possible index given, to save having to run through the entire list.

            // First look up the possible index.

            if (possibleindex < chunktable.Count)
            {
                FileChunkEntry possibleentry = chunktable[possibleindex];

                if (hashtosearch == possibleentry.hash)
                {
                    // Great! The possible index given and hash matched immedietely, so return this - no need to do a full search
                    return possibleentry;
                }
            }
            
            // If we got here then the possible index was not matched or the possible index was outside the count of chunktable items
            // so we have to do a full search

            foreach (FileChunkEntry entry in chunktable)
            {
                if (entry.hash == hashtosearch)
                {
                    return entry;
                }
            }

            // Now if we got here then we couldn't find the hash, so return null
            return null;
        }

        public void ByteArrayToFile(string outputfilepath, byte[] buffer)
        {
            FileStream aFile = new FileStream(outputfilepath, FileMode.Create);

            aFile.Seek(0, SeekOrigin.Begin);
            aFile.Write(buffer, 0, buffer.Length);
            aFile.Close();

        }

        public void MapChunkTableToFile(string filetomap, List<FileChunkEntry> sourcechunktable)
        {

            byte[] buffer;

            FileStream fileStream = new FileStream(filetomap, FileMode.Open, FileAccess.Read);

            int chunksize = 8000;
            bool filepointerskipped = false;

            // When we divide the file length by the chunk size it rounds the number down to the smallest non decimal number.
            // so we might miss some stuff at the end of the file, so we do a final check to make sure than when the number of chunks
            // are multiplied by the chunk size we check the calculated size. If the calculated size is smaller, then we need another chunk.
            long numchunks = fileStream.Length / chunksize;

            long calcsize = numchunks * chunksize;

            if (fileStream.Length > calcsize)
            {
                numchunks++;
            }

            long filepointer = 0;
            long workingchunksize = 0;
            long numiterations = 0;
            long progress;
            


            for (long a = 0; a < fileStream.Length; a++)
            {
                FileWorkerState fwstate = new FileWorkerState();
                fwstate.currentoffset = a;
                fwstate.currentlength = fileStream.Length;

                progress = (a * 100) / (int)fileStream.Length;

                bwFileworker.ReportProgress((int)progress, fwstate);

                numiterations++;

                /*
                if (numiterations == 40)
                {
                    Application.Exit();
                }
                 */

                filepointerskipped = false;

                if ((fileStream.Length - filepointer) >= chunksize)
                {
                    workingchunksize = chunksize;
                    buffer = new byte[chunksize];

                    fileStream.Seek(filepointer, SeekOrigin.Begin);
                    fileStream.Read(buffer, 0, chunksize);

                    //ByteArrayToFile("C:\\Temp\\IFC\\BufferFileOffset" + a.ToString() + ".dat", buffer);
                }
                else

                {
                    long lastchunksize = fileStream.Length - filepointer;
                    workingchunksize = lastchunksize;

                    buffer = new byte[lastchunksize];

                    fileStream.Seek(filepointer, SeekOrigin.Begin);
                    fileStream.Read(buffer, 0, (int)lastchunksize);

                    //ByteArrayToFile("C:\\Temp\\IFC\\LastBufferFileOffset" + a.ToString() + ".dat", buffer);
                }

                // At this point we have our buffer array populated with the current chunk (even the last one)
                string currenthash = CalculateMD5(buffer);

                if ((a % chunksize) == 0) // If we are on the starting offset of a chunk 
                {
                    System.Diagnostics.Debug.WriteLine("We are on the starting offset of a chunk, a is: " + a.ToString ());

                    // The chunk index will be easily divisible
                    int possiblechunkindex = ((int)a / chunksize);

                    System.Diagnostics.Debug.WriteLine("Possible chunk index is: " + possiblechunkindex.ToString ());

                    // Get the possible entry
                    FileChunkEntry possibleentry = GetChunkEntry(sourcechunktable, possiblechunkindex, currenthash);

                    if (possibleentry != null)
                    {
                        // This means this chunk we're on matched the chunk at the same place in the chunk table - nice
                        // just output this for now
                        System.Diagnostics.Debug.WriteLine("Map File to Chunk Table: Matched at offset: " + a.ToString());

                        // Now advance our file pointer
                        a += chunksize;
                        filepointer += chunksize;
                        filepointerskipped = true;
                    }
                    else
                    {
                        System.Diagnostics.Debug.WriteLine("Map File to Chunk Table: Chunk Boundary mismatch at offset: " + a.ToString() + " resuming incremental mapping");
                    }
                }
                else
                {
                    // If we are not on the starting offset of a chunk, i.e. there was a mismatch and we're on incremental mapping
                    //System.Diagnostics.Debug.WriteLine("We are NOT on the starting offset of a chunk");

                    // Search for a hash at this offset
                    FileChunkEntry possibleentry = GetChunkEntry(sourcechunktable, 0, currenthash);

                    if (possibleentry != null)
                    {
                        // A match was found at this offset
                        System.Diagnostics.Debug.WriteLine("Map File to Chunk Table: Match found at incremental offset: " + a.ToString() + "   Source Index: " + possibleentry.index.ToString () + "   Source Offset: " + possibleentry.offset.ToString ());
                    
                        // Advance the file pointer, as we found a match at this offset
                        a += chunksize;
                        filepointer += chunksize;
                        filepointerskipped = true;
                    }
                }

                /*
                FileChunkEntry newentry = new FileChunkEntry();

                newentry.hash = CalculateMD5(buffer);
                newentry.offset = filepointer;
                newentry.length = (int)workingchunksize;
                newentry.index = a;

                chunklist.Add(newentry);
                */
                if (filepointerskipped == false)
                {
                    filepointer++;
                }
            }

        }

        public string HashOnFile(string filepath)
        {
            byte[] buffer;

            FileStream fileStream = new FileStream(filepath, FileMode.Open, FileAccess.Read);

            buffer = new byte[fileStream.Length];

            fileStream.Read(buffer, 0, (int)fileStream.Length);

            return CalculateMD5(buffer);

        }

        public void ReadFile(string filepath)
        {
            byte[] buffer;

            FileStream fileStream = new FileStream(filepath, FileMode.Open, FileAccess.Read);

            int chunksize = 0;

            // Determine the root chunk size
            chunksize = CalcRootChunkSize(fileStream.Length);


            long numchunks = fileStream.Length / chunksize;

            long calcsize = numchunks * chunksize;

            if (fileStream.Length > calcsize)
            {
                numchunks++;
            }

            long filepointer = 0;

            int progress = 0;
            long workingchunksize = 0;

            //for (int a = 0; a < numchunks; a++)
            for (int a = 0; a < numchunks; a++)
            {

                if ((fileStream.Length - filepointer) >= chunksize)
                {
                    workingchunksize = chunksize;
                    buffer = new byte[chunksize];

                    fileStream.Read(buffer, 0, chunksize);

                }
                else
                {
                    long lastchunksize = fileStream.Length - filepointer;
                    workingchunksize = lastchunksize;

                    buffer = new byte[lastchunksize];

                    fileStream.Read(buffer, 0, (int) lastchunksize);
                }


                //_md5list.Add(CalculateMD5(buffer));

                FileChunk rootchunk = new FileChunk();

                rootchunk.chunkmd5 = CalculateMD5(buffer);
                rootchunk.chunkmodifieddate = File.GetLastWriteTime(filepath);
                rootchunk.filepath = filepath;
                rootchunk.chunkoffset = filepointer;
                rootchunk.chunklength = (int)workingchunksize;
                rootchunk.chunk_parent_id = null; // null meaning root

                int returnedchunkid = StoreChunkInfo(rootchunk);

                if (GetChunkSizeClass(chunksize) == ChunkSizes.Maximum)
                {
                    ProcessMediumChunks(filepath, returnedchunkid, buffer);
                }


                progress = (a * 100) / (int)numchunks;

                bwFileworker.ReportProgress(progress);

                filepointer += workingchunksize;

           

            }
            
        }

        /// <summary>
        /// Calculates a MD5 hash from the given string and uses the given
        /// encoding.
        /// </summary>
        /// <param name="Input">Input string</param>
        /// <param name="UseEncoding">Encoding method</param>
        /// <returns>MD5 computed string</returns>
        public static string CalculateMD5(byte[] Input, Encoding UseEncoding)
        {
            System.Security.Cryptography.MD5CryptoServiceProvider CryptoService;
            CryptoService = new System.Security.Cryptography.MD5CryptoServiceProvider();

            byte[] OutputBytes;
            OutputBytes = CryptoService.ComputeHash(Input);
            return BitConverter.ToString(OutputBytes).Replace("-", "");
        }

        /// <summary>
        /// Calculates a MD5 hash from the given string. 
        /// (By using the default encoding)
        /// </summary>
        /// <param name="Input">Input string</param>
        /// <returns>MD5 computed string</returns>
        public static string CalculateMD5(byte[] Input)
        {
            // That's just a shortcut to the base method
            return CalculateMD5(Input, System.Text.Encoding.Default);
        }

        public void TestDatabase()
        {
            FileInfo fi = new FileInfo();

            fi.filename = "Filename";
            fi.filepath = "C:\\Temp\\Somepath";
            fi.filemodifieddate = DateTime.Now;
            fi.filesize = 100000;

            //StoreFileInfo(fi);

            FileChunk ci = new FileChunk();

            ci.chunk_parent_id = null;
            ci.chunkdata = null;
            ci.chunklength = 20000;
            ci.chunkmd5 = "ABCDEF";
            ci.chunkmodifieddate = DateTime.Now;
            ci.chunkoffset = 100;
            ci.filepath = fi.filepath;

            //int mychunkid = StoreChunkInfo(ci);

            FileChunk ci2 = new FileChunk();
            ci2.chunk_parent_id = 2;
            ci2.chunkdata = new byte[7999];
            ci2.chunklength = 7999;
            ci2.chunkmd5 = "28238ABC";
            ci2.chunkmodifieddate = DateTime.Now;
            ci2.chunkoffset = 20;
            ci2.filepath = fi.filepath;

            StoreFileChunk(ci2);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            /*
            OpenDatabaseConnection();

            //_md5list = new List<string>();
            //ConnectToDatabase();

            FileWorkerParams fwparams = new FileWorkerParams();

            fwparams.filename = "E:\\Video\\Net Force.avi";

            FileStream fileStream = new FileStream(fwparams.filename, FileMode.Open, FileAccess.Read);

            FileInfo fileinfo = new FileInfo();
            fileinfo.filename = Path.GetFileName(fwparams.filename);
            fileinfo.filepath = fwparams.filename;
            fileinfo.filesize = fileStream.Length;
            fileinfo.filemodifieddate = File.GetLastWriteTime(fwparams.filename);

            fileStream.Close();

            StoreFileInfo(fileinfo);
            
            bwFileworker.RunWorkerAsync(fwparams);
            */

            //List<FileChunkEntry> entrylist;

            //entrylist = BuildFileChunkTable("C:\\Temp\\IFC\\Original.avi");


            //MapChunkTableToFile("C:\\Temp\\IFC\\Modified.avi", entrylist);



            //MessageBox.Show("done");
            //MessageBox.Show(mychunkid.ToString());

            
            FileWorkerParams fwparams = new FileWorkerParams();


            fwparams.originalfile = "C:\\Temp\\IFC\\Original.avi";
            fwparams.modifiedfile = "C:\\Temp\\IFC\\Modified.avi";
           
            bwFileworker.RunWorkerAsync(fwparams);
             

            //System.Diagnostics.Debug.WriteLine(HashOnFile("C:\\Temp\\picture.jpg"));

            //MessageBox.Show(HashOnFile ("C:\\Temp\\picture.jpg"));

        }

        private void bwFileworker_DoWork(object sender, DoWorkEventArgs e)
        {
            FileWorkerParams fwparams = (FileWorkerParams)e.Argument;

            //ReadFile(fwparams.filename);

            List<FileChunkEntry> entrylist;

            entrylist = BuildFileChunkTable(fwparams.originalfile);
            MapChunkTableToFile(fwparams.modifiedfile, entrylist);

        }

        private void bwFileworker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            barProgress.Value = e.ProgressPercentage;

            FileWorkerState workerstate = (FileWorkerState) e.UserState;

            label1.Text = workerstate.currentoffset.ToString();
            label2.Text = workerstate.currentlength.ToString();
        }

        private void bwFileworker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //CloseDatabaseConnection();

            //MessageBox.Show().ToString());
            MessageBox.Show("done");
        }
    }

    public class FileChunk
    {
        public string filepath;
        public string chunkmd5;
        public DateTime chunkmodifieddate;
        //public string filename;
        public long chunkoffset;
        public int chunklength;
        public int? chunk_parent_id;
        //public long filesize;
        public byte[] chunkdata;
    }

    public class FileInfo
    {
        public string filename;
        public string filepath;
        public DateTime filemodifieddate;
        public long filesize;
    }

    public class FileWorkerParams
    {
        public string originalfile;
        public string modifiedfile;
    }

    public class FileWorkerState
    {
        public long currentoffset;
        public long currentlength;
    }

    public class FileChunkEntry
    {
        public string hash;
        public long offset;
        public int length;
        public long index;
    }
}
