﻿namespace DataFortress
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.bwFileworker = new System.ComponentModel.BackgroundWorker();
            this.barProgress = new System.Windows.Forms.ProgressBar();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(120, 49);
            this.button1.TabIndex = 0;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // bwFileworker
            // 
            this.bwFileworker.WorkerReportsProgress = true;
            this.bwFileworker.WorkerSupportsCancellation = true;
            this.bwFileworker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwFileworker_DoWork);
            this.bwFileworker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bwFileworker_ProgressChanged);
            this.bwFileworker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwFileworker_RunWorkerCompleted);
            // 
            // barProgress
            // 
            this.barProgress.Location = new System.Drawing.Point(279, 18);
            this.barProgress.Name = "barProgress";
            this.barProgress.Size = new System.Drawing.Size(388, 35);
            this.barProgress.Step = 1;
            this.barProgress.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(281, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(455, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "label2";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(873, 545);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.barProgress);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.ComponentModel.BackgroundWorker bwFileworker;
        private System.Windows.Forms.ProgressBar barProgress;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

